package com.ibm.ferari;

import com.ibm.ferari.client.InterSiteClient;
import com.ibm.ferari.client.IntraSiteClient;

/****
 * A class that is used by {@link InterSiteClient} and {@link IntraSiteClient} to resolve site name to URIs
 * @author yacovm
 *
 */
public interface AddressResolver {

	/***
	 * @param site 
	 * @return The URIs of the site's coordinator
	 */
	public String[] getHostsOfSite(String site);
	
	/****
	 * @return The URIs of the optimizer's instance(s)
	 */
	public String[] getOptimizerHosts();
	
	/***
	 * @return The name of the optimizer's site
	 */
	public String getOptimizerSite();
	
	/****
	 * @return the URI of the dashboard's WebSocket's endpoint
	 */
	public String getDashboardWSURI();
	
}
