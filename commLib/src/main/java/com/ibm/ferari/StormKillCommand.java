package com.ibm.ferari;

public class StormKillCommand extends FerariMessage {

	private static final long serialVersionUID = -4178521416783585299L;

	public StormKillCommand(String recipient, String topologyId) {
		super(FerariConstants.OPTIMIZER, recipient, topologyId);
		setMsgType(MessageType.StormKillCmd);
	}

}
