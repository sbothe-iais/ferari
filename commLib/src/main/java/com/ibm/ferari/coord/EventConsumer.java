package com.ibm.ferari.coord;

import com.ibm.ferari.FerariEvent;

public interface EventConsumer {
	
	public void consumeEvent(FerariEvent event);

}
