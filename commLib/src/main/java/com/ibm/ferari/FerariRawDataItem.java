package com.ibm.ferari;

import java.io.Serializable;

import org.apache.storm.tuple.Values;

public class FerariRawDataItem implements Serializable {

	private static final long serialVersionUID = -628738443952627006L;
	private Values rawEvent;

	public Values getRawEvent() {
		return rawEvent;
	}

	public void setRawEvent(Values rawEvent) {
		this.rawEvent = rawEvent;
	}
	
	public String toString() {
		return rawEvent.toString();
	}
	
	
}
