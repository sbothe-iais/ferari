package com.ibm.ferari;

import java.io.Serializable;

public class OptimizerRequest extends FerariMessage  {

	private static final long serialVersionUID = -8105648892971048050L;

	public OptimizerRequest(String recipient, Serializable msg) {
		super(FerariConstants.OPTIMIZER, recipient, msg);
		setMsgType(MessageType.OptimizerRequest);
	}

}
