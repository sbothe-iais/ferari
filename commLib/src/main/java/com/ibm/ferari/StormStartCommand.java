package com.ibm.ferari;

import java.io.Serializable;

import org.apache.storm.Config;
import org.apache.storm.generated.StormTopology;

public class StormStartCommand extends FerariMessage {

	private static final long serialVersionUID = -4199051015881087830L;

	public StormStartCommand(String recipient, Config conf, StormTopology topology) {
		super(FerariConstants.OPTIMIZER, recipient, new TopologyStartRequest(recipient, conf, topology));
		setMsgType(MessageType.StormStartCmd);
	}

	public static class TopologyStartRequest implements Serializable {
		private static final long serialVersionUID = 1150014471125435688L;

		public Config getConf() {
			return conf;
		}

		public void setConf(Config conf) {
			this.conf = conf;
		}

		public StormTopology getTopology() {
			return topology;
		}

		public void setTopology(StormTopology topology) {
			this.topology = topology;
		}

		private Config conf;
		private StormTopology topology;
		private String site;

		public TopologyStartRequest(String site, Config conf, StormTopology topology) {
			this.conf	  = conf;
			this.topology = topology;
			this.site     = site;
		}

		public String getSite() {
			return site;
		}

		public void setSite(String site) {
			this.site = site;
		}

	}

}
