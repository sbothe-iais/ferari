package com.ibm.ferari;

import java.io.Serializable;

import org.apache.storm.generated.ClusterSummary;

public interface StormMonitoringFilter {

	public Serializable filterClusterSummary(ClusterSummary clusterSummary);
	
}
