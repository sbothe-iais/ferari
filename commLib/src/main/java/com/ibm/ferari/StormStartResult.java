package com.ibm.ferari;


public class StormStartResult extends FerariMessage {

	private static final long serialVersionUID = -1512520468015351412L;

	public StormStartResult(String senderId, Throwable e) {
		super(senderId, FerariConstants.OPTIMIZER, e);
		setMsgType(MessageType.StormStartResult);
	}
	
	public StormStartResult(String senderId, String topologyId) {
		super(senderId, FerariConstants.OPTIMIZER, topologyId);
		setMsgType(MessageType.StormStartResult);
	}

}
