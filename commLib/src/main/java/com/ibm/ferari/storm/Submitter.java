package com.ibm.ferari.storm;


import org.apache.storm.Config;
import org.apache.storm.generated.StormTopology;

public interface Submitter {
	
	/***
	 * Submits a topology to the Storm cluster. 
	 * The method blocks until a topology was successfully submitted to the cluster,
	 * or throws an exception if something went wrong
	 * @param conf
	 * @param topology
	 * @return The topology name, used later for killing that topology
	 * @throws an exception, if something went wrong
	 */
	public String submitTopology(Config conf, StormTopology topology) throws Exception;
	
	/***
	 * Kills a running topology
	 * @param topologyName
	 */
	public void killTopology(String topologyName);

}
