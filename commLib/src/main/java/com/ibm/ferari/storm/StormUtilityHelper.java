package com.ibm.ferari.storm;

import java.util.LinkedList;
import java.util.List;

import org.apache.storm.thrift.TException;

import com.ibm.ferari.FerariConstants;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.TopologySummary;
import org.apache.storm.generated.Nimbus.Client;
import org.apache.storm.utils.NimbusClient;

public class StormUtilityHelper {

	private Client _cl;
	
	public List<TopologySummary> listTopologies(LocalCluster lc) {
		return new LinkedList<TopologySummary>(lc.getClusterInfo().get_topologies());
	}

	public List<TopologySummary> listTopologies(String nimbusHost, int nimbusPort) {
		try {
			if (_cl == null) {
				Config conf = new Config();
				conf.put(Config.NIMBUS_HOST, nimbusHost);
				conf.put(Config.NIMBUS_THRIFT_PORT, nimbusPort);
				conf.setDebug(false);
				conf.put("storm.thrift.transport", FerariConstants.NIMBUS_THRIFT_DEF_PROTOCOL);
				NimbusClient client = new NimbusClient(conf, nimbusHost, nimbusPort);
				_cl = client.getClient();
			}
			
			return new LinkedList<TopologySummary>(_cl.getClusterInfo().get_topologies());
			
		} catch (TException e) {
			throw new RuntimeException("Failed obtaining topology information", e);
		}
	}
	
}
