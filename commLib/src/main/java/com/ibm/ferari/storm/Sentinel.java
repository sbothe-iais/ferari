package com.ibm.ferari.storm;

import java.lang.reflect.Constructor;


import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.client.InterSiteClient;
import com.ibm.ferari.exceptions.FerariStartupException;


public class Sentinel {

	private Submitter _submitter;
	private InterSiteClient _client;
	private String _siteId;
	private AddressResolver _resolver;

	public Sentinel(Submitter submitter, String siteId, AddressResolver resolver) {
		_submitter = submitter;
		_siteId = siteId;
		_resolver = resolver;
		_client = createHackyOptimizerClient();
		
	}

	public void close() {
		_client.close();
	}

	private InterSiteClient createHackyOptimizerClient() {
		try {
			Class<?> interSiteCls = Class.forName("com.ibm.ferari.client.impl.IntersiteClientImpl");
			Constructor<?> ctor = interSiteCls.getDeclaredConstructor(String.class, AddressResolver.class,
					Submitter.class);
			ctor.setAccessible(true);
			return (InterSiteClient) ctor.newInstance(_siteId, _resolver, _submitter);
		} catch (Exception e) {
			throw new FerariStartupException("Failed instantiating intersite client", e);
		}
	}

}
