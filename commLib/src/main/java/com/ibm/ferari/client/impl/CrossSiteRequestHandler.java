package com.ibm.ferari.client.impl;

import java.io.Serializable;

public interface CrossSiteRequestHandler {

	public void handleCrossSiteRequest(Serializable msg, String source);
	
	public void cleanCrossSiteRequest(Serializable msg, String source);
	
}
