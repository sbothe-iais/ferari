package com.ibm.ferari.client;

import java.io.Serializable;

public interface MessageHandler {

	public void onMessage(Serializable msg);
	
}
