package com.ibm.ferari.client;

public interface DashboardForwarder extends WSForwarder {
	
	public void forward(String event, String context);
	
	public void forwardSync(String event, String context);
	
	
	
}
