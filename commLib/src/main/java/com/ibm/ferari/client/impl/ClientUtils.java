package com.ibm.ferari.client.impl;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.util.Logger;
import com.ibm.ferari.util.Util;

public class ClientUtils {

	static ClientSession connect2SomeBroker(String[] hosts,Logger logger,String mySiteId) throws Exception {
		Map<String,ClientSession> validSessMap = new ConcurrentHashMap<String,ClientSession>();
		Semaphore sem = new Semaphore(0);
		for (String host : hosts) {
			Util.runAsyncTask(new Runnable() {
				public void run() {
					try {
						ClientSession sess = connect2Broker(host,mySiteId);
						validSessMap.put(host, sess);
						sem.release(hosts.length);
					} catch (JMSException e) {
						logger.error("Failed connecting to " + host, e);
						sem.release();
					}
				}
			});
		}
		
		// wait for all establish attempts to fail, or for one to succeed
		try {
			sem.acquire(hosts.length);
		} catch (InterruptedException e) {
			logger.warn("Got interrupt while waiting for sessions to be established");
		}
		
		if (validSessMap.size() > 0) {
			return validSessMap.values().iterator().next();
		} else {
			throw new Exception("Failed establishing session");
		}
	}
	
	private static ClientSession connect2Broker(String host,String mySiteId) throws JMSException {
		String hostname = host.split(":")[0];	
		int port = Integer.parseInt(host.split(":")[1]);
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				"tcp://" + hostname + ":" + port);
		connectionFactory.setClientID(FerariConstants.ANONYMOUS_CLIENT_ID + "." + UUID.randomUUID().toString() + "." + mySiteId);
		connectionFactory.setConnectionIDPrefix(FerariConstants.ANONYMOUS_CLIENT_ID);
		
		ActiveMQConnection connection = (ActiveMQConnection) connectionFactory
				.createConnection();

		connection.start();
		
		return new ClientSession(connection,connection
					.createSession(false, Session.AUTO_ACKNOWLEDGE));
	}
	
	
	public static FerariMessage extractMessage(Message msg,Logger logger) {
		if (msg instanceof ObjectMessage) {
			ObjectMessage objectMsg = (ObjectMessage) msg;
			try {
				Serializable s = objectMsg.getObject();
				if (! (s instanceof FerariMessage)) {
					logger.warn("Received a serializable of invalid type (" + s.getClass().getName() + " )");
					return null;
				}
				
				FerariMessage fMsg = (FerariMessage) s;
				return fMsg;
				
			} catch (JMSException e) {
				logger.error(
						"Failed extracting Serializable from jms message", e);
			}
		} else {
			logger.warn("Received a non-object message of type "
					+ msg.getClass());
		}
		return null;
	}
	
	static class ClientSession {
		public Session getSess() {
			return sess;
		}

		public Connection getConn() {
			return conn;
		}

		private Session sess;
		private Connection conn;
		
		ClientSession(Connection conn,Session sess) {
			this.conn = conn;
			this.sess = sess;
		}

		public void close() {
			try {
				sess.close();
				conn.stop();
				conn.close();
			} catch (JMSException e) {
				
			}
		}
	}
	
	
	
}

