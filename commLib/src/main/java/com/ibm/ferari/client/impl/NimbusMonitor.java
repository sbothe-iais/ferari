package com.ibm.ferari.client.impl;

import org.apache.storm.thrift.TException;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.client.NimbusMonitorConfig;
import com.ibm.ferari.util.Logger;
import com.ibm.ferari.util.Util;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.ClusterSummary;
import org.apache.storm.generated.Nimbus.Client;
import org.apache.storm.utils.NimbusClient;

class NimbusMonitor implements Runnable {

	private volatile boolean _alive = true;
	private int _monitorInterval;
	private Logger _logger;
	private String _nimbusHost;
	private int _nimbusPort;
	private NimbusNotifier _nimbusNotifier;
	private Thread _thisThread;
	private LocalCluster _localCluster;

	NimbusMonitor(NimbusMonitorConfig config, NimbusNotifier notifier) {
		_localCluster = config.getLocalCluster();
		_logger = Logger.createLogger("NimbusMonitor" + config.getNimbusHost() + ":" + config.getNimbusPort());
		_nimbusHost = config.getNimbusHost();
		_nimbusPort = config.getNimbusPort();
		_monitorInterval = config.getMonitorInterval();
		_nimbusNotifier = notifier;
		_thisThread = new Thread(this, "NimbusMonitor");
		_thisThread.start();
	}

	private void monitor() throws TException {
		ClusterSummary summary;
		if (_localCluster != null) {
			summary = _localCluster.getClusterInfo();
		} else {
			Config conf = new Config();
			conf.put(Config.NIMBUS_HOST, _nimbusHost);
			conf.put(Config.NIMBUS_THRIFT_PORT, _nimbusPort);
			conf.setDebug(false);
			conf.put("storm.thrift.transport", FerariConstants.NIMBUS_THRIFT_DEF_PROTOCOL);
			NimbusClient client = new NimbusClient(conf, _nimbusHost, _nimbusPort);
			Client cl = client.getClient();
			summary = cl.getClusterInfo();
			client.close();
		}
		_logger.info("Sending state " + summary);
		_nimbusNotifier.emitState(summary);
	}

	public void stop() {
		_alive = false;
		_thisThread.interrupt();
	}
	
	@Override
	public void run() {
		while (_alive) {
			try {
				monitor();
			} catch (TException e) {
				_logger.error("Failed monitoring Nimbus", e);
				_nimbusNotifier.emitError(e);
			} catch (Exception e) {
				_logger.error("Something went wrong, got an unexpected error. Skipped error emission", e);
			}
			Util.sleep(_monitorInterval);
		}
	}

}
