package com.ibm.ferari.client.impl;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class DashboardSocketServer extends WebSocketServer {

	public DashboardSocketServer(String address,  int port ) throws UnknownHostException {
		//super( new InetSocketAddress(address, port ) );
		super( new InetSocketAddress(port) );
	}

	

	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake ) {
		System.out.println("Dashboard connected");
	}

	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
		System.out.println( conn + " Dashboard disconnected" );
	}

	@Override
	public void onMessage( WebSocket conn, String message ) {
		this.send( message );
		System.out.println( conn + ": " + message );
	}

	
	
	@Override
	public void onError( WebSocket conn, Exception ex ) {
		ex.printStackTrace();
		if( conn != null ) {
			conn.close();
			// some errors like port binding failed may not be assignable to a specific websocket
		}
	}

	/**
	 * Sends <var>text</var> to all currently connected WebSocket clients.
	 * 
	 * @param text
	 *            The String to send across the network.
	 * @throws InterruptedException
	 *             When socket related I/O errors occur.
	 */
	public void send( String text ) {
		Collection<WebSocket> con = connections();
			for( WebSocket c : con ) {
				c.send( text );
			}
	}
	
	public void sendSync( String text ) {
		Collection<WebSocket> con = connections();
		synchronized ( con ) {
			for( WebSocket c : con ) {
				c.send( text );
			}
		}
	}
}