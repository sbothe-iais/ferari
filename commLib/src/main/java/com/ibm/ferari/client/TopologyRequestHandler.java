package com.ibm.ferari.client;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface TopologyRequestHandler {
	
	public void onTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig);

}
