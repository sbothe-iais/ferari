package com.ibm.ferari.client;

import java.io.Serializable;
import java.util.Set;

import com.ibm.ferari.AddressResolver;

/****
 * An intersite client, used for sending messages across sites.
 * Uses an {@link AddressResolver} that should be preconfigured beforehand, 
 * by setting it in the {@link FerariClientBuilder} method.
 * @author yacovm
 *
 */
public interface InterSiteClient {
	
	/****
	 * Sends a message to a remote site
	 * @param message The message to send
	 * @param site The remote site to send the message to
	 */
	public void sendMsg(Serializable message,String site) ;
	
	/***
	 * Sends a cross site request to some site
	 * The message is processed by {@link IntraSiteClient}'s setCrossSiteRequestHandler method
	 * @param message The message to send
	 * @param site The site to send to
	 */
	public void crossSiteRequest(Serializable message,String site) ;
	
	/***
	 * Shuts down the client
	 */
	public void close();
	
	/****
	 * @return The sites the current topology configures
	 */
	public Set<String> getRecipients();
	
	
	/***
	 * @return The sites that may send to this site
	 */
	public Set<String> getSources();
	
	/***
	 * Sends a message to the optimizer
	 * @param message
	 */
	public void send2Optimizer(Serializable message) ;
	
	
}
