package com.ibm.ferari.client.impl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import com.ibm.ferari.FerariMessage;

public class LocalMessageStorage {
	
	private final static String STORAGE_FILE_TEMPLATE = "msgLogX.arc";
	
	private File _logFile;
	private RandomAccessFile _raf;
	
	public LocalMessageStorage(String targetSite) throws IOException {
		_logFile = new File(STORAGE_FILE_TEMPLATE.replace("X", targetSite));
		if (! _logFile.exists()) {
			_logFile.createNewFile();
		}
		_raf = new RandomAccessFile(_logFile, "rws");
		
		// scan the log file, and see if it's full of zeroes.
		_raf.seek(0);
	}
	
	synchronized void appendMessage(byte[] messageData) throws IOException {
		_raf.seek(_raf.length());
		_raf.writeInt(messageData.length);
		_raf.write(messageData);
		_raf.writeBoolean(true);
	}
	
	synchronized void prepareRecovery() throws IOException {
		_raf.seek(0);
	}
	
	synchronized private void truncateFile() throws IOException {
		_raf.setLength(0);
		_raf.seek(0);
	}
	
	synchronized FerariMessage getNextMessage() throws IOException {
		long currPos = _raf.getFilePointer();
		long len = _raf.length();
		if (len == 0) {
			return null;
		}
		long diff = len - currPos;
		// can we read more than an int?
		if (diff <= 4) {
			truncateFile();
			return null;
		}
		int msgSize = _raf.readInt();
		// can we read a buffer in length of msgSize?
		currPos = _raf.getFilePointer();
		diff = len - currPos;
		long posAfterReading = currPos + msgSize + 1;
		if (posAfterReading > len) {
			System.err.println("Truncating " + 60);
			truncateFile();
			return null;
		}
		
		byte[] data = new byte[msgSize];
		_raf.readFully(data);
		boolean committed = _raf.readBoolean();
		if (! committed) {
			System.err.println("Truncating " + 69);
			truncateFile();
			return null;
		}
		
		// if we failed deserializing the message, it means that the write was corrupted, and an exception was thrown
		// to the overlying layer.
		try {
			FerariMessage msg = FerariMessage.fromBytes(data);
			return msg;
		} catch (ClassNotFoundException e) {
			System.err.println("Truncating " + 80);
			truncateFile();
		}
		return null;
	}
	
	synchronized boolean isEmpty() throws IOException {
		return _raf.length() == 0;
	}

}
