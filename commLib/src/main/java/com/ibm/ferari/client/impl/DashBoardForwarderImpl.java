package com.ibm.ferari.client.impl;

import com.ibm.ferari.client.DashboardForwarder;

public class DashBoardForwarderImpl extends WSForwarderImpl implements DashboardForwarder {

	DashBoardForwarderImpl(String uri, String topic) {
		super(uri, "fraudEvents");
	}

	@Override
	public void forward(String event, String context) {
		super.forward(event, context);
	}

	@Override
	public void forwardSync(String event, String context) {
		super.forwardSync(event, context);
	}
	
}
