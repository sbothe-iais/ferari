package com.ibm.ferari.client.impl;

import org.apache.storm.generated.ClusterSummary;

public interface NimbusNotifier {
	
	void emitError(Throwable e);
	
	void emitState(ClusterSummary clusterSummary);
}
