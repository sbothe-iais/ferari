package com.ibm.ferari.client.impl;


import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Semaphore;

import com.ibm.ferari.client.WSForwarder;

public class WSForwarderImpl implements WSForwarder {

	private static final int PAUSE = 500;
	protected String _uri;
	//private io.socket.client.Socket _wss;
	private DashboardSocketServer  wss;
	private String _topic;
	private Semaphore _sem = new Semaphore(1);
	private static int id = 0;
	WSForwarderImpl(String uri, String topic) {
		_uri = uri;
		_topic = topic;
		try {
		     wss = new DashboardSocketServer(uri, 3001);
		     wss.start();
		     System.out.println("DashboardSocketServer started");

		    } catch (Exception e) {
			throw new 	RuntimeException(e);
		}
	}
	
	public void forward(String event, String context) {
		event = "{\"event\":" + event + ", \"context\":\"" + context + "\"}";
		wss.send(event);
	}
	
	
	public void forwardSync(String event, String context) {
		wss.sendSync(event);
		if(PAUSE > 0) {
			try {
				Thread.sleep(PAUSE);
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void forward(String event) {
		if (_topic.equals("fraudEvents")) {
			event = "{\"event\": \"eventData\", \"context\":\"contextData\"}";
			
		}
		wss.send(event);
	}


	@Override
	public void close() {
		System.err.println("Closing WSS");
		try {
			wss.stop();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void forwardSync(String event) {
		forwardSync(event, "");
	}


	private void acquire() {
		while (true) {
			try {
				_sem.acquire();
				return;
			} catch (InterruptedException e) {
			}
		}
	}

}