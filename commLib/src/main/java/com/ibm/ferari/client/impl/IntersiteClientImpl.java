package com.ibm.ferari.client.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;

import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.FerariMessage.MessageType;
import com.ibm.ferari.StormKillCommand;
import com.ibm.ferari.StormStartCommand.TopologyStartRequest;
import com.ibm.ferari.StormStartResult;
import com.ibm.ferari.TopologyRequest;
import com.ibm.ferari.client.InterSiteClient;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.NimbusMonitorConfig;
import com.ibm.ferari.client.impl.ClientUtils.ClientSession;
import com.ibm.ferari.client.impl.OptimizerClient.TopologyChangeListener;
import com.ibm.ferari.client.impl.OptimizerClient.OptimizerMessagelistener;
import com.ibm.ferari.exceptions.FerariStartupException;
import com.ibm.ferari.storm.Submitter;
import com.ibm.ferari.util.Logger;
import com.ibm.ferari.util.Util;

import org.apache.storm.generated.ClusterSummary;

public class IntersiteClientImpl implements InterSiteClient {

	private static final int TOPOLOGY_REQUEST_SLEEPTIME = 10;

	private static final int RETRANSMISSION_THRESHOLD_SECONDS = 3600;

	private Map<String, SiteSender> _clients = new ConcurrentHashMap<String, SiteSender>();
	private OptimizerClient _optimizerClient;

	private String _siteId;
	private AddressResolver _addrResolver;
	private com.ibm.ferari.util.Logger _logger;
	private volatile boolean _isOptimizerAClient = false;
	private IntraSiteClient _intraClient;
	private NimbusMonitor _nimbusMonitor;
	private volatile Set<String> sources;
	
	IntersiteClientImpl(String siteId, AddressResolver addrResolver) {
		_addrResolver = addrResolver;
		_siteId = siteId;
		_logger = Logger.createLogger(getClass().getSimpleName() + "-" + siteId);
		_logger.info("Started client, _siteId : " + _siteId);
	}

	IntersiteClientImpl(String siteId, AddressResolver addrResolver, NimbusMonitorConfig config)
			throws FerariStartupException {
		_addrResolver = addrResolver;
		_siteId = siteId;
		_logger = Logger.createLogger(getClass().getSimpleName() + "-" + siteId);
		_logger.info("Started client, _siteId : " + _siteId);

		if (_siteId.equals(_addrResolver.getOptimizerSite())) {
			_intraClient = new IntraSiteClientImpl(_siteId, _addrResolver.getOptimizerHosts(), _logger);
		} else {
			_intraClient = new IntraSiteClientImpl(_siteId, _addrResolver.getHostsOfSite(_siteId), _logger);
		}

		connect2Optimizer();

		if (config == null) {
			System.out.println("Returning");
			return;
		}

		config.validate();

		_nimbusMonitor = new NimbusMonitor(config, new NimbusNotifier() {

			@Override
			public void emitState(ClusterSummary clusterSummary) {
				Serializable filteredMonInfo = config.getMonitoringFilter().filterClusterSummary(clusterSummary);
				send2Optimizer(filteredMonInfo);
			}

			@Override
			public void emitError(Throwable e) {
				send2Optimizer(e);
			}
		});
		
	}

	IntersiteClientImpl(String siteId, AddressResolver addrResolver, Submitter submitter)
			throws FerariStartupException {
		_addrResolver = addrResolver;
		_siteId = siteId;
		_logger = Logger.createLogger(getClass().getSimpleName() + "-" + siteId);
		_logger.info("Started client, _siteId : " + _siteId);

		_optimizerClient = new OptimizerClient(new TopologyChangeListener() {
			@Override
			public void topologyChange(Map<String, List<String>> topologyGraph,
					Map<String, Serializable> customconfig) {
			}
		}, new OptimizerMessagelistener() {
			@Override
			public void onMessage(FerariMessage msg) {
				if (msg.getMsgType() == MessageType.StormStartCmd) {
					TopologyStartRequest ssr = (TopologyStartRequest) msg.getUserMsg();
					if (! ssr.getSite().equals(siteId)) {
						return;
					}
					try {
						String topologyId = submitter.submitTopology(ssr.getConf(), ssr.getTopology());
						_optimizerClient.sendMsg(new StormStartResult(_siteId, topologyId));
					} catch (Exception e) {
						_logger.error("Failed starting topology", e);
						_optimizerClient.sendMsg(new StormStartResult(_siteId, e));
					}
				}
				
				if (msg.getMsgType() == MessageType.StormKillCmd) {
					StormKillCommand skc = (StormKillCommand) msg;
					submitter.killTopology((String) skc.getUserMsg());
				}
			}
		}, _siteId, addrResolver.getOptimizerHosts(), TOPOLOGY_REQUEST_SLEEPTIME, _logger);

	}

	private void connect2Optimizer() throws FerariStartupException {
		_optimizerClient = new OptimizerClient(new TopologyChangeListener() {
			@Override
			public void topologyChange(Map<String, List<String>> topologyGraph,
					Map<String, Serializable> customConfig) {
				_logger.debug("Got topology change message from optimizer");
				IntersiteClientImpl.this.topologyChange(topologyGraph, customConfig);
			}
		}, new OptimizerMessagelistener() {
			@Override
			public void onMessage(FerariMessage msg) {
				((IntraSiteClientImpl) _intraClient).forwardMessageFromOptimizer(msg);
			}
		}, _siteId, _addrResolver.getOptimizerHosts(), TOPOLOGY_REQUEST_SLEEPTIME, _logger);
		_optimizerClient.addConnectionListener(new ConnectionListener() {
			@Override
			public void onStatusChange(boolean connected) {
				if (connected) {
					try {
						_optimizerClient.sendMsg(new TopologyRequest(_siteId));
					} catch (IOException e) {
						_logger.error("Failed sending topology request");
					}
				}
			}
		});
	}

	private void topologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
		_logger.debug("Entering");

		recalculateSources(topologyGraph);

		sendTopologyChangeEvent(topologyGraph, customConfig);

		List<String> recipients = topologyGraph.get(_siteId);

		if (recipients == null) {
			_logger.warn("Didn't find myself (" + _siteId + ") in recipient list");
			return;
		}

		List<String> prevRecipients = new LinkedList<String>();
		prevRecipients.addAll(_clients.keySet());

		// if previous recipient is equal to new recipient, abort
		// reconfiguration
		if (Util.compareLists(recipients, prevRecipients)) {
			_logger.debug("Exiting, previous recipients hasn't changed");
			return;
		}

		closeSiteSenders();

		_isOptimizerAClient = false;
		for (String recipient : recipients) {
			if (recipient.equals(_addrResolver.getOptimizerSite())) {
				_isOptimizerAClient = true;
				continue;
			}
			try {
				_logger.info("Creating new FerariSiteClient with id of " + _siteId + " for " + recipient);

				_clients.put(recipient, new SiteSender(_siteId, recipient, _addrResolver.getHostsOfSite(recipient),
						RETRANSMISSION_THRESHOLD_SECONDS, _logger));
			} catch (FerariStartupException e) {
				_logger.error("Failed creating a client to " + recipient, e);
			}
		}

		_logger.debug("Exiting, remote site senders are: " + _clients.keySet());
	}

	private void recalculateSources(Map<String, List<String>> topologyGraph) {
		Set<String> s = new HashSet<String>();
		for (String src : topologyGraph.keySet()) {
			if (topologyGraph.get(src).contains(_siteId)) {
				s.add(src);
			}
		}
		sources = s;
	}

	private void sendTopologyChangeEvent(Map<String, List<String>> topologyGraph,
			Map<String, Serializable> customConfig) {
		_logger.debug("Entering " + topologyGraph + " " + customConfig);
		_intraClient.notifyTopologyChange(topologyGraph, customConfig);
	}

	@Override
	public void sendMsg(Serializable message, String site) {
		_logger.debug("Entering, remote site senders are: " + _clients.keySet());
		_logger.info("Sending to " + site + ": " + message);
		SiteSender cl = _clients.get(site);
		if (cl == null) {
			_logger.warn(
					site + " doesn't exist in remote sites list, creating new " + SiteSender.class.getSimpleName());
			synchronized (this) {
				try {
					_clients.put(site, new SiteSender(_siteId, site, _addrResolver.getHostsOfSite(site),
							RETRANSMISSION_THRESHOLD_SECONDS, _logger));
					cl = _clients.get(site);
				} catch (FerariStartupException e) {
					_logger.error("Failed creating a client to " + site, e);
					_logger.debug("Exiting");
					return;
				}
			}
		}

		cl.sendMsg(new FerariMessage(_siteId, site, message));
		_logger.debug("Exiting");
	}

	@Override
	public void crossSiteRequest(Serializable message, String site) {
		try {
			ClientSession sess = ClientUtils.connect2SomeBroker(_addrResolver.getHostsOfSite(site), _logger, _siteId);
			sendCrossSiteRequest(sess.getSess(), message, site);
			sess.close();
		} catch (Exception e) {
			_logger.error("Failed sending cross site request", e);
		}
	}

	private void sendCrossSiteRequest(Session sess, Serializable message, String site) throws JMSException {
		_logger.info("Sending " + MessageType.CrossSiteRequest + " + message " + message + " to " + site);
		Topic crossSiteRequestTopic = sess.createTopic(FerariConstants.CROSS_SITE_REQUEST_TOPIC);
		MessageProducer topicSender = sess.createProducer(crossSiteRequestTopic);

		FerariMessage ferrMsg = new FerariMessage(_siteId, site, message);
		ferrMsg.setMsgType(MessageType.CrossSiteRequest);

		ObjectMessage objMsg = sess.createObjectMessage();
		objMsg.setObject(ferrMsg);
		topicSender.send(objMsg);
		topicSender.close();
	}

	private void closeSiteSenders() {
		_logger.debug("Entering");
		for (SiteSender ss : _clients.values()) {
			ss.close();
		}

		boolean someoneIsRunning = true;

		while (someoneIsRunning) {
			_logger.debug("Waiting for connections threads to shutdown...");
			someoneIsRunning = false;
			for (SiteSender ss : _clients.values()) {
				someoneIsRunning = someoneIsRunning || ss.isRunning();
			}
		}
		_clients.clear();
		_logger.debug("Exiting");
	}

	@Override
	public void close() {
		if (_nimbusMonitor != null) {
			_nimbusMonitor.stop();
		}
		if (_intraClient != null) {
			_intraClient.close();
		}
		if (_optimizerClient != null) {
			_optimizerClient.close();
			while (_optimizerClient.isRunning()) {
				Util.sleep(1);
			}
		}

		closeSiteSenders();
	}

	@Override
	public void send2Optimizer(Serializable message) {
		if (_optimizerClient == null) {
			connect2Optimizer();
		}
		
		_optimizerClient.sendMsg(new FerariMessage(_siteId, FerariConstants.OPTIMIZER, message));
	}

	@Override
	public Set<String> getRecipients() {
		Set<String> returnedSet = new HashSet<String>(_clients.keySet());
		if (_isOptimizerAClient) {
			returnedSet.add(_addrResolver.getOptimizerSite());
		}
		return returnedSet;
	}

	@Override
	public Set<String> getSources() {
		if (sources == null) {
			return new HashSet<>();
		}
		return new HashSet<String>(sources);
	}

}
