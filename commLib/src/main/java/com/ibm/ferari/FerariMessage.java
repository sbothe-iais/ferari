package com.ibm.ferari;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

public class FerariMessage implements Serializable {

	private static transient AtomicLong msgSeq = new AtomicLong(System.currentTimeMillis());

	private static final long serialVersionUID = -307709149347743959L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public enum MessageType {
		Unknown, Event, TopologyRequest, TopologyPlan, CrossSiteRequest, TopologyChange, OptimizerRequest, IntraSiteRequest, StormStartCmd, StormKillCmd, StormStartResult
	}

	private MessageType msgType;
	private Serializable userMsg;
	private String recipient;
	private String senderId;
	protected long sequence;
	private long timestamp = System.currentTimeMillis();

	public FerariMessage(String senderId, String recipient, Serializable msg) {
		this.senderId = senderId;
		this.userMsg = msg;
		this.recipient = recipient;
		this.sequence = msgSeq.getAndIncrement();
		this.msgType = MessageType.Event;
	}

	public static FerariMessage fromBytes(byte[] data) throws IOException, ClassNotFoundException {
		System.err.println("Deserializing " + data.length + " bytes");
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		ObjectInputStream ois = new ObjectInputStream(bais);
		Object obj = ois.readObject();
		FerariMessage msg = (FerariMessage) obj;
		ois.close();
		return msg;
	}

	public byte[] toByteArray() throws IOException {
		return serializeObj(this);
	}

	private static byte[] serializeObj(Serializable msg) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(msg);
		oos.flush();
		oos.close();
		return baos.toByteArray();
	}

	public long getSequence() {
		return sequence;
	}

	public Serializable getUserMsg() {
		return userMsg;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getSenderId() {
		return senderId;
	}

	public String getRecipient() {
		return recipient;
	}

	public MessageType getMsgType() {
		return msgType;
	}

	public void setMsgType(MessageType msgType) {
		this.msgType = msgType;
	}

	public String toString() {
		return msgType.name() + " seqNum " + sequence + " from " + senderId + " to " + recipient + " payload: "
				+ userMsg;
	}

}
