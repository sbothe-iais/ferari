package com.ibm.ferari;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TopologyPlan extends FerariMessage {

	private static final long serialVersionUID = 3355001996058248580L;

	public TopologyPlan(String recipient, Map<String,List<String>> topologyGraph, Map<String, Serializable> customConfig) {
		super(FerariConstants.OPTIMIZER, recipient, new TopologyConfig(topologyGraph, customConfig));
		setMsgType(MessageType.TopologyPlan);
	}
	
	public Map<String,List<String>> getTopologyGraph() {
		return ((TopologyConfig)getUserMsg()).topologyGraph;
	}
	
	public Map<String, Serializable> getCustomConfig() {
		return ((TopologyConfig)getUserMsg()).customConfig;
	}
	
	public static class TopologyConfig implements Serializable {
		
		private static final long serialVersionUID = 1246165663633824809L;
		
		public TopologyConfig(Map<String,List<String>> topologyGraph, Map<String, Serializable> customConfig) {
			this.topologyGraph = topologyGraph;
			this.customConfig = customConfig;
		}
		
		@SuppressWarnings("unused")
		public TopologyConfig() {
			
		}
		
		public Map<String, List<String>> getTopologyGraph() {
			return topologyGraph;
		}

		public void setTopologyGraph(Map<String, List<String>> topologyGraph) {
			this.topologyGraph = topologyGraph;
		}

		public Map<String, Serializable> getCustomConfig() {
			return customConfig;
		}

		public void setCustomConfig(Map<String, Serializable> customConfig) {
			this.customConfig = customConfig;
		}

		private Map<String,List<String>> topologyGraph; 
		private Map<String, Serializable> customConfig;
	}

}
