package com.ibm.ferari.util;


import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.jms.Connection;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Util {

	public static void sleep(int seconds) {
		try {
			Thread.sleep(1000 * seconds);
		} catch (InterruptedException e) {
		}
	}

	public static Thread runAsyncTask(Runnable run) {
		Thread t = new Thread(run);
		t.start();
		return t;
	}

	public static boolean probeBroker(String host, int port) {
		boolean succ = false;
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				"tcp://" + host + ":" + port);
		try {
			Connection connection = connectionFactory.createConnection();
			connection.start();
			connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.close();
			succ = true;
		} catch (Exception e) {
			return false;
		}
		return succ;
	}

	public static void clearDirectory(String dirName) {
		File dir = new File(dirName);
		if (!dir.exists()) {
			System.err.println(dir.getAbsolutePath() + " not exists");
			return;
		}
		for (File f : dir.listFiles()) {
			if (f.isFile()) {
				f.delete();
			}
		}
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				clearDirectory(f.getAbsolutePath());
				f.delete();
			}
		}
	}
	
	public static boolean compareLists(List<String> l1,List<String> l2) {
		Collections.sort(l1);
		Collections.sort(l2);
		return Arrays.toString(l1.toArray(new String[]{})).equals(Arrays.toString(l2.toArray(new String[]{})));
	}
}
