package com.ibm.ferari.tests;

import java.io.PrintStream;

public class SupervisorRunner {

	public static void main(String[] args) {
		try {
			System.setErr(new PrintStream("supervisor.err"));
			System.setOut(new PrintStream("supervisor.out"));
			Class<?> supervisorCls = Class.forName("backtype.storm.daemon.supervisor");
			supervisorCls.getDeclaredMethod("main", String[].class).invoke(null, (Object) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
