package com.ibm.ferari.tests;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;

import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.StormMonitoringFilter;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.InterSiteClient;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.MessageHandler;
import com.ibm.ferari.client.TopologyRequestHandler;
import com.ibm.ferari.client.impl.CrossSiteRequestHandler;
import com.ibm.ferari.coord.Coordinator;
import com.ibm.ferari.coord.EventConsumer;
import com.ibm.ferari.coord.Optimizer;
import com.ibm.ferari.exceptions.FerariStartupException;
import com.ibm.ferari.storm.Sentinel;
import com.ibm.ferari.storm.Submitter;
import com.ibm.ferari.util.Util;

import org.apache.storm.Config;
import org.apache.storm.generated.ClusterSummary;
import org.apache.storm.generated.StormTopology;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClientTests {

	private static Optimizer opt;
	private static Coordinator coord;
	private static Coordinator coord2;
	private static AddressResolver addressResolver;

	static {
		addressResolver = new AddressResolver() {

			@Override
			public String[] getOptimizerHosts() {
				return new String[] { "localhost:5610" };
			}

			@Override
			public String[] getHostsOfSite(String site) {
				if (site.equals("unknown")) {
					return new String[] { "unknown:5611" };
				}
				if (site.equals("TEST")) {
					return new String[] { "localhost:5611" };
				}
				if (site.equals("TEST2")) {
					return new String[] { "localhost:5612" };
				}
				return new String[] { "localhost:5610" };
			}

			@Override
			public String getOptimizerSite() {
				return "CENTER";
			}

			@Override
			public String getDashboardWSURI() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		FerariClientBuilder.getInstance().setAddressResolver(addressResolver);
	}

	private static Map<String, String> site2SiteEvents = new ConcurrentHashMap<>();
	private static Map<String, FerariEvent> optEvents = new ConcurrentHashMap<String, FerariEvent>();
	private static Vector<Serializable> stormMonitoringEvents = new Vector<Serializable>();
	private static Vector<Exception> stormMonitoringErrors = new Vector<Exception>();
	private static Map<String, FerariEvent> coordEvents = new ConcurrentHashMap<String, FerariEvent>();
	private static Queue<String> messageOrders = new LinkedBlockingQueue<String>();
	private static AtomicInteger speedCount = new AtomicInteger();

	private static int threadCountAtStartup;

	@BeforeClass
	public static void setup() {
		threadCountAtStartup = Thread.getAllStackTraces().size();

		for (File f : new File(".").listFiles()) {
			if (f.getName().endsWith(".arc")) {
				f.delete();
			}
		}

		opt = new Optimizer("CENTER", "center", 5610, new EventConsumer() {
			@Override
			public void consumeEvent(FerariEvent event) {
				if (event.getEvent() instanceof Exception) {
					stormMonitoringErrors.add((Exception) event.getEvent());
					return;
				}
				if (event.getEvent() instanceof ClusterSummary) {
					stormMonitoringEvents.add(event);
					return;
				}
				optEvents.put((String) event.getEvent(), event);
			}
		});

		opt.start();

		coord = new Coordinator("TEST", "test", 5611, new EventConsumer() {

			@Override
			public void consumeEvent(FerariEvent event) {
				speedCount.getAndIncrement();
			}
		});

		coord.start();

		coord2 = new Coordinator("TEST2", "test2", 5612, new EventConsumer() {

			@Override
			public void consumeEvent(FerariEvent event) {
				coordEvents.put((String) event.getEvent(), event);
				messageOrders.add((String) event.getEvent());
				site2SiteEvents.put("events", (String) event.getEvent());
			}
		});

		coord2.start();

	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

/*	@Test(expected = FerariStartupException.class)
	public void testBadURIsIntraSite() throws FerariStartupException  {
		createIntraSiteClient("unknown");
	}*/

	@Test
	public void testPullRequest()  {
		Semaphore sem = new Semaphore(0);
		IntraSiteClient intraClient = createIntraSiteClient("TEST");
		intraClient.setCrossSiteRequestHandler(new CrossSiteRequestHandler() {

			@Override
			public void handleCrossSiteRequest(Serializable msg, String source) {
				sem.release();
			}

			@Override
			public void cleanCrossSiteRequest(Serializable msg, String source) {

			}
		});

		InterSiteClient interClient = createInterSiteClient("TEST");
		interClient.crossSiteRequest("PULLLLLL", "TEST");
		try {
			if (! sem.tryAcquire(1, TimeUnit.SECONDS)) {
				Assert.fail("Didn't get pull request in time");
			}
		} catch (InterruptedException e) {
			
		}

		interClient.close();
		intraClient.close();
	}

	@Test
	public void testSendToOptimizer()  {
		String siteName = UUID.randomUUID().toString();
		InterSiteClient interClient = createInterSiteClient(siteName);

		Map<String, List<String>> topology = new HashMap<String, List<String>>();
		topology.put(siteName, Arrays.asList(new String[] { "TEST" }));

		opt.setTopologyGraph(topology, new HashMap<>());

		interClient.send2Optimizer("testSendToOptimizer");
		Util.sleep(1);

		Assert.assertTrue(optEvents.containsKey("testSendToOptimizer"));
		optEvents.get("testSendToOptimizer").getSource().equals(siteName);

		interClient.close();
	}

	@Test
	public void testSendNormalMessage()  {
		InterSiteClient interClient = createInterSiteClient("TEST");
		Map<String, List<String>> topology = new HashMap<String, List<String>>();
		topology.put("TEST", Arrays.asList(new String[] { "TEST2" }));

		opt.setTopologyGraph(topology, new HashMap<>());
		sleep(1000);
		interClient.sendMsg("testSendNormalMessage", "TEST2");

		Assert.assertTrue(coordEvents.containsKey("testSendNormalMessage"));
		interClient.close();
	}
	
	@Test
	public void testSpeed() {
		InterSiteClient interClient = createInterSiteClient("TEST2");
		long start = System.currentTimeMillis();
		for (int i=0; i<1000; i++) {
			interClient.sendMsg("", "TEST");
		}
		System.out.println(System.currentTimeMillis() - start);
		System.out.println(speedCount);
		
		interClient.close();
	}
	
	@Test 
	public void testToplogyConfiChange() {
		InterSiteClient interClient = createInterSiteClient("TEST");
		IntraSiteClient isc = createIntraSiteClient("TEST");
		sleep(1000);
		
		AtomicInteger n = new AtomicInteger(0);
		
		isc.setTopologyRequestHandler(new TopologyRequestHandler() {
			
			@Override
			public void onTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
				Assert.assertEquals("custom", customConfig.get("TEST"));
				n.incrementAndGet();
			}
		});
		Map<String, List<String>> topology = new HashMap<String, List<String>>();
		topology.put("TEST", Arrays.asList(new String[] { "TEST2" }));
		Map<String, Serializable> customConf = new HashMap<>();
		customConf.put("TEST", "custom");
		
		opt.setTopologyGraph(topology, customConf);
		
		sleep(2000);

		Assert.assertEquals(1, n.get());
		
		isc.close();
		interClient.close();
	}
	
	
	@Test
	public void testConnectedSite() {
		InterSiteClient interClient2 = createInterSiteClient("TEST2");
		InterSiteClient interClient1 = createInterSiteClient("TEST");
		
		opt.getActiveSites();
		
		Util.sleep(5);
		
		interClient1.close();
		interClient2.close();
	}

	@Test
	public void testSendToYourself() {
		InterSiteClient interClient = createInterSiteClient("TEST2");
		Map<String, List<String>> topology = new HashMap<String, List<String>>();
		topology.put("TEST2", Arrays.asList(new String[] { "TEST2" }));

		opt.setTopologyGraph(topology, new HashMap<>());
		sleep(1000);
		interClient.sendMsg("testSendToYourself", "TEST2");

		Assert.assertTrue(coordEvents.containsKey("testSendToYourself"));
		interClient.close();
	}
	
	@Test 
	public void site2Site() {
		InterSiteClient isc1 = createInterSiteClient("TEST");
		Util.sleep(2);
		isc1.sendMsg("bla bla", "TEST2");
		Assert.assertEquals("bla bla", site2SiteEvents.get("events"));
		System.out.println(site2SiteEvents.get("events"));
		isc1.close();
	}
	
	@Test
	public void testOptimizerSendToSite() {
		InterSiteClient isc0 = createInterSiteClient("TEST");
		IntraSiteClient isc1 = createIntraSiteClient("TEST");
		Util.sleep(2);
		Semaphore s = new Semaphore(0);
		
		isc1.setOptimizerRequestHandler(new MessageHandler() {
			@Override
			public void onMessage(Serializable msg) {
				s.release(1);
			}
		});
		
		opt.broadcast("bla");
		opt.sendRequestToSite("bla bla", "TEST");
		
		try {
			if (! s.tryAcquire(2, 2000, TimeUnit.MILLISECONDS)) {
				Assert.fail("didn't receive messages in a timely manner");
			}
		} catch (InterruptedException e) {
			
		}
		
		isc1.close();
		isc0.close();
	}
	
	@Test 
	public void testMembership() {
		InterSiteClient isc1 = createInterSiteClient("TEST");
		InterSiteClient isc2 = createInterSiteClient("TEST2");
	
		Util.sleep(2);

		Assert.assertEquals(2, opt.getActiveSites().size());
		
		isc1.close();
		isc2.close();
	}
	
	@Test
	public void testIntraSiteBroadcast() {
		IntraSiteClient isc1 = createIntraSiteClient("TEST");
		IntraSiteClient isc2 = createIntraSiteClient("TEST");
		IntraSiteClient isc3 = createIntraSiteClient("TEST");
		
		
		Util.sleep(5);
		
		Semaphore s = new Semaphore(0);
		isc1.setbroadcastRequestHandler(new MessageHandler() {
			
			@Override
			public void onMessage(Serializable msg) {
				s.release();
			}
		});
		
		isc2.setbroadcastRequestHandler(new MessageHandler() {
			
			@Override
			public void onMessage(Serializable msg) {
				s.release();
			}
		});
		
		isc3.broadcastInOurSite("1");
		isc3.broadcastInOurSite("2");
		isc3.broadcastInOurSite("3");
		
		try {
			if (! s.tryAcquire(6,2000, TimeUnit.MILLISECONDS)) {
				Assert.fail("Didn't receive broadcast message in time");
			}
		} catch (InterruptedException e) {
			
		}
		
		isc1.close();
		isc2.close();
		isc3.close();
	}

	@Test
	public void testMessageOrder() {
		String siteName = UUID.randomUUID().toString();
		InterSiteClient interClient = createInterSiteClient(siteName);
		Map<String, List<String>> topology = new HashMap<String, List<String>>();
		topology.put(siteName, Arrays.asList(new String[] { "TEST2" }));
		opt.setTopologyGraph(topology, new HashMap<>());
		sleep(2000);

		messageOrders.clear();
		for (int i = 0; i < 100; i++) {
			interClient.sendMsg("msg" + i, "TEST2");
		}

		for (int i = 0; i < 100; i++) {
			String $ = messageOrders.remove();
			Assert.assertEquals("msg" + i, $);
		}

		interClient.close();
	}
	
	@Test
	public void testSentinel() {
		AtomicBoolean topSubmitted = new AtomicBoolean(false);
		AtomicBoolean topKilled = new AtomicBoolean(false);
		
		StormTopology top = new StormTopology();
		top.set_bolts(new HashMap<>());
		top.set_spouts(new HashMap<>());
		top.set_state_spouts(new HashMap<>());
		
		Sentinel sentinel = new Sentinel(new Submitter() {
			
			@Override
			public String submitTopology(Config conf, StormTopology topology) throws Exception {
				topSubmitted.set(true);
				Assert.assertEquals(top, topology);
				Assert.assertEquals("test", conf.get("ferari.test"));
				return "top-id";
			}
			
			@Override
			public void killTopology(String topologyName) {
				topKilled.set(true);
			}
		}, "TEST", addressResolver);
		
		Config c = new Config();
		c.put("ferari.test", "test");
		String id = opt.startTopology("TEST", c, top);
		Assert.assertTrue(topSubmitted.get());
		Assert.assertEquals("top-id", id);
		opt.killTopology("TEST", id);
		Util.sleep(2);
		Assert.assertTrue(topKilled.get());
		sentinel.close();
	}

	@Test
	public void testDisconnectedClient()  {
		InterSiteClient interClient = createInterSiteClient("TEST");
		sleep(1000);
		Assert.assertEquals(1, opt.getActiveSites().size());
		closeSocketWithReflection(interClient);
		sleep(200);
		Assert.assertEquals(0, opt.getActiveSites().size());
		sleep(1000);
		interClient.close();
	}

	private void closeSocketWithReflection(InterSiteClient interClient) {
		try {
			Object conn = getFieldByName(getFieldByName(interClient, null, "_optimizerClient"),
					Class.forName("com.ibm.ferari.client.impl.SiteSender"), "_connection");
			Object transport = getFieldByName(conn, Class.forName("org.apache.activemq.ActiveMQConnection"),
					"transport");
			Object next;
			while ((next = getFieldByName(transport, Class.forName("org.apache.activemq.transport.TransportFilter"),
					"next")) != null) {
				if (next.getClass().getName().contains("failover")) {
					Object fot = Class.forName("org.apache.activemq.transport.failover.FailoverTransport").cast(next);
					next = invokeMethod(fot, "getConnectedTransport");
				}
				transport = next;
				if (transport.getClass().getName().contains("tcp")) {
					break;
				}
			}
			Socket socket = (Socket) getFieldByName(transport,
					Class.forName("org.apache.activemq.transport.tcp.TcpTransport"), "socket");
			socket.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

/*	@Test
	public void testStormMonitoring() {
		StormTestServer testServer = new StormTestServer();
		try {
			FerariClientBuilder.getInstance().setMonitoringFilter(new StormMonitoringFilter() {

				@Override
				public Serializable filterClusterSummary(ClusterSummary clusterSummary) {
					return clusterSummary;
				}
			});

			FerariClientBuilder.getInstance().setNimbusHost("localhost");
			FerariClientBuilder.getInstance().setNimbusPort(6627);
			FerariClientBuilder.getInstance().setStormMonitoringInterval(3);

			Util.sleep(5);
			InterSiteClient interClient = createMonitoringInterSiteClient("TEST");

			Util.sleep(8);
			
			Assert.assertTrue(stormMonitoringErrors.isEmpty());
			Assert.assertFalse(stormMonitoringEvents.isEmpty());

			interClient.close();
		} finally {
			testServer.shutdown();
			Util.sleep(5);
		}
	}*/

/*	@Test
	public void testzzZZZShutdown() {
		opt.stop();
		coord.stop();
		coord2.stop();
		sleep(5000);
		
		int threadCount = 0;
		for (Thread t : Thread.getAllStackTraces().keySet()) {
			if (! t.getName().equals("process reaper")) {
				threadCount++;
			}
		}
		
		boolean threadsCleaned = threadCount == threadCountAtStartup;
		
		if (!threadsCleaned) {
			for (Thread t : Thread.getAllStackTraces().keySet()) {
				System.out.println(t.getName());
				for (StackTraceElement ste : t.getStackTrace()) {
					System.out.println(
							"       " + ste.getClassName() + "." + ste.getMethodName() + ":" + ste.getLineNumber());
				}
			}
		}
		Assert.assertTrue(threadsCleaned);

	}*/

	private InterSiteClient createInterSiteClient(String siteId) {
		return FerariClientBuilder.getInstance().createInterSiteClient(siteId, true);
	}

	private InterSiteClient createMonitoringInterSiteClient(String siteId) {
		return FerariClientBuilder.getInstance().createMonitoringIntersiteClient(siteId);
	}

	private IntraSiteClient createIntraSiteClient(String siteId) throws FerariStartupException {
		return FerariClientBuilder.getInstance().createIntraSiteClient(siteId);
	}

	private static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
	}

	private static Object invokeMethod(Object o, String method) throws Exception {
		return o.getClass().getDeclaredMethod(method, new Class[] {}).invoke(o, new Object[] {});
	}

	private static Object getFieldByName(Object o, Class<?> cls, String fieldName) throws Exception {
		Class<?> clazz = cls != null ? cls : o.getClass();
		for (Field f : clazz.getDeclaredFields()) {
			f.setAccessible(true);
			if (f.getName().equals(fieldName)) {
				return f.get(o);
			}
		}
		return null;
	}

}
