package com.ibm.ferari.tests;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


import com.ibm.ferari.util.Util;
import org.apache.storm.shade.org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.storm.shade.org.apache.zookeeper.server.ZooKeeperServer;

public class StormTestServer {

	private ZKServer _zkServer;
	private Proc _supervisor;
	private Proc _nimbus;

	public StormTestServer() {
		_zkServer = new ZKServer();
		_supervisor = new Proc(SupervisorRunner.class);
		_nimbus = new Proc(NimbusRunner.class);
	}

	public void shutdown() {
		_supervisor.kill();
		_nimbus.kill();
		_zkServer.stop();
	}

	private static class Proc {
		private Process _p;

		Proc(Class<?> cls) {
			try {
				_p = runClass(cls);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			Util.runAsyncTask(new Runnable() {

				@Override
				public void run() {
					try {
						_p.waitFor();
						_p.exitValue();
					} catch (InterruptedException e) {
					}
				}
			});
		}

		private void kill() {
			try {
				_p.getOutputStream().close();
				_p.destroy();
				clearStream(_p.getErrorStream());
				clearStream(_p.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		private void clearStream(InputStream in) {
			try {
				while (in.read() != -1);
			} catch (IOException e) {
			} finally {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

		private Process runClass(Class<?> cls) throws IOException {
			String javaExecutable = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
			String cp = System.getProperty("java.class.path");
			String[] cmd = new String[] { javaExecutable, "-cp", cp, cls.getCanonicalName() };
			return Runtime.getRuntime().exec(cmd);
		}
	}

	private static class ZKServer {
		private ZooKeeperServer _zkServer;
		private ServerCnxnFactory _scf;

		ZKServer() {
			try {
				_zkServer = new ZooKeeperServer(new File("snapDir"), new File("logdir"), 2000);
				_scf = ServerCnxnFactory.createFactory(2181, 10);
				_scf.startup(_zkServer);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		void stop() {
			_zkServer.shutdown();
			_scf.shutdown();
		}

	}

}
