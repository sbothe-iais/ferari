# Demo
The following provides instructions for demonstrating different capabilities of the FERARI architecture.

## Scenario
We will consider listen counts for different music artists and compute trends for each of them.
The users do not connect to the same service endpoint.
There will be five sites, each receiving data for a given set of users.
At each instance, for every interval of time _t_ the listen count is computed.
[Linear regression](https://en.wikipedia.org/wiki/Linear_regression) is used to predict a trend of the counts.
This is done for each artist.
If a local trend for one artists deviates more than a predefined threshold (Variance of _0.1_) from the assumed global trend,
a resolution protocol is invoked.
To do this, a violation message, including the local trend, is sent to a central site.
There, a coordinator can perform multiple actions dependent on the circumstances.
1. The  global trend has changed without the local site knowing about it and the local trend is not off by much from the global one.
The coordinator sends the global trend to the site that sent the violation.
2. The local trend deviation is larger than the threshold.
The coordinator will query additional nodes for their trends. 
Upon receiving them, a new average trend is computed.
If all present trends do not differ by more than the threshold from the average, the new trend is sent to all nodes that sent theirs.
When the trends of all locations are used for the average computation, the computed average is a new global trend.


## Running the demo
There are three sets of steps the first concerns preprocessing of data,
the second covers compilation and the last is about running the program.
The first two  can be run in parallel.
Downloading and preprocessing require a lot of time

### Requirements
* Python 3 including the pandas, numpy and progressbar packages for data processing
* Java and Maven for running the program
* Curl to upload the configuration to the system
* A web browser

### Cloning the source
* Check out the source using `git clone https://bitbucket.org/sbothe-iais/ferari`
* Change in to the cloned directory: `cd ferari`

### Data
The preprocessing takes a lot of time and memory. 4 Gb of RAM is not enough.
* Download the [last-fm data](http://www.dtic.upf.edu/~ocelma/MusicRecommendationDataset/lastfm-1K.html)
* Extract it to the multiTenant directory of the repository
* Change to the multiTenant directory `cd multiTenant`
* Run ```./transform_lastfm.py lastfm-dataset-1K/userid-timestamp-artid-artname-traid-traname.tsv```
* Run the compilation steps and depending on time of the day have lunch or a coffee break

### Compilation
* Change to the root directory of the repository.
* Run ``` mvn clean compile install -DskipTests=true```
* Copy the generated jar for the _learning_ module to the working directory: ```cp learning/target/learning-0.0.1-SNAPSHOT-jar-with-dependencies.jar multiTenant/```

### Run
* Complete the instructions regarding __Data__ and __Compilation__
* Change back to the directory of this instructions `cd multiTenant`
* Run `java -jar target/multiTenantDeployment-0.0.1-SNAPSHOT-jar-with-dependencies.jar ./`
* Wait until all sites (cent1, site1, site2, site3, site4) are reported as started (not starting)
* Open a second terminal, change the directory to the one of the `multiTenant` module and run ```curl -d @combined.json 127.0.0.1:8080``` from the command-line interface from the directory of this file
* Wait until a window pops up, showing the structure of the distributed system. Minimize it. If you close it, the whole program will be terminated.
* the console output in the first terminal, i.e. the one running java, will give you details of the resolution protocol.
* Open your browser and navigate to the `artistChart.html` located in the `examples/distributedLearning/performanceMonitor` directory of the repository. The complete path depends on the actual location on your machine. In most browsers you can navigate your local folder structure by entering `file://` into the address bar.
* This may not work immediately, as the system needs some time until it is fully started.
Keep reloading until the dialog confirms the connection.
* There will be three charts.
The first show the top five artists.
The second reveals the trends for all artists.
The last one gives the average loss suffered by the regression.
