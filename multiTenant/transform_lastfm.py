#!/usr/bin/python                   

"""
Created on Mon Dec 12 11:54:53 2016

@author: Gunar Ernis
@modifications: Moritz Fürneisen
"""

import pandas as pd
import numpy as np
from progressbar import ProgressBar
import datetime as dt
import sys
from os import mkdir,remove,path


def line_transformer(line, occurence):
    user, timestamp, mbid_artist, artist, mbid_title, title = line.values
    return 'Name=Play;OccurrenceTime=' + occurence.strftime('%d/%m/%Y-%H:%M:%S') + ';user=' + user + ';timestamp='\
                + timestamp.strftime('%d/%m/%Y-%H:%M:%S') + ';artistID=' + mbid_artist\
                + ';artist=' + artist + ';titleID=' + mbid_title + ';title=' + title + ';\n'
    

    
def main(filename):
    #filename = 'data/lastfm-dataset-1K/userid-timestamp-artid-artname-traid-traname.tsv'
    df = pd.read_csv(filename, 
                     delimiter='\t', 
                     parse_dates=[1],
                     #date_parser=(lambda dates: [pd.datetime.strptime(x,"%Y-%m-%dT%H:%M:%SZ") for x in dates]),
                     infer_datetime_format=True,
                     header=None,
                     names=['user','timestamp', 'mbid_artist', 'artist', 'mbid_title', 'title'])
    df.fillna('-999', inplace=True)
    np.random.seed(42)
    indices = np.random.permutation(len(df))

    start_time = df.timestamp.min()#pd.datetime.now()
    end_time = df.timestamp.max() #start_time + pd.to_timedelta('30 min')
    print(start_time)
    print(end_time)
    
    actual_delta=(end_time - start_time).total_seconds()
    print(actual_delta)
    desired_delta=dt.timedelta(0,2*60*60).total_seconds()
    factor=desired_delta/actual_delta
    print(factor)
    # Specify names of files to create
    sites=["cent1", "site1", "site2", "site3", "site4"]
    n = len(sites)
    n_indices = np.array_split(indices, n)
    pbar_out = ProgressBar()

    
    try:
        mkdir("Topology_input_files")
    except OSError:
        pass
    for i in pbar_out(range(n)):
        n_indices[i].sort()
        n_indices[i]=n_indices[i][::-1]
        pbar_in = ProgressBar()
        file_name='FERARI/Topology_input_files/' + sites[i]
        if path.isfile(file_name):
                remove(file_name)
        with open(file_name + '_FERARI_1M_v2.txt', 'w') as fn:
            for j in pbar_in(n_indices[i]):
                    time_delta=df.timestamp[j]-start_time
                    scaled_delta=time_delta.total_seconds()*factor
                    newtime=pd.datetime.fromtimestamp(0)+dt.timedelta(0,scaled_delta)
                    fn.write(line_transformer(df.ix[j], newtime).replace("'","_"))

                    
if __name__ == "__main__" : 
    main(sys.argv[1]) 
