package com.ibm.ferari.deploy;

import java.io.*;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.coord.EventConsumer;
import com.ibm.ferari.coord.Optimizer;
import com.ibm.ferari.util.Util;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.ClusterSummary;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichSpout;
import eu.ferari.core.commIntegration.AddressResolverRegistry;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.commIntegration.Event;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;
import eu.ferari.examples.mobileFraud.storm.bolts.OptimizerBolt;
import eu.ferari.examples.mobileFraud.storm.spouts.OptiSpout;

import eu.ferari.optimizer.locationGraph.LGraphNode;
import eu.ferari.optimizer.locationGraph.LocationGraph;
import eu.ferari.optimizer.locationGraph.NetworkParser;

import static eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology.*;

public class OptimizerInstance {

	static {
		ProtonMobileFraudTopology.isInDevMode = false;
		OptimizerBolt.shouldStartOptimizer = false;
	}

	private static String inputFileName = "FERARI_1M_v2.txt";
	private static boolean dashboard = true;
	private static boolean fixdelay = false;
	private static int delay = 50;

	private Map<String, ClusterSummary> clusterSummaries = new ConcurrentHashMap<String, ClusterSummary>();

	private Map<String, AtomicInteger> _counters = new ConcurrentHashMap<String, AtomicInteger>();
	private String _htmlTemplate = readTemplates("view.html");
	private String _cssTemplate = readTemplates("view.css");
	private String _inputFileName;
	private String _nodeID;
	private String _path;
	private LocalCluster _cluster;
	private Optimizer _opt;

	static {
		OptiSpout.fetchFromExternalProgram = false;
	}

	public OptimizerInstance(String path, String inputFileName) {
		_path = path;
		_inputFileName = inputFileName;
		_cluster = new LocalCluster();

		if (AddressResolverRegistry.getInstance() == null) {
			AddressResolverRegistry.register(new UDPBasedAddressResolver(CommIntegrationConstants.OPT_SITE_ID));
		}
		FerariClientBuilder.getInstance().setAddressResolver(AddressResolverRegistry.getInstance());

		runOptimizer();
		runCommandServer();

	}

	private void runCommandServer() {
		new CommandServer() {

			@Override
			public String onPost(String requestURI, String requestBody) {
				String networkGraph = extractNotJSON(requestBody), jsonconfig = extractJSON(requestBody);

				if (networkGraph == null || networkGraph.length() < 2) {
					return "NetworkGraph not supplied";
				}

				if (jsonconfig == null || jsonconfig.length() < 2) {
					return "JsonConfig not supplied";
				}

				try {
					return newPlan(networkGraph, jsonconfig);
				} catch (IOException e) {
					e.printStackTrace();
					return e.getMessage();
				}
			}

			@Override
			public String onGet(String requestURI) {
				StringBuilder sb = new StringBuilder();
				StringBuilder counterData = new StringBuilder();
				clusterSummaries.entrySet().stream().forEach(cs -> {
					String topologies = cs.getValue().get_topologies().stream().map(ts -> {
						String color = (ts.get_status().equals("ACTIVE") ? "green" : "red");
						return "{<font color=\"" + color + "\">" + ts.get_name() + ", uptime:" + ts.get_uptime_secs()
								+ " sec</font>}";
					}).collect(Collectors.toList()).toString();
					sb.append("<tr><td style=\"width: 10%\">" + cs.getKey() + "</td><td>" + topologies + "</td></tr>");
				});
				_counters.entrySet().stream().forEach(entry -> {
					counterData.append("<tr><td style=\"width:80%\">").append(entry.getKey())
							.append("</td><td align=\"center\">").append(entry.getValue()).append("</td></tr>");
				});
				if (requestURI.contains(".css")) {
					return _cssTemplate;
				}
				return _htmlTemplate.replace("TOPOlOGIES", sb.toString()).replace("COUNTERS", counterData.toString());
			}

			@Override
			public String onDelete(String requestURI) {
				String context = requestURI.substring(1);
				return _opt.getActiveSites().parallelStream().map((site) -> {
					_opt.killTopology(site, context);
					return site;
				}).collect(Collectors.toSet()).toString();
			}
		};
	}

	private Config getConf() {
		Config stormConfig = new Config();
		stormConfig.setDebug(false);
		stormConfig.put("mode", "local");
		stormConfig.setMaxTaskParallelism(1);
		return stormConfig;
	}

	public String newPlan(String networkGraph, String jsonConf) throws IOException {
		String context = UUID.randomUUID().toString();
		getTopologies(networkGraph).entrySet().stream().parallel().forEach(e -> {
			String site = e.getKey();
			ProtonMobileFraudTopology top = e.getValue();
			top.createTopologyDescriptor("", (topology, config) -> {
				_opt.startTopology(site, config, topology);
				System.err.println("-+-+-+ started topology for " + site + " -+-+-+");
			}, context);
		});

		Util.sleep(5);

		OptiSpout.OPTISPOUT.injectToQueue("j" + jsonConf);
		OptiSpout.OPTISPOUT.injectToQueue("n" + networkGraph);

		return context;

	}

	private void runOptimizer() {
		StormTopology toplogy = initOptimizerTopology();
		Config conf = getConf();
		conf.put("mode", "local");
		_cluster.submitTopology("Optimizer", conf, toplogy);

		int port = Integer.parseInt(InMemoryGlobalAddressResolver.getInstance().getOptimizerHosts()[0].split(":")[1]);
		_opt = new Optimizer(CommIntegrationConstants.OPT_SITE_ID, CommIntegrationConstants.OPT_SITE_ID, port,
				new EventConsumer() {
					@Override
					public void consumeEvent(FerariEvent event) {
						String source = event.getSource();
						clusterSummaries.put(source, (ClusterSummary) event.getEvent());
					}
				});
		_opt.start();
		OptimizerBolt.optimizer = _opt;
		runSocketServer();
	}

	private StormTopology initOptimizerTopology() {
		TopologyBuilder builder = new TopologyBuilder();
		BaseRichSpout optimizerSpout = new OptiSpout(Constants.OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON,
				Constants.OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS, _nodeID);
		builder.setSpout(Constants.OPTIMIZER_SPOUT_NAME, optimizerSpout, 1);
		IRichBolt optimizer = new OptimizerBolt(OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_STREAM_ID,
				OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_NEWJSON_STREAM_ID,
				OPTIMIZER_BOLT_TO_INPUTSPOUT_STREAM_ID,
				OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON,
				OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS,
				_inputFileName, _path);
		BoltDeclarer optimizerBoltDeclarer = builder.setBolt(Constants.OPTIMIZER_BOLT_NAME, optimizer, 1);
		optimizerBoltDeclarer.allGrouping(Constants.OPTIMIZER_SPOUT_NAME,
				Constants.OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON);
		optimizerBoltDeclarer.allGrouping(Constants.OPTIMIZER_SPOUT_NAME,
				Constants.OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS);
		return builder.createTopology();
	}

	private Map<String, ProtonMobileFraudTopology> getTopologies(String networkGraph) throws IOException {
		Map<String, ProtonMobileFraudTopology> topologies = new ConcurrentHashMap<String, ProtonMobileFraudTopology>();
		LocationGraph lg1 = null;
		NetworkParser np = new NetworkParser(null, ";");
		lg1 = np.parseFile(networkGraph);
		lg1.allPairsShortestPath(false);
		lg1.eventLatenciesPerPlacement();
		lg1.calculateHops(2);
		lg1.computeLocalHopedFrequency();
		String centralNode = lg1.centralNode().location;

		ProtonMobileFraudTopology topology;
		for (LGraphNode n : lg1.nodes) {
			topology = new ProtonMobileFraudTopology(null, n.location.equals(centralNode), n.location, dashboard, _path,
					fixdelay, delay, inputFileName);
			topologies.put(n.location, topology);
		}

		return topologies;
	}

	private static String extractJSON(String s) {
		Pattern p = Pattern.compile(".*?(\\{.+\\}).*?");
		Matcher m = p.matcher(s);
		if (m.matches()) {
			return m.group(1);
		}
		return null;
	}

	private static String extractNotJSON(String s) {
		Pattern p = Pattern.compile("(.*?)\\{.+\\}(.*?)");
		Matcher m = p.matcher(s);
		if (m.matches()) {
			return !m.group(1).equals("") ? m.group(1) : m.group(2);
		}
		return null;
	}

	private String readTemplates(String file) {
		StringBuilder sb = new StringBuilder();
		BufferedReader r;
		try (InputStream fis = this.getClass().getClassLoader().getResource(file).openStream()) {
			if(fis==null){
				r=new BufferedReader(new FileReader(file));
			}
			else
				r = new BufferedReader(new InputStreamReader(fis));

			String line;
			while ((line = r.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void runSocketServer() {
		Configuration config = new Configuration();
		config.setHostname(AddressResolverRegistry.getInstance().getOptimizerHosts()[0].split(":")[0]);
		config.setPort(3000);

		final SocketIOServer server = new SocketIOServer(config);

		server.addEventListener("fraudEvents", Event.class, new DataListener<Event>() {

			@Override
			public void onData(SocketIOClient client, Event data, AckRequest ackSender) throws Exception {
				data.setContext(extractContext(data.getEvent()));
				data.setEvent(extractEvent(data.getEvent()));

				_counters.putIfAbsent(data.getContext(), new AtomicInteger());
				_counters.get(data.getContext()).incrementAndGet();
			}
		});

		server.start();
		System.out.println("Started Socket-IO server");

	}

	private static String extractContext(String s) {
		Pattern p = Pattern.compile(".*\"context\":\"(.+?)\".*");
		Matcher m = p.matcher(s);
		m.matches();
		return m.group(1);
	}

	private static String extractEvent(String s) {
		Pattern p = Pattern.compile(".+\"event\":(.+),\\s+\"context\":.+");
		Matcher m = p.matcher(s);
		m.matches();
		return m.group(1);
	}

	public static void main(String[] args) {
		AddressResolver resolver = new UDPBasedAddressResolver(CommIntegrationConstants.OPT_SITE_ID);
		AddressResolverRegistry.register(resolver);
		FerariClientBuilder.getInstance().setAddressResolver(resolver);
		ProtonMobileFraudTopology.isInDevMode = false;
		new OptimizerInstance(args[0], "FERARI_1M_v2.txt");
	}

}
