package com.ibm.ferari.deploy;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings("restriction")
public abstract class CommandServer implements com.sun.net.httpserver.HttpHandler {
	
	private static final int PORT = 8080;

	public CommandServer() {
		try {
			runServer();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void runServer() throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(PORT), 0);
        server.createContext("/", this);
        server.setExecutor(null); 
        server.start();
	}

	@Override
	public void handle(HttpExchange http) {
		try {
			String request = readReq(http);
			String response = "Not implemented";
			if (http.getRequestMethod().equals("POST")) {
				response = onPost(http.getRequestURI().toString(), request);
			}
			
			if (http.getRequestMethod().equals("GET")) {
				response = onGet(http.getRequestURI().toString());
			}
			
			if (http.getRequestMethod().equals("DELETE")) {
				response = onDelete(http.getRequestURI().toString());
			}
			
			sendResponse(response, http);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			try {
				sendResponse(e.getMessage(), http);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}
	
	public String readReq(HttpExchange http) throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(http.getRequestBody()));
		String line;
		StringBuilder sb = new StringBuilder();
		while (( line = r.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}
	
	public void sendResponse(String response, HttpExchange http) throws IOException {
		http.sendResponseHeaders(200, response.length());
		http.getResponseBody().write(response.getBytes());
		http.getResponseBody().flush();
		http.getResponseBody().close();
	}
	
	public abstract String onPost(String requestURI, String requestBody);
	
	public abstract String onDelete(String requestURI);
	
	public abstract String onGet(String requestURI);
	
	public static void main(String[] args) {
		new CommandServer() {
			
			@Override
			public String onPost(String requestURI, String requestBody) {
				return requestURI + "\n" + requestBody;
			}
			
			@Override
			public String onGet(String requestURI) {
				return requestURI;
			}
			
			@Override
			public String onDelete(String requestURI) {
				return requestURI;
			}
		};
	}
	

}
