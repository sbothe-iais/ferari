package com.ibm.ferari.deploy;

import java.util.Arrays;

import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.util.Util;

import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.examples.mobileFraud.ExternalProgram_demo;
import eu.ferari.core.commIntegration.AddressResolverRegistry;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;
import eu.ferari.examples.mobileFraud.storm.spouts.OptiSpout;

public class Demo {

	static {
		AddressResolverRegistry.register(InMemoryGlobalAddressResolver.getInstance());
		FerariClientBuilder.getInstance().setAddressResolver(AddressResolverRegistry.getInstance());
		ProtonMobileFraudTopology.isInDevMode = false;
	}
	static String confPath="/home/yacovm/FERARI_DEMO/";

	public static void main(String[] args) {
//		System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "off");
		if(args.length>0)
			confPath=args[0];
		Util.runAsyncTask(() -> {
			ExternalProgram_demo.main(new String[]{confPath+"conf.txt"});
		});
		new OptimizerInstance(confPath, "FERARI_1M_v2.txt");
		
		OptiSpout.fetchFromExternalProgram = true;
		
		Arrays.stream(new String[]{"site1", "site2", "site3", "site4", "cent1"}).parallel().forEach(site -> {
			System.out.println("Starting " + site);
			try {
				new Site(site, confPath, "FERARI_1M_v2.txt");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
}
