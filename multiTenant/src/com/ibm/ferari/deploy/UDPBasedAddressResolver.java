package com.ibm.ferari.deploy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.util.Util;
import eu.ferari.core.commIntegration.CommIntegrationConstants;


public class UDPBasedAddressResolver implements AddressResolver {

	private final static int PORT = 2000;
	private final static int SLEEPTIME = 200;
	private final static int MAX_SLEEPTIME = 5000;
	
	private final String site;
	private final String endpoint;
	private final DatagramSocket incSocket;
	private final DatagramSocket outSocket;
	
	private final Map<String,String> site2Endpoint = new ConcurrentHashMap<String,String>();
	

	@Override
	public String[] getHostsOfSite(String site) {
		return new String[]{site2Endpoint.get(site)};
	}

	@Override
	public String[] getOptimizerHosts() {
		return new String[]{site2Endpoint.get(CommIntegrationConstants.OPT_SITE_ID)};
	}

	@Override
	public String getOptimizerSite() {
		return CommIntegrationConstants.OPT_SITE_ID;
	}

	@Override
	public String getDashboardWSURI() {
		String optimizerIP = getHostsOfSite(getOptimizerSite())[0];
		return "http://" + optimizerIP.split(":")[0] + ":3000";
	}
	
	public UDPBasedAddressResolver(String site) {
		this.site = site;
		endpoint = getLocalIPAddress() + ":" + 5611;
		try {
			incSocket = new DatagramSocket(PORT);
			outSocket = new DatagramSocket();
		} catch (SocketException e) {
			throw new RuntimeException(e);
		}
		listen();
		discover();
		sleep(MAX_SLEEPTIME);
		System.out.println("View:");
		site2Endpoint.entrySet().stream().forEach(e -> {
			System.out.println(e.getKey() + ":" + e.getValue());
		});
	}

	private void discover() {
		String[] ip = endpoint.split("\\.");
		String prefix = ip[0] + "." + ip[1] + "." + ip[2] + ".";
		HelloMessage hello = new HelloMessage();
		hello.setEndpoint(endpoint);
		hello.setSite(site);
		byte[] data = hello.asByteArray();
		Util.runAsyncTask(() -> {
			int sleepTime = SLEEPTIME;
			while (true) {
				for (int i=1; i<20; i++) {
					sendHello(prefix + i, data);
				}
				sleepTime = Math.min(sleepTime, MAX_SLEEPTIME);
				sleep(sleepTime);
				sleepTime *= 2;
			} // while
		});
		
	}
	
	private static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void sendHello(String dest, byte[] data) {
		ByteBuffer bb = ByteBuffer.allocate(data.length + 4);
		bb.putInt(data.length);
		bb.put(data);
		try {
			DatagramPacket p = new DatagramPacket(bb.array(), bb.array().length, InetAddress.getByName(dest), PORT);
			outSocket.send(p);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}

	private static String getLocalIPAddress() {
		try {
			try (DatagramSocket s = new DatagramSocket()) {
				s.connect(InetAddress.getByAddress(new byte[] { 8, 8, 8, 8 }), 0);
				return NetworkInterface.getByInetAddress(s.getLocalAddress()).getNetworkInterfaces().nextElement()
						.getInetAddresses().nextElement().getHostAddress();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void listen() {
		Util.runAsyncTask(() -> {
			while (true) {
				byte[] buff = new byte[1024];
				DatagramPacket p = new DatagramPacket(buff, buff.length);
				try {
					incSocket.receive(p);
					buff = p.getData();
					handleMessage(HelloMessage.fromByteArray(buff));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	private void handleMessage(HelloMessage msg) {
		site2Endpoint.put(msg.site, msg.endpoint);
	}

	private static class HelloMessage implements Serializable {

		private static final long serialVersionUID = 5661686264612705486L;
		
		public String getEndpoint() {
			return endpoint;
		}

		public void setEndpoint(String endpoint) {
			this.endpoint = endpoint;
		}

		public String getSite() {
			return site;
		}

		public void setSite(String site) {
			this.site = site;
		}

		private String endpoint;
		private String site;
		
		public static HelloMessage fromByteArray(byte[] data) {
			try {
				ByteBuffer bb = ByteBuffer.wrap(data);
				int dataSize =bb.getInt();
				data = new byte[dataSize];
				bb.get(data);
				ByteArrayInputStream bais = new ByteArrayInputStream(data);
				ObjectInputStream ois = new ObjectInputStream(bais);
				HelloMessage msg = (HelloMessage) ois.readObject();
				ois.close();
				bais.close();
				return msg;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		public byte[] asByteArray() {
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(this);
				oos.flush();
				oos.close();
				return baos.toByteArray();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: " + UDPBasedAddressResolver.class.getCanonicalName() + " site_name");
			return;
		}
		new UDPBasedAddressResolver(args[0]);
	}

}
