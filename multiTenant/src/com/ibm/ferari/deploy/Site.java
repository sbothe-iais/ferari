package com.ibm.ferari.deploy;

import java.io.IOException;
import java.util.UUID;

import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.coord.Coordinator;
import com.ibm.ferari.coord.EventConsumer;
import com.ibm.ferari.storm.Sentinel;
import com.ibm.ferari.storm.Submitter;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichSpout;
import eu.ferari.core.commIntegration.AddressResolverRegistry;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.examples.mobileFraud.proton.FileReaderSpout;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;

public class Site {
	
	static {
		ProtonMobileFraudTopology.isInDevMode = false;
	}
	
	private LocalCluster _cluster = new LocalCluster();
	private String siteId;
	
	private String _path;
	private String _inputFile;
	private int _delay = 500;

	public Site(String nodeID, String path, String inputFile) throws IOException {
		_path = path;
		_inputFile = inputFile;
		siteId = nodeID;
		if (AddressResolverRegistry.getInstance() == null) {
			AddressResolverRegistry.register(new UDPBasedAddressResolver(siteId));
		}
		FerariClientBuilder.getInstance().setAddressResolver(AddressResolverRegistry.getInstance());
		createCoordinator(nodeID).start();
		new Sentinel(new Submitter() {
			
			@Override
			public String submitTopology(Config conf, StormTopology topology) throws Exception {
				System.err.println("Sentinel for " + nodeID + " got topology start request");
				String topologyId = UUID.randomUUID().toString();
				String context = (String) conf.get(CommIntegrationConstants.EVENT_CONTEXT);
				_cluster.submitTopology(context, conf, topology);
				return topologyId;
			}
			
			@Override
			public void killTopology(String topologyName) {
				_cluster.killTopology(topologyName);
			}
		}, nodeID, InMemoryGlobalAddressResolver.getInstance());
		createFileReaderSpout();
		FerariClientBuilder.getInstance().setLocalCluster(_cluster);
		FerariClientBuilder.getInstance().setMonitoringFilter( o -> {return o;});
		FerariClientBuilder.getInstance().createMonitoringIntersiteClient(siteId);
		System.out.println("Started " + nodeID);
	}

	private void createFileReaderSpout() {
		TopologyBuilder builder = new TopologyBuilder();
		BaseRichSpout fileReaderSpout = new FileReaderSpout(null, siteId, 
				_inputFile, _path, true, _delay, "frs_to_routing_primitiveEvents", true);
		builder.setSpout("fileReaderSpout-" + siteId, fileReaderSpout);
		Config conf = new Config();
		conf.put("mode", "local");
		_cluster.submitTopology("FileReaderSpout", conf, builder.createTopology());
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: Site siteID folder");
			return;
		}
		AddressResolver resolver = new UDPBasedAddressResolver(args[0]);
		AddressResolverRegistry.register(resolver);
		FerariClientBuilder.getInstance().setAddressResolver(resolver);
		try {
			new Site(args[0], args[1], "FERARI_1M_v2.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Coordinator createCoordinator(String nodeId) {
		String selfPort = InMemoryGlobalAddressResolver.getInstance().getHostsOfSite(nodeId)[0].split(":")[1];
		final IntraSiteClient relayer = FerariClientBuilder.getInstance().createIntraSiteClient(nodeId);
		Coordinator coord = new Coordinator(nodeId, nodeId, Integer.parseInt(selfPort), new EventConsumer() {
			
			public void consumeEvent(FerariEvent event) {
				relayer.broadcastInOurSite(event);
			}
		});
		return coord;
	}

}
