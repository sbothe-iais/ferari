package com.ibm.ferari.deploy;

public interface Constants {

	 static final String COMMUNICATOR_SPOUT_NAME = "communicator_spout";
	 static final String OPTIMIZER_SPOUT_NAME = "optimizer_spout";

	 static final String TIME_MACHINE_BOLT_NAME = "timemachine";
	 static final String GATEKEEPER_BOLT_NAME = "gatekeeper";
	 static final String COMMUNICATOR_BOLT_NAME = "communicator";
	 static final String OPTIMIZER_BOLT_NAME = "optimizer";

	 static final String DEFAULT_STREAM_ID = "default_stream";
	 static final String TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID = "timemachine_to_communicator";
	 static final String COMMUNICATOR_TO_GATEKEEPER_STREAM_ID = "communicator_to_gatekeeper";
	 static final String COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID = "communicator_to_timemachine";
	 static final String COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID = "coordinator_to_comm_thresh_pass";
	 static final String COORDINATOR_TO_COMMUNICATOR_SENDLSV_STREAM_ID = "coordinator_to_timemachine_sendLSV_request";	
	 static final String TIMEMACHINE_TO_COORDINATOR_LSV_STREAM_ID = "timemachine_to_coordinator_lsv";
	 static final String COMMUNICATOR_TO_GATEKEEPER_ESTIMATE_STREAM_ID = "communicator_to_gatekeeper_Estimate";
	 static final String COORDINATOR_TO_COMMUNICATOR_ESTIMATE_STREAM_ID ="coordinator_to_comm_Estimate";
	 static final String COMMUNICATOR_TO_COORDINATOR_LSV_VIOL_STREAM_ID = "com_to_coord_CounterUpd";
	 static final String COMMUNICATORSPOUT_TO_COORDINATOR_COUNTER_UPDATE_STREAM_ID = "comSpout_to_coord_CounterUpd";
	 static final String COMMUNICATOR_TO_COORDINATOR_LSV_STREAM_ID ="ComSpout_to_coord_LSV";
	 static final String PUBSUBSPOUT_TO_TIMEMACHINE_PULL_STREAM_ID  ="ComSpout_to_timemachine_pull";
	 static final String INPUTSPOUT_TO_ROUTING_PUSH_STREAM_ID  ="InputSpout_to_routing_push";
	 static final String INPUTSPOUT_TO_CSVFORMATTER_JSON_STREAM_ID  ="InputSpout_to_csvformatter_json";
	 static final String TIMEMACHINE_TO_COMMUNICATOR_LSV_STREAM_ID = "timemachine_to_communicator_lsv";
	 static final String TIMEMACHINE_TO_COMMUNICATOR_PUSH_STREAM_ID = "timemachine_to_communicator_push";
	 static final String GATEKEEPER_TO_COMMUNICATOR_LSV_STREAM_ID = "gatekeeper_to_communicator_lsv";
	 static final String COMMUNICATOR_TO_TIMEMACHINE_PLAY_STREAM_ID = "communicator_to_timemachine_play";
	 static final String COMMUNICATOR_TO_TIMEMACHINE_PULL_STREAM_ID = "communicator_to_timemachine_pull";
	 static final String COMMUNICATORSPOUT_TO_PROTON_OUTPUT_FACADE_BOLT_PROPERTIES_STREAM_ID = "Comspout_to_POFB_properties";
	 static final String COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID = "Comspout_to_routing_properties";
	 static final String COMMUNICATORSPOUT_TO_TIMEMACHINE_PROPERTIES_STREAM_ID = "Comspout_to_timeMachine_properties";
	 static final String COMMUNICATORSPOUT_TO_GATEKEEPER_PROPERTIES_STREAM_ID = "Comspout_to_gatekeeper_properties";
	 static final String COMMUNICATORSPOUT_TO_COMMUNICATOR_PROPERTIES_STREAM_ID = "Comspout_to_communicator_properties";
	 static final String COMMUNICATORSPOUT_TO_COMMUNICATOR_NEW_JSON_STREAM_ID = "Comspout_to_communicator_jsonfile";
	 static final String OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON = "Optimizerspout_to_optimizer_json";
	 static final String OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS = "Optimizerspout_to_optimizer_parameters";
	 static final String OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_STREAM_ID = "optimizer_to_comspout_conffiles";
	 static final String OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_NEWJSON_STREAM_ID = "optimizer_to_comspout_newjson";
	 static final String OPTIMIZER_BOLT_TO_INPUTSPOUT_STREAM_ID = "optimizer_to_inputspout_conffiles";
	 static final String FILEREADERSPOUT_TO_ROUTING_BOLT_PRIM_EVENTS_STREAM_ID = "frs_to_routing_primitiveEvents";
	
}
