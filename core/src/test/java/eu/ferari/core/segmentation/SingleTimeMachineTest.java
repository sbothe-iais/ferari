package eu.ferari.core.segmentation;

import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.misc.Event;
import eu.ferari.core.misc.InputMap;

import static org.mockito.Mockito.*;

import eu.ferari.core.utils.RealValuedVector;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by mofu on 14/09/16.
 */
public class SingleTimeMachineTest {
	SingleTimeMachine aggregator;
	IComEffState mockState;
	InputMap mockMap;

	@Before
	public void setUp(){
		mockState = mock(IComEffState.class);
		when(mockState.getLocalVariable()).thenReturn(new RealValuedVector(20));
		aggregator = new SingleTimeMachine(mockState,20);
		mockMap=mock(InputMap.class);
		when(mockMap.transformInput(any(Event.class))).thenReturn(new Double[]{2.0,3.0,4.0,5.0});

	}

	@Test
	public void initiallyNoException(){
		aggregator.update(mock(Event.class),mockMap);

	}

	@Test
	public void exceptionAfterPause(){
		RealValuedVector vec= new RealValuedVector();
		vec.setOccurrenceTime(20);
		aggregator.pause(vec);
		try{
			aggregator.update(mock(Event.class),mockMap);
		}catch (SingleTimeMachine.StatePausedException e){

		}

	}

	@Test
	public void updateAfterPlay(){
		RealValuedVector vec= new RealValuedVector();
		vec.setOccurrenceTime(17);
	}
	/*TODO  is there a need to test the output of play and pause()
	  		they are simple calls to the tested LimitedSizeOrderedSet.
	  		*/
}
