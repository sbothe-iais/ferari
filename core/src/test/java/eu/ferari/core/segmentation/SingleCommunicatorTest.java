package eu.ferari.core.segmentation;

import eu.ferari.core.utils.RealValuedVector;

import static eu.ferari.core.segmentation.SingleCommunicator.State.*;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;


/**
 * Created by mofu on 14/09/16.
 */
public class SingleCommunicatorTest {

	SingleCommunicator state;
	RealValuedVector vec;

	@Before
	public void setUp() {
		state = new SingleCommunicator();
		vec = new RealValuedVector(0);
	}

	private void setVecEarlier(){
			state.setOccurrenceTime(Long.MAX_VALUE);
			vec.setOccurrenceTime(0);
		}
	private void setVecLater(){
			state.setOccurrenceTime(0);
			vec.setOccurrenceTime(Long.MAX_VALUE);
	}

	private void assertEarlierForViolatationPausingLocal(boolean ret){
		assertThat(ret, is(equalTo(true)));
		assertThat(state.getState(), is(SingleCommunicator.State.PausingLocalThresholdCrossing));
	}

	@Test
	public void violationSetsTimestampAndPausesWhenMonitoring() {
		state.setState(SingleCommunicator.State.Monitoring);
		boolean ret = state.onViolation(new RealValuedVector(2));
		assertThat(ret, is(equalTo(true)));
		assertThat(state.getState(), is(SingleCommunicator.State.PausingLocalThresholdCrossing));
	}

	@Test
	public void earlierThresholdCrossingSetsTimestampOnLocalPause() {

		state.setState(SingleCommunicator.State.PausingLocalThresholdCrossing);
		setVecEarlier();
		boolean ret = state.onViolation(vec);
		assertEarlierForViolatationPausingLocal(ret);
	}

	@Test
	public void earlierThresholdCrossingSetsTimestampOnCounterRequest() {
		state.setState(SingleCommunicator.State.PausingCounterRequestMessage);
		setVecEarlier();
		boolean ret = state.onViolation(vec);
		assertEarlierForViolatationPausingLocal(ret);
	}

	@Test
	public void earlierThresholdCrossingSetsTimestampOnWaitingCoord() {
		state.setState(SingleCommunicator.State.WaitingForCoordinator);
		setVecEarlier();
		boolean ret = state.onViolation(vec);
		assertEarlierForViolatationPausingLocal(ret);
	}

	@Test
	public void earlierThresholdCrossingSetsTimestampOnWaitingGK() {
		state.setState(SingleCommunicator.State.WaitingForGatekeeper);
		setVecEarlier();
		boolean ret = state.onViolation(vec);
		assertEarlierForViolatationPausingLocal(ret);
	}


	private void assertLaterViolation(SingleCommunicator.State oldState){
		assertThat(state.getState(),equalTo(oldState));
	}

	@Test
	public void laterThresholdCrossingIsIgnoredOnLocalCrossing() {
		setVecLater();
		SingleCommunicator.State testState= PausingLocalThresholdCrossing;
		state.setState(testState);
		state.onViolation(vec);
		assertLaterViolation(testState);
	}

	@Test
	public void laterThresholdCrossingIsIgnoredOnCounterRequest() {
		setVecLater();
		SingleCommunicator.State testState= PausingCounterRequestMessage;
		state.setState(testState);
		state.onViolation(vec);
		assertLaterViolation(testState);
	}

	@Test
	public void laterThresholdCrossingIsIgnoredOnWaitingForCoordinator() {
		setVecLater();
		SingleCommunicator.State testState= WaitingForCoordinator;
		state.setState(testState);
		state.onViolation(vec);
		assertLaterViolation(testState);
	}


}
