package eu.ferari.core.misc;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.SortedSet;

import eu.ferari.core.misc.LimitedSizeOrderedSet;
import org.junit.Before;
import org.junit.Test;

public class LimitedSizeOrderedSetTest {
	private final int MAX_SIZE = 3;
	private LimitedSizeOrderedSet<Integer> set;
	
	@Before
	public void setUp() throws Exception {
		set = new LimitedSizeOrderedSet<Integer>(MAX_SIZE);
		set.add(2);
		set.add(4);
		set.add(6);
	}

	@Test
	public void testAddElementAtFullCapacity_PopsFirstElement() {
		set.add(7);
		assertThat(set.size(), is(equalTo(MAX_SIZE)));
		assertThat(set.first(), is(equalTo(4)));		
	}
	
	@Test
	public void testRewindToEqualOrLesserGetsCorrectSubset_WhenElementExists() {
		SortedSet<Integer> rewindResult = set.rewindToEqualOrLarger(4);
		assertThat(rewindResult.size(), is(equalTo(2)));
		assertTrue(rewindResult.contains(4));
		assertTrue(rewindResult.contains(6));
	}
	
	@Test
	public void testRewindToEqualOrLesserGetsCorrectSubset_WhenElementDoesNotExist_AndLessThanExistingMaximum() {
		SortedSet<Integer> rewindResult = set.rewindToEqualOrLarger(5);
		assertThat(rewindResult.size(), is(equalTo(2)));
		assertTrue(rewindResult.contains(4));
		assertTrue(rewindResult.contains(6));		
	}
	
	@Test
	public void testRewindToEqualOrLesserGetsCorrectSubset_WhenElementDoesNotExist() {
		SortedSet<Integer> rewindResult = set.rewindToEqualOrLarger(7);
		assertThat(rewindResult.size(), is(equalTo(0)));
	}
	
	@Test
	public void testGet(){
		SortedSet<Integer> rewindResult = set.rewindToEqualOrLarger(1);
		assertThat(rewindResult.size(), is(equalTo(3)));
	}
	
	@Test
	public void testGetGetsCorrectElement(){
		int result = set.get(4);
		assertThat(result, is(equalTo(4)));
	}
	
	@Test
	public void testGetGetsNull_IfDoesNotExist(){
		Integer result = set.get(7);
		assertNull(result);
	}
}
