package eu.ferari.core.misc;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import eu.ferari.core.monitoring.ConfigValues;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mofu on 04/11/16.
 */
public class InputMapTest {
	private InputMap inputMap;
	@Before
	public void setUp(){
		JsonArray elementConf=new JsonArray();
		JsonObject conf=new JsonObject();
		JsonArray fieldNames=new JsonArray();
		conf.addProperty(ConfigValues.EVENT_NAME_PARAMETER_NAME,"event1");
		fieldNames.add("key1");
		fieldNames.add("");
		conf.add(ConfigValues.EVENT_FIELDS_PARAMETER_NAME, fieldNames);
		elementConf.add(conf);

		conf= new JsonObject();
		fieldNames=new JsonArray();
		conf.addProperty(ConfigValues.EVENT_NAME_PARAMETER_NAME,"event2");
		fieldNames.add("key3");
		conf.add(ConfigValues.EVENT_FIELDS_PARAMETER_NAME,fieldNames);
		elementConf.add(conf);
		inputMap=new InputMap(elementConf);
	}
	@Test
	public void event1Test(){
		Map<String,Object> attributes= new HashMap<>();
		attributes.put("key1",5.0);
		Event event=new Event("event1",0L,attributes);
		Double[] result= inputMap.transformInput(event);
		assertThat(result,equalTo(new Double[]{5.0,1.0}));

	}
	@Test
	public void event2Test(){
		Map<String,Object> attributes= new HashMap<>();
		attributes.put("key3",3.0);
		Event event=new Event("event2",0L,attributes);
		Double[] result= inputMap.transformInput(event);
		assertThat(result,equalTo(new Double[]{3.0}));

	}
}
