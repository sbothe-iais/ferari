package eu.ferari.core.comEfficient.interfaces;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by mofu on 10/12/15.
 */
public class RealValuedVectorTest {
    HashMap<RealValuedVector, Double> map;
    double value = 2.3;
   
    @Before
    public void setUp(){
        map = new HashMap<>(20);
    }
    
    // we are canceling this test, please see documentation in RealValuedVector for the reason
    @Test
    public void hashingSameVector(){
        RealValuedVector a = new RealValuedVector(new Double[]{2.0,3.0});
        RealValuedVector b = new RealValuedVector(new Double[]{2.0,3.0});
        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
        map.put(a,value);
        assertThat(map.get(b), is(equalTo(value)));
    }
    @Test
    public void hashingDifferentVectors(){
        RealValuedVector a = new RealValuedVector(new Double[]{4.0,7.0});
        RealValuedVector b = new RealValuedVector(new Double[]{7.0,4.0});
        map.put(a,value);
        assertThat(map.get(b), not(equalTo(value)));

    }
    
    @Test public void testAddVectors() {
    	RealValuedVector a = new RealValuedVector(new Double[]{1.0,1.2, 5.34, 0.0});
    	RealValuedVector b = new RealValuedVector(new Double[]{0.1,3.4, 1.23, 0.2});
    	b.add(a);
    	RealValuedVector expected = new RealValuedVector(new Double[]{1.1,4.6, 6.57, 0.2});
    	for(int i = 0 ; i < expected.getDimension(); i++) {
    		assertTrue(Math.abs(b.get(i) - expected.get(i)) < 0.0001);
    	}
     }
    
    @Test public void testScalarMult() {
    	RealValuedVector a = new RealValuedVector(new Double[]{0.5, 1.0,1.2, 3.34, 0.0});
    	double alpha = 0.5;
    	a.scalarMultiply(alpha);
    	RealValuedVector expected = new RealValuedVector(new Double[]{0.25,0.5, 0.6, 1.67, 0.0});
    	for(int i = 0 ; i < expected.getDimension(); i++) {
    		assertTrue(Math.abs(a.get(i) - expected.get(i)) < 0.0001);
    	}
     }
    
    @Test public void testAverage() {
    	RealValuedVector a = new RealValuedVector(new Double[]{0.5, 1.0, 1.2, 3.34, 0.0});
    	RealValuedVector b = new RealValuedVector(new Double[]{0.5, 3.0, 1.2, 3.0, 1.1});
    	Collection<IVector<Integer, Double>> vectorsList = new ArrayList<IVector<Integer, Double>>();
    	vectorsList.add(a);
    	vectorsList.add(b);
    	IVector<Integer, Double> result = a.average(vectorsList);
    	RealValuedVector expected = new RealValuedVector(new Double[]{0.5,2.0, 1.2, 3.17, 0.55});
    	for(int i = 0 ; i < expected.getDimension(); i++) {
    		assertTrue(Math.abs(result.get(i) - expected.get(i)) < 0.0001);
    	}
     }
    
    
    @Test public void testDistance() {
    	RealValuedVector a = new RealValuedVector(new Double[]{0.5, 1.0, 1.2, 3.34, 0.0});
    	double[] b = new double[]{0.5, 3.0, 1.2, 3.0, 1.1};
    	
    	double result = a.distanceSquare(b);
    	double expected = 5.3256;
    	assertTrue(Math.abs(result - expected) < 0.000001);
    	
     }
    
    @Test public void testInnerProduct() {
    	RealValuedVector a = new RealValuedVector(new Double[]{0.5, 1.0, 1.2, 3.34, 0.0});
    	double[] b = new double[]{0.5, 3.0, 1.2, 3.0, 1.1};
    	
    	double result = a.innerProduct(b);
    	double expected = 14.71;
    	assertTrue(Math.abs(result - expected) < 0.000001);
    	
     }
}