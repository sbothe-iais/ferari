package eu.ferari.core.state;

import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.IConfigurableState;
import eu.ferari.core.misc.Event;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.utils.MessageType;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
public class MessageReplicatorStateTest {

	@Test(expected=RuntimeException.class)
	public void testWhenUnknownFunctionOccurs_ThrowsException() {
		ISend sender = Mockito.mock(ISend.class);
		Map<String, Map<String, List<String>>> eventToInterestedFunctions = Maps.newHashMap();
		IConfigurableState eventPartitionReplicator = new MessageReplicatorState(eventToInterestedFunctions, sender);
		DataTuple input = new DataTuple(1);
		
		eventPartitionReplicator.update(input);
	}
	
	@Test
	public void testGeneratesExpectedTuples(){
		ISend sender = mock(ISend.class);
		Map<String, Map<String, List<String>>> eventToInterestedFunctions = ImmutableMap.of(
				"event",
				ImmutableMap.of(
					"function1", ImmutableList.of("key1", "key2"),
					"function2", ImmutableList.of("key3", "key1")
				)
		);				
		IState eventPartitionReplicator = new MessageReplicatorState(eventToInterestedFunctions, sender);
		
		Event event = new Event("event", 0, ImmutableMap.of(
				"key1", 1,
				"key2", "value2",
				"key3", 3
			));		
		DataTuple input = new DataTuple(event);
		
		String context1 = "function1;1,value2";
		String context2 = "function2;3,1";
		DataTuple output1 = new DataTuple(event,MessageType.Event,context1);
		DataTuple output2 = new DataTuple(event,MessageType.Event,context2);
		
		eventPartitionReplicator.update(input);
		
		verify(sender, times(1)).signal(output1);
		verify(sender, times(1)).signal(output2);
	}

	@Test
	public void testGeneratesExpectedTuplesFromJson(){
		JsonObject fun1=new JsonObject();
		fun1.addProperty(ConfigValues.FUNCTION_NAME_PARAMETER_NAME,"function1");
		JsonObject eventObject=new JsonObject();
		JsonArray eventArray=new JsonArray();
		eventObject.addProperty(ConfigValues.EVENT_NAME_PARAMETER_NAME,"event");
		eventArray.add(eventObject);
		fun1.add(ConfigValues.RELEVANT_EVENTS_PARAMETER_NAME,eventArray);
		JsonArray segmentationInfo1=new JsonArray();
		segmentationInfo1.add("key1");
		segmentationInfo1.add("key2");
		fun1.add(ConfigValues.SEGMENTATION_INFO_PARAMETER_NAME,segmentationInfo1);
		JsonObject fun2=new JsonObject();
		fun2.addProperty(ConfigValues.FUNCTION_NAME_PARAMETER_NAME,"function2");
		fun2.add(ConfigValues.RELEVANT_EVENTS_PARAMETER_NAME,eventArray);
		JsonArray segmentationInfo2=new JsonArray();
		segmentationInfo2.add("key3");
		segmentationInfo2.add("key1");
		fun2.add(ConfigValues.SEGMENTATION_INFO_PARAMETER_NAME,segmentationInfo2);
		JsonArray confArray=new JsonArray();
		JsonObject jsonObject=new JsonObject();
		jsonObject.add( "config",fun1);
		confArray.add(jsonObject);
		jsonObject=new JsonObject();
		jsonObject.add( "config",fun2);
		confArray.add(jsonObject);

		IConfigurableState eventReplicator= new MessageReplicatorState();
		ISend sender = mock(ISend.class);
		eventReplicator.setSender(sender);
		eventReplicator.updateConfig((JsonElement)confArray);

		Event event = new Event("event", 0, ImmutableMap.of(
				"key1", 1,
				"key2", "value2",
				"key3", 3
		));
		DataTuple input = new DataTuple(event);

		String context1 = "function1;1,value2";
		String context2 = "function2;3,1";
		DataTuple output1 = new DataTuple(event,MessageType.Event,context1);
		DataTuple output2 = new DataTuple(event,MessageType.Event,context2);

		eventReplicator.update(input);

		verify(sender, times(1)).signal(output2);
		verify(sender, times(1)).signal(output1);
	}

}
