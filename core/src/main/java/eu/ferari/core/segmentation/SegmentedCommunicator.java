package eu.ferari.core.segmentation;

import java.util.HashMap;

/**
 * Created by mofu on 04/09/16.
 */
public class SegmentedCommunicator {
	HashMap<String,SingleCommunicator> states=new HashMap<>();


	public SingleCommunicator getState(String segmentationInfo){
		SingleCommunicator st = states.get(segmentationInfo);
		if(st==null){
			st= new SingleCommunicator(SingleCommunicator.State.Monitoring);
			states.put(segmentationInfo,st);
		}
		return st;
	}
}
