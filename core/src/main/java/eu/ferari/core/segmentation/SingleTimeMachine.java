package eu.ferari.core.segmentation;


import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.misc.Event;
import eu.ferari.core.misc.InputMap;
import eu.ferari.core.misc.LimitedSizeOrderedSet;
import eu.ferari.core.misc.TimeComparator;
import eu.ferari.core.utils.RealValuedVector;

import java.util.Arrays;
import java.util.SortedSet;

/**
 * Created by mofu on 31/08/16.
 */


public class SingleTimeMachine {
	public enum PlayState{Play, Pause}
	public class StatePausedException extends RuntimeException{
		public StatePausedException(){
			super("The function segment is paused.");
		}
	}
	//TODO play inBuffer instead of outBuffer for learning;
	private LimitedSizeOrderedSet<Event> inBuffer;
	private LimitedSizeOrderedSet<IVector> outBuffer;
	private IComEffState aggregator;
	private PlayState state=PlayState.Play;

	public SingleTimeMachine(IComEffState aggregator, int buffersize){
		this.aggregator=aggregator;
		inBuffer=new LimitedSizeOrderedSet<>(buffersize, TimeComparator.getInstance());
		outBuffer=new LimitedSizeOrderedSet<>(buffersize, TimeComparator.getInstance());
	}

	public IVector update(Event event, InputMap inputMap){
		inBuffer.add(event);
		aggregator.processInput(new DataTuple(new Double[][]{inputMap.transformInput(event)}));
		IVector vec=aggregator.getLocalVariable();
		vec.setOccurrenceTime(event.getOccurrenceTime());
		outBuffer.add(vec);
		if(state == PlayState.Pause){
			throw new StatePausedException();
		}
		return vec;
	}
	
	public void updateArtistIdName(Event event){
		aggregator.setArtistIdName(String.valueOf(event.getAttributes().get("artistID")), String.valueOf(event.getAttributes().get("artist")));
	}

	public void updateEstimate(IVector vec){
		aggregator.setLocalVariable(vec);
	}
	public SortedSet<IVector> play(IVector vec){
		state=PlayState.Play;
		return outBuffer.rewindToLarger(vec);
	}


	public SortedSet<IVector>pause(IVector vec){
		state=PlayState.Pause;
		return outBuffer.rewindToEqualOrLarger(vec);
	}

	IVector getLsv(){
		return aggregator.getLocalVariable();
	}
}
