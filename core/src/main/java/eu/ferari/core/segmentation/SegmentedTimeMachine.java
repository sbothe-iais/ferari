package eu.ferari.core.segmentation;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.misc.Event;
import eu.ferari.core.misc.InputMap;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.monitoring.states.StateFactory;
import eu.ferari.core.utils.RealValuedVector;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mofu on 02/09/16.
 */
public class SegmentedTimeMachine {

	private InputMap inputMap;
	private JsonObject config;
	private Map<String,SingleTimeMachine> states;
	IVector defaultLsv;

	public SegmentedTimeMachine(JsonObject config){
		this.config=config;
		inputMap=new InputMap(config.getAsJsonObject("config").get(ConfigValues.RELEVANT_EVENTS_PARAMETER_NAME));
		states = new HashMap<>();
	}


	public SingleTimeMachine getBA(String segmentationInfo) throws NotInitializedException{
		SingleTimeMachine ret = states.get(segmentationInfo);
		if(ret ==null){
			throw new NotInitializedException();
		}
		return ret;
	}

	public SingleTimeMachine createNewSegment(String segmentationInfo){
		SingleTimeMachine ret = new SingleTimeMachine(StateFactory.createInstance(config),5000);
		states.put(segmentationInfo,ret);
		defaultLsv=ret.getLsv();
		return ret;
	}

	public IVector getDefaultLsv(){
		return defaultLsv;
	}
	public IVector update(String segmentationInfo, Event event)throws NotInitializedException{
		return getBA(segmentationInfo).update(event, inputMap);
	}

	public InputMap getInputMap() {
		return inputMap;
	}

	public class NotInitializedException extends RuntimeException{

	}

	public void updateArtistIdName(String segmentationInfo, Event event) {
		 getBA(segmentationInfo).updateArtistIdName(event);
	}
}
