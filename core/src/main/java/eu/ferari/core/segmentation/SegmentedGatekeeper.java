package eu.ferari.core.segmentation;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.monitoring.syncOps.LocalSyncOpFactory;

import java.util.HashMap;

/**
 * Created by mofu on 01/11/16.
 */
public class SegmentedGatekeeper {
	private JsonObject config;
	HashMap<String,ILocalSyncOp> segments = new HashMap<>();
	public SegmentedGatekeeper(JsonObject config){
		this.config=config;
	}

	public ILocalSyncOp getSyncOp(String segmentationInfo) {
		ILocalSyncOp ret = segments.get(segmentationInfo);
		if (ret == null) {
			ret = initFunction();
			segments.put(segmentationInfo, ret);
		}
		return ret;
	}

	private ILocalSyncOp initFunction(){
		return LocalSyncOpFactory.createLocalSyncOp(config);
	}
}
