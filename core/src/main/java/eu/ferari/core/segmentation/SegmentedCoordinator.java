package eu.ferari.core.segmentation;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.monitoring.CoordinatorFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mofu on 29/09/16.
 */
public class SegmentedCoordinator {
	private Map<String,SingleCoordinator> coordinators=new HashMap<>();
	private JsonObject config;

	public SingleCoordinator getTaskOp(String segmentationValue){
		SingleCoordinator ret =coordinators.get(segmentationValue);
		if(ret==null){
			ret = CoordinatorFactory.createInstance(config);
			coordinators.put(segmentationValue,ret);
		}
		return ret;
	}


	public void initCoordinator(JsonObject config){
		this.config=config;
	}
}
