package eu.ferari.core.segmentation;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.*;
import eu.ferari.core.utils.MessageType;
import eu.ferari.core.utils.PreviousResolutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mofu on 29/09/16.
 */
public class SingleCoordinator implements ITime {
	private long timestamp= Long.MAX_VALUE;
	private ICoordTaskOp taskOperator;
	private IAggregationMethod aggregationMethod;
	private IResolutionProtocolString resolutionProtocol;
	private Collection<String> balancingSetIds;
	private Collection<IVector> balancingSetLsvs;
	private List<String> unqueriedNodes;
	private HashSet<String> allNodes =new HashSet<>();
	private boolean activeResolution;
	private IVector referencePoint;
	private final Logger logger = LoggerFactory.getLogger(SingleCoordinator.class);

	public SingleCoordinator(IAggregationMethod aggregationMethod, ICoordTaskOp taskOp, IResolutionProtocolString resolutionProtocol) {
		this.taskOperator = taskOp;
		this.resolutionProtocol = resolutionProtocol;
		this.aggregationMethod=aggregationMethod;
	}

	@Override
	public long getOccurrenceTime() {
		return timestamp;
	}

	@Override
	public void setOccurrenceTime(long time) {
		timestamp=time;
	}


	public synchronized Collection<DataTuple> handleViolation(IVector vec, String nodeId){
		if(activeResolution) {
			if (vec.getOccurrenceTime() >= timestamp) {
				logger.debug("Coordinator received later violation with time {} at time {}",vec.getOccurrenceTime() ,  timestamp);
				throw new PreviousResolutionException();
			}
			logger.debug("resetting previous resolution");
		}
		logger.info("Received violation from node {}",nodeId);
		reset(vec, nodeId);
		return addLSV(vec,nodeId);
	}

	public synchronized Collection<DataTuple> addLSV(IVector vector, String nodeId){
		if(vector.getOccurrenceTime()>timestamp)
			throw new PreviousResolutionException();
		logger.info("Received data from {}. LSV: {}",nodeId,vector);
		balancingSetIds.add(nodeId);
		balancingSetLsvs.add(vector);
		logger.info("Information present from nodes: {}",balancingSetIds);
		logger.info("Unqueried Nodes: {}",unqueriedNodes);
		vector.setOccurrenceTime(timestamp);
		IVector average=aggregationMethod.syncWeights(balancingSetLsvs);
		average.setOccurrenceTime(getOccurrenceTime());
		if(!activeResolution) {
			if (vector.getOccurrenceTime() <= timestamp) {
				return Collections.singletonList(new DataTuple(average, MessageType.UpdateLSV, nodeId));
			}
			return Collections.emptyList();
		}
		Boolean stillViolated = taskOperator.isViolated(referencePoint, average, allNodes.size(), balancingSetIds.size());
		if(balancingSetIds.size()==allNodes.size()){
			//all nodes where part of the resolution.
			logger.info("ResolutionComplete. Send reference point {} to all Nodes",average);
			referencePoint=average;
			activeResolution=false;
			return allNodes.stream().map((String id) ->
				new DataTuple(average, MessageType.UpdateEstimate, id)
			).collect(Collectors.toList());
		}
		if(stillViolated) {
			if(unqueriedNodes.size()==0){
				logger.info("All nodes have been queried.");
				return Collections.emptyList();
			}
			Collection<String> nodesToQuery=resolutionProtocol.getNodesForLSVRequest(balancingSetIds, unqueriedNodes, allNodes);
			logger.info("Requesting LSVs from nodes {}",nodesToQuery);
			for(String node:nodesToQuery)
				unqueriedNodes.remove(node);
			return nodesToQuery.stream().map((String id) ->
					new DataTuple(vector,MessageType.RequestLSV, id)
			).collect(Collectors.toList());
		}
		else{
			logger.info("Send LSV, partial Resolution for nodes {}. LSV: {}",balancingSetIds, average);
			activeResolution=false;
			return balancingSetIds.stream().map((String id) ->
					new DataTuple(average,MessageType.UpdateLSV,id)
			).collect(Collectors.toList());
		}
	}

	private synchronized void reset(IVector vec, String nodeId){
		balancingSetIds =new HashSet<>();
		balancingSetLsvs = new ArrayList<>();
		unqueriedNodes = new ArrayList<>(allNodes);
		unqueriedNodes.remove(nodeId);
		Collections.shuffle(unqueriedNodes);
		activeResolution=true;
		setOccurrenceTime(vec.getOccurrenceTime());
	}

	public void registerNode(String nodeId){
		allNodes.add(nodeId);
	}

}
