package eu.ferari.core.segmentation;

import eu.ferari.core.interfaces.ITime;
import eu.ferari.core.utils.IllegalMessageException;
import eu.ferari.core.utils.PreviousResolutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static eu.ferari.core.segmentation.SingleCommunicator.State.*;

/**
 * Created by mofu on 04/09/16.
 */
public class SingleCommunicator implements ITime{
	public enum State {
		Monitoring,
		PausingCounterRequestMessage,
		PausingLocalThresholdCrossing,
		WaitingForCoordinator,
		WaitingForGatekeeper
	}

	long timestamp;
	State state;
	private static final Logger logger = LoggerFactory.getLogger(SingleCommunicator.class);

	public SingleCommunicator(){
		this(Monitoring);
	}

	public SingleCommunicator(long timestamp){
		this(Monitoring,timestamp);
	}

	public SingleCommunicator(State state) {
		this(state,Long.MAX_VALUE);
	}

	public SingleCommunicator(State state, long timestamp){
		this.state=state;
		this.timestamp=timestamp;
	}

	@Override
	public long getOccurrenceTime() {
		return timestamp;
	}

	@Override
	public void setOccurrenceTime(long time) {
		this.timestamp=time;
	}

	/**
	 * Processes a violation from the Gatekeeper.
	 * @param timedobject
	 * @return
	 */
	public boolean onViolation(ITime timedobject){
		switch (state){
			case Monitoring:
				setStateAndTime(PausingLocalThresholdCrossing,timedobject);
				return true;
			case PausingLocalThresholdCrossing:
			case PausingCounterRequestMessage:
			case WaitingForCoordinator:
			case WaitingForGatekeeper:
				if (timedobject.getOccurrenceTime() < timestamp) {
					setStateAndTime(PausingLocalThresholdCrossing,timedobject);
					return true;
				}
				break;
		}
		return false;
	}

	public void onLSV( ITime timedObject){
		if(timedObject.getOccurrenceTime()> timestamp)
			return; // just ignore data from a previously requested time.
		switch (state){
			case PausingCounterRequestMessage:
				setState(WaitingForCoordinator);
			case WaitingForCoordinator:
				return;
			default:
				throw new IllegalMessageException();
		}
	}

	public void onPauseConfirmation(ITime timedObject){
		switch (state){
			case PausingCounterRequestMessage:
			case PausingLocalThresholdCrossing:
				if(timedObject.getOccurrenceTime() == timestamp) {
					setStateAndTime(WaitingForCoordinator, timedObject);
					return;
				}
			default:
				logger.debug("Current state: {} at time {} received PauseConfirmation message from time {}",new Object[]{state, timestamp,timedObject.getOccurrenceTime()});
		}
	}

	public void onUpdateConfirm(){
		switch (state){
			case WaitingForCoordinator:
			case WaitingForGatekeeper:
				this.state=Monitoring;
				this.timestamp=Long.MAX_VALUE;
				return;
			case Monitoring:
				return;
			default:
				logger.warn("Current state: {} at time {} received UpdateConfirm message from time {}",new Object[]{state, timestamp});
				throw new IllegalMessageException();
		}
	}

	public void onLSVRequest(ITime timedObject){
		switch(state){
			case Monitoring:
				setStateAndTime(PausingCounterRequestMessage,timedObject);
				return;
			case WaitingForCoordinator:
			case PausingLocalThresholdCrossing:
			case PausingCounterRequestMessage:
			case WaitingForGatekeeper:
				if(timedObject.getOccurrenceTime()>timestamp){
					//not yet done with old resolution!
					logger.warn("Current state: {} at time {} received LSVRequest message from time {}",new Object[]{state, timestamp, timedObject.getOccurrenceTime()});
					throw new PreviousResolutionException();//not really the case but useful as trigger.
				}
				setStateAndTime(PausingCounterRequestMessage,timedObject);
		}
	}

	public void updateFromCoordinator(ITime timedObject){
		switch (state){
			case Monitoring:
				//TODO no need for play message, when there was no violation?
				//TODO possibly set to monitoring and handle on updateConfirm with exception.
				this.setStateAndTime(WaitingForGatekeeper,timedObject);
				break;
			case PausingCounterRequestMessage:
			case PausingLocalThresholdCrossing:
			case WaitingForCoordinator:
		}
		if(timedObject.getOccurrenceTime()> timestamp){
			throw new PreviousResolutionException();
		}
		//this is result of an earlier resolution or model/threshold update.
		this.setStateAndTime(WaitingForGatekeeper,timedObject);
	}
	private void setStateAndTime(State state, ITime timedobject){
		setStateAndTime(state,timedobject.getOccurrenceTime());
	}
	private void setStateAndTime(State state, long timestamp){
		setState(state);
		setOccurrenceTime(timestamp);
	}
	void monitoring(){
		setStateAndTime(Monitoring,Long.MAX_VALUE);
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
