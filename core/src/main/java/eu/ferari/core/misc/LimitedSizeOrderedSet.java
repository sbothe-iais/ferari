package eu.ferari.core.misc;

import eu.ferari.core.interfaces.ITime;

import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;


@SuppressWarnings("serial")
public class LimitedSizeOrderedSet<T> extends TreeSet<T> {
	private int maxSize;
	
	public LimitedSizeOrderedSet(int maxSize){
		this.maxSize = maxSize;
	}

	public LimitedSizeOrderedSet(int maxSize, Comparator<? super T> comparator){
		super(comparator);
		this.maxSize=maxSize;
	}
	
	@Override
	public boolean add(T e) {
		boolean returnValue = super.add(e);
		if (size() > maxSize)
			remove(first());
		
		return returnValue;
	}
	
	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean ret=true;
		for(T element : c){
			ret &= add(element);
		}
		return ret;
	}
	/**
	 * @param element - element to search for.
	 * @return a set with all elements greater or equal to element. If the set doesn't contain 
	 * element returns a set with all elements greater or equal to the element smaller than element. If such
	 * an element doesn't exist returns an empty list. If the element is larger than all in the set returns an
	 * empty list.
	 */
	public SortedSet<T> rewindToEqualOrLarger(T element){
		if (contains(element)){
			return tailSet(element, true);
		} else {
			SortedSet<T> tailSet=tailSet(element);
			if (tailSet.isEmpty())
				return new LimitedSizeOrderedSet<T>(0,comparator());
			if (tailSet.size() == size())
				return tailSet(element);
			T firstLowerElement = lower(element);
			if (firstLowerElement == null){
				return new LimitedSizeOrderedSet<T>(0,comparator());
			} else {
				return tailSet(firstLowerElement, true);
			}
		}
	}

	public SortedSet<T> rewindToLarger(T element){
		return tailSet(element, false); 
	}
	
	public T get(T element){
		return (contains(element) ? floor(element) : null);
	}
}
