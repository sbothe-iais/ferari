package eu.ferari.core.misc;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.utils.Helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mofu on 02/09/16.
 */
public class InputMap {
	Map<String,List<String>> eventFieldConfig=new HashMap<>();

	public InputMap(JsonElement fieldConfig){
		for(JsonElement element: Helper.jsonIterable(fieldConfig)) {
			processPrimitive(element.getAsJsonObject());
		}
	}
	private void processPrimitive(JsonObject object){
		String eventName= object.get(ConfigValues.EVENT_NAME_PARAMETER_NAME).getAsString();
		if(eventFieldConfig.containsKey(eventName)){
			throw new RuntimeException("Relevant fields for Event "+eventName+" already configured.");
		}
		List<String>fieldNames=new ArrayList<>();
		for(JsonElement element:Helper.jsonIterable(object.get(ConfigValues.EVENT_FIELDS_PARAMETER_NAME)))
			fieldNames.add(element.getAsString());
		eventFieldConfig.put(eventName,fieldNames);
	}


	public Double[] transformInput(Event event){
		ArrayList<Double> retList=new ArrayList<Double>();
		List<String> relevantFields = eventFieldConfig.get(event.getEvent());
		for(String fieldName:relevantFields){
			if(fieldName.equals(""))
				retList.add(1.0);
			else {
				appendToList(retList, event.getAttributes().get(fieldName));
			}
		}
		return retList.toArray(new Double[retList.size()]);
	}

	private static void appendToList(ArrayList<Double> list, Object input) {
		if (input instanceof Number) {
			list.add(((Number) input).doubleValue());
		} else if (input instanceof double[]) {
			double[] in = (double[]) input;
			for (int i = 0; i < in.length; ++i) {
				list.add(in[i]);
			}
		} else if (input instanceof Number[]) {
			Number[] in = (Number[]) input;
			for (int i = 0; i < in.length; ++i) {
				list.add(in[i].doubleValue());
			}
		}
	}
}
