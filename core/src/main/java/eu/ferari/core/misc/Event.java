package eu.ferari.core.misc;

import eu.ferari.core.interfaces.ITime;

import java.io.Serializable;
import java.util.Map;

public class Event implements Comparable<Event>, ITime, Serializable {

	private String event;
	private long eventOccurrenceTime;
	private Map<String,Object> attributes;


	public Event(String event, long eventOccurrenceTime,
				 Map<String, Object> attributes) {
		super();
		this.event = event;
		this.eventOccurrenceTime = eventOccurrenceTime;
		this.attributes = attributes;
	}
	public String getEvent() {
		return event;
	}
	public long getOccurrenceTime() {
		return eventOccurrenceTime;
	}

	public void setOccurrenceTime(long eventOccurrenceTime) {
		this.eventOccurrenceTime = eventOccurrenceTime;
	}
	public Map<String, Object> getAttributes() {
		return attributes;
	}
	@Override
	public int compareTo(Event o) {
		return (int)(eventOccurrenceTime-o.eventOccurrenceTime);
	}	
}
