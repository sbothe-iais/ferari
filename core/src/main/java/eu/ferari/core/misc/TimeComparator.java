package eu.ferari.core.misc;

import eu.ferari.core.interfaces.ITime;

import java.util.Comparator;

/**
 * Created by mofu on 03/09/16.
 */
public class TimeComparator implements Comparator<ITime> {
	static TimeComparator instance=null;
	@Override
	public int compare(ITime o1, ITime o2) {
		return Long.compare(o1.getOccurrenceTime(),o2.getOccurrenceTime());
	}
	public static TimeComparator getInstance(){
		if(instance==null)
			instance=new TimeComparator();
		return instance;
	}
}
