package eu.ferari.core.interfaces;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.utils.Helper;

/**
 * Created by mofu on 27/10/16.
 */
public interface IConfigurableState extends ILocalState{
	void updateConfig(JsonObject configObject);
	default void updateConfig(Iterable<JsonElement> configObjects){
		for(JsonElement configObject:configObjects)
			updateConfig((JsonObject)configObject);
	}

	default void updateConfig(JsonElement config){
		updateConfig(Helper.jsonIterable(config));
	}
}
