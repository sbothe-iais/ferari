package eu.ferari.core.interfaces;

import java.io.Serializable;


/**
 * @author Rania
 *
 */
public class ISyncOp<Index, Value> implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -7000625427158785506L;
	protected ILocalSyncOp <Index, Value> localSyncOperator;
	protected ICoordTaskOp <Index, Value> coordSyncOperator;

	public ISyncOp(ILocalSyncOp <Index, Value> localSyncOperator, ICoordTaskOp <Index, Value> coordSyncOperator) {
		this.localSyncOperator = localSyncOperator;
		this.coordSyncOperator = coordSyncOperator;
	}
	
	
	public ILocalSyncOp<Index, Value> getLocalSyncOp() {
		return localSyncOperator;
	}
	
	public ICoordTaskOp<Index, Value> getCoordTaskOp(){
		return coordSyncOperator;
	}
	
}
