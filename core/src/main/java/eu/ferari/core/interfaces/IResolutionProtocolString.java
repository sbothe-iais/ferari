package eu.ferari.core.interfaces;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by mofu on 20/11/16.
 */
public interface IResolutionProtocolString extends Serializable {
	Collection<String> getNodesForLSVRequest(Collection<String > balancingSet
			, List<String> unqueriedNodes, Collection<String> allNodes);

	void  resetResolution();
}
