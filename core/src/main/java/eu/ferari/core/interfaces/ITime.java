package eu.ferari.core.interfaces;

/**
 * Created by mofu on 03/09/16.
 */
public interface ITime {
	long getOccurrenceTime();
	void setOccurrenceTime(long time);
}
