package eu.ferari.core.interfaces;

import eu.ferari.core.utils.Message;

/**
 * Created by mofu on 26/10/16.
 */
public interface ICoordinatorSubscriber {
	Message nextMessage();
}
