package eu.ferari.core.interfaces;

import java.io.Serializable;

public interface ICoordinatorState extends IState, Serializable {
    public void setSender(ISend sender);

}
