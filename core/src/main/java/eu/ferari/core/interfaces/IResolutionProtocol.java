package eu.ferari.core.interfaces;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;


/**
 * a resolution strategy interface, used in the synchronization process of the learning protocol in order to bring the topology into a synchronized state 
 * (up to a variance threshold) 
 * @author Rania
 *
 */
public abstract interface IResolutionProtocol extends Serializable{


	
	
	/**
	 * called when the system has stabilised after the resolution protocol was run
	 */
	public abstract void resetResolution();

	/**
	 * /**
	 * Poll nodes for their models and returned the list of queried nodes
	 * @param balancingSet
	 * @param queriedNodes
	 * @param allNodes
	 * @return
	 */
	public abstract Collection<Integer> getNodesForLSVRequest(Collection<Integer> balancingSet,
			Collection<Integer> queriedNodes, Collection<Integer> allNodes);

}
