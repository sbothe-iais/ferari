package eu.ferari.core.interfaces;

import java.io.Serializable;

/**
 * Created by mofu on 20/11/16.
 */
public interface ICoordTaskOp<Index,Value> extends Serializable {



	/**
	 * Decide, whether the condition is violated or not.
	 *
	 */
	Boolean isViolated(IVector<Index,Value> referencePoint, IVector<Index,Value> aggregate,
					   int totalNumNodes, int currentNumNodes);
}
