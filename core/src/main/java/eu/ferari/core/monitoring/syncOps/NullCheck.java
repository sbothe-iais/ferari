package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 30/08/16.
 */
	public class NullCheck<Index,Value> implements ILocalSyncOp<Index, Value> {
    @Override
    public boolean checkLocalCondition(IVector<Index, Value> lsv) {
        if(lsv !=null)return true;
        return false;
    }

    @Override
    public void syncOperator(IVector<Index, Value> var) {

    }

    @Override
    public void setThreshold(double newThreshold) {

    }
}
