package eu.ferari.core.monitoring.syncOps;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.monitoring.ConfigValues;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by mofu on 29/08/16.
 */
public class LocalSyncOpFactory {
	private static final Logger logger = LoggerFactory.getLogger(LocalSyncOpFactory.class);
	static public ILocalSyncOp createLocalSyncOp(JsonObject config){

		switch (config.get("type").getAsString()) {
			case "gm":
				return createMonitoringSyncOp(config.getAsJsonObject("config"));
			case "gml":
				return createLearningSyncOp(config.getAsJsonObject("config"));
			default:
				throw new RuntimeException("No valid Configuration");
		}
	}

	private static ILocalSyncOp createLearningSyncOp(JsonObject config){
		String functionClass=config.get("synchProtocol").getAsString();
		String functionLocation=config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
		URLClassLoader urlCl;
		Class<? extends ISyncOp> newSyncOp=null;
		Class[] argsClass = new Class[]{JsonObject.class};
		try{
			newSyncOp = (Class<? extends ISyncOp>) Class.forName(functionClass);
		} catch(ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL()}, ClassLoader.getSystemClassLoader());
				newSyncOp = (Class<? extends ISyncOp>) urlCl.loadClass(functionClass);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				logger.error("Class not found for function: "+functionClass);
				return null;
			}
		}
		try {
			Object[] args = new Object[]{config};
			Constructor<? extends ISyncOp> argsConstructor = null;
			argsConstructor = newSyncOp.getConstructor(argsClass);
			return argsConstructor.newInstance(args).getLocalSyncOp();
		}catch (NoSuchMethodException e){
			throw new RuntimeException("Constructor not Found.",e);
		}catch (Exception e){
			throw new RuntimeException("Could not Initialize state.", e);
		}
	}

	private static ILocalSyncOp createMonitoringSyncOp(JsonObject config){
		String functionClass = config.get(ConfigValues.CONDITION_CHECK_PARAMETER_NAME).getAsString();
		String functionLocation = config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
		double threshold = config.get(ConfigValues.THRESHOLD_PARAMETER_NAME).getAsDouble();
		int lsvDim = config.get(ConfigValues.LSV_DIMENSION_PARAMETER_NAME).getAsInt();
		boolean equalityAboveThreshold = config.get(ConfigValues.THRESHOLD_EQUALITY_PARAMETER_NAME).getAsBoolean();
		boolean dynamicThreshold= config.get(ConfigValues.DYNAMIC_THRESHOLD_PARAMETER_NAME).getAsBoolean();
		Class<? extends ConditionCheck> newFunction = null;
		URLClassLoader urlCl;
		Class[] argsClass = new Class[] {double.class, int.class,boolean.class, boolean.class};
		try{
			newFunction = (Class<? extends ConditionCheck>) Class.forName(functionClass);
		} catch(ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL()}, ClassLoader.getSystemClassLoader());
				newFunction = (Class<? extends ConditionCheck>) urlCl.loadClass(functionClass);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				System.out.println(functionClass+": Class not found for this function");
				return null;
			}
		}
		try {
			Object[] args = new Object[]{threshold, lsvDim, equalityAboveThreshold, dynamicThreshold};
			Constructor<? extends ILocalSyncOp> argsConstructor = null;
			argsConstructor = newFunction.getConstructor(argsClass);
			return argsConstructor.newInstance(args);
		}catch (NoSuchMethodException e){
			throw new RuntimeException("Constructor not Found.");
		}catch (Exception e){
			throw new RuntimeException("Could not Initialize state.");
		}
	}
}
