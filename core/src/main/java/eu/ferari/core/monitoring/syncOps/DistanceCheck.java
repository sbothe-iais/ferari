package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 06/11/16.
 */
public class DistanceCheck extends ConditionCheck {
	public DistanceCheck(double threshold, int estimateLength, boolean equalityInAboveThresholdRegion, boolean dynamicThreshold) {
		super(threshold, estimateLength, equalityInAboveThresholdRegion, dynamicThreshold);
	}

	public boolean checkLocalCondition(IVector<Integer, Double> lsv) {
		return lsv.distance(refrencePoint)>threshold;
	}

	double F(IVector<Integer, Double> lsv) {
		return lsv.get(0);
	}

}
