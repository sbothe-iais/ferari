package eu.ferari.core.monitoring;

/**
 * Created by mofu on 26/10/16.
 */
public class ConfigValues {
	public static String THRESHOLD_PARAMETER_NAME = "globalThreshold";
	public static String NUM_NODE_PARAMETER_NAME = "numBillingNode";
	public static String THRESHOLD_EQUALITY_PARAMETER_NAME = "equalityInAboveThresholdRegion";
	public static String DEFAULT_VALUE_PARAMETER_NAME = "defLSVValue";
	public static String DYNAMIC_THRESHOLD_PARAMETER_NAME = "dynamicThreshold";
	public static String FUNCTION_NAME_PARAMETER_NAME = "functionName";
	public static String FUNCTION_CLASS_PARAMETER_NAME = "functionClass";
	public static String FUNCTION_LOCATION_PARAMETER_NAME = "functionLocation";
	public static String WINDOW_TYPE_PARAMETER_NAME = "windowType";
	public static String CONDITION_CHECK_PARAMETER_NAME = "conditionCheckName";
	public static String LSV_DIMENSION_PARAMETER_NAME = "LSVDimension";
	public static String SEGMENTATION_INFO_PARAMETER_NAME = "segmentationFields";
	public static String RELEVANT_EVENTS_PARAMETER_NAME = "relevantEvents";
	public static String HISTORY_SIZE_PARAMETER_NAME = "historySize";
	public static String EVENT_NAME_PARAMETER_NAME = "eventName";
	public static String EVENT_FIELDS_PARAMETER_NAME = "eventFields";
}