package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public abstract class ConditionCheck implements ILocalSyncOp<Integer,Double>{
	double threshold;
    IVector<Integer,Double> refrencePoint;
    IVector<Integer,Double> lastSent;



    //TODO no need to store drift, can be parameters to condition check.
    RealValuedVector driftVector;

    boolean equalityInAboveThresholdRegion;
    boolean dynamicThreshold;

    public ConditionCheck(double threshold, int estimateLength, boolean equalityInAboveThresholdRegion, boolean dynamicThreshold){

        this.threshold = threshold;
        this.equalityInAboveThresholdRegion = equalityInAboveThresholdRegion;

        refrencePoint = new RealValuedVector(estimateLength);
        this.dynamicThreshold = dynamicThreshold;
		driftVector=new RealValuedVector(estimateLength);



        // Initialize refrencePoint to zero
        refrencePoint.initializeToZeroVector();
    }


    /**
     * DV = LSV - lastSent
     */
    RealValuedVector computeDelta(IVector<Integer,Double> lsv){
        RealValuedVector tmp = new RealValuedVector(lsv.getDimension());
        tmp.initialise(lsv);
        if(lastSent != null){
            tmp.subtract(lastSent);
        }
    return tmp;
    }

    /** drift vector= referencePoint + dV */
    public void setDriftVector(IVector<Integer,Double> lsv){
        RealValuedVector deltaV = computeDelta(lsv);
        deltaV.add(refrencePoint);
        driftVector=deltaV;
    }
    abstract double F(IVector<Integer,Double> lsv);
    public void setThreshold(double threshold){

        this.threshold = threshold;
    }


	@Override
	public void syncOperator(IVector<Integer, Double> var) {
		refrencePoint=var;
	}
}
