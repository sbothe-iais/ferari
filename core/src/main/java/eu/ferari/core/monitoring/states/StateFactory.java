package eu.ferari.core.monitoring.states;

import com.google.gson.JsonObject;
import org.json.JSONObject;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.monitoring.ConfigValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by mofu on 01/11/16.
 */
public class StateFactory {

	private static final Logger logger = LoggerFactory.getLogger(StateFactory.class);
	public static IComEffState createInstance(JsonObject config){
		switch (config.get("type").getAsString()){
			case "gm":
				return createMonitoringState(config.getAsJsonObject("config"));
			case "gml":
				return createLearningState(config.getAsJsonObject("config"));
			default:
				throw new RuntimeException("No valid Configuration");
		}

	}
	private static IComEffState createLearningState(JsonObject config){
		String functionClassName = config.get(ConfigValues.FUNCTION_CLASS_PARAMETER_NAME).getAsString();
		Class<? extends IComEffState> newFunction = null;
		URLClassLoader urlCl;
		Class[] argsClass = new Class[] {JSONObject.class};
		try{
			newFunction = (Class<? extends IComEffState>) Class.forName(functionClassName);
		} catch(ClassNotFoundException cnfe) {
			try {
				String functionLocation=config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL(), new File("target/classes").toURI().toURL()});
				newFunction = (Class<? extends IComEffState>) urlCl.loadClass(functionClassName);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				logger.error("Class not found for function: "+functionClassName);
				return null;
			}
		}
		try {
				//Learning constructors use JSONObject not JsonObject!
				Object[] args = new Object[]{new JSONObject(config.toString())};
				Constructor<? extends IComEffState> argsConstructor = null;
				argsConstructor = newFunction.getConstructor(argsClass);
				return argsConstructor.newInstance(args);
		}catch (NoSuchMethodException e){
				throw new RuntimeException("Constructor not Found.");
		}catch (Exception e){
				throw new RuntimeException("Could not Initialize state.");
		}
	}

	private static IComEffState createMonitoringState(JsonObject config){
		String functionClassName = config.get(ConfigValues.FUNCTION_CLASS_PARAMETER_NAME).getAsString();
		int historySize = config.get(ConfigValues.HISTORY_SIZE_PARAMETER_NAME).getAsInt();
		int lsvDim = config.get(ConfigValues.LSV_DIMENSION_PARAMETER_NAME).getAsInt();
		double defaultLsvValue = config.get(ConfigValues.DEFAULT_VALUE_PARAMETER_NAME).getAsDouble();
		Class<? extends IComEffState> newFunction = null;
		URLClassLoader urlCl;
		Class[] argsClass = new Class[] {int.class, int.class, double.class};
		try{
			newFunction = (Class<? extends IComEffState>) Class.forName(functionClassName);
		} catch(ClassNotFoundException cnfe) {
			try {
				String functionLocation=config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL(), new File("target/classes").toURI().toURL()});
				newFunction = (Class<? extends IComEffState>) urlCl.loadClass(functionClassName);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				logger.error("Class not found for function: {}",functionClassName);
				return null;
			}
		}
		try {
				Object[] args = new Object[]{historySize, lsvDim, defaultLsvValue};
				Constructor<? extends IComEffState> argsConstructor = null;
				argsConstructor = newFunction.getConstructor(argsClass);
				return argsConstructor.newInstance(args);
		}catch (NoSuchMethodException e){
				throw new RuntimeException("Constructor not Found.");
		}catch (Exception e){
				throw new RuntimeException("Could not Initialize state.");
		}
	}
}
