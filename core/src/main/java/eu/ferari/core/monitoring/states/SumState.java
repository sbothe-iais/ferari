package eu.ferari.core.monitoring.states;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public class SumState extends MonitoringState {
    private int counter;
    public SumState(int historySize, int lsvDim, double defaultValue){
        super(historySize,lsvDim,defaultValue);
    }

    @Override
    public IVector<Integer, Double> getLocalVariable() {
        Double[] sum =(Double[])createDefaultLsv();
        for(Double[] ds : history)
            for(int i =0;i<ds.length;++i)
                sum[i] +=ds[i];

        return new RealValuedVector(sum);
    }

	@Override
	public void setArtistIdName(String artistId, String artistName) {
		// TODO Auto-generated method stub
		
	}

}
