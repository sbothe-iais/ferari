package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 30/08/16.
 */
abstract public class SafeZoneCheck extends BallCheck {

    protected SafeZone[] safeZones;

    public SafeZoneCheck(double threshold, int estimateLength, boolean equalityInAboveThresholdRegion, boolean dynamicThreshold) {
        super(threshold, estimateLength, equalityInAboveThresholdRegion, dynamicThreshold);
    }

    abstract protected void computeSafeZones();

    @Override
    public boolean checkLocalCondition(IVector<Integer, Double> lsv){
        computeSafeZones();

        if (safeZones == null){
//            StormOutputWriter.Instance().print("No Safe Zone found - Checking for Ball Violation");
            return super.checkLocalCondition(lsv);
        }
        else {
            for (int i = 0; i < safeZones.length; i++)
                if (safeZones[i].hasSafeZoneViolation(refrencePoint, driftVector,equalityInAboveThresholdRegion ))
                    return true;
//            StormOutputWriter.Instance().print("No Local Violation detected");
            return false;
        }

    }
}
