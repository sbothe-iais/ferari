package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 30/08/16.
 */
	//TODO transform into sum check. that sums up all entries of lsv.
    //This could be used for identityCheck and SphereSafeZoneCheck.
public class IdentityCheck extends BallCheck {
    public IdentityCheck(double threshold, int estimateLength, boolean equalityInAboveThresholdRegion, boolean dynamicThreshold){
        //estimateLength just needed for getting constructor from factory.
        super(threshold,1,equalityInAboveThresholdRegion,dynamicThreshold);
    }
    @Override
    double F(IVector<Integer, Double> lsv) {
        return lsv.get(0);
    }
}
