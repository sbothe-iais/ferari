package eu.ferari.core.monitoring.states;


import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 15/10/15.
 */
public class IdentityState extends MonitoringState {
    public IdentityState(int historySize,int lsvDim, double defaultValue){
        super(1, lsvDim, defaultValue);
        history.add(new Double[]{defaultValue});
    }

    @Override
    public IVector<Integer, Double> getLocalVariable() {
        return new RealValuedVector(history.get(0));
    }

	@Override
	public void setArtistIdName(String artistId, String artistName) {
		// TODO Auto-generated method stub
		
	}

}