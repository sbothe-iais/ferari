package eu.ferari.core.monitoring;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.*;
import eu.ferari.core.segmentation.SingleCoordinator;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.DistanceTaskOp;
import eu.ferari.core.sync.resolution.FullSyncResolutionString;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by mofu on 20/11/16.
 */
public class CoordinatorFactory {

	private static final Logger logger = LoggerFactory.getLogger(CoordinatorFactory.class);
	public static SingleCoordinator createInstance(JsonObject config){
		ICoordTaskOp taskOp;
		IResolutionProtocolString resolutionProtocol;
		IAggregationMethod aggregationMethod;
		JsonObject functionConfig=config.getAsJsonObject("config");
		switch (config.get("type").getAsString()) {
			case "gm":
				taskOp = createMonitoringTaskOp(functionConfig);
				resolutionProtocol= createMonitoringResolution(functionConfig);
				aggregationMethod=createMonitoringAggregationOp(functionConfig);
				break;
			case "gml":
				taskOp= createLearningTaskOp(functionConfig);
				resolutionProtocol = createLearningResolutionProtocol(functionConfig);
				aggregationMethod = createLearningAggregation(functionConfig);
				break;
			default:
				throw new RuntimeException("No valid Configuration");
		}
		return new SingleCoordinator(aggregationMethod, taskOp, resolutionProtocol);
	}

	public static ICoordTaskOp createLearningTaskOp(JsonObject config){
		String functionClass=config.get("synchProtocol").getAsString();
		String functionLocation=config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
		URLClassLoader urlCl;
		Class<? extends ISyncOp> newSyncOp=null;
		Class[] argsClass = new Class[]{JsonObject.class};
		try{
			newSyncOp = (Class<? extends ISyncOp>) Class.forName(functionClass);
		} catch(ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL()}, ClassLoader.getSystemClassLoader());
				newSyncOp = (Class<? extends ISyncOp>) urlCl.loadClass(functionClass);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				logger.error("Class not found for function: "+functionClass);
				return null;
			}
		}
		try {
			Object[] args = new Object[]{config};
			Constructor<? extends ISyncOp> argsConstructor = null;
			argsConstructor = newSyncOp.getConstructor(argsClass);
			return argsConstructor.newInstance(args).getCoordTaskOp();
		}catch (NoSuchMethodException e){
			throw new RuntimeException("Constructor not Found.");
		}catch (Exception e){
			throw new RuntimeException("Could not Initialize state.");
		}
	}

	public static IResolutionProtocolString createLearningResolutionProtocol (JsonObject config){
		String functionClass=config.get("resolutionProtocol").getAsString();
		String functionLocation=config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
		URLClassLoader urlCl;
		Class<? extends IResolutionProtocolString> newResolutionProtocol=null;
		Class[] argsClass = new Class[]{JsonObject.class};
		try{
			newResolutionProtocol = (Class<? extends IResolutionProtocolString>) Class.forName(functionClass);
		} catch(ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL()}, ClassLoader.getSystemClassLoader());
				newResolutionProtocol = (Class<? extends IResolutionProtocolString>) urlCl.loadClass(functionClass);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				System.out.println(functionClass+": Class not found for this function");
				return null;
			}
		}
		try {
			Object[] args = new Object[]{config};
			Constructor<? extends IResolutionProtocolString> argsConstructor = null;
			argsConstructor = newResolutionProtocol.getConstructor(argsClass);
			return argsConstructor.newInstance(args);
		}catch (NoSuchMethodException e){
			throw new RuntimeException("ResolutionProtocol Constructor not Found.",e);
		}catch (Exception e){
			throw new RuntimeException("Could not Initialize ResolutionProtocol.", e);
		}

	}

	public static IAggregationMethod createLearningAggregation (JsonObject config){
		String functionClass=config.get("aggregationMethod").getAsString();
		String functionLocation=config.get(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME).getAsString();
		URLClassLoader urlCl;
		Class<? extends IAggregationMethod> newAggregation=null;
		Class[] argsClass = new Class[]{};
		try{
			newAggregation = (Class<? extends IAggregationMethod>) Class.forName(functionClass);
		} catch(ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURI().toURL()}, ClassLoader.getSystemClassLoader());
				newAggregation = (Class<? extends IAggregationMethod>) urlCl.loadClass(functionClass);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e){
				System.out.println(functionClass+": Class not found for this function");
				return null;
			}
		}
		try {
			Object[] args = new Object[]{};
			Constructor<? extends IAggregationMethod> argsConstructor = null;
			argsConstructor = newAggregation.getConstructor(argsClass);
			return argsConstructor.newInstance(args);
		}catch (NoSuchMethodException e){
			throw new RuntimeException("Aggregation Constructor not Found.", e);
		}catch (Exception e){
			throw new RuntimeException("Could not Initialize Aggregation.", e);
		}

	}

	public static IResolutionProtocolString createMonitoringResolution(JsonObject conf){
		return new FullSyncResolutionString(new JsonObject());

	}
	public static ICoordTaskOp createMonitoringTaskOp(JsonObject conf){
		return new DistanceTaskOp(conf);

	}

	public static IAggregationMethod createMonitoringAggregationOp(JsonObject conf){
		return new Averaging();
	}
}
