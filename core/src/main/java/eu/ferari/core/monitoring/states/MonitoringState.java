package eu.ferari.core.monitoring.states;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.IVector;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by mofu on 30/08/16.
 */
abstract public class MonitoringState implements IComEffState<Integer, Double>, Serializable {
    final int historySize;
	final int defaultLsvDim;
	final double defaultLsvValue;
	CircularFifoQueue<Double[]> history;
    public MonitoringState(int historySize, int defaultLsvDim, double defaultLsvValue){
        this.defaultLsvValue=defaultLsvValue;
		this.defaultLsvDim=defaultLsvDim;
		this.historySize = historySize;
		history = new CircularFifoQueue<Double[]>(historySize);
    }
	public void processInput(DataTuple data) {
		history.add((Double[])data.getValue(0));
	}
	@Override
	public void setLocalVariable(IVector<Integer, Double> iVector) {
		//No need for functionality. In monitoring the ComEffState only uses observed data.
	}

	Double[] createDefaultLsv(){
		Double[] lsv = new Double[defaultLsvDim];
		for(int i=0;i<lsv.length;++i)
			lsv[i]=defaultLsvValue;
		return  lsv;
	}
}
