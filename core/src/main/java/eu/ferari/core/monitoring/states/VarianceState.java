package eu.ferari.core.monitoring.states;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public class VarianceState extends MonitoringState{
    public VarianceState(int historySize, int lsvDim, double defaultValue){
        super(historySize,lsvDim,defaultValue);
		throw new RuntimeException("VarianceState is not yet implemented.");
    }

    @Override
    public IVector<Integer, Double> getLocalVariable() {

        double sum=0.0;
        double sumOfSquares=0.0;
		//TODO numerical unstable!
        for(Double[] ds:history){
            //TODO use covariance!
            sum +=ds[0];
            sumOfSquares += ds[0]*ds[0];
        }
        RealValuedVector ret =new RealValuedVector(defaultLsvDim);
        ret.set(0,sumOfSquares);
        ret.set(1,sum);
        return ret;
    }

	@Override
	public void setArtistIdName(String artistId, String artistName) {
		// TODO Auto-generated method stub
		
	}

}
