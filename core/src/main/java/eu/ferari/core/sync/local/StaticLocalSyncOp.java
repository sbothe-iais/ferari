package eu.ferari.core.sync.local;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.IVector;

public class StaticLocalSyncOp <Index, Value> implements ILocalSyncOp <Index, Value> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5157109754060979388L;

	// the number of steps needed to send an update to the coordinator, usually some constant
	private int numSteps;

	// the number of updates in the local model so far
	private int numUpdates = 0;

	public StaticLocalSyncOp(int miniBatchSize) {
		this.numSteps = miniBatchSize;
	}
	
	public StaticLocalSyncOp(JSONObject config) throws JSONException {
		this(config.getInt("batchSize"));
	}

	@Override
	public boolean checkLocalCondition(IVector<Index, Value> lsv) {
		numUpdates++;
		if (isViolation()) {
			numUpdates = 0;
			return true;
		}
		return false;
	}


	private boolean isViolation() {
		return numUpdates >= numSteps;  // another possibility besides ">", have %numSteps

	}

	@Override
	public void syncOperator(IVector<Index, Value> var) {
		
		
	}

	@Override
	public void setThreshold(double newThreshold) {
	}
}
