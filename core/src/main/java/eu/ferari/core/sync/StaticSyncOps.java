package eu.ferari.core.sync;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.sync.coord.NumNodesTaskOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mofu on 18.02.17.
 */
public class StaticSyncOps<Index, Value> extends ISyncOp<Index, Value> {
	public StaticSyncOps(JsonObject config) throws JSONException{
		super(new StaticLocalSyncOp<Index, Value>(new JSONObject(config.toString())), new NumNodesTaskOp(config));
	}
}
