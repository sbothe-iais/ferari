package eu.ferari.core.sync.resolution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.json.JSONObject;

import eu.ferari.core.interfaces.IResolutionProtocol;

/**
 * a resolution protocol whereby the number of nodes queried each time is double the previous number, until 
 * half the nodes are polled and then we query all the nodes
 * @author Rania
 *
 */
public class ExponentialResolution implements IResolutionProtocol {
	
	private int nextQueriedNumber = 1;

	public ExponentialResolution() {
		
	}
	/**
	 * 
	 */
	public ExponentialResolution(JSONObject config) {
		
	}
	@Override
	public Collection<Integer> getNodesForLSVRequest(
			Collection<Integer> balancingSet, Collection<Integer> queriedNodes,
			Collection<Integer> allNodes) {
		List <Integer> availableNodes = new ArrayList<Integer>(allNodes); // available nodes for querying
		availableNodes.removeAll(balancingSet);
		shuffleNodes(availableNodes);
		int numberToQuery = nextQueriedNumber;
		if(numberToQuery > allNodes.size() || numberToQuery > allNodes.size()/2 || numberToQuery > availableNodes.size()) {
			numberToQuery = availableNodes.size();
		}
		Collection <Integer> nodesToQuery = availableNodes.subList(0, numberToQuery);
		nextQueriedNumber*=2;
		return nodesToQuery;
	}

	
	private static final long serialVersionUID = 4520526278542322799L;

	
	
	private void shuffleNodes(List<Integer> availableNodes) {
		long seed = System.nanoTime();
		Collections.shuffle(availableNodes, new Random(seed));
	}

	@Override
	public void resetResolution() {
		nextQueriedNumber = 1;
	}

	
}
