package eu.ferari.core.sync.resolution;

import java.util.Collection;

import org.json.JSONObject;

import eu.ferari.core.interfaces.IResolutionProtocol;

/**
 * This resolution strategy merely polls all the learner nodes for their models 
 * @author Rania
 *
 */
public class FullSyncResolution implements IResolutionProtocol {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8989372652671159267L;

	public FullSyncResolution() {
		
	}
	/**
	 * 
	 */
	public FullSyncResolution(JSONObject config) {
		
	}
	

	@Override
	public void resetResolution() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Collection<Integer> getNodesForLSVRequest(
			Collection<Integer> balancingSet, Collection<Integer> queriedNodes,
			Collection<Integer> allNodes) {
		return allNodes;
	}


}
