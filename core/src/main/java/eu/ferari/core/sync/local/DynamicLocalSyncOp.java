package eu.ferari.core.sync.local;
import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.IVector;

public class DynamicLocalSyncOp <Index, Value> implements ILocalSyncOp<Index, Value> {



	/**
	 * 
	 */
	private static final long serialVersionUID = 189522452483294825L;

	protected double varianceThreshold;
	
	/**
	 * the reference vector which is a shared value among all nodes and the coordinator
	 */
	protected IVector<Index, Value> referenceVector;

	// the number of steps needed to send an update to the coordinator, usually some constant
	protected int miniBatchSize;

	// the number of updates in the local model so far
	protected int numUpdates = 0;
	
	public DynamicLocalSyncOp(int miniBatchSize, double varianceThreshold) {
		this.miniBatchSize = miniBatchSize;
		this.varianceThreshold = varianceThreshold;
	}
	
	public DynamicLocalSyncOp(JSONObject configObject) throws JSONException {
		JSONObject config = configObject.getJSONObject("synchProtocolParams");
		miniBatchSize=config.getInt("batchSize");
		varianceThreshold=config.getDouble("varianceThreshold");
	}

	@Override
	public boolean checkLocalCondition(IVector<Index, Value> lsv) {
		numUpdates++;
		if (numUpdates >= miniBatchSize && isViolation(lsv)) { // can be >=, another possibility is to have %numSteps
			numUpdates = 0;
			return true;
		}
		return false;
	}
	
	protected boolean isViolation(IVector<Index, Value> var) {
		if(referenceVector== null)
			return true;
		double variance = var.distance(referenceVector);
		if(variance > varianceThreshold/2) {
			return true;
		}
		return false;
	}


	@Override
	public void syncOperator(IVector<Index, Value> var) {
		referenceVector = var.copy();
	}

	@Override
	public void setThreshold(double newThreshold) {
		this.varianceThreshold=newThreshold;
	}
}
