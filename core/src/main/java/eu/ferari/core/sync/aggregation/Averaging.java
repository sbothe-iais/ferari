package eu.ferari.core.sync.aggregation;

import java.io.Serializable;
import java.util.Collection;

import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.IVector;

/**
 * the mean update model is suitable for combining linear models that have been learned in parallel on 
 * independent training set
 * @author Rania	
 *
 */
public class Averaging <Index, Value> implements IAggregationMethod <Index, Value>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 688848442751015268L;

	@Override
	public IVector<Index, Value> syncWeights(
			Collection<IVector<Index, Value>> balancingSetWeights) {
		long starttime = System.nanoTime();
		IVector<Index, Value> singleModel = balancingSetWeights.iterator().next();
		IVector<Index, Value> averagedModel = singleModel.average(balancingSetWeights);
		Long diff = (System.nanoTime() - starttime);
		//System.out.println("Average Mean Sync time: " + diff );
		return averagedModel;
	}

}