package eu.ferari.core.sync.resolution;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.IResolutionProtocolString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by mofu on 14.02.17.
 */
public class ExponentialResolutionString implements IResolutionProtocolString{

	private int numNodesToQuery=2;
	public ExponentialResolutionString(JsonObject config){

	}

	@Override
	public Collection<String> getNodesForLSVRequest(Collection<String> balancingSet, List<String> unqueriedNodes, Collection<String> allNodes) {
		//Leave out increase of numNodesTo Query, b/c answers will arrive at different times.
		//Keeping it at 2 will result in double of new requests after each round of requests is finished.
		return new ArrayList<>(unqueriedNodes.subList(0, Math.min(numNodesToQuery,unqueriedNodes.size())));
	}

	@Override
	public void resetResolution() {
		numNodesToQuery=2;
	}
}
