package eu.ferari.core.sync.coord;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ICoordTaskOp;
import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 14.02.17.
 */
public class NumNodesTaskOp implements ICoordTaskOp {

	public NumNodesTaskOp(JsonObject config){

	}

	@Override
	public Boolean isViolated(IVector referencePoint, IVector aggregate, int totalNumNodes, int currentNumNodes) {
		return totalNumNodes>currentNumNodes;
	}
}
