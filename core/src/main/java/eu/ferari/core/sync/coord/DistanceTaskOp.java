package eu.ferari.core.sync.coord;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ICoordTaskOp;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.monitoring.ConfigValues;

/**
 * Created by mofu on 21/11/16.
 */
public class DistanceTaskOp implements ICoordTaskOp{
	double threshold;

	public DistanceTaskOp(double d){
		this.threshold=d;
	}

	public DistanceTaskOp(JsonObject json){
		JsonElement element=  json.get(ConfigValues.THRESHOLD_PARAMETER_NAME);
		if(element!=null)
			this.threshold=element.getAsDouble();
		else{
			element=json.get("synchProtocolParams");
			this.threshold=element.getAsJsonObject().get("varianceThreshold").getAsDouble();
		}
	}

	@Override
	public Boolean isViolated(IVector referencePoint, IVector aggregate,
							  int totalNumNodes, int currentNumNodes) {
		if(referencePoint==null)
			return true;
		return referencePoint.distance(aggregate)>threshold;
	}

}
