package eu.ferari.core.sync.resolution;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.IResolutionProtocolString;

import java.util.Collection;
import java.util.List;

/**
 * Created by mofu on 20/11/16.
 */
public class FullSyncResolutionString implements IResolutionProtocolString{

	public FullSyncResolutionString(JsonObject config){

	}
	public Collection<String> getNodesForLSVRequest(Collection<String> balancingSet, List<String> unqueriedNodes, Collection<String> allNodes) {
		return allNodes;
	}
	public void resetResolution(){

	}
}
