package eu.ferari.core.sync;

import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.sync.coord.DistanceTaskOp;
import eu.ferari.core.sync.local.DynamicLocalSyncOp;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mofu on 18.02.17.
 */
public class DynamicSyncOps<Index, Value> extends ISyncOp<Index,Value> {
	public DynamicSyncOps(JsonObject config) throws JSONException{
		super(new DynamicLocalSyncOp<Index, Value>(new JSONObject(config.toString())), new DistanceTaskOp(config));
	}
}
