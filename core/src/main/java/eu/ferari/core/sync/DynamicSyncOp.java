package eu.ferari.core.sync;

import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.sync.coord.DynamicCoordSyncOp;
import eu.ferari.core.sync.local.DynamicLocalSyncOp;
/**
 * creates a dynamic coordinator and a dynamic local sync operator
 * @author Rania
 *
 * @param <Index>
 * @param <Value>
 */
public class DynamicSyncOp <Index, Value> extends ISyncOperator<Index, Value> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4168438206699973543L;

	public DynamicSyncOp(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		super(new DynamicLocalSyncOp<Index, Value>(config), new DynamicCoordSyncOp<Index, Value>(config));
	}

}
