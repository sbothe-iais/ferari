package eu.ferari.core.sync;
import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;

public class StaticSyncOp<Index, Value> extends ISyncOperator<Index, Value> {

	private static final long serialVersionUID = -4710409931662781279L;
	
	public StaticSyncOp(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		super(new StaticLocalSyncOp<Index, Value>(config), new StaticCoordSyncOp<Index, Value>(config));
	}
}
