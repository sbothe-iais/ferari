package eu.ferari.core.commIntegration;

import java.io.Serializable;

public class Event implements Serializable {

	private static final long serialVersionUID = -9137375855595710957L;
	
	public Event() {
		
	}
	
	public Event(String event) {
		this(event, null);
	}
	
	public Event(String event, String context) {
		this.event = event;
		this.context = context;
	}
	
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	private String event;
	private String context;
	
	public String toString() {
		return event + " " + context;
	}
	
	
	

}
