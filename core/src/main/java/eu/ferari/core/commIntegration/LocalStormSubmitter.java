package eu.ferari.core.commIntegration;

import com.ibm.ferari.storm.Submitter;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;

public class LocalStormSubmitter implements Submitter {
	
	private LocalCluster _cluster;
	
	public LocalStormSubmitter() {
		_cluster = new LocalCluster();
	}

	@Override
	public String submitTopology(Config conf, StormTopology topology) throws Exception {
		return null;
	}

	@Override
	public void killTopology(String topologyName) {
		
	}

}
