package eu.ferari.core.commIntegration;

import java.io.Serializable;

public class TopologyCustomConfig implements Serializable {

	private static final long serialVersionUID = -725484438232605198L;
	public String getConfFile() {
		return confFile;
	}
	public void setConfFile(String confFile) {
		this.confFile = confFile;
	}
	public String getJsonFile() {
		return jsonFile;
	}
	public void setJsonFile(String jsonFile) {
		this.jsonFile = jsonFile;
	}
	private String confFile;
	private String jsonFile;
	
}
