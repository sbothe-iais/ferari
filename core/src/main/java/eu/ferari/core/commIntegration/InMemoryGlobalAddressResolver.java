package eu.ferari.core.commIntegration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ibm.ferari.AddressResolver;

public class InMemoryGlobalAddressResolver implements AddressResolver {
	
	private static final int PORTS_INIT_RANGE = 5611;
	private static volatile int nextIncrement = 1;
	private static final Map<String,String> sites2addresses = new ConcurrentHashMap<>();
	private static final AddressResolver instance = new InMemoryGlobalAddressResolver();
	
	public static AddressResolver getInstance() {
		if (AddressResolverRegistry.getInstance() == null) {
			return instance;
		}
		return AddressResolverRegistry.getInstance();
	}
	
	@Override
	public String[] getHostsOfSite(String site) {
		if (site.equals(getOptimizerSite())) {
			return getOptimizerHosts();
		}
		
		String addr = sites2addresses.get(site);
		if (addr != null) {
			return new String[]{addr};
		}
		
		synchronized (InMemoryGlobalAddressResolver.class) {
			if (sites2addresses.containsKey(site)) {
				return new String[]{sites2addresses.get(site)};
			}
			addr = "localhost:" + ((int)(PORTS_INIT_RANGE + nextIncrement++));
			System.err.println(site + "-->" + addr);
			sites2addresses.put(site, addr);
		}
		
		return new String[]{addr};
		
	}

	@Override
	public String[] getOptimizerHosts() {
		return new String[]{"localhost:" + PORTS_INIT_RANGE};
	}

	@Override
	public String getOptimizerSite() {
		return CommIntegrationConstants.OPT_SITE_ID;
	}

	@Override
	public String getDashboardWSURI() {
		return "http://localhost:3000";
	}
	
	
	

}
