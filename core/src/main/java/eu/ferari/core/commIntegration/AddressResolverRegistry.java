package eu.ferari.core.commIntegration;

import com.ibm.ferari.AddressResolver;

public class AddressResolverRegistry {

	private static AddressResolver instance;
	
	public synchronized static AddressResolver getInstance() {
		return instance;
	}
	
	public synchronized static void register(AddressResolver resolver) {
		if (instance != null) {
			return;
		}
		instance = resolver;
	}
	
}
