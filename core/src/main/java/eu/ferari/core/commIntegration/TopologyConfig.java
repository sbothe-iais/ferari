package eu.ferari.core.commIntegration;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TopologyConfig implements Serializable {
	
	private static final long serialVersionUID = 1246165663633824809L;
	
	public TopologyConfig(Map<String,List<String>> topologyGraph, Map<String, Serializable> customConfig) {
		this.topologyGraph = topologyGraph;
		this.customConfig = customConfig;
	}
	
	@SuppressWarnings("unused")
	public TopologyConfig() {
		
	}
	
	public Map<String, List<String>> getTopologyGraph() {
		return topologyGraph;
	}

	public void setTopologyGraph(Map<String, List<String>> topologyGraph) {
		this.topologyGraph = topologyGraph;
	}

	public Map<String, Serializable> getCustomConfig() {
		return customConfig;
	}

	public void setCustomConfig(Map<String, Serializable> customConfig) {
		this.customConfig = customConfig;
	}

	private Map<String,List<String>> topologyGraph; 
	private Map<String, Serializable> customConfig;
}