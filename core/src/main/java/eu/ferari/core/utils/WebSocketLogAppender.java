package eu.ferari.core.utils;


import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

import eu.ferari.core.DataTuple;




public class WebSocketLogAppender extends AppenderSkeleton {
	private transient WebSocketSender sender = new WebSocketSender();
	
	@Override
	protected void append(LoggingEvent event) {
		DataTuple message = new DataTuple(event.getMessage(), System.currentTimeMillis(), "info", event.getLoggerName());
		sender.signal(message);
	}
	
	public void close() {


	}

	public boolean requiresLayout() {
		return false;
	}

}
