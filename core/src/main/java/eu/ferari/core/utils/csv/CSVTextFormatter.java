package eu.ferari.core.utils.csv;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author itaip
 * @modifications Moritz Fuerneisen
 * 
 */
public class CSVTextFormatter implements Serializable{

	private static final long serialVersionUID = 4614457904898265586L;
	//private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected static final String NULL_STRING = "null";
	private static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy-HH:mm:ss";

	protected DateFormat dateFormatter;
	protected String delimiter;
	// It's important that attributeNames will be a list ordered by order of
	// insertion, because of the format of CSV file.
	protected Attribute[] attributes;
	

	public CSVTextFormatter(List<String> attributeNames, List<Attribute.Type> attributeTypes,
			String dateFormat, String delimiter) throws Exception {

		if (attributeNames.size() != attributeTypes.size()) {
			// TODO throw good exception
			throw new Exception();
		}

		try {
			if (dateFormat == null) {
				dateFormatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
				
			} else {
				dateFormatter = new SimpleDateFormat(dateFormat);
			}
		} catch (IllegalArgumentException e) {
			// wrong date format string
			// TODO throw some exception
			throw new Exception();
		}

		this.delimiter = delimiter;
		if (this.delimiter == null) {
			this.delimiter = ",";
		}

		this.attributes = new Attribute[attributeNames.size()];
		for (int i = 0; i < attributeNames.size(); ++i) {
			Attribute attribute = new Attribute(attributeNames.get(i),
					attributeTypes.get(i), null);
			attributes[i]=attribute;
		}
	}

	public CSVTextFormatter(
			ArrayList<Attribute> attributeDescriptions,
			String dateFormatString, String delimiter) {
			this.delimiter=delimiter;
			this.dateFormatter = new SimpleDateFormat(dateFormatString);
			this.attributes=new Attribute[attributeDescriptions.size()];
			for(int i = 0;i < attributes.length;++i){
				attributes[i]=(Attribute) attributeDescriptions.get(i);
				attributes[i].setDefaultValue(parseAttributeValue((String)attributes[i].defaultValue, attributes[i].type));
			}
	}

	public HashMap<String, Object> parseText(String eventText){

		// limit -1 to cancel avoiding trailing empty slots
		String[] attributeStringValues = eventText.split(delimiter, -1);


		HashMap<String, Object> attributeValues = new HashMap<String, Object>();
		Object attributeValue;
		Attribute attribute;
		int index = -1;
		for (String attributeStringValue : attributeStringValues) {
			++index;
			attribute = attributes[index];
			attributeStringValue = attributeStringValue
					.trim();
			attributeValue = attributes[index].defaultValue;
			attributeValue = parseAttributeValue(attributeStringValue, attribute.type);
				
			attributeValues.put(attribute.name, attributeValue);
			
		}
		for(++index;index<attributes.length;++index){
			attribute = attributes[index];
			attributeValues.put(attribute.name, attribute.defaultValue);
		}
		
		return attributeValues;
	}

	private Object parseAttributeValue(String value, Attribute.Type type){
		
		if(value == null || value.equals(NULL_STRING)) return null;
		try{
			switch (type) {
			case Integer:
				return Integer.parseInt(value);
			case Long:
				return Long.parseLong(value);
			case Double:
				return Double.parseDouble(value);
			case String:
				return value;
			case Date:
				return parseDate(value);
			}
		}catch(ParseException | NumberFormatException e){
			throw new InputFormatMismatchException(value, type);
		}
		return null;
	}

	public String formatTimestamp(long timestamp) {
		return dateFormatter.format(new Date(timestamp));
	}

	public long parseDate(String dateString) throws ParseException {
			return dateFormatter.parse(dateString).getTime();
		
	}
}
