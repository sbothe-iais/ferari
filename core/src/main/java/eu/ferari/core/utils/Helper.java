package eu.ferari.core.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.*;
import java.util.*;

public class Helper {
    /**
     * Write the stack trace of a Throwable to a string.
     * 
     * @param throwable The Throwable to be written to a String
     * @result String with stack trace information
     */
    public static String stackTraceOf(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        return sw.toString();
    }

    /**
     * @return the current absolute path-name of the directory the application
     *         is running in.
     */
    public static String currentAbsolutePath() {
        return new File(new File("mmap.temp").getAbsolutePath()).getParentFile().getAbsolutePath();
    }

    public static Properties readProperties(InputStream is) {
        Properties p = new Properties();
        try {
            p.load(is);
        } catch (Throwable e) {
            throw new RuntimeException(is + " konnte nicht korrekt eingelesen werden!", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                throw new RuntimeException("cannot close InputStream", e);
            }
        }
        return p;

    }

    public static Properties readProperties(String path) {
        InputStream is = fileInputStreamOf(path);
        return readProperties(is);
    }

    public static Properties readProperties(File path) {
        InputStream is = fileInputStreamOf(path);
        return readProperties(is);
    }

    public static FileInputStream fileInputStreamOf(File path) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
        } catch (Throwable e) {
            throw new RuntimeException(" Datei nicht lesbar: " + path, e);
        }
        return fis;
    }

    public static FileInputStream fileInputStreamOf(String path) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
        } catch (Throwable e) {
            throw new RuntimeException(" Datei nicht lesbar: " + path, e);
        }
        return fis;
    }

    public static InputStream getReaderForLocalFile(Class<?> resourceClazz, String localFilename) {
        return resourceClazz.getResourceAsStream(localFilename);

    }

    public static List<String> splitC(String s, char delimiter) {
        // a reusable field we will use for reflection in case 4
        List<String> ll = new ArrayList<String>();

        char[] c = s.toCharArray();

        int index = 0;

        for (int i = 0; i < s.length(); i++) {
            if (c[i] == delimiter) {
                ll.add(s.substring(index, i));
                index = i + 1;
            }
        }
        // add last
        if (c[s.length() - 1] != delimiter) {
            ll.add(s.substring(index, c.length - 1));
        }
        return ll;

    }

    public static List<String> split(String s, char delimiter) {
        // a reusable field we will use for reflection in case 4
        List<String> ll = new ArrayList<String>();
        int pos = 0, end;
        while ((end = s.indexOf(delimiter, pos)) >= 0) {
            ll.add(s.substring(pos, end));
            pos = end + 1;
        }

        // add last
        if (s.charAt(s.length() - 1) != delimiter) {
            ll.add(s.substring(pos, s.length() - 1));
        }
        return ll;

    }

    public static int parseInt(final String str) {
        {
            int ival = 0, idx = 0, end;
            boolean sign = false;
            char ch;

            if (str == null || (end = str.length()) == 0 || ((ch = str.charAt(0)) < '0' || ch > '9')
                    && (!(sign = ch == '-') || ++idx == end || (ch = str.charAt(idx)) < '0' || ch > '9')) {
                throw new NumberFormatException(str);
            }

            for (;; ival *= 10) {
                ival += '0' - ch;
                if (++idx == end) {
                    return sign ? ival : -ival;
                }
                if ((ch = str.charAt(idx)) < '0' || ch > '9') {
                    throw new NumberFormatException(str);
                }
            }
        }
    }
    public static Iterable<JsonElement> jsonIterable(JsonElement element){
        if (element instanceof JsonArray){
            return (Iterable<JsonElement>) element;
        }
		return Collections.singletonList(element);
    }

    public static BufferedReader  bufferFromResource(Object klass, String identifier){
		try {
			return new BufferedReader(new FileReader(
					new  File(klass.getClass().getClassLoader().getResource(identifier).getFile())));
		}catch (FileNotFoundException e){
			return new BufferedReader ( new StringReader(""));
		}
	}

	public static String buildContents(BufferedReader reader){
		String line;
		StringBuilder sb = new StringBuilder();

		try
		{
			while ((line = reader.readLine()) != null)
			{
				sb.append(line);
			}

		}catch(Exception e)
		{
			e.printStackTrace();
		}finally
		{
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
