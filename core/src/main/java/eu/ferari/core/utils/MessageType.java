package eu.ferari.core.utils;



/**
 * Created by mofu on 29/08/16.
 */
public enum MessageType {
	//all types are annotated with the supplied auxillary data.
	Event,
	Violation , //LSV
	UpdateEstimate,
	UpdateLSV, //different to updateEstimate?
	RequestLSV, //Ivector for timestamp
	SendLSV, //LSV :: Ivector
	Threshold, //Threshold :: Double , inside of IVector for timestamp.
	RegisterNode,
	Play,
	PlayAndUpdateEstimate, //LSV, for the timestamp.
	Pause, //LSV, for the timestamp.
	PauseConfirmation, // LSV
	Aggregate, //LSV
	Configuration, //JsonElement, cna be JsonObject or JsonArray
	UpdateThresholdConfirmation,
	UpdateEstimateConfirmation
}

