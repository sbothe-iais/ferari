package eu.ferari.core.utils;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

import java.io.Serializable;

/**
 * Created by mofu on 21/11/16.
 */
public class NoopSender implements Serializable,ISend {
	@Override
	public void signal(DataTuple data) {
		return;
	}

	@Override
	public int getTaskId() {
		return 0;
	}
}
