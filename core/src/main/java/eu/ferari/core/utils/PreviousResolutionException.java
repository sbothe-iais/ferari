package eu.ferari.core.utils;

/**
 * Created by mofu on 04/09/16.
 */

/**
 * Exception being thrown, when a previous resolution is still in progress.
 */
public class PreviousResolutionException  extends RuntimeException{
}
