package eu.ferari.core.utils.csv;

import java.io.Serializable;


public class Attribute implements Serializable{
	/**
	 * 
	 */
	
	public enum Type{
		String ("String"),
		Long ("Long"),
		Double ("Double"),
		Integer ("Integer"),
		Date ("Date");
		
		
		private final String representation;
		private Type(String representation){
			this.representation=representation;
		}
		
		public String getRepresentation(){
			return this.representation;
		}
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7050337311865352853L;
	String name;
	Type type;
	Object defaultValue;

	public Attribute(String name, Type type, Object defaultValue) {
		this.name = name;
		this.type = type;
		this.defaultValue=defaultValue;
	}
	
	public void setDefaultValue(Object defaultValue){
		this.defaultValue=defaultValue;
	}
	
	
}