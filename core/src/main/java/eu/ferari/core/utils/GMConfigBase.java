package eu.ferari.core.utils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class GMConfigBase implements Serializable{
	private static final long serialVersionUID = -2858731400688322561L;
	
	public String tag;
	public String name;
	public String functionName;
	public List<Map<String, String>> monitoringObject;
	public List<Map<String, String>> derivedEvents;
	public String context;
}
