package eu.ferari.core.utils;

import java.io.Serializable;

import com.google.gson.Gson;

import eu.ferari.core.DataTuple;

public class LogMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3005806606001843521L; 
	
	
	private long timestamp;
	private String message;
	private String messageType;
	private String source;
	

	public LogMessage(long timestamp, String message, String messageType, String source) {
		this.timestamp = timestamp;
		this.message = message;
		this.messageType = messageType;
		this.source = source;
	}
	
	public LogMessage(DataTuple data) {
		message = data.getString(0);
		timestamp = data.getLong(1);
		messageType = data.getString(2);
		source = data.getString(3);
	}

	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public String getSource() {
		return source;
	}

	public void setLocal(String source) {
		this.source = source;
	}
	
	public String toJSON(){
		Gson gson = new Gson();
		
		return gson.toJson(this);
	}

}
