package eu.ferari.core;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by mofu on 27/10/16.
 */
public class Constants {

	public static final ArrayList<String> fields =
			new ArrayList<String>(Arrays.asList(new String[]{
					"Data","MessageType", "SegmentationInfo"})
			);
	public static int mesageTypeIndex=1;
	public static int segmentationInfoIndex=2;
	public static int payLoadIndex=0;
	public static int sourceNodeIdIndex=3;
}
