package eu.ferari.core.state;

import com.google.gson.JsonObject;
import eu.ferari.core.Constants;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.*;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.segmentation.SegmentedCommunicator;
import eu.ferari.core.segmentation.SegmentedCoordinator;
import eu.ferari.core.segmentation.SingleCoordinator;
import eu.ferari.core.utils.MessageType;
import eu.ferari.core.utils.PreviousResolutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by mofu on 29/09/16.
 */
public class CoordinatorState implements IConfigurableState {
	private ISend sender;
	private Map<String,SegmentedCoordinator> coordinators=new HashMap<>();
	private static final Logger logger = LoggerFactory.getLogger(CoordinatorState.class);
	public void setSender(ISend sender) {
		this.sender=sender;
	}

	public void update(DataTuple data) {
		String[] splitted=data.getString(Constants.segmentationInfoIndex).split(";");
		String sourceNodeId=data.getString(Constants.sourceNodeIdIndex);
		IVector lsv;
		SingleCoordinator coordinator= getCoordinator(splitted[0],splitted[1]);
		Collection<DataTuple> answers;
		switch ((MessageType) data.getValue(Constants.mesageTypeIndex)){
			case SendLSV:
				logger.debug("Received data from {}", sourceNodeId);
				logger.info("Received LSV message for segment {} of function {}",splitted[1],splitted[0]);
				try {
					lsv = (IVector) data.getValue(Constants.payLoadIndex);
					answers = coordinator.addLSV(lsv, sourceNodeId);
				}catch (PreviousResolutionException e){
					return;
				}
				break;
			case Violation:
				logger.info("Received Violation message for segment {} of function {}",splitted[1],splitted[0]);
				lsv = (IVector)data.getValue(Constants.payLoadIndex);
				try{
					answers=coordinator.handleViolation(lsv,sourceNodeId);
				}catch (PreviousResolutionException e){
					//no-op just ignore the resolution
					return;
				}
				break;
			case RegisterNode:
				coordinator.registerNode(sourceNodeId);
			default:answers=Collections.emptyList();
		}
		answers.forEach((DataTuple answer) -> sender.signal(
				new DataTuple(answer.getValue(0),answer.getValue(1),
						data.getString(Constants.segmentationInfoIndex),answer.getValue(2))
		));
	}

	@Override
	public void handleFromCommunicator(DataTuple data) {

	}

	@Override
	public void updateConfig(JsonObject configObject) {
		String funName = configObject.getAsJsonObject("config").get(ConfigValues.FUNCTION_NAME_PARAMETER_NAME).getAsString();
		SegmentedCoordinator segmentedCoordinator = new SegmentedCoordinator();
		segmentedCoordinator.initCoordinator(configObject);
		coordinators.put(funName,segmentedCoordinator);
	}

	private SingleCoordinator getCoordinator(String funName, String segmentationInfo){
		SegmentedCoordinator segmentedCoord = coordinators.get(funName);
		if(segmentedCoord==null) throw new RuntimeException("Function not yet configured.");
		return segmentedCoord.getTaskOp(segmentationInfo);
	}
}
