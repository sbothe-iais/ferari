package eu.ferari.core.state;

import java.util.*;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.Constants;
import eu.ferari.core.interfaces.*;
import eu.ferari.core.misc.Event;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.utils.Helper;
import eu.ferari.core.utils.MessageType;
import org.apache.storm.shade.com.google.common.collect.Maps;
import org.apache.storm.shade.com.google.common.collect.Maps.EntryTransformer ;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;

import eu.ferari.core.DataTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Replicates tuples based on which functions are interested in which events.
 * @author yoni
 *
 */
public class MessageReplicatorState implements IConfigurableState {
	private static final int EVENT_INDEX_IN_TUPLE = 0;
	// Map of event type to function to set of segmentation fields
	private final Map<String, Map<String, List<String>>> eventToInterestedFunctions;
	private ISend sender;
	private static final Logger logger = LoggerFactory.getLogger(MessageReplicatorState.class);

	public MessageReplicatorState(){
		eventToInterestedFunctions=new HashMap<>();
	}

	public MessageReplicatorState(
			Map<String, Map<String, List<String>>> eventToInterestedFunctions,
			ISend sender) {
		Preconditions.checkNotNull(eventToInterestedFunctions);
		Preconditions.checkNotNull(sender);
		this.eventToInterestedFunctions = ImmutableMap.copyOf(eventToInterestedFunctions);
		this.sender = sender;
	}

	@Override
	public void update(DataTuple data) {
		for (DataTuple tuple : replicate(data))
			sender.signal(tuple);

	}

	/**
	 * Create a new tuple for each function interested in this message. The new
	 * tuple contains exactly the same information as the input tuple with the
	 * addition of context information at index 1. The order is MessageType,
	 * context, all other data at the old index + 1
	 * 
	 * @param tuple Input tuple.
	 * @return A new tuple for each function interested in this message.
	 */
	private Collection<DataTuple> replicate(final DataTuple tuple) {
		Event event = getEventFromTuple(tuple);
		String eventType = event.getEvent();
		if (!eventToInterestedFunctions.containsKey(eventType))
		//just ignore unknown event Types.
			return Collections.emptyList();
		//	throw new RuntimeException("Unknown message type " + eventType);

		// Create a context string for each function using event name , function and event attributes
		Map<String, String> functionToContext = Maps.transformEntries(eventToInterestedFunctions.get(eventType), new EntryTransformer<String, List<String>, String>(){
			@Override
			public String transformEntry(String functionName, List<String> contextFieldNames) {
				StringBuilder sb = new StringBuilder();
				Map<String, Object> eventAttributes = event.getAttributes();
				if(contextFieldNames.size()>0){
					for (String fieldName : contextFieldNames) {
						if (eventAttributes.containsKey(fieldName)){
							sb.append(eventAttributes.get(fieldName).toString());
							sb.append(",");
						}
					}
				}else
					sb.append("FERARI_DEFAULT_SEGMENT,");
				sb.deleteCharAt(sb.length() - 1); // Remove last comma
				return Joiner.on(";").join(functionName, sb.toString());
			}
		});
		
		// Now create a tuple for each function
		Collection<DataTuple> replicatedTuples = Collections2.transform(functionToContext.values(), new Function<String, DataTuple>(){
			@Override
			public DataTuple apply(String context) {
				DataTuple tupleWithContext = new DataTuple(tuple.getValue(0), MessageType.Event,context);
				for (int i = 1; i < tuple.asList().size(); i++)
					tupleWithContext.add(tuple.getValue(i));

				return tupleWithContext;
			}			
		});
		
		return replicatedTuples;		
	}

	public void handleFromCommunicator(DataTuple dataTuple) {
		MessageType type = (MessageType) dataTuple.getValue(Constants.mesageTypeIndex);
		if(type ==MessageType.Configuration){
			JsonElement config=(JsonElement)dataTuple.getValue(Constants.payLoadIndex);
			updateConfig((JsonElement)config);
		}
		throw new RuntimeException("Received incorrect message from coordinator: "+type.toString());

	}

	@Override
	public void updateConfig(JsonObject configObject){
		JsonObject config=configObject.getAsJsonObject("config");
		String funName = config.get(ConfigValues.FUNCTION_NAME_PARAMETER_NAME).getAsString();
		JsonElement events= config.get(ConfigValues.RELEVANT_EVENTS_PARAMETER_NAME);
		Iterable<JsonElement> segmentationFields=Helper.jsonIterable(config.get(ConfigValues.SEGMENTATION_INFO_PARAMETER_NAME));
		String eventName;
		for(JsonElement event: Helper.jsonIterable(events)) {
			eventName=event.getAsJsonObject().get(ConfigValues.EVENT_NAME_PARAMETER_NAME).getAsString();
			Map<String, List<String>> funInfos = eventToInterestedFunctions.get(eventName);
			if (funInfos == null) {
				funInfos = new HashMap<>();
				eventToInterestedFunctions.put(eventName, funInfos);
			}

			List<String> segmentationFieldSet = new ArrayList<>();
			for (JsonElement segmentationField : segmentationFields)
				segmentationFieldSet.add(segmentationField.getAsString());
			funInfos.put(funName, segmentationFieldSet);
		}
	}

	private Event getEventFromTuple(DataTuple tuple){
		return ((Event) tuple.getValue(Constants.payLoadIndex));
	}

	public void setSender(ISend sender){
		this.sender=sender;
	}
}
