package eu.ferari.core.state;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.Constants;
import eu.ferari.core.interfaces.*;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.monitoring.syncOps.LocalSyncOpFactory;
import eu.ferari.core.DataTuple;
import eu.ferari.core.segmentation.SegmentedGatekeeper;
import eu.ferari.core.utils.IllegalMessageException;
import eu.ferari.core.utils.MessageType;
import eu.ferari.core.utils.RealValuedVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static eu.ferari.core.utils.MessageType.*;

import java.util.HashMap;


/**
 * Created by mofu on 29/08/16.
 */
public class GatekeeperState implements IConfigurableState{
	private HashMap<String, SegmentedGatekeeper> functions =new HashMap<>();
	private ISend sendToCommunicator;
	private static final Logger logger = LoggerFactory.getLogger(GatekeeperState.class);

	@Override
	public void update(DataTuple data) {
		String[] splitted=data.getString(Constants.segmentationInfoIndex).split(";");
		ILocalSyncOp currentSyncOp=getSyncOp(splitted[0],splitted[1]);
		if(currentSyncOp==null) return;
			if(currentSyncOp.checkLocalCondition((IVector)data.getValue(Constants.payLoadIndex))){
				sendToCommunicator.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
						Violation,data.getString(Constants.segmentationInfoIndex)));
			}
	}


	ILocalSyncOp getSyncOp(String functionName, String segmentationInfo){
		ILocalSyncOp ret;
		SegmentedGatekeeper currentFunction=functions.get(functionName);
		if(currentFunction==null)
			throw new RuntimeException("Function "+functionName+ " not yet configured.");
		ret= currentFunction.getSyncOp(segmentationInfo);

		return ret;
	}

	public void setSender(ISend sender) {
		sendToCommunicator = sender;
	}

	public void handleFromCommunicator(DataTuple data) {
		logger.debug("Communicator received message from Coordinator: {}",data.getValue(Constants.mesageTypeIndex));
		String[] splitted=data.getString(Constants.segmentationInfoIndex).split(";");
		MessageType mType = (MessageType)data.getValue(Constants.mesageTypeIndex);
		if(mType.equals(Configuration) ) {
			updateConfig((JsonElement)data.getValue(Constants.payLoadIndex));
			return;
		}
		ILocalSyncOp currentSyncOp = getSyncOp(splitted[0], splitted[1]);
		if (currentSyncOp == null) return;
		IVector timeInfo;
		DataTuple answer;
		switch (mType) {
			case Threshold:
				timeInfo= (IVector) data.getValue(Constants.payLoadIndex);
				currentSyncOp.setThreshold((Double) timeInfo.get(0));
				answer=new DataTuple(timeInfo,UpdateThresholdConfirmation,data.getString(Constants.segmentationInfoIndex));
				break;
			case UpdateEstimate:
				timeInfo=(IVector)data.getValue(Constants.payLoadIndex);
				currentSyncOp.syncOperator((IVector) timeInfo);
				answer=new DataTuple(timeInfo,UpdateEstimateConfirmation,data.getString(Constants.segmentationInfoIndex));
				break;
			default:
				throw new IllegalMessageException();
		}
		sendToCommunicator.signal(answer);
	}

	public void updateConfig(JsonObject configObject) {

		String funName = configObject.getAsJsonObject("config").get(ConfigValues.FUNCTION_NAME_PARAMETER_NAME).getAsString();
		if(functions.containsKey(funName))
			throw new RuntimeException("Function is already configured.");
		functions.put(funName,new SegmentedGatekeeper(configObject));
	}
}
