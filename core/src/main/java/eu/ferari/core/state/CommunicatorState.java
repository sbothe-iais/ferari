package eu.ferari.core.state;

import com.google.gson.JsonObject;
import eu.ferari.core.Constants;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.*;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.segmentation.SegmentedCommunicator;
import eu.ferari.core.segmentation.SingleCommunicator;
import eu.ferari.core.utils.IllegalMessageException;
import eu.ferari.core.utils.MessageType;
import eu.ferari.core.utils.PreviousResolutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static eu.ferari.core.utils.MessageType.*;

/**
 * Created by mofu on 04/09/16.
 */
public class CommunicatorState implements IConfigurableState{
	String nodeId;
	//TODO nodeID has to be send to coordinator!
	ISend coordinatorsender;
	ISend timeMachineSender;
	ISend gatekeeperSender;
	ISend replicatorSender;
	Map<String,SegmentedCommunicator> functions=new HashMap<>();
	private static final Logger logger = LoggerFactory.getLogger(CommunicatorState.class);
	public CommunicatorState(String nodeId){
		this.nodeId=nodeId;
	}

	/**
	 * Handles data from inside the topology. I.e., gatekeeper or time machine.
	 * @param data
	 */
	public void update(DataTuple data) {
		MessageType mType = (MessageType) data.getValue(Constants.mesageTypeIndex);
		String[] splitted=data.getString(Constants.segmentationInfoIndex).split(";");
		SegmentedCommunicator currentFun=functions.get(splitted[0]);
		if(currentFun==null) {
			currentFun = new SegmentedCommunicator();
			functions.put(splitted[0],currentFun);
		}
		SingleCommunicator currentState= currentFun.getState(splitted[1]);
		logger.debug("Update: {} {}", mType, data.getValue(Constants.payLoadIndex));
		switch (mType){
			case Violation:
				try{
					if(currentState.onViolation((ITime)data.getValue(Constants.payLoadIndex)))
						timeMachineSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
								Pause,data.getString(Constants.segmentationInfoIndex)));
				}catch (IllegalMessageException e){
					//TODO log error
				}
				break;
			case SendLSV:
				 try{
					 currentState.onLSV((ITime)data.getValue(Constants.payLoadIndex));
					 coordinatorsender.signal(data);
				 }catch(IllegalMessageException e){
				 	logger.error("IllegalMessageException");
					//TODO more details.
				 }
				 break;
			case PauseConfirmation:
				currentState.onPauseConfirmation((ITime)data.getValue(Constants.payLoadIndex));
				coordinatorsender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
						Violation,data.getString(Constants.segmentationInfoIndex)));
				break;
			case UpdateEstimateConfirmation:
				try{
					currentState.onUpdateConfirm();
					timeMachineSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
							PlayAndUpdateEstimate,data.getString(Constants.segmentationInfoIndex)));
				}catch (IllegalMessageException e){
					//TODO log error
				}
				break;
			case UpdateThresholdConfirmation:
				currentState.onUpdateConfirm();
				timeMachineSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
						Play,data.getString(Constants.segmentationInfoIndex)));
				break;
			case RegisterNode:
				coordinatorsender.signal(data);
		}
	}

	/**
	 * Handles data received from the in channel, receiving data from the coordinator;
	 * @param data
	 */
	public void handleFromCommunicator(DataTuple data) {
		logger.info("Received Message from Communicator: {} {} ",data.getValue(Constants.mesageTypeIndex),data.getValue(Constants.payLoadIndex));
		MessageType mType = (MessageType) data.getValue(Constants.mesageTypeIndex);
		String[] splitted=data.getString(Constants.segmentationInfoIndex).split(";");
		SegmentedCommunicator currentFun=functions.get(splitted[0]);
		SingleCommunicator currentState = currentFun.getState(splitted[1]);
		DataTuple ret = new DataTuple();
		switch (mType){
			case RequestLSV:
				try{
					currentState.onLSVRequest((ITime)data.getValue(Constants.payLoadIndex));
					timeMachineSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
							RequestLSV, data.getString(Constants.segmentationInfoIndex)));
				}catch (PreviousResolutionException e){
					//nothing to do;
					logger.info("Previous Resolution in place.");
				}
				break;
			case UpdateEstimate:
				try {
					currentState.updateFromCoordinator((IVector) data.getValue(Constants.payLoadIndex));
					gatekeeperSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
							UpdateEstimate,data.getString(Constants.segmentationInfoIndex)));
				}catch (PreviousResolutionException e){

				}
				break;
			case UpdateLSV:
				try{
					currentState.onUpdateConfirm();
					timeMachineSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
							PlayAndUpdateEstimate,data.getString(Constants.segmentationInfoIndex)));
				}catch (IllegalMessageException e){
					//TODO log error
				}
				break;
			case Threshold:
				try {
					currentState.updateFromCoordinator((IVector) data.getValue(3));
					gatekeeperSender.signal(new DataTuple(data.getValue(Constants.payLoadIndex),
							Threshold, data.getString(Constants.segmentationInfoIndex)));
				}catch (PreviousResolutionException e){

				}
				break;
			case Configuration:
				try{
					updateConfig((JsonObject)data.getValue(Constants.payLoadIndex));
					replicatorSender.signal(data);
					timeMachineSender.signal(data);
					gatekeeperSender.signal(data);
				}catch (RuntimeException e){
					if(!e.getMessage().equals("Function is already configured."))
						throw e;
				}
				break;
		}

	}

	public void updateConfig(JsonObject configObject) {
		String funName=configObject.getAsJsonObject("config").get(ConfigValues.FUNCTION_NAME_PARAMETER_NAME).getAsString();
		if(!functions.containsKey(funName)){
			functions.put(funName,new SegmentedCommunicator());
		}
		else throw new RuntimeException("Function is already configured.");
	}

	public void setSender(ISend sender) {
		setCoordinatorSender(sender);
	}

	public void setCoordinatorSender(ISend sender){
		coordinatorsender=sender;
	}

	public void setTimeMachineSender(ISend sender){
		timeMachineSender=sender;
	}

	public void setGateKeeperSender(ISend sender){
		gatekeeperSender=sender;
	}

	public void setReplicatorSender(ISend sender){
		replicatorSender=sender;
	}
}
