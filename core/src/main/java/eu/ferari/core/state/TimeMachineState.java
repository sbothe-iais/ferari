package eu.ferari.core.state;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.ferari.core.Constants;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IConfigurableState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.misc.Event;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.segmentation.SingleTimeMachine;
import eu.ferari.core.segmentation.SegmentedTimeMachine;
import eu.ferari.core.utils.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static eu.ferari.core.utils.MessageType.*;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.SortedSet;


/**
 * Created by mofu on 31/08/16.
 */
public class TimeMachineState implements IConfigurableState{
	String nodeId;
	EntityManager entityManager;
	boolean sendToDashboard;

	ISend sendToGK;
	ISend sendToCom;

	HashMap<String, SegmentedTimeMachine> functions=new HashMap<>();

	private static final Logger logger = LoggerFactory.getLogger(TimeMachineState.class);

	public void setSendToGK(ISend sendToGK) {
		this.sendToGK = sendToGK;
	}

	public void setSendToCom(ISend sendToCom) {
		this.sendToCom = sendToCom;
	}

	public TimeMachineState(String nodeId) {

		this.nodeId = nodeId;
	}

	@Override
	public void update(DataTuple data) {
		IVector vec;
		/*
		if(((Event)(data.getValue(0))).getAttributes().containsKey("artistID")) {
			SegmentedTimeMachine currentFunction=functions.get("LastFMPredictor");
			currentFunction.updateArtistIdName("FERARI_DEFAULT_SEGMENT", (Event) data.getValue(0));
			return;
		} 
		else {*/
			String[] splitted=data.getString(Constants.segmentationInfoIndex).split(";");
			SegmentedTimeMachine currentFunction=functions.get(splitted[0]);

			try {
				currentFunction.updateArtistIdName(splitted[1],(Event) data.getValue(0));
				vec = currentFunction.update(splitted[1], (Event) data.getValue(Constants.payLoadIndex));
			}catch (SegmentedTimeMachine.NotInitializedException e){
				SingleTimeMachine singleTimeMachine = currentFunction.createNewSegment(splitted[1]);
				currentFunction.updateArtistIdName(splitted[1],(Event) data.getValue(0));
				vec= singleTimeMachine.update((Event)data.getValue(Constants.payLoadIndex),currentFunction.getInputMap());
				sendToCom.signal(new DataTuple("",RegisterNode,data.getString(Constants.segmentationInfoIndex)));
			}catch (SingleTimeMachine.StatePausedException e){
				return;
			}
		//}
		DataTuple out= new DataTuple(vec,Event,data.getString(Constants.segmentationInfoIndex));
		sendToGK.signal(out);
	}

	@Override
	public void setSender(ISend sender) {
		sendToGK=sender;
	}

	@Override
	public void handleFromCommunicator(DataTuple dataTuple) {
		String splitted[]=dataTuple.getString(Constants.segmentationInfoIndex).split(";");
		SegmentedTimeMachine currentFun=functions.get(splitted[0]);
		MessageType mType = (MessageType)dataTuple.getValue(Constants.mesageTypeIndex);
		if(mType==Configuration) {
			updateConfig((JsonElement)dataTuple.getValue(Constants.payLoadIndex));
			return;
		}
		SingleTimeMachine aggregator;
		try {
			aggregator=currentFun.getBA(splitted[1]);
		}catch (SegmentedTimeMachine.NotInitializedException e) {
			aggregator=currentFun.createNewSegment(splitted[1]);
		}
		IVector vec =(IVector) dataTuple.getValue(Constants.payLoadIndex);
		SortedSet<IVector> laterLSVs;
		logger.info("Got message: {} {}",mType,vec);
		switch (mType){
			case Pause:
				laterLSVs=aggregator.pause(vec);
				IVector forCoordinator=null;
				if(laterLSVs.size()>0)
					forCoordinator=laterLSVs.first().copy();
				else{
					forCoordinator=currentFun.getDefaultLsv().copy();
				}
				forCoordinator.setOccurrenceTime(vec.getOccurrenceTime());
				sendToCom.signal(new DataTuple(forCoordinator,MessageType.PauseConfirmation,
						dataTuple.getString(Constants.segmentationInfoIndex)));
				break;
			case PlayAndUpdateEstimate:
				aggregator.updateEstimate(vec);
				//no break here need to send out the vectors and set play state.
			case Play:
				laterLSVs=aggregator.play(vec);
				for(IVector out:laterLSVs) {

					sendToGK.signal(new DataTuple(out,MessageType.Event,
							dataTuple.getString(Constants.segmentationInfoIndex)));
				}
				break;
			case RequestLSV:
				laterLSVs=aggregator.pause(vec);
				if (laterLSVs.size()>0){
					forCoordinator=laterLSVs.first().copy();
					forCoordinator.setOccurrenceTime(vec.getOccurrenceTime());
				}
				else {
					forCoordinator=vec;
				}
				sendToCom.signal(new DataTuple(forCoordinator, MessageType.SendLSV,
						dataTuple.getString(Constants.segmentationInfoIndex)));
	}

	}

	public void updateConfig(JsonObject configObject) {
		String funName = configObject.getAsJsonObject("config").get(ConfigValues.FUNCTION_NAME_PARAMETER_NAME).getAsString();
		if(functions.containsKey(funName))
			throw new RuntimeException("Function is already configured.");
		SegmentedTimeMachine newTM= new SegmentedTimeMachine(configObject);
		functions.put(funName,newTM);
	}
}
