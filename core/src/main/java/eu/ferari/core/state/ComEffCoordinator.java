package eu.ferari.core.state;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ICoordinatorState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.Message;

public class ComEffCoordinator <Index, Value> implements ICoordinatorState {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2730909271003237088L;
	


	/**
	 * the synchronisation operator
	 */
	private ICoordTaskOperator<Index, Value> taskOp;

	private ISend sendToLocal;

	// a map of nodes and the time stamp of the last time their interacred with coordinator (for timeout calculation) 
	private Map <Integer, Long> allNodes = new HashMap <Integer, Long>();
	
	private Map<Integer, IVector<Index, Value>> balancingSetWeights = new HashMap<Integer, IVector<Index, Value>>();
	
	
	private Set <Integer> balancingSet = new HashSet <Integer>();

	
	
	
	
	public ComEffCoordinator(ISyncOperator<Index, Value> syncOp) {
		this.taskOp = syncOp.getCoordTaskOp();
	}


	public ComEffCoordinator(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		ISyncOperator<Index, Value> syncOp = (ISyncOperator)(this.getClass().getClassLoader().loadClass(config.getString("synchProtocol")).getConstructor(JSONObject.class).newInstance(config));
		this.taskOp = syncOp.getCoordTaskOp();
	}


	@Override
	public void update(DataTuple data) {
		Message message = new Message(data);
		switch (message.getType()) {
		case RegisterNode:
			registerNode(message.getNodeId());
			return;
		case Violation:
			handleViolation(message);
			break;
		case SendLSV:
			handleResolutionResponse(message);
			break;
		default:
			break;
		}

		Message responseMessage = taskOp.decideAction(allNodes.keySet(),
				balancingSetWeights.values(), message.getType(),
				message.getNodeId());
		if (responseMessage != null) {
			sendToLocal.signal(responseMessage.toDataTuple());
		}
		// boolean shouldSync =
		// coordSyncOp.checkGlobalConditions(balancingSetWeights);

	}

	private void handleResolutionResponse(Message message) {
		//CoordLogger.logTrace("Coordinator handling resolution response from node " + message.getNodeId());
		IVector<Index, Value> receivedWeights = (IVector<Index, Value>) message.getLSV();
		balancingSetWeights.put(message.getNodeId(), receivedWeights);
	}


	private void handleViolation(Message message) {
		//CoordLogger.logTrace("Coordinator handling violation from node " + message.getNodeId());
		IVector<Index, Value> receivedWeights = (IVector<Index, Value>) message.getLSV();
		balancingSetWeights.put(message.getNodeId(), receivedWeights);
		int senderId = message.getNodeId();
		balancingSet.add(senderId);	
	}

	/**
	 * 
	 * @param nodeId
	 */
	private void registerNode(int nodeId) {
		//CoordLogger.logTrace("Coordinator registering node " + nodeId);
		allNodes.put(nodeId, System.currentTimeMillis());
	}




	/*private void sendLSVRequest(ComEffNode[] nodes) {

	}

	private void sendSetLSV(ComEffNode[] nodes) {

	}

	private void sendSetRefPoint(ComEffNode[] nodes) {

	}*/
	@Override
	public void setSender(ISend sendToLocal) {
		this.sendToLocal = sendToLocal;
	}

}
