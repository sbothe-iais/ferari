package eu.ferari.core.state;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.ILocalState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.Message;
import eu.ferari.core.utils.MessageType;


public class ComEffNode <Index, Value> implements ILocalState {
	private static final long serialVersionUID = -4962431130304854116L;
	private ISend sendToCoordinator;
	//private IVector <Value,Index> variable;
    private ComEffLocalPair<Index,Value> state;
	private int nodeId;
	private String annotation = "";
	private long commMessages = 0; // the size of the accumulated message sizes sent between between the coordinator and the locals and vice versa
	private static final Logger logger = Logger.getLogger("communication");
	

	
	public ComEffNode(IComEffState <Index, Value>comEffState, ISyncOperator<Index,Value> syncOp) {
        this.state = new ComEffLocalPair<>(comEffState,syncOp.getLocalSyncOp());
	}
	
	public ComEffNode(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		IComEffState<Index, Value> comEffState = ( IComEffState<Index, Value>)(this.getClass().getClassLoader().loadClass(config.getString("functionName")).getConstructor(JSONObject.class).newInstance(config));
		ISyncOperator<Index, Value> syncOp = (ISyncOperator)(this.getClass().getClassLoader().loadClass(config.getString("synchProtocol")).getConstructor(JSONObject.class).newInstance(config));
		this.state = new ComEffLocalPair<>(comEffState,syncOp.getLocalSyncOp());
	}

	public void setLogAnnotation(String stAnnotation) //stupid hack to distinguish logs of different topologies
	{
		annotation = stAnnotation;
	}



	@Override
	public void setSender(ISend sender) {
		this.sendToCoordinator = sender;
		nodeId = sendToCoordinator.getTaskId();
		registerNode();
	}
	/**
	 * register the node at the coordinator side
	 */
	private void registerNode() {
		Message violationMsg = new Message(MessageType.RegisterNode, nodeId);
		sendToCoordinator.signal(violationMsg.toDataTuple());
	}

	//
	@Override
	public void update(DataTuple data) {
		receiveData(data);
	}
	/**
	 * responsible for handling data instances
	 * @param data
	 */
	public void receiveData(DataTuple data) {
		state.processInput(data);
		//variable = (IVector<Value, Index>) state.getLocalVariable();
		if(state.hasViolation()) {
			sendViolation();
		}	
	}


	@Override
	public void handleFromCommunicator(DataTuple data) {
		Message message = new Message(data);
		commMessages += getMessageSize(message);
		logger.log(Level.INFO, "communication: "  + commMessages);
		switch (message.getType()) {
		case UpdateLSV:
			logger.log(Level.INFO, "communication,UpdateLSV," + nodeId+","+annotation);
            state.updateReferencePoint((IVector<Index,Value>)message.getLSV(), false);
            state.setFinishedSync();
			break;
		case UpdateEstimate: // node is updating its model to the synchronised value and its reference vector as well
			logger.log(Level.INFO, "communication,UpdateRefPoint," + nodeId+","+annotation);
            state.updateReferencePoint((IVector<Index,Value>)message.getLSV(), true);
			state.setFinishedSync();
			break;
		case RequestLSV: // the node is being polled for its model
			logger.log(Level.INFO , "communication,RequestLSV," + nodeId+","+annotation);
			answerResolution();
			break;
		default:
			break;
		}
	}
	
	private long getMessageSize(Message message) {
	
		return 1;
	}

	// report to Coordinator that a violation is reached
	private void sendViolation() {
		sendModel(MessageType.Violation);
	}
	
	//sending the local model as an answer to a resolution query
	public void answerResolution() {
		sendModel(MessageType.SendLSV);
	}

	// send the local model to the coordinator
	private void sendModel(MessageType msgType){
		if (!state.isInSyncAlready()) {
			//LocalLogger.logTrace("task id: " + nodeId + " sending its model as " + msgType);
			Message message = new Message(msgType, nodeId, state.getRepresentation());
			//String debugStr = "message type" + msgType + message.getData();
			//System.out.println("(local) " + debugStr + " from task : " + nodeId);
			sendToCoordinator.signal(message.toDataTuple());
			commMessages += getMessageSize(message);
			//System.out.println("communication " + nodeId + " " + commMessages);
			logger.log(Level.INFO, "communication: "  + commMessages);
			state.startSync();
		}
	}
	
}
