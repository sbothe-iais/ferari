package statistics;

import org.apache.storm.tuple.Tuple;

public class CountStatistics extends Statistics{
	public CountStatistics(int epoch, String eType) {
		super(epoch, eType);
	}


	private int count = 0;
	private static int countEvents = 0;
	private static long seconds = 0;

	public int getCount() {
		return count;
	}

	public void setCount(int _count) {
		count = _count;
	}

	@Override
	public void updateStatistics(Tuple input) {
		count ++ ;		
	}

	@Override
	public boolean check_If_Transmit() {
		if (eType.equals("events"))
		{
			countEvents++;
			int x = countEvents%epoch;
			return(x==0);
		}
		else if(eType.equals("seconds"))
		{
			if(seconds == 0){
				return false;
			}
			else
			{
				long now = System.currentTimeMillis();
				if((now - seconds )*1000 > epoch)
				{
					seconds = now;
					return true;
				}
				else{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	@Override
	public  String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("count: ");
		sb.append(count);

		return sb.toString();
	}

	@Override
	public Statistics clone() {
		CountStatistics newCountStatistics = new CountStatistics(epoch, eType);
		newCountStatistics.setCount(count);
		return newCountStatistics;
	}


}
