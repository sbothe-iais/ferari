package statistics;

import org.apache.storm.tuple.Tuple;

public abstract class  Statistics {
	protected final int epoch;
	protected final String eType;
	
	public  Statistics(int epoch, String eType){	
		this.epoch = epoch;
		this.eType = eType;
	}
	
	public abstract void updateStatistics(Tuple input);
	public abstract boolean check_If_Transmit();

	
	public void receiveEvent(Tuple input){
		updateStatistics(input);
		
	}
	
	abstract public Statistics clone();
}
