//package com.ibm.hrl.proton;
//
//import backtype.storm.topology.BoltDeclarer;
//import backtype.storm.topology.TopologyBuilder;
//import backtype.storm.topology.base.BaseRichBolt;
//import backtype.storm.topology.base.BaseRichSpout;
//import backtype.storm.tuple.Fields;
//
//import com.ibm.hrl.proton.agents.EPAManagerBolt;
//import com.ibm.hrl.proton.context.ContextBolt;
//import com.ibm.hrl.proton.expression.facade.EEPException;
//import com.ibm.hrl.proton.expression.facade.EepFacade;
//import com.ibm.hrl.proton.metadata.parser.ParsingException;
//import com.ibm.hrl.proton.protonAdapter.CsvFormatterBolt;
//import com.ibm.hrl.proton.protonAdapter.FileReaderSpout;
//import com.ibm.hrl.proton.protonAdapter.MissingCSVSectionException;
//import com.ibm.hrl.proton.protonAdapter.NoFilenameParameterException;
//import com.ibm.hrl.proton.routing.RoutingBolt;
//import com.ibm.hrl.proton.routing.STORMMetadataFacade;
//import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
//import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
//import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;
//
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.JSONValue;
//import org.slf4j.Logger;
//
//import java.io.File;
//import java.util.Properties;
//
//
///**
// * Created by mofu on 28/07/15.
// */
//public class ProtonTopologyBuilder {
//	static Logger logger = org.slf4j.LoggerFactory.getLogger(ProtonTopologyBuilder.class);
//
//	private static final String INPUT_NAME="protonInput";
//	private static final String FORMATTING_BOLT_NAME = "protonFormattingBolt";
//	private static final String CONTEXT_BOLT_NAME = "protonContextBolt";
//	private static final String EPA_MANAGER_BOLT_NAME = "protonEpaManagerBolt";
//
//	public static void buildProtonTopology(TopologyBuilder builder, String pathToJson, String ROUTING_BOLT_NAME) {
//		buildProtonTopology(builder, pathToJson, ROUTING_BOLT_NAME, null);
//	}
//	
//	public static void buildProtonTopology(TopologyBuilder builder, String pathToJson, String ROUTING_BOLT_NAME, Properties routingBoltProperties)  {
//		logger.info("File path is {}",new File(".").getAbsolutePath());
//		 String jsonString = Utils.createInputContents(pathToJson);
//		 JSONObject parsed=null;
//		 try {
//			parsed = (JSONObject)JSONValue.parse(jsonString);
//		} catch (NullPointerException e) {
//		// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	 	JSONObject root = (JSONObject) parsed.get("epn");
//	 	JSONArray events = (JSONArray) root.get("events");
//	 	logger.info("There are {} event types.",events.size());
//		JSONArray producers = (JSONArray) root.get("producers");
//		BaseRichSpout inputSpout=null;
//		BaseRichBolt formatterBolt=null;
//		//TODO allow multiple file inputs.
//	 	for(Object obj : producers){
// 			if(!((JSONObject) obj).get("type").equals("File")) continue;
// 			try{
// 				JSONArray properties= (JSONArray) ((JSONObject)obj).get("properties");
// 				logger.info("There are %d properties\n", properties.size());
// 				logger.info("Producer properties {}",properties);
// 				inputSpout = new FileReaderSpout(properties);
// 				formatterBolt = new CsvFormatterBolt(properties, events);
// 			}catch(NoFilenameParameterException e){
// 				continue;
// 			}
// 			catch(MissingCSVSectionException e){
// 				inputSpout=null;
// 				continue;
// 			}
// 		}
//	 	if(inputSpout == null || formatterBolt==null) throw new RuntimeException("Could not parse input configuration.");
//	 	EepFacade eep=null;
//	 	STORMMetadataFacade facade=null;
//		try {
//			eep = new EepFacade();
//			facade = new STORMMetadataFacade(jsonString,eep);
//		} catch (ParsingException | EEPException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		FacadesManager facadesManager = new FacadesManager();
//		facadesManager.setEepFacade(eep);
//
//
//
//
//    	TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
//        facadesManager.setTimerServiceFacade(timerServiceFacade);
//        WorkManagerFacade workManagerFacade = new WorkManagerFacade();
//        facadesManager.setWorkManager(workManagerFacade);
//		logger.info("Initialized Proton components.");
//
//		RoutingBolt routingBolt;
//		BoltDeclarer routingBoltDeclarer = null;
//		builder.setSpout(INPUT_NAME, inputSpout);
//		builder.setBolt(FORMATTING_BOLT_NAME, formatterBolt).shuffleGrouping(INPUT_NAME);
//		if(routingBoltProperties != null) {
//			routingBolt = new RoutingBolt(facadesManager,facade, routingBoltProperties);
//			routingBoltDeclarer = builder.setBolt(ROUTING_BOLT_NAME, routingBolt);
//		}
//		else{
//			routingBolt = new RoutingBolt(facadesManager,facade);
//			routingBoltDeclarer = builder.setBolt(ROUTING_BOLT_NAME, routingBolt);
//		}
//		routingBoltDeclarer.shuffleGrouping(FORMATTING_BOLT_NAME).
//				shuffleGrouping(EPA_MANAGER_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM);
//		builder.setBolt(CONTEXT_BOLT_NAME, new ContextBolt(facadesManager,facade)).
//			fieldsGrouping(ROUTING_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM,
//					new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,STORMMetadataFacade.CONTEXT_NAME_FIELD));
//		builder.setBolt(EPA_MANAGER_BOLT_NAME, new EPAManagerBolt(facadesManager,facade)).
//			fieldsGrouping(CONTEXT_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM,
//					new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,STORMMetadataFacade.CONTEXT_PARTITION_FIELD));
//		//TODO create some stuff for flexible output.
//
//	 }
//}
