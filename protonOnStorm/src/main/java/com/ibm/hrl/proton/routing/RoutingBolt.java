/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.ibm.hrl.proton.routing;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import statistics.Statistics;
import json.java.JSON;
import json.java.JSONArray;
import json.java.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import com.ibm.hrl.proton.StormOutputWriter;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.context.CompositeContextType;
import com.ibm.hrl.proton.metadata.context.TemporalContextType;
import com.ibm.hrl.proton.metadata.epa.interfaces.IEventProcessingAgent;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.metadata.event.IEventType;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.metadata.type.TypeAttribute;
import com.ibm.hrl.proton.runtime.event.EventInstance;
import com.ibm.hrl.proton.runtime.event.interfaces.IEventInstance;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;


public class RoutingBolt extends BaseRichBolt {

	OutputCollector _collector;		
	private static final Logger logger = LoggerFactory.getLogger(RoutingBolt.class.getName());
	FacadesManager facadesManager;
	STORMMetadataFacade metadataFacade;
	protected Map<String, List<Statistics>> allstatistics;
	protected Map<String, Statistics> statistics;
	protected Map <String , Boolean> hasPullEvens;
	protected Map <String, List<String>> eventsToPull;
	protected List<String> AlleventsToPull;
	protected List<String> DashboardEvents;
	protected Map <String, IEventType> AllEvents;
	protected Map<String,List<String>> pushModeEvents;

	protected boolean firstEvent;
	protected boolean pull = false;
	protected String statisticsClass;
	protected int epoch;
	protected String eType;
	protected String jarLocation;
	protected Double startingTime;
	protected Double endingTime;
	protected List<String> pushEvents_;

	protected String nodeID;
	protected boolean isPropertiesSet = false;
	protected boolean isJsonSet = false;
	protected String jsonString;

	protected final String commToRoutingPropertiesStreamId;
	protected  final String FRSToRoutingPrimEventsSteamId;
	protected String path;

	public static enum ConfigRoutingBoltKeys {
		pull, statisticsClass, epoch, eType, jarLocation
	}

	public RoutingBolt(FacadesManager facadesManager,STORMMetadataFacade metadataFacade, 
			String commToRoutingPropertiesStreamId, String frsToRoutingPrimEventsSteamId, String nodeID, String path) {
		this.facadesManager = facadesManager;
		this.metadataFacade = metadataFacade;
		this.commToRoutingPropertiesStreamId = commToRoutingPropertiesStreamId;
		this.FRSToRoutingPrimEventsSteamId = frsToRoutingPrimEventsSteamId;
		this.nodeID = nodeID;
		this.path = path;
	}

	public void prepare() {
		allstatistics = new HashMap<String, List<Statistics>>();
		statistics = new  HashMap<String, Statistics>();
		hasPullEvens = new HashMap<String, Boolean>();
		eventsToPull = new HashMap<String, List<String>>();
		AllEvents = new HashMap<String, IEventType>();
		pushModeEvents = new HashMap<String, List<String>>();
		AlleventsToPull = new ArrayList<String>();
		DashboardEvents = new ArrayList<String>();
		pushEvents_ = new   ArrayList<String>();
		firstEvent = true;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		_collector = collector;							
		logger.debug("prepare: done initializing RoutingBolt with task id..." + context.getThisTaskId());
		prepare();
		StormOutputWriter.Instance().setPath(path);
		StormOutputWriter.Instance().setOut();
	}

	@Override
	public void execute(Tuple input) {
	//	System.out.println("Routing Bolt " + nodeID + " received :" + " " + input + ". From source: " + input.getSourceStreamId());

		//********************************************************/
		//***********print to html file***************************/
		//********************************************************/
		if(!input.getSourceStreamId().equals("Comspout_to_communicator_jsonfile") && 
				!input.getSourceStreamId().equals("Comspout_to_communicator_properties") && 
				!input.getSourceStreamId().equals("metadatatoRouting") && 
				!input.getSourceStreamId().equals("Comspout_to_routing_properties")){
			String EventType = (String)input.getValueByField(EventHeader.NAME_ATTRIBUTE);
			Map<String,Object> attributes = null;
			attributes = (Map<String,Object>)input.getValueByField(STORMMetadataFacade.ATTRIBUTES_FIELD);
			long occurrenceTime = 0;
			for (Entry<String, Object> hmp: attributes.entrySet() ){
				if (hmp.getKey().equals("OccurrenceTime")){
					if( hmp.getValue() instanceof Double) {
						occurrenceTime = ((Double) hmp.getValue()).longValue();
					} else {
						occurrenceTime = (Long) hmp.getValue();
					}
				}				
			}	

			if(input.getSourceStreamId().equals("InputSpout_to_routing_push")) {
				if(nodeID.equals("cent1")) {
					StormOutputWriter.Instance().print(nodeID + " received " + EventType + " at " + occurrenceTime , "cent1", "red");
				} 
				else if (nodeID.equals("site1")) {
					StormOutputWriter.Instance().print(nodeID + " received " + EventType + " at " + occurrenceTime , "site1", "red");
				}
				else if (nodeID.equals("cent2")){
					StormOutputWriter.Instance().print(nodeID + " received " + EventType + " at " + occurrenceTime , "cent2", "red");

				}
			} else if(input.getSourceStreamId().equals(FRSToRoutingPrimEventsSteamId)) {
				if(nodeID.equals("cent1")) {
					StormOutputWriter.Instance().print(nodeID + " received " + EventType + " at " + occurrenceTime , "cent1", "blue");
				} 
				else if (nodeID.equals("site1")) {
					StormOutputWriter.Instance().print(nodeID + " received " + EventType + " at " + occurrenceTime , "site1", "blue");
				}
				else if (nodeID.equals("cent2")){
					StormOutputWriter.Instance().print(nodeID + " received " + EventType + " at " + occurrenceTime , "cent2", "blue");

				} 
			} else {
				if(nodeID.equals("cent1")) {
					StormOutputWriter.Instance().print(nodeID + " detected " + EventType + " at " + occurrenceTime , "cent1", "green");
				} 
				else if (nodeID.equals("site1")) {
					StormOutputWriter.Instance().print(nodeID + " detected " + EventType + " at " + occurrenceTime , "site1", "green");
				}
				else if (nodeID.equals("cent2")){
					StormOutputWriter.Instance().print(nodeID + " detected " + EventType + " at " + occurrenceTime , "cent2", "green");

				}
			}
		}

		if(!isPropertiesSet){
			if(input.getSourceStreamId().equals(commToRoutingPropertiesStreamId)) {
				List<String> properties = (List<String>)input.getValue(0);
				pull = Boolean.valueOf(properties.get(8));
				statisticsClass  = properties.get(9);
				epoch = Integer.valueOf(properties.get(10));
				eType = properties.get(11);
				jarLocation = properties.get(12);
				isPropertiesSet = true;
				return;
			}
		}
		if(!isJsonSet) {
			if(input.getSourceStreamId().equals("metadatatoRouting")) {//json

				jsonString = (String)input.getValue(1);
				EepFacade eep = null;
				STORMMetadataFacade facade = null;

				try {
					eep = new EepFacade();
				} catch (EEPException e) {
					e.printStackTrace();
				}
				try {
					facade = new STORMMetadataFacade(jsonString, eep);
				} catch (ParsingException e) {
					e.printStackTrace();
				} catch (EEPException e) {
					e.printStackTrace();
				}

				facadesManager = new FacadesManager();
				facadesManager.setEepFacade(eep);

				TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
				facadesManager.setTimerServiceFacade(timerServiceFacade);
				WorkManagerFacade workManagerFacade = new WorkManagerFacade();
				facadesManager.setWorkManager(workManagerFacade);
				metadataFacade = facade;
				prepare();
				setPullStructures();
				isJsonSet = true;

				//********************************************************/
				//***********Send Pull Request****************************/
				//***********PushMode Events******************************/
				//********************************************************/
				for(IEventType Event: AllEvents.values()){
					for(TypeAttribute atr :Event.getTypeAttributes()){
						String EventType = Event.getName();
						if(atr.getName().equals("PushToCoordinators")){
							String nodeId_s = (String) atr.getDefaultValue();
							pushEvents_.add(EventType);

							List<Object> pullmsg = new ArrayList<Object>();
							pullmsg.add("PushModeEvent");
							pullmsg.add(EventType);
							pullmsg.add(nodeId_s);
							_collector.emit("PushModeEvent", pullmsg);
							if(nodeID.equals("cent1")) {
								StormOutputWriter.Instance().print(nodeID + " set event: " + EventType +" in pushMode for sites:  "+ nodeId_s + "<br />", "cent1", "black");
							} 
							else if (nodeID.equals("site1")) {
								StormOutputWriter.Instance().print(nodeID + " set event: " + EventType + " in pushMode for sites:  "+ nodeId_s + "<br />", "site1", "black");
							}
							else if(nodeID.equals("cent2")) {
								StormOutputWriter.Instance().print(nodeID + " set event: " + EventType + " in pushMode for sites:  "+ nodeId_s + "<br />", "cent2", "black");
							}
							break;
						}
					}
				}

				//********************************************************/
				//***********Set event structure for Dashboard************/
				//********************************************************/
				for(Entry<String, IEventType> Event: AllEvents.entrySet()){
					for(TypeAttribute atr :Event.getValue().getTypeAttributes()){
						if(atr.getName().equals("SendToDashboard")){
							String EventType = Event.getKey();
							Boolean sendToDashboard = (Boolean) atr.getDefaultValue();
							if(sendToDashboard){
								DashboardEvents.add(EventType);
							}
						}
						break;
					}
				}
				return;
			}
		}


		if(!isPropertiesSet || !isJsonSet) {
			return;
		}

		//Events - no push events
		if(input.getSourceStreamId().equals("events") || 
				input.getSourceStreamId().equals("default")|| 
				input.getSourceStreamId().equals("InputSpout_to_routing_push") ||
				input.getSourceStreamId().equals(FRSToRoutingPrimEventsSteamId)) {
			String EventType = (String)input.getValueByField(EventHeader.NAME_ATTRIBUTE);

			Map<String,Object> attributes = null;
			attributes = (Map<String,Object>)input.getValueByField(STORMMetadataFacade.ATTRIBUTES_FIELD);
			if(!input.getSourceStreamId().equals(FRSToRoutingPrimEventsSteamId)){
				if( attributes.get("EventId") != null){
					if( attributes.get("EventId").getClass().getName().toString().equals("java.lang.String")){
						String eventID = (String)attributes.get("EventId");
						UUID uid = UUID.fromString(eventID);  
						attributes.remove("EventId");
						attributes.put("EventId", uid);
					}
				}
			}
			long occurrenceTime = 0;
			for (Entry<String, Object> hmp: attributes.entrySet() ){
				if (hmp.getKey().equals("OccurrenceTime")){
					if( hmp.getValue() instanceof Double) {
						occurrenceTime = ((Double) hmp.getValue()).longValue();
					//	System.out.println("ROUTING BOLT " + nodeID +" received(d) an Event with typeName: "+ EventType+" at timestamp: "+ occurrenceTime);
					} else {
						occurrenceTime = (Long) hmp.getValue();
					//	System.out.println("ROUTING BOLT " + nodeID + " received(l) an Event with typeName: "+ EventType+" at timestamp: "+ occurrenceTime);
					}
				}				
			}	

			if(pull && !input.getSourceStreamId().equals("InputSpout_to_routing_push")){

				//********************************************************/
				//**************DashboardEvents***************************/
				//**************send to TimeMachine***********************/
				//********************************************************/
				if(AllEvents.containsKey(EventType)){
					if(!isDerivedEvent(EventType)){
						List<Object> eventmsg = new ArrayList<Object>();
						eventmsg.add("Event");
						eventmsg.add(EventType);
						eventmsg.add(occurrenceTime);
						eventmsg.add(attributes);
						eventmsg.add(false);
						_collector.emit("Event", eventmsg);//emit to timemachine for push
					}
					else
					{
						List<Object> eventmsg = new ArrayList<Object>();
						eventmsg.add("Event");
						eventmsg.add(EventType);
						eventmsg.add(occurrenceTime);
						eventmsg.add(attributes);
						eventmsg.add(true);
						_collector.emit("Event", eventmsg);//emit to timemachine for push
					}
					//********************************************************/
					//**************events - push modeEvents******************/
					//**************send to TimeMachine***********************/
					//********************************************************/
//				}else if(pushEvents_.contains(EventType)){
//					if(!isDerivedEvent(EventType)){
//						List<Object> eventmsg = new ArrayList<Object>();
//						eventmsg.add("Event");
//						eventmsg.add(EventType);
//						eventmsg.add(occurrenceTime);
//						eventmsg.add(attributes);
//						eventmsg.add(false);
//						_collector.emit("Event", eventmsg);//emit to timemachine for push
//					}
//					else
//					{
//						List<Object> eventmsg = new ArrayList<Object>();
//						eventmsg.add("Event");
//						eventmsg.add(EventType);
//						eventmsg.add(occurrenceTime);
//						eventmsg.add(attributes);
//						eventmsg.add(true);
//						_collector.emit("Event", eventmsg);//emit to timemachine for push
//					}
					//********************************************************/
					//***events - pull request form state transition events***/
					//**************send to TimeMachine***********************/
					//********************************************************/
//				}else if(AlleventsToPull.contains(EventType)){
//					if(isDerivedEvent(EventType)){
//						List<Object> eventmsg = new ArrayList<Object>();
//						eventmsg.add("Event");
//						eventmsg.add(EventType);
//						eventmsg.add(occurrenceTime);
//						eventmsg.add(attributes);
//						eventmsg.add(true);
//						_collector.emit("Event", eventmsg);//emit to timemachine
//					}
//					else{
//						List<Object> eventmsg = new ArrayList<Object>();
//						eventmsg.add("Event");
//						eventmsg.add(EventType);
//						eventmsg.add(occurrenceTime);
//						eventmsg.add(attributes);
//						eventmsg.add(false);
//						_collector.emit("Event", eventmsg);//emit to timemachine
//					}
				}
			}

				//********************************************************/
				//**************events - push modeEvents******************/
				//**************send to TimeMachine***********************/
				//********************************************************/
				//				if(pushEvents_ != null){
				//					for(int i = 0; i<pushEvents_.size();i++){
				//						if (EventType.equals(pushEvents_.get(i))){
				//							if(!isDerivedEvent(EventType)){
				//								List<Object> eventmsg = new ArrayList<Object>();
				//								eventmsg.add("Event");
				//								eventmsg.add(EventType);
				//								eventmsg.add(occurrenceTime);
				//								eventmsg.add(attributes);
				//								eventmsg.add(false);
				//								_collector.emit("Event", eventmsg);//emit to timemachine for push
				//								break;
				//							}
				//							else
				//							{
				//								List<Object> eventmsg = new ArrayList<Object>();
				//								eventmsg.add("Event");
				//								eventmsg.add(EventType);
				//								eventmsg.add(occurrenceTime);
				//								eventmsg.add(attributes);
				//								eventmsg.add(true);
				//								_collector.emit("Event", eventmsg);//emit to timemachine for push
				//								break;
				//							}
				//						}
				//					}
				//				}

				//********************************************************/
				//***events - pull request form state transition events***/
				//**************send to TimeMachine***********************/
				//********************************************************/
				//				for(int i=0; i<AlleventsToPull.size(); i++){
				//					if(EventType.equals(AlleventsToPull.get(i))){
				//						//if(AllEvents.get(EventType)!=null){
				//						if(isDerivedEvent(EventType)){
				//							List<Object> eventmsg = new ArrayList<Object>();
				//							eventmsg.add("Event");
				//							eventmsg.add(EventType);
				//							eventmsg.add(occurrenceTime);
				//							eventmsg.add(attributes);
				//							eventmsg.add(true);
				//							_collector.emit("Event", eventmsg);//emit to timemachine
				//							break;
				//						}
				//						else{
				//							List<Object> eventmsg = new ArrayList<Object>();
				//							eventmsg.add("Event");
				//							eventmsg.add(EventType);
				//							eventmsg.add(occurrenceTime);
				//							eventmsg.add(attributes);
				//							eventmsg.add(false);
				//							_collector.emit("Event", eventmsg);//emit to timemachine
				//							break;
				//						}
				//					}
				//				}
				//			}

				//			List<IEventProcessingAgent> epas = isInputEventToEPA(EventType);
				//			if(!epas.isEmpty()){
				//				for(IEventProcessingAgent epa : epas){
				//					//System.out.println(epa.getName());
				//				}
				//			}


				//************************************************************************/
				//*********************send Pull Request**********************************/
				//************************************************************************/ 
				if(pull){
					sendPullRequest(input);
				}

				//************************************************************************/
				//******************Update Statistics*************************************/
				//************************************************************************/ 

				if(pull ){//&& !input.getSourceStreamId().equals("InputSpout_to_routing_push")) {
					//updateStatistic(input, EventType);
					updateStatistics(input, EventType);
				}



				//get the information - to which context and agent to route
				//System.out.println("Input: "+ input);
				Set<Pair<String,String>> routingInfo = getRoutingInfo(input);

				//for each agent/context pair, add this information to the tuple and emit the tuple
				String eventTypeName = (String)input.getValueByField(EventHeader.NAME_ATTRIBUTE);

				List<Object> tuple = new ArrayList<Object>();	   			 
				tuple.add(eventTypeName);
				tuple.add(attributes);

				if (isConsumerEvent(eventTypeName)){
//////////////////////////////////////////////					_collector.emit(STORMMetadataFacade.CONSUMER_EVENTS_STREAM,tuple);
				}

				if (routingInfo != null){
					//add the agent/context routing info
					for (Iterator iterator = routingInfo.iterator(); iterator.hasNext();) {
						List<Object> eventTuple = new ArrayList<Object>();
						eventTuple.addAll(tuple);
						Pair<String,String> routingEntry = (Pair<String,String>) iterator.next();			
						eventTuple.add(routingEntry.getFirstValue());
						eventTuple.add(routingEntry.getSecondValue());
						_collector.emit(STORMMetadataFacade.EVENT_STREAM, eventTuple);
					}
				}
				_collector.ack(input);
			}
		}

		private void updateStatistics(Tuple input, String EventType) {
			List<Statistics> InpEvent_statistics = allstatistics.get(EventType);
			if(InpEvent_statistics == null) {
				List<Statistics> listStatistics = new ArrayList<Statistics>();
				Statistics stat = setStatistic();
				listStatistics.add(stat);
				//edw tha valw kai ta upoloipa add otan tha exw alla statistika
				allstatistics.put(EventType, listStatistics);
				for(Statistics s : listStatistics){
					s.receiveEvent(input);
					if(s.check_If_Transmit()){
						List<Object> statisticsmsg = new ArrayList<Object>();
						statisticsmsg.add("Statistics");
						Map<String, List<Statistics>> new_statistics =  new HashMap<String, List<Statistics>>();
						for(Entry<String, List<Statistics>> st: allstatistics.entrySet() ){
							List<Statistics> newlistStatist = new ArrayList<Statistics>();
							List<Statistics> listStatist = st.getValue();
							for(Statistics x: listStatist){									
								newlistStatist.add(x.clone());									
							}
							new_statistics.put(st.getKey(), newlistStatist);
						}
						statisticsmsg.add(new_statistics);
						_collector.emit("Statistics", statisticsmsg);
					}
				}
			} else {
				for(Statistics s : InpEvent_statistics){
					s.receiveEvent(input);
					if(s.check_If_Transmit()){
						List<Object> statisticsmsg = new ArrayList<Object>();
						statisticsmsg.add("Statistics");
						Map<String, List<Statistics>> new_statistics =  new HashMap<String, List<Statistics>>();
						for(Entry<String, List<Statistics>> st: allstatistics.entrySet() ){
							List<Statistics> newlistStatist = new ArrayList<Statistics>();
							List<Statistics> listStatist = st.getValue();
							for(Statistics x: listStatist){
								newlistStatist.add(x.clone());
							}
							new_statistics.put(st.getKey(), newlistStatist);
						}
						statisticsmsg.add(new_statistics);
						_collector.emit("Statistics", statisticsmsg);
					}
				}
			}
		}

		private void updateStatistic(Tuple input, String EventType) {
			Statistics inpEvent_statistics = statistics.get(EventType);
			if(inpEvent_statistics == null) {
				inpEvent_statistics = setStatistic();
				statistics.put(EventType, inpEvent_statistics);
			}
			inpEvent_statistics.receiveEvent(input);
			if(inpEvent_statistics.check_If_Transmit()){
				Map<String, Statistics> new_statistics =  new HashMap<String, Statistics>();
				for(Entry<String, Statistics> st: statistics.entrySet() ){
					new_statistics.put(st.getKey(), st.getValue().clone());
				}
				List<Object> countstatisticsmsg = new ArrayList<Object>();
				countstatisticsmsg.add("Statistic");
				countstatisticsmsg.add(new_statistics);
				countstatisticsmsg.add(1);
				_collector.emit("Statistic", countstatisticsmsg);
			}


			StringBuilder sbcs = new StringBuilder("***************************** Routing Bolt "+" ***************************** \n");
			sbcs.append("received " + EventType+"\n");
			sbcs.append("So far Routing Bolt has encountered: \n");
			for (Entry<String, Statistics> hmp: statistics.entrySet() ){
				sbcs.append(hmp.getKey());
				sbcs.append(" --> ");
				sbcs.append(hmp.getValue().toString());
				sbcs.append("\n");
			}
			sbcs.append("*********************************************************************** \n");
			System.out.println(sbcs);

		}

		private boolean isConsumerEvent(String eventTypeName) {
			return metadataFacade.getMetadataFacade().getRoutingMetadataFacade().isConsumerEvent(eventTypeName);
		}



		/**
		 * Return routing info for a tuple. For each tuple determines what agents/context pairs it should be 
		 *	routed to
		 * @param input
		 * @return
		 */
		public Set<Pair<String,String>> getRoutingInfo(Tuple input){
			logger.info("RoutingBolt: getRoutingInfo: getting routing info for tuple: "+input);
			Set<Pair<String,String>> routingInfo = null;		
			//List<String> tupleFields = input.getFields().toList();

			//assuming each tuple has a "name" field		
			String eventTypeName = (String)input.getValueByField(EventHeader.NAME_ATTRIBUTE);
			if (null != eventTypeName){			
				IEventType eventType = metadataFacade.getMetadataFacade().getEventMetadataFacade().getEventType(eventTypeName);
				Map<String,Object> attributes = new HashMap<String,Object>();		

				if (null != eventType){
					//the content of the tuple is not important, need to get routing info, and it is only based on the event
					//name				
					IEventInstance eventInstance = new EventInstance(eventType, attributes);						
					//determine routing 
					routingInfo= metadataFacade.getMetadataFacade().getRoutingMetadataFacade().determineRouting(eventInstance);
				}

			}
			logger.info("RoutingBolt: getRoutingInfo: got routing info for tuple: "+input+", routing info: "+routingInfo);
			return routingInfo;
		}

		public String isDerivedEventFromEpa(String EventType){
			for (Iterator<IEventProcessingAgent> iterator = metadataFacade.getMetadataFacade().getEpaManagerMetadataFacade().getAgentDefinitions().iterator(); iterator.hasNext();) {
				IEventProcessingAgent epa = iterator.next();
				int i=0;
				while (i<epa.getDerivationSchema().getDerivationConditions().size()){
					String derivedEvent = epa.getDerivationSchema().getDerivationConditions().get(i).getType().getName();
					i++;
					if(derivedEvent.equals(EventType))
					{
						return epa.toString();
					}
				}		
			}
			return null;
		}


		public Boolean isDerivedEvent(String EventType){
			for (Iterator<IEventProcessingAgent> iterator = metadataFacade.getMetadataFacade().getEpaManagerMetadataFacade().getAgentDefinitions().iterator(); iterator.hasNext();) {
				IEventProcessingAgent epa = iterator.next();
				int i=0;
				while (i<epa.getDerivationSchema().getDerivationConditions().size()){
					String derivedEvent = epa.getDerivationSchema().getDerivationConditions().get(i).getType().getName();
					i++;
					if(derivedEvent.equals(EventType)){
						return true;
					}
				}		
			}
			return false;
		}


		public Boolean isInputEvent(String EventType){
			for (Iterator<IEventProcessingAgent> iterator = metadataFacade.getMetadataFacade().getEpaManagerMetadataFacade().getAgentDefinitions().iterator(); iterator.hasNext();) {
				IEventProcessingAgent epa = iterator.next();	
				List<IEventType> InputList = epa.getInputEvents();
				for (IEventType inputEvent : InputList) {
					String inputEventName = inputEvent.getName();
					if(inputEventName.equals(EventType)){
						return true;
					}
				}
			}
			return false;
		}


		public List<IEventProcessingAgent> isInputEventToEPA(String EventType){
			List<IEventProcessingAgent> epas = new ArrayList<IEventProcessingAgent>();
			for (Iterator<IEventProcessingAgent> iterator = metadataFacade.getMetadataFacade().getEpaManagerMetadataFacade().getAgentDefinitions().iterator(); iterator.hasNext();) {
				IEventProcessingAgent epa = iterator.next();
				List<IEventType> InputList = epa.getInputEvents();

				for (IEventType inputEvent : InputList) {
					String inputEventName = inputEvent.getName();
					if(inputEventName.equals(EventType)){
						epas.add(epa);
					}
				}
			}
			return epas;
		}


		public List<String> other_InputEventsToEpa(IEventProcessingAgent epa, String eventName){
			List<IEventType> InputList = epa.getInputEvents();
			List<String> list = new ArrayList<String>();
			for(IEventType inputEvent: InputList){
				if (!eventName.equals(inputEvent.getName())){
					list.add(inputEvent.getName());
				}
			}
			return list;
		}

		public void sendPullRequest(Tuple input){
			//send pull message to communicator

			String EventType = (String)input.getValueByField(EventHeader.NAME_ATTRIBUTE);

			if(!eventsToPull.isEmpty()){
				if(eventsToPull.get(EventType) != null && !eventsToPull.get(EventType).isEmpty()){
					long windowDuration = nextEpaWindowDuration(EventType);
					//double occurrenceTime = 0;
					long minOccTime = 0;
					long maxOccTime = 0;
					Map<String,Object> attributes = (Map<String,Object>)input.getValueByField(STORMMetadataFacade.ATTRIBUTES_FIELD);

					for (Entry<String, Object> hmp: attributes.entrySet() ){
						//if (hmp.getKey().equals("OccurrenceTime")){
						//	occurrenceTime = hmp.getValue() instanceof Long ? ((Long)hmp.getValue()).longValue() : ((Double)hmp.getValue()).doubleValue();
						//}	
						if (hmp.getKey().equals("minOccTime")){
							minOccTime = (long) (hmp.getValue() instanceof Long ? ((Long)hmp.getValue()).longValue() : ((Double)hmp.getValue()).doubleValue());
						}
						if (hmp.getKey().equals("maxOccTime")){
							maxOccTime = (long) (hmp.getValue() instanceof Long ? ((Long)hmp.getValue()).longValue() : ((Double)hmp.getValue()).doubleValue());
						}
					}	

					long starttime = (long) (maxOccTime - windowDuration);
					long endingtime = (long) (minOccTime + windowDuration);

					List<Object> pullmsg = new ArrayList<Object>();
					pullmsg.add("Pull");
					pullmsg.add(eventsToPull.get(EventType));
					pullmsg.add(starttime);
					pullmsg.add(endingtime);
					pullmsg.add(nodeID);
					_collector.emit("Pull", pullmsg);
					for(Entry<String, List<String>> x: eventsToPull.entrySet()){
						for(String ev :x.getValue()){
							if(nodeID.equals("cent1")) {
								StormOutputWriter.Instance().print(nodeID + " send PullRequest for  "  + ev, "cent1", "grey");
							} 
							else if (nodeID.equals("site1")) {
								StormOutputWriter.Instance().print(nodeID + "  send PullRequest for  " + ev, "site1", "grey");
							} 
							else if(nodeID.equals("cent2")) {
								StormOutputWriter.Instance().print(nodeID + "  send PullRequest for  " + ev, "cent2", "grey");
							}
						}
					}
				}
			}
		}

		public List<String> setPullStructures(){
			List<String> eventsToPull_ = new ArrayList<String>();
			if(firstEvent == true){
				JSONObject parsed = null;
				try {
					parsed = parsed = ((JSONObject) JSON.parse(jsonString));
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JSONObject root = (JSONObject) parsed.get("epn");			
				JSONArray eventsArray = (JSONArray) root.get("events");
				//HashMap<String, IEventType> map = new HashMap<String, IEventType>();
				for(int i=0;i<eventsArray.size();i++)
				{
					String eventName = ((JSONObject)eventsArray.get(i)).get("name").toString();
					AllEvents.put(eventName, (IEventType) (metadataFacade.getMetadataFacade().getEventMetadataFacade().getEventType(eventName)));
				}

				StringBuilder sb2 = new StringBuilder();
				sb2.append("\n");
				sb2.append("****All Events : "+ nodeID + "****" + "\n");
				for(Entry<String, IEventType> Event: AllEvents.entrySet()){
					sb2.append(Event.getKey()+"\n");		
					if(Event.getValue().getTypeAttributes().toString().contains("Intermediate"))
					{
						hasPullEvens.put(Event.getKey(), true);
						List<IEventProcessingAgent> epas = isInputEventToEPA(Event.getKey());
						for(IEventProcessingAgent epa : epas){
							List<String> pullEvents = other_InputEventsToEpa(epa,Event.getKey());
							List <String> getPullEvents = eventsToPull.get(Event.getKey());
							if (getPullEvents == null){
								getPullEvents = new ArrayList<String>();
								eventsToPull.put(Event.getKey(), getPullEvents);
							}
							for(String pE: pullEvents){
								if(!getPullEvents.contains(pE)){
									getPullEvents.add(pE);
									AlleventsToPull.add(pE);
								}
							}
						}
					}
					else
					{
						hasPullEvens.put(Event.getKey(), false);
					}
				}
				System.out.println(sb2+"\n");


				StringBuilder sb1 = new StringBuilder();
				sb1.append("****Event has pull events: "+ nodeID + "****" + "\n");
				sb1.append("\n");
				for (Entry<String, Boolean> _Event: hasPullEvens.entrySet() ){
					sb1.append(_Event.getKey() + " -> "+ _Event.getValue());
					sb1.append("\n");
				}
				sb1.append("\n");
				System.out.println(sb1);



				StringBuilder sb = new StringBuilder();
				sb.append("****Events to pull: "+ nodeID + "****" + "\n");
				sb1.append("\n");
				for (Entry<String, List<String>> Event_: eventsToPull.entrySet() ){
					sb.append(Event_.getKey() + " -> "+ Event_.getValue()+"\n");
					eventsToPull_.addAll(Event_.getValue());
				}
				sb.append("\n");
				System.out.println(sb.toString());
				firstEvent = false;
			}
			return eventsToPull_;
		}


		public Statistics setStatistic(){
			URLClassLoader urlCl = null;
			Class<? extends Statistics> newStatistic = null;
			Class[] argsClass = new Class[] {int.class, String.class};

			try{
				newStatistic = (Class<? extends Statistics>) Class.forName(statisticsClass);
			} catch(ClassNotFoundException cnfe) {
				try {
					File file = new File(jarLocation);
					urlCl = new URLClassLoader(new URL[] {file.toURL(), new File("target\\classes").toURL()}, getClass().getClassLoader());   
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}

			try {
				if(newStatistic == null) {
					newStatistic = (Class<? extends Statistics>) urlCl.loadClass(statisticsClass);
				}
				Object[] args = new Object[] {epoch, eType};
				Constructor<? extends Statistics> argsConstructor = null;
				argsConstructor = newStatistic.getConstructor(argsClass);
				Statistics statistic = argsConstructor.newInstance(args);
				return statistic;
			} catch (Exception e) {
				System.out.println(statisticsClass + ": Class not found for this statistic type");
				return null;
			}
		}

		public long nextEpaWindowDuration(String EventType){
			long window = 0 ;
			for (Iterator<IEventProcessingAgent> iterator = metadataFacade.getMetadataFacade().getEpaManagerMetadataFacade().getAgentDefinitions().iterator(); iterator.hasNext();) {
				IEventProcessingAgent epa = iterator.next();	
				List<IEventType> InputList = epa.getInputEvents();
				for (IEventType inputEvent : InputList) {
					String inputEventName = inputEvent.getName();
					if(inputEventName.equals(EventType)){
						//System.out.println(metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName()).getType());
						if(metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName()).getType().toString().equals("TEMPORAL_INTERVAL")){
							//System.out.println(((TemporalContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).isNeverEnding() + "isneverEnding");
							//System.out.println(((TemporalContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).hasRelativeTimeTerminator());
							if(((TemporalContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).hasRelativeTimeTerminator()){
								window = ((TemporalContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).getRelativeTimeTerminators().get(0).getRelativeTerminationTime();
							}
							else if(((TemporalContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).isNeverEnding()){

								window = Long.MAX_VALUE;
							}
						}
						else
						{
							//System.out.println(((CompositeContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).getTemporalMemberContext(0).isNeverEnding() + "neverenfdfgfgg");
							//System.out.println(((CompositeContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).getTemporalMemberContext(0).hasRelativeTimeTerminator());
							if(((CompositeContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).getTemporalMemberContext(0).hasRelativeTimeTerminator()){
								window = ((CompositeContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).getTemporalMemberContext(0).getRelativeTimeTerminators().get(0).getRelativeTerminationTime();
							}
							else if(((CompositeContextType)metadataFacade.getMetadataFacade().getContextMetadataFacade().getContext(epa.getContextTypeName())).getTemporalMemberContext(0).isNeverEnding()){
								window = Long.MAX_VALUE;
							}

						}
					}
				}
			}
			return window;
		}

		public void setFacadesManager(FacadesManager facadesManager) {
			this.facadesManager = facadesManager;
		}

		public void setMetadataFacade(STORMMetadataFacade metadataFacade) {
			this.metadataFacade = metadataFacade;
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {		
			List<String> fieldNames = new ArrayList<String>();
			fieldNames.add(EventHeader.NAME_ATTRIBUTE);
			fieldNames.add(STORMMetadataFacade.ATTRIBUTES_FIELD);
			declarer.declareStream(STORMMetadataFacade.CONSUMER_EVENTS_STREAM, new Fields(fieldNames));

			//add the agentName and contextName fields
			List<String> eventFieldNames = new ArrayList<String>();
			eventFieldNames.addAll(fieldNames);
			eventFieldNames.add(STORMMetadataFacade.AGENT_NAME_FIELD);
			eventFieldNames.add(STORMMetadataFacade.CONTEXT_NAME_FIELD);

			logger.debug("RoutingBolt: declareOutputFields:declaring stream for RoutingBolt: stream name: " +STORMMetadataFacade.EVENT_STREAM+ "with fields "+fieldNames);
			declarer.declareStream(STORMMetadataFacade.EVENT_STREAM, new Fields(eventFieldNames));

			declarer.declareStream("Statistics", new Fields("type", "statistics"));
			declarer.declareStream("Statistic", new Fields("type", "countstatistics", "x"));
			declarer.declareStream("Pull", new Fields("type", "pullEvents", "startTime", "endTime", "nodeID"));//To communicator
			declarer.declareStream("Event", new Fields("type", "eventName", "occurrenceTime", "attributes", "derivedEvent"));
			declarer.declareStream("PushModeEvent", new Fields("type", "eventName", "nodeIDs"));
		}
	}