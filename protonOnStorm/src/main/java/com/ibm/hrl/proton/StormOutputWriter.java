package com.ibm.hrl.proton;


import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class StormOutputWriter {

	private static StormOutputWriter instance;
	private String path;

	private StormOutputWriter() {
		
	}

	private PrintWriter outcent1;
	private PrintWriter outcent2;
	private PrintWriter outsite1;


	public static StormOutputWriter Instance() {
		if(instance == null) {
			instance = new StormOutputWriter();
		}
		return instance;
	}

	private Random rand = new Random(); 
	
	public void setOut(){
		try {
			File outputCenter1 = new File(path + "FERARI/Debugging/" + "output_center1.html");
			File outputCenter2 = new File(path + "FERARI/Debugging/" + "output_center2.html");
			File outputSite1 = new File(path + "FERARI/Debugging/" + "output_site1.html");
			outcent1 = new PrintWriter(outputCenter1);
			outcent2 = new PrintWriter(outputCenter2);
			outsite1 = new PrintWriter(outputSite1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public synchronized void print(String line, String node, String color){
		if(node.equals("cent1")) {
			//			float r = rand.nextFloat() / 2f + 0.5f;
			//			float g = rand.nextFloat() / 2f + 0.5f;
			//			float b = rand.nextFloat() / 2f + 0.5f;
			//			Color randomColor = new Color(r, g, b);
			//			out.println("<font color=\"" + Integer.toHexString(randomColor.getRGB()) + "\">" + line + "</font><br /><br />");
			outcent1.println("<font color=\"" + color +  "\">" + line + "</font><br /><br />");
			outcent1.flush();
		} else if(node.equals("cent2")){
			outcent2.println("<font color=\"" +  color +  "\">" + line + "</font><br /><br />");
			outcent2.flush();
		}
		else if(node.equals("site1")){
			outsite1.println("<font color=\"" +  color +  "\">" + line + "</font><br /><br />");
			outsite1.flush();
		}
	}

	public synchronized void close () {
		if(outcent1 != null) {
			outcent1.close();
			outcent1 = null;
		}
		if(outcent2 != null) {
			outcent2.close();
			outcent2 = null;
		}
		if(outsite1 != null) {
			outsite1.close();
			outsite1 = null;
		}
	}

	public void setPath(String path2) {
		this.path = path2;		
	}

}
