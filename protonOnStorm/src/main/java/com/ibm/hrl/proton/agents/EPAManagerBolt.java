/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.ibm.hrl.proton.agents;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import com.ibm.hrl.proton.epaManager.EPAManagerFacade;
import com.ibm.hrl.proton.epaManager.exceptions.EPAManagerException;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.routing.StormEventRouter;
import com.ibm.hrl.proton.runtime.event.interfaces.IEventInstance;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;

public class EPAManagerBolt extends BaseRichBolt {
	private static final Logger logger = LoggerFactory.getLogger(EPAManagerBolt.class);
	OutputCollector _collector;
	String jsonTxt;
	private FacadesManager facadesManager;
	private STORMMetadataFacade metadataFacade;
	
	
	public EPAManagerBolt(FacadesManager facadesManager,STORMMetadataFacade metadataFacade) {
		super();		
		this.facadesManager = facadesManager;
		this.metadataFacade = metadataFacade;
	}
	
	public void prepare() {
		StormEventRouter eventRouter = new StormEventRouter(_collector,metadataFacade);
		EPAManagerFacade epaManager = new EPAManagerFacade(facadesManager.getWorkManager(), eventRouter, null, metadataFacade.getMetadataFacade());
		facadesManager.setEventRouter(eventRouter);
		facadesManager.setEpaManager(epaManager);
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;
		logger.debug("prepare: initializing EPAManagerBolt with task id..."+context.getThisTaskId());
		if(metadataFacade != null) {
			prepare();
		}
		logger.debug("prepare: done initializing EPAManagerBolt with task id..."+context.getThisTaskId());

	}

	@Override
	public void execute(Tuple input) {
		
		if(input.getSourceStreamId().equals("metadatatoEpaManager")) {
			String jsonString = (String)input.getValue(1);
			EepFacade eep = null;
			STORMMetadataFacade facade = null;
			
				try {
					eep = new EepFacade();
				} catch (EEPException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					facade = new STORMMetadataFacade(jsonString,eep);
				} catch (ParsingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (EEPException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			facadesManager = new FacadesManager();
			facadesManager.setEepFacade(eep);

			TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
			facadesManager.setTimerServiceFacade(timerServiceFacade);
			WorkManagerFacade workManagerFacade = new WorkManagerFacade();
			facadesManager.setWorkManager(workManagerFacade);
			metadataFacade = facade;
			prepare();
			return;
		}
		//decide what kind of tuple received - whether it is a regular tuple or termination tuple
		//call appropriate method of EPAManagerFacade
		logger.debug("EPAManagerBolt: execute : passing tuple : "+ input+" for processing...");
		String agentName = (String)input.getValueByField(STORMMetadataFacade.AGENT_NAME_FIELD);
		String contextPartition = (String)input.getValueByField(STORMMetadataFacade.CONTEXT_PARTITION_FIELD);
		Map<String,Object> segmentationValues = (Map<String,Object>)input.getValueByField(STORMMetadataFacade.CONTEXT_SEGMENTATION_VALUES);

		String eventTypeName = (String)input.getValueByField(EventHeader.NAME_ATTRIBUTE);
		
		try{
			if (!eventTypeName.equals(STORMMetadataFacade.TERMINATOR_EVENT_NAME))
			{
				//real event and not just termination notification
				IEventInstance event = metadataFacade.createEventFromTuple(input);
				logger.debug("EPAManagerBolt: execute : created event instance  : "+ event+" from tuple "+input+", for agent: "+agentName+" and context partition "+contextPartition+", passing for processing to EPA manager...");
				Set<Pair<String,Map<String,Object>>> partitionsToProcess = new HashSet<Pair<String,Map<String,Object>>>();
				partitionsToProcess.add(new Pair<String,Map<String,Object>>(contextPartition,segmentationValues));
				facadesManager.getEpaManager().processEvent(event, agentName, partitionsToProcess);
			}else
			{
				logger.debug("EPAManagerBolt: execute : received termination notification for agent: "+ agentName+" and contextPartitition: "+contextPartition+", passing for processing...");
				facadesManager.getEpaManager().processDeffered(agentName, contextPartition, segmentationValues);
			}

		}catch (EPAManagerException e) {		
			e.printStackTrace();
			logger.error("Could not pass event for processing to EPAManager, reason: "+e.getMessage());
			throw new RuntimeException(e);
		}
		_collector.ack(input);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {	
		List<String> fieldNames = new ArrayList<String>();
		fieldNames.add(EventHeader.NAME_ATTRIBUTE);
		fieldNames.add(STORMMetadataFacade.ATTRIBUTES_FIELD);
		logger.debug("EPAManagerBolt: declareOutputFields:declaring stream " +STORMMetadataFacade.EVENT_STREAM+ "with fields "+fieldNames);
		declarer.declareStream(STORMMetadataFacade.EVENT_STREAM, new Fields(fieldNames));
		
	}

	public OutputCollector get_collector() {
		return _collector;
	}

	public void setFacadesManager(FacadesManager facadesManager) {
		this.facadesManager = facadesManager;
	}

	public void setMetadataFacade(STORMMetadataFacade metadataFacade) {
		this.metadataFacade = metadataFacade;
	}
	
}
