///*******************************************************************************
// * Copyright 2015 IBM
// * 
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// * 
// *   http://www.apache.org/licenses/LICENSE-2.0
// * 
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// ******************************************************************************/
//package com.ibm.hrl.proton;
//
//import backtype.storm.Config;
//import backtype.storm.spout.SpoutOutputCollector;
//import backtype.storm.task.TopologyContext;
//import backtype.storm.topology.OutputFieldsDeclarer;
//import backtype.storm.topology.base.BaseRichSpout;
//import backtype.storm.tuple.Fields;
//import backtype.storm.tuple.Values;
//import com.ibm.hrl.proton.routing.STORMMetadataFacade;
//import com.ibm.hrl.proton.utilities.containers.Pair;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.BufferedReader;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.nio.charset.Charset;
//import java.text.SimpleDateFormat;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//
//
//public class TestProtonSpout extends BaseRichSpout {
//	private boolean done;
//	public static final String CALL_DATA_READ = "CallData";	
//	private static final String ATTR_PHONE_NUMBER = "phoneNumber";
//	private static final String CALL_DURATION = "callDuration";
//	private static final String NODE_ID = "nodeID";
//	private static final String ATTR_TIMESTAMP = "timestamp";
//	
//	
//	private static final int ATTR_PHONE_NUMBER_INDEX = 0;
//	private static final int ATTR_CALL_DURATION_INDEX = 1;
//	private static final int ATTR_NODE_ID_INDEX = 2;
//	private static final int ATTR_TIMESTAMP_INDEX = 3;
//		
//	//total expected number of fields in a csv line. Assuming here that the length histogram is the ending part of csv
//	private static final int NUM_FIELDS = 4;
//	
//	private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss"); 
//	
//	public static Logger LOG = LoggerFactory.getLogger(TestProtonSpout.class);
//	boolean _isDistributed;
//	SpoutOutputCollector _collector;
//	private BufferedReader eventReader;
//	private String file;
//	private long prevTimestamp;
//	
//	public TestProtonSpout(String fileName) 
//	{
//	        this(true,fileName);
//	}
//	
//	public TestProtonSpout(boolean isDistributed, String file) 
//	{
//	        _isDistributed = isDistributed;
//	        this.file = file;
//	        prevTimestamp = 0;
//	        
//	}
//	
//	
//	public void open(Map conf, TopologyContext context,
//			SpoutOutputCollector collector) {
//		 _collector = collector;
//		 try {
//				eventReader = new BufferedReader(new FileReader(file));
//			} catch (FileNotFoundException e) {
//				throw new RuntimeException("could not open input file: "+file+" for reading, file not found");
//			}
//		
//	}
//
//	public Pair<Values,Long> fromBytes(byte[] bytes) {
//		String name = CALL_DATA_READ;
//
//		try {
//
//			String[] tuple = new String(bytes).split(",");
//			
//			int tupleLength = tuple.length;
//			
//			//extend to expected length padding the rest with nulls
//			if(tupleLength < NUM_FIELDS){
//				String[] padded = Arrays.copyOf(tuple, NUM_FIELDS);
//				
//				Arrays.fill(padded, tupleLength, NUM_FIELDS-1, "0");
//				
//				tuple = padded;
//			}
//
//			
//			long timestamp = Long.valueOf(tuple[ATTR_TIMESTAMP_INDEX]);
//
//			HashMap<String, Object> attrMap = new HashMap<String, Object>();
//			
//			attrMap.put(ATTR_PHONE_NUMBER, tuple[ATTR_PHONE_NUMBER_INDEX]);
//			attrMap.put(CALL_DURATION, Integer.parseInt(tuple[ATTR_CALL_DURATION_INDEX]));
//			attrMap.put(NODE_ID, tuple[ATTR_NODE_ID_INDEX]);
//			attrMap.put(ATTR_TIMESTAMP, timestamp);
//			
//			
//			return new Pair<Values,Long>(new Values(name, attrMap), timestamp);
//
//		} catch (Exception e) {
//			throw new ParsingError(
//					"Error parsing CSV for aggregated traffic reading", e);
//		}
//	}
//	
//	private Pair<Values,Long> nextEvent() throws Exception {		
//		
//		String line = eventReader.readLine();
//
//		if (line != null) {
//			String csv = new String(line.getBytes(
//					Charset.forName("UTF-8")), "UTF-8");				
//			Pair<Values,Long> pair =  fromBytes(csv.getBytes(Charset.forName("UTF-8")));
//			long timestamp  = pair.getSecondValue();
//			Values values = pair.getFirstValue();
//			
//			long delayMillis = prevTimestamp > 0 ? timestamp - prevTimestamp : 0;
//
//			if (delayMillis >= 0) {
//				prevTimestamp = timestamp;
//			} else {
//				delayMillis = 0;
//				// leave prevTimestamp as its last value - either this
//				// is the 1st event, or the current event has earlier
//				// timestamp than the previous one
//			}
//			return new Pair<Values,Long>(values,delayMillis);
//		}
//		else {
//			return null;
//		}
//	}
//	
//	public void nextTuple() {
//		try {
//			if (!done) {
//				
//				Pair<Values,Long> event= nextEvent();
//
//				if (event == null) {
//					done = true;
//					eventReader.close();
//				
//					return;
//				}
//				
//				Thread.sleep(event.getSecondValue());
//
//				_collector.emit(event.getFirstValue());
//			}
//		} catch (Exception e) {
//			System.out.println("error reading next event");
//		}
//		
//		
//	}
//
//	
//	 
//	
//	public void declareOutputFields(OutputFieldsDeclarer declarer) {
//		 declarer.declare(new Fields("Name",STORMMetadataFacade.ATTRIBUTES_FIELD));
//		
//	}
//	
//	public void ack(Object msgId) {
//		System.out.println("tupple acked");
//    }
//
//    public void fail(Object msgId) {
//    	System.out.println("tupple failed");
//    }
//    
//    @Override
//    public Map<String, Object> getComponentConfiguration() {
//        if(!_isDistributed) {
//            Map<String, Object> ret = new HashMap<String, Object>();
//            ret.put(Config.TOPOLOGY_MAX_TASK_PARALLELISM, 1);
//            return ret;
//        } else {
//            return null;
//        }
//    }    
//
//}
