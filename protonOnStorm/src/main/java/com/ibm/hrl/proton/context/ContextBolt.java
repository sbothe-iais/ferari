/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.ibm.hrl.proton.context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import com.ibm.hrl.proton.agentQueues.exception.AgentQueueException;
import com.ibm.hrl.proton.agentQueues.queuesManagement.AgentQueuesManager;
import com.ibm.hrl.proton.context.facade.ContextServiceFacade;
import com.ibm.hrl.proton.eventHandler.IEventHandler;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.runtime.event.interfaces.IEventInstance;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;


public class ContextBolt extends BaseRichBolt {

	OutputCollector _collector;
	private static final Logger logger = LoggerFactory.getLogger(ContextBolt.class);
	private FacadesManager facadesManager;
	private STORMMetadataFacade metadataFacade;

	public ContextBolt(FacadesManager facadesManager,STORMMetadataFacade metadataFacade) {		
		super();
		logger.debug("ContextBolt constructor start");
		this.facadesManager = facadesManager;
		this.metadataFacade = metadataFacade;
		logger.debug("ContextBolt constructor end");
	}

	public void prepare() {
		IEventHandler stormEventHandler = new StormEventHandler(_collector,metadataFacade,facadesManager);
		facadesManager.setEventHandler(stormEventHandler);
		AgentQueuesManager agentQueuesManager = new AgentQueuesManager(facadesManager.getTimerServiceFacade(), stormEventHandler, facadesManager.getWorkManager(),metadataFacade.getMetadataFacade());
		facadesManager.setAgentQueuesManager(agentQueuesManager);
		try {
			ContextServiceFacade contextServiceFacade = new ContextServiceFacade(facadesManager.getTimerServiceFacade(), stormEventHandler, metadataFacade.getMetadataFacade().getContextMetadataFacade());
			facadesManager.setContextServiceFacade(contextServiceFacade);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not initialize Context bolt, reason : "+e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;

		// make sure the metadata is parsed, only once per JVM and all singeltones initiated
		logger.debug("prepare: initializing ContextBolt with task id..."+context.getThisTaskId());
		if(metadataFacade != null) {
			prepare();
		}
		logger.debug("prepare: done initializing ContextBolt with task id..."+context.getThisTaskId());

	}

	@Override
	public void execute(Tuple input) {
		if(input.getSourceStreamId().equals("metadatatoContext")) {
			String jsonString = (String)input.getValue(1);
			EepFacade eep = null;
			STORMMetadataFacade facade = null;

			try {
				eep = new EepFacade();
			} catch (EEPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				facade = new STORMMetadataFacade(jsonString,eep);
			} catch (ParsingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EEPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			facadesManager = new FacadesManager();
			facadesManager.setEepFacade(eep);

			TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
			facadesManager.setTimerServiceFacade(timerServiceFacade);
			WorkManagerFacade workManagerFacade = new WorkManagerFacade();
			facadesManager.setWorkManager(workManagerFacade);
			metadataFacade = facade;
			if(metadataFacade != null){
				prepare();
			}
			return;
		}
		//get the tuple, remove the agent and context information
		logger.debug("ContextBolt: execute : passing tuple : "+ input+" for processing...");
		IEventInstance eventInstance = metadataFacade.createEventFromTuple(input);		
		try {
			String contextName = (String)input.getValueByField(STORMMetadataFacade.CONTEXT_NAME_FIELD);
			String agentName = (String)input.getValueByField(STORMMetadataFacade.AGENT_NAME_FIELD);
			Set<Pair<String,String>> agentContextSet = new HashSet<Pair<String,String>>();
			Pair<String,String> agentContextPair = new Pair<String,String>(agentName,contextName);
			agentContextSet.add(agentContextPair);
			//System.err.println("ContextBolt: execute : created event instnace: "+eventInstance+" from tuple: "+input+"passing to agent queues for"+agentContextSet+"for processing...");
			facadesManager.getAgentQueuesManager().passEventToQueues(eventInstance, agentContextSet);
			//AgentQueuesManager.getInstance().passEventToQueues(eventInstance);
		} catch (AgentQueueException e) {
			e.printStackTrace();
			//System.err.println("Could not pass event for processing of context service, reason: " +e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		//System.err.println("ContextBolt: execute : done processing tuple "+input);
		_collector.ack(input);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		List<String> fieldNames = new ArrayList<String>();
		fieldNames.add(EventHeader.NAME_ATTRIBUTE);
		fieldNames.add(STORMMetadataFacade.ATTRIBUTES_FIELD);
		//add the agentName and contextName fields
		fieldNames.add(STORMMetadataFacade.AGENT_NAME_FIELD);
		fieldNames.add(STORMMetadataFacade.CONTEXT_PARTITION_FIELD);
		fieldNames.add(STORMMetadataFacade.CONTEXT_SEGMENTATION_VALUES);

		logger.debug("ContextBolt: declareOutputFields:declaring stream " +STORMMetadataFacade.EVENT_STREAM+ "with fields "+fieldNames);
		declarer.declareStream(STORMMetadataFacade.EVENT_STREAM, new Fields(fieldNames));


		//also declare a stream for termination
		/*List<String> terminationFields = new ArrayList<String>();
		terminationFields.add(MetadataFacade.AGENT_NAME_FIELD);
		terminationFields.add(MetadataFacade.CONTEXT_PARTITION_FIELD);
		terminationFields.add(MetadataFacade.CONTEXT_SEGMENTATION_VALUES);
		logger.fine("ContextBolt: declareOutputFields:declaring termination stream " +MetadataFacade.TERMINATION_EVENTS_STREAM+ "with fields "+terminationFields);
		declarer.declareStream(MetadataFacade.TERMINATION_EVENTS_STREAM, new Fields(terminationFields));*/
	}

	public OutputCollector get_collector() {
		return _collector;
	}

	public void setFacadesManager(FacadesManager facadesManager) {
		this.facadesManager = facadesManager;
	}

	public void setMetadataFacade(STORMMetadataFacade metadataFacade) {
		this.metadataFacade = metadataFacade;
	}

}
