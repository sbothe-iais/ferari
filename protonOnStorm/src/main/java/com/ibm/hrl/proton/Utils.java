//package com.ibm.hrl.proton;
//
//import com.ibm.hrl.proton.protonAdapter.Attribute;
//import com.ibm.hrl.proton.protonAdapter.MultiEventCSVFormatter;
//import com.ibm.hrl.proton.utilities.containers.Pair;
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.JSONValue;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.*;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by mofu on 03/11/15.
// */
//public class Utils implements Serializable {
//    static final Logger logger= LoggerFactory.getLogger(Utils.class);
//    public static String createInputContents(String inputFileName) {
//		System.out.println("path: "+ System.getProperty("user.dir"));
//        String line;
//        StringBuilder sb = new StringBuilder();
//        BufferedReader in = null;
//		InputStream inputStream= null;
//		try {
//			inputStream = new FileInputStream(new File(inputFileName));
//		} catch (FileNotFoundException e) {
//			inputStream = Utils.class.getClassLoader().getResourceAsStream(inputFileName);
//		}
//        logger.info("File path is {}",new File(".").getAbsolutePath());
//        try
//        {
//            in = new BufferedReader(new InputStreamReader(inputStream));
//            while ((line = in.readLine()) != null)
//            {
//                sb.append(line);
//            }
//
//        }catch(IOException e )
//        {
//            throw new RuntimeException(e);
//        }
//
//            try {
//                in.close();
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                throw new RuntimeException(e);
//            }
//
//        return sb.toString();
//    }
//    public static void main(String args[]){
//	String _eventName="DEFAULT_EVENT";
//	String _DEFAULT_DELIMITER = ",";
//	String _DEFAULT_DATE_FORMAT =  "dd/MM/yyyy-HH:mm:ss";
//		JSONObject parsed = null;
//			parsed = (JSONObject) JSONValue.parse(createInputContents("DistributedCounter-a11.json"));
//		JSONObject root = (JSONObject) parsed.get("epn");
//	 	JSONArray events = (JSONArray) root.get("events");
//		MultiEventCSVFormatter formatters = new MultiEventCSVFormatter(
//				(JSONArray)((JSONObject)((JSONArray)root.get("producers")).get(0)).get("properties")
//				,(JSONArray)root.get("events"));
//
//			logger.info("Event block parsed.");
//			try {
//				BufferedReader reader = new BufferedReader(new FileReader("/home/mofu/git/ferari/examples/distributedcount/src/test/resources/newestInput.csv"));
//				String input;
//				while (true){
//					input = reader.readLine();
//					if(input == null) break;
//					logger.info("line: {}", input);
//					int firstComma = input.indexOf(',');
//		Map<String,Object> event = formatters.parse(input);
//					logger.info("parsed: {}", event);
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//
//	}
//	private static Map<String, Pair<Attribute.Type, String>> getEventTypes(String eventName,
//			JSONArray events) {
//		Map<String, Pair<Attribute.Type, String>> map = new HashMap<String, Pair<Attribute.Type,String>>();
//		JSONArray attributes;
//		String parsedName;
//		JSONObject eventDescription, eventAttribute;
//		for(Object obj : events){
//			eventDescription = (JSONObject) obj;
//			parsedName= (String) eventDescription.get("name");
//			if(parsedName == null || !parsedName.equals(eventName)) continue;
//			//The correct event is in eventDescription!!
//			String attributeName, defaultValue;
//			Attribute.Type attributeType;
//			attributes = (JSONArray) eventDescription.get("attributes");
//			for(Object obj2 : attributes){
//				eventAttribute=(JSONObject) obj2;
//				attributeName=(String)eventAttribute.get("name");
//				defaultValue=(String)eventAttribute.get("defaultValue");
//				attributeType=Attribute.Type.valueOf((String)eventAttribute.get("type"));
//				map.put(attributeName, new Pair<Attribute.Type, String>(attributeType,defaultValue));
//			}
//			break;//There should be only one event.
//
//		}
//		return map;
//	}
//
//}
