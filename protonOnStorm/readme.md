About
======

This is a variant of the Proton on Storm code found on [GitHub](http://github.com/ishkin/Proton).
It is augmented to our needs by providing statistics of the processed events.