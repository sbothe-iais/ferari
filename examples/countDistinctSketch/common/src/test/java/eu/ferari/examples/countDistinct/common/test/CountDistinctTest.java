package eu.ferari.examples.countDistinct.common.test;

import org.junit.Assert;
import org.junit.Test;

import eu.ferari.examples.countDistinct.common.countdistinct.CountDistinctSetBased;
import eu.ferari.examples.countDistinct.common.countdistinct.CountDistinctSketchBased;

public class CountDistinctTest {

    @Test
    public void testCountDistinctSetBased() {
        CountDistinctSetBased countDistinct = new CountDistinctSetBased();
        countDistinct.add("d1");
        countDistinct.add("d1");
        countDistinct.add("d2");

        Assert.assertEquals(2, countDistinct.getCountDistinct());

    }

    @Test
    public void testCountMergeSetBased() {
        CountDistinctSetBased s1 = new CountDistinctSetBased();
        CountDistinctSetBased s2 = new CountDistinctSetBased();

        s1.add("d1");
        s1.add("d1");
        s1.add("d2");

        s2.add("d2");
        s2.add("d2");
        s2.add("d3");

        s1.mergeWith(s2);

        Assert.assertEquals(3, s1.getCountDistinct());

    }

    @Test
    public void testCountMergeSketchBased() {
        CountDistinctSketchBased s1 = new CountDistinctSketchBased();
        CountDistinctSketchBased s2 = new CountDistinctSketchBased();

        s1.add("d1");
        s1.add("d1");
        s1.add("d2");

        s2.add("d2");
        s2.add("d2");
        s2.add("d3");

        s1.mergeWith(s2);

        Assert.assertEquals(3, s1.getCountDistinct());

    }

    @Test
    public void testCountDistinctSketchBased() {
        CountDistinctSketchBased countDistinct = new CountDistinctSketchBased();
        countDistinct.add("d1");
        countDistinct.add("d1");
        countDistinct.add("d2");

        Assert.assertEquals(2, countDistinct.getCountDistinct());

    }

}
