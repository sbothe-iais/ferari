package eu.ferari.examples.countDistinct.common;

public class countDistinctConstants {
    public final static String USER_COUNTDISTINCT = "AllUserCountDistinct"; // under which all users are counted
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final long MAX_REACH = 50; //reset all counts after global count distinct reaches indicated number of distinct users 
    public static final int NUM_PARRALELL_OPS = 16;

    public static final String INFILE = "INFILE";
    public static final String INFILENAME = "sorted_sample.tsv";

}
