#!/usr/bin/python2
import datetime as date
import random as rand
import hashlib
import sys



tracksets = { "The Range": ["Loftmane", "Everything But", "The One", "Jamie", "Seneca", "FM Myth", "Hamiltonian", "Postie", "Sad Song", "Telescope", "Metal Swing"]
            , "Shy FX feat. UK Apachi": ["Original Nuttah"]
            , "Icicle": ["Arrow"]
            , "The Specials" : ["A Message to you Rudy", "Do the Dog", "It's up to You", "Nite Klub", "Doesn't Make it Allright", "Concrete Jungle", "Too Hot", "Monkey Man", "(Dawning of) A New Era", "Blank Expression", "Stupid Marriage", "Too Much Too Young",  "You're Wondering now", "Gangster"]
            , "Johann Sebastian Bach" : ["BWV 531", "BWV 551", "BWV 543", "BWV 557", "BWV 564", "BWV 578", "BWV 581a", "BWV 565", "BWV 1086", "BWV 1043"]
            , "Fischer Z" : ["Berlin", "Marliese", "Red Skies over Paradise (A Brighton Dream)", "In England", "You'll never Find Brian Here", "Batallion of Strangers", "Song and Dance Brigade", "The Writer", "Bathroom Scenario", "Wristcutters Lullaby", "Cruise Missiles", "Luton to Lisbon / Multinationals Bite"]
            , "Nick Cave and the Bad Seeds": ["We No Who U R", "Wide Lovely Eyes", "Water's Edge", "Jubilee Street", "Mermaids", "We Real Cool", "Finishing Jubilee Street", "Higgs Boson Blues"]
            , "Bit Depth": ["Stalk Them"]
            , "Tom Waits": ["Danny Says", "Return of Jacky and Judy", "I Don't Wanna Grow Up", "Way Down in the Hole"]
            , "Eminem": ["Loose yourself"]
            , "Alabama 3": ["Woke up this Morning"]
            , "Regina Spektor": ["You've got Time"]
            , "AnnenMayKantereit": ["James", "What He Wanted the Most", "Leavin", "Baby, We'll Be Old", "Summer Days don't Die my Friend"]
            , "Santogold": ["L.E.S. Artistes", "You'll Find a Way", "Shove It", "Say Aha", "Creator", "My Superman", "Lights Out", "Starstruck", "Unstoppable", "I'm a Lady", "Anne", "You'll find a Way"]
            , "Fatboy Slim": ["Build it Up, Tear it Down"]
            , "Dillinja": ["Never Believe"]
            , "The Turtles": ["Happy Together"]
            , "Quake": ["Push It", "Dark Air"]
            }


def main():
    entries = []
    num = int(sys.argv[1])
    while len(entries) <= num:
        for user in ["user_000001", "user_000050", "user_000100", "user_002342", "user_001000"]:
            month = rand.randint(1,12)
            day = rand.randint(1,28)  #just to be safe.
            hour = rand.randint(0,23)
            minute = rand.randint(0,59)
            artist = tracksets.keys()[rand.randint(0,len(tracksets)-1)]
            length = len(tracksets[artist])
            start = rand.randint(0,length -1)
            end = rand.randint(length-(start+1),length-1)
            time = date.datetime(2013,month, day, hour, minute)
            for track in tracksets[artist][start: end]:
                entries.append((user, time, formatHash(artist), artist, formatHash(track), track))
                time += date.timedelta(0,0,0,0,rand.randint(3,5))
    entries.sort(key = (lambda x: x[1]))
    
    for e in entries[:num]:
        print "\t".join((e[0],formatDate(e[1]))+e[2:])
            
def formatHash(o):
    string=hashlib.md5(str(o)).hexdigest()
    return "-".join([string[0:8],string[8:12],string[12:16], string[16:20], string[20:]])

def formatDate(d):
    return d.strftime("%Y-%M-%dT%H:%M:%SZ")


if __name__ == "__main__":
    main()
