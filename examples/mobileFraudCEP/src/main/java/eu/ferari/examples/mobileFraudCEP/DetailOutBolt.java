package eu.ferari.examples.mobileFraudCEP;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import eu.ferari.examples.mobileFraudCEP.entity.Proton_cdr;

public class DetailOutBolt extends BaseRichBolt {

	
	private static final long serialVersionUID = 1L;
	private String topologyName;
	private OutputCollector _collector;
	private EntityManager entityManager;
	private EntityManagerFactory emfactory;

	public DetailOutBolt(String topologyName) {
		this.topologyName = topologyName;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;

		if (entityManager == null) {
			emfactory = Persistence.createEntityManagerFactory("ferari");
			entityManager = emfactory.createEntityManager();
		}
	}

	@Override
	public void execute(Tuple input) {

		if (input == null)
			throw new NullPointerException("tuple: null");
		
		String parsedTuple = input.toString();

		parsedTuple = parsedTuple.substring(parsedTuple.indexOf("[") + 1);
		parsedTuple = parsedTuple.substring(parsedTuple.indexOf("{") + 1);
		parsedTuple = parsedTuple.substring(0, parsedTuple.indexOf('}'));

		String[] parsedTupleArray = parsedTuple.split(", ");

		Map<String, String> tupleMap = new HashMap<String, String>();
		for (String item : parsedTupleArray) {
			String[] parsedTupleArray2 = item.split("=");
			tupleMap.put(parsedTupleArray2[0], parsedTupleArray2[1]);
		}
		insertToSql(tupleMap);
	}

	/*
	 * Inserting records to MySql database
	 */
	
	private void insertToSql(Map<String, String> tupleMap) {

		Proton_cdr cdr = new Proton_cdr();
		
		if (tupleMap.containsKey("Name")
				&& (!tupleMap.get("Name").toString().equals("null"))) {
			cdr.setDerived_event_name(tupleMap.get(
					"Name").toString());
		}
		if (tupleMap.containsKey("billed_msdn")
				&& (!tupleMap.get("billed_msdn").toString().equals("null"))) {
			cdr.setBilled_msisdn(tupleMap.get(
					"billed_msdn").toString());
		}
		if (tupleMap.containsKey("other_party_tel_number_prefix")
				&& (!tupleMap.get("other_party_tel_number_prefix").toString().equals("null"))) {
			System.out.println();
			cdr.setOther_party_tel_number_prefix(tupleMap.get(
					"other_party_tel_number_prefix").toString());
		}
		if ((tupleMap.containsKey("other_party_tel_number"))
				&& (!tupleMap.get("other_party_tel_number").toString().equals("null"))) {
			cdr.setOther_party_tel_number(tupleMap.get(
					"other_party_tel_number").toString());
		}
		if (tupleMap.containsKey("call_direction")
				&& (!tupleMap.get("call_direction").toString().equals("null"))) {
			cdr.setCall_direction(tupleMap.get(
					"call_direction").toString());
		}
		if (tupleMap.containsKey("called_number")
				&& (!tupleMap.get("called_number").toString().equals("null"))) {
			cdr.setCalled_number(tupleMap.get(
					"called_number").toString());
		}
		if (tupleMap.containsKey("calling_number")
				&& (!tupleMap.get("calling_number").toString().equals("null"))) {
			cdr.setCalling_number(tupleMap.get(
					"calling_number").toString());
		}
		if (tupleMap.containsKey("conversation_duration")
				&& (!tupleMap.get("conversation_duration").toString().equals("null"))) {
			cdr.setConversation_duration(Integer
					.parseInt(tupleMap.get("conversation_duration").toString()));
		}
		if (tupleMap.containsKey("tap_related")
				&& (!tupleMap.get("tap_related").toString().equals("null"))) {
			cdr.setTap_related(tupleMap.get(
					"tap_related").toString());
		}
		if (tupleMap.containsKey("total_call_charge_amount")
				&& (!tupleMap.get("total_call_charge_amount").toString().equals("null"))) {
			cdr.setTotal_call_charge_amount((Double) Double
					.parseDouble(tupleMap.get("total_call_charge_amount").toString()));
		}
		if (tupleMap.containsKey("call_start_date")
				&& (!tupleMap.get("call_start_date").toString().equals("null"))) {
			cdr.setCall_start_date(new java.sql.Timestamp((Long) Long
					.parseLong(tupleMap.get("call_start_date").toString())));
		}
		if (tupleMap.containsKey("call_start_time")
				&& (!tupleMap.get("call_start_time").toString().equals("null"))) {
			cdr.setCall_start_time(Integer.parseInt(tupleMap.get("call_start_time").toString()));
		}
		if (tupleMap.containsKey("object_id")
				&& (!tupleMap.get("object_id").toString().equals("null"))) {
			cdr.setObject_id(new BigInteger(tupleMap.get(
					"object_id").toString()));
		}
		
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		cdr.setInsertion_datetime(now);

		//generating cell_id artificially, which will connect to artificial coordinates in the base
		Random r = new Random();
		int randomInt = r.nextInt(300) + 1;
		cdr.setCall_cell_id(randomInt);

		entityManager.getTransaction().begin();
		entityManager.persist(cdr);
		entityManager.getTransaction().commit();

	}

	@Override
	public void cleanup() {
		entityManager.close();
		emfactory.close();
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}

}
