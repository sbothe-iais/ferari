/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.ferari.examples.mobileFraudCEP;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import json.java.JSONObject;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

import eu.ferari.examples.mobileFraudCEP.entity.Derived_event;

import eu.ferari.examples.mobileFraudCEP.entity.Derived_event_details;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;

import json.java.JSONArray;

public class FrontEndOutputBolt extends BaseRichBolt {

	private EntityManager entityManager;
	private EntityManagerFactory emfactory;

	private static final long serialVersionUID = 8193070886394233027L;

	public static final String REDIS_SERVER_IP = "localhost";
	public static final int REDIS_SERVER_PORT = 6379;

	OutputCollector _collector;
	BufferedWriter out;
	private static final String LINE_SEPARATOR = System
			.getProperty("line.separator");
	private String topologyName;

	RedisConnection<String, String> redis;

	public FrontEndOutputBolt(String topologyName) {
		this.topologyName = topologyName;
	}

	@SuppressWarnings("rawtypes")
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {

		if (entityManager == null) {
			emfactory = Persistence.createEntityManagerFactory("ferari");
			entityManager = emfactory.createEntityManager();
		}

		this._collector = _collector;
		String fileName = "/home/proton/artifacts/" + this.topologyName
				+ "output.txt";
		File file = new File(fileName);

		RedisClient client = new RedisClient(REDIS_SERVER_IP, REDIS_SERVER_PORT);
		redis = client.connect();

		try {
			boolean fileExists = file.createNewFile();
			FileWriter fstream = new FileWriter(fileName);
			out = new BufferedWriter(fstream);
		} catch (IOException e) {
			e.printStackTrace();
		} // we create a file in case doesn't exist*/
	}

	@SuppressWarnings("unchecked")
	static <K, V> List<Object> collectValues(Map<K, V> map) {

		List<V> list = new ArrayList<V>();
		for (Map.Entry<K, V> e : map.entrySet())
			list.add(e.getValue());

		return (List<Object>) list;
	}

	public void execute(Tuple input) {

		if (input == null)
			throw new NullPointerException("tuple: null");

		try {

			String eventTypeName = (String) input
					.getValueByField(EventHeader.NAME_ATTRIBUTE);

			if (eventTypeName != null && !eventTypeName.contains("CallPOPDWH")) {

				@SuppressWarnings("unchecked")
				Map<String, Object> attributes = (Map<String, Object>) input
						.getValueByField(STORMMetadataFacade.ATTRIBUTES_FIELD);

				List<Object> listToSql;
				listToSql = collectValues(attributes);

				if ((listToSql.get(0)).toString().charAt(0) == '[') {
					insertToMySQLMultiDates(attributes, listToSql);
				} else {
					insertToMysqlOneDate(attributes, listToSql);
				}
				listToSql = null;

				JSONObject jsonObjToRedis = new JSONObject();
				List<String> listValuesFromMap = new ArrayList<String>();

				for (Map.Entry<String, Object> entry : attributes.entrySet()) {
					if (!entry.getKey().contains("EventId")
							&& !entry.getKey().contains("Annotation")
							&& !entry.getKey().contains("Chronon")
							&& !entry.getKey().contains("EventSource")
							&& !entry.getKey().contains("ExpirationTime")) {
						System.out.println();

						if (entry.getKey().contains("call_start_dates")) {

							JSONArray jsonArray1 = new JSONArray();
							listValuesFromMap.add((String) entry.getValue()
									.toString());

							for (String item : listValuesFromMap) {
								jsonArray1.add(item.toString());
							}
							jsonObjToRedis.put(entry.getKey(), jsonArray1);
						}

						else {
							listValuesFromMap.add((String) entry.getValue()
									.toString());
							jsonObjToRedis.put(entry.getKey(), entry.getValue());
						}
					}
				}

				redis.publish("fraudEvents", jsonObjToRedis.toString());
				out.write(input.toString() + LINE_SEPARATOR);
				out.flush();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 *  Inserting to MySql database records containing multiple dates
	 */
	
	private void insertToMySQLMultiDates(Map<String, ?> attributes,
			List<Object> listToSql) {

		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();

		String multipleDates = listToSql.get(0).toString();
		multipleDates = multipleDates.substring(1, multipleDates.length() - 1);
		String[] arrayDates = multipleDates.split(", ");

		int arrayDatesSize = arrayDates.length;

		Derived_event derivedEventMultiDates = new Derived_event();

		if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
				&& (!attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
						.equals("null"))) {
			derivedEventMultiDates.setDerived_event_name(attributes.get(
					EventHeader.NAME_ATTRIBUTE).toString());
		}
		if (attributes.containsKey(EventHeader.CERTAINTY_ATTRIBUTE)
				&& !attributes.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()
						.equals("null")) {
			derivedEventMultiDates.setCertainty((Double) Double
					.parseDouble(attributes
							.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()));
		}
		if (attributes.containsKey(EventHeader.COST_ATTRIBUTE)
				&& !attributes.get(EventHeader.COST_ATTRIBUTE).toString()
						.equals("null")) {
			derivedEventMultiDates.setCost((Double) Double
					.parseDouble(attributes.get(EventHeader.COST_ATTRIBUTE)
							.toString()));
		}
		if (attributes.containsKey(EventHeader.DURATION_ATTRIBUTE)
				&& !attributes.get(EventHeader.DURATION_ATTRIBUTE).toString()
						.equals("null")) {
			derivedEventMultiDates.setDuration((Double) Double
					.parseDouble(attributes.get(EventHeader.DURATION_ATTRIBUTE)
							.toString()));
		}
		if (attributes.containsKey("call_direction")
				&& !attributes.get("call_direction").toString().equals("null")) {
			derivedEventMultiDates.setCall_direction(attributes.get(
					"call_direction").toString());
		}
		if (attributes.containsKey("calling_number")
				&& !attributes.get("calling_number").toString().equals("null")) {
			derivedEventMultiDates.setCalling_number(attributes.get(
					"calling_number").toString());
		}
		if (attributes.containsKey(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				&& !attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
						.toString().equals("null")) {
			derivedEventMultiDates
					.setOccurence_time(new java.sql.Timestamp((Long) attributes
							.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
		}
		if (attributes.containsKey("CallsCount")
				&& !attributes.get("CallsCount").toString().equals("null")) {
			double callCount = (Double) attributes.get("CallsCount");
			derivedEventMultiDates.setCalls_count((int) callCount);
		}
		if (attributes.containsKey(EventHeader.EXPIRATION_TIME_ATTRIBUTE)
				&& attributes.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE) != null) {
			derivedEventMultiDates.setExpiration_time(new java.sql.Timestamp(
					(Long) attributes
							.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE)));
		}
		derivedEventMultiDates.setInsertion_datetime(now);

		Long callStartDate = Long.parseLong(arrayDates[0].toString());
		if (callStartDate != null) {
			derivedEventMultiDates.setCall_start_date(new java.sql.Timestamp(
					callStartDate));
		}

		entityManager.getTransaction().begin();
		entityManager.persist(derivedEventMultiDates);

		for (int i = 1; i < (arrayDatesSize + 1); i++) {

			Derived_event_details tableDetails = new Derived_event_details();

			if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
					&& !attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
							.equals(null)) {
				tableDetails.setDerived_event_name(attributes.get(
						EventHeader.NAME_ATTRIBUTE).toString());
			}
			if (attributes.containsKey("calling_number")
					&& !attributes.get("calling_number").toString()
							.equals(null)) {
				tableDetails.setCalling_number(attributes.get("calling_number")
						.toString());
			}
			tableDetails.setInsertion_datetime(now);
			if (!attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
					.toString().equals("null")) {
				tableDetails.setOccurence_time(new java.sql.Timestamp(
						(Long) attributes
								.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
			}

			Long callStartTime = Long.parseLong(arrayDates[i - 1].toString());
			if (callStartTime != null) {
				tableDetails.setCall_start_date(new Date(callStartTime));
			}

			entityManager.persist(tableDetails);
			if (i == arrayDatesSize) {
				entityManager.getTransaction().commit();
			}
		}
	}

	/*
	 *  Inserting to MySql database records containing one date
	 */
	
	private void insertToMysqlOneDate(Map<String, ?> attributes,
			List<Object> listToSql) {

		Derived_event derivedEventOneDate = new Derived_event();

		if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
				&& (!attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
						.equals("null"))) {
			derivedEventOneDate.setDerived_event_name(attributes.get(
					EventHeader.NAME_ATTRIBUTE).toString());
		}
		if (attributes.containsKey(EventHeader.CERTAINTY_ATTRIBUTE)
				&& !attributes.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()
						.equals("null")) {
			derivedEventOneDate.setCertainty((Double) Double
					.parseDouble(attributes
							.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()));
		}
		derivedEventOneDate.setOther_party_tel_number(listToSql.get(6)
				.toString());
		if (attributes.containsKey(EventHeader.COST_ATTRIBUTE)
				&& !attributes.get(EventHeader.COST_ATTRIBUTE).toString()
						.equals("null")) {
			derivedEventOneDate.setCost((Double) Double.parseDouble(attributes
					.get(EventHeader.COST_ATTRIBUTE).toString()));
		}
		if (attributes.containsKey(EventHeader.DURATION_ATTRIBUTE)
				&& !attributes.get(EventHeader.DURATION_ATTRIBUTE).toString()
						.equals("null")) {
			derivedEventOneDate.setDuration((Double) Double
					.parseDouble(attributes.get(EventHeader.DURATION_ATTRIBUTE)
							.toString()));
		}
		if (attributes.containsKey("call_direction")
				&& !attributes.get("call_direction").toString().equals("null")) {
			derivedEventOneDate.setCall_direction(attributes.get(
					"call_direction").toString());
		}
		if (attributes.containsKey("calling_number")
				&& !attributes.get("calling_number").toString().equals("null")) {
			derivedEventOneDate.setCalling_number(attributes.get(
					"calling_number").toString());
		}
		if (attributes.containsKey("called_number")
				&& !attributes.get("called_number").toString().equals("null")) {
			derivedEventOneDate.setCalled_number(attributes
					.get("called_number").toString());
		}
		derivedEventOneDate.setCall_start_date(new java.sql.Timestamp(
				(Long) listToSql.get(0)));
		derivedEventOneDate.setCalls_count(1);

		if (attributes.containsKey(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				&& !attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
						.toString().equals("null")) {
			derivedEventOneDate
					.setOccurence_time(new java.sql.Timestamp((Long) attributes
							.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
		}
		if (attributes.containsKey("conversation_duration")
				&& !attributes.get("conversation_duration").toString()
						.equals("null")) {
			derivedEventOneDate.setConversation_duration(Integer
					.parseInt(attributes.get("conversation_duration")
							.toString()));
		}
		derivedEventOneDate.setCalls_length_sum(Integer.parseInt(attributes
				.get("conversation_duration").toString()));

		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		derivedEventOneDate.setInsertion_datetime(now);

		if (attributes.containsKey(EventHeader.EXPIRATION_TIME_ATTRIBUTE)) {
			if (attributes.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE) != null) {
				derivedEventOneDate.setExpiration_time(new Date(
						(Long) attributes
								.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE)));
			}
		}

		Derived_event_details tableDetails = new Derived_event_details();

		if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
				&& (!attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
						.equals("null"))) {
			tableDetails.setDerived_event_name(attributes.get(
					EventHeader.NAME_ATTRIBUTE).toString());
		}
		if (attributes.containsKey("calling_number")
				&& (!attributes.get("calling_number").toString().equals("null"))) {
			tableDetails.setCalling_number(attributes.get("calling_number")
					.toString());
		}
		tableDetails.setInsertion_datetime(now);

		if (attributes.containsKey(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				&& !attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
						.toString().equals("null")) {
			tableDetails
					.setOccurence_time(new java.sql.Timestamp((Long) attributes
							.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
		}
		tableDetails.setCall_start_date(new java.sql.Timestamp((Long) listToSql
				.get(0)));

		entityManager.getTransaction().begin();
		entityManager.persist(tableDetails);
		entityManager.persist(derivedEventOneDate);
		entityManager.getTransaction().commit();

	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}

	@Override
	public void cleanup() {
		entityManager.close();
		emfactory.close();
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		return null;
	}

}
