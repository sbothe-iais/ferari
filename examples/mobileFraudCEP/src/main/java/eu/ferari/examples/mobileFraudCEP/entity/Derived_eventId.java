package eu.ferari.examples.mobileFraudCEP.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * @author proton
 * 
 * Class declaring compound primary keys, related to the class Derived_event
 * 
 */

public class Derived_eventId implements Serializable {

	private static final long serialVersionUID = 1L;
	Date insertion_datetime;
	Date call_start_date;
	Integer conversation_duration;

	public int hashCode() {
		return getInsertion_datetime().hashCode()
				+ getCall_start_date().hashCode()
				+ getConversation_duration().hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Derived_event))
			return false;
		return getInsertion_datetime().equals(
				((Derived_event) obj).getInsertion_datetime());
	}

	public Date getInsertion_datetime() {
		return insertion_datetime;
	}

	public void setInsertion_datetime(Date insertion_datetime) {
		this.insertion_datetime = insertion_datetime;
	}

	public Date getCall_start_date() {
		return call_start_date;
	}

	public void setCall_start_date(Date call_start_date) {
		this.call_start_date = call_start_date;
	}

	public Integer getConversation_duration() {
		return conversation_duration;
	}

	public void setConversation_duration(Integer conversation_duration) {
		this.conversation_duration = conversation_duration;
	}
}
