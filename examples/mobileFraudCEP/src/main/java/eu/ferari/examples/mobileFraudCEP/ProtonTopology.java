/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.ferari.examples.mobileFraudCEP;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;

import com.ibm.hrl.proton.agents.EPAManagerBolt;
import com.ibm.hrl.proton.context.ContextBolt;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.routing.RoutingBolt;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;

public class ProtonTopology {

	static String jsonFileName = "/home/proton/www/derivedEvents/MobileFraud.json";

	public static void main(String[] args) throws Exception {

		final String INPUT_NAME = "input";
		final String ROUTING_BOLT_NAME = "routingBolt";
		final String CONTEXT_BOLT_NAME = "contextBolt";
		final String EPA_MANAGER_BOLT_NAME = "epaManagerBolt";
		final String OUTPUT_BOLT = "outputBolt";
		final String DETAIL_OUT_BOLT = "detailOutputputBolt";

		String topologyName = "test";

		String inputFileName = "/home/proton/www/derivedEvents/ferari_POP_CDR_export_sample_20146.txt";

		TopologyBuilder builder = new TopologyBuilder();

		if (args != null && args.length == 1) {
			topologyName = args[0];
			System.out.println("Submitting topology by name: " + topologyName);
		}

		String jsonTxt = buildJSON(jsonFileName);

		EepFacade eep = new EepFacade();

		STORMMetadataFacade facade;
		facade = new STORMMetadataFacade(jsonTxt, eep);

		FacadesManager facadesManager = new FacadesManager();
		facadesManager.setEepFacade(eep);

		TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
		facadesManager.setTimerServiceFacade(timerServiceFacade);

		WorkManagerFacade workManagerFacade = new WorkManagerFacade();
		facadesManager.setWorkManager(workManagerFacade);

		builder.setSpout(INPUT_NAME, new TestProtonSpout(inputFileName,
				jsonFileName));

		builder.setBolt(ROUTING_BOLT_NAME,
				new RoutingBolt(facadesManager, facade))
				.shuffleGrouping(INPUT_NAME)
				.shuffleGrouping(EPA_MANAGER_BOLT_NAME,
						STORMMetadataFacade.EVENT_STREAM);

		builder.setBolt(CONTEXT_BOLT_NAME,
				new ContextBolt(facadesManager, facade)).fieldsGrouping(
				ROUTING_BOLT_NAME,
				STORMMetadataFacade.EVENT_STREAM,
				new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,
						STORMMetadataFacade.CONTEXT_NAME_FIELD));

		builder.setBolt(EPA_MANAGER_BOLT_NAME,
				new EPAManagerBolt(facadesManager, facade)).fieldsGrouping(
				CONTEXT_BOLT_NAME,
				STORMMetadataFacade.EVENT_STREAM,
				new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,
						STORMMetadataFacade.CONTEXT_PARTITION_FIELD));

		builder.setBolt(OUTPUT_BOLT, new FrontEndOutputBolt(topologyName))
				.shuffleGrouping(ROUTING_BOLT_NAME,
						STORMMetadataFacade.CONSUMER_EVENTS_STREAM);

		builder.setBolt(DETAIL_OUT_BOLT, new DetailOutBolt(topologyName))
				.shuffleGrouping(INPUT_NAME);

		Config conf = new Config();
		conf.setDebug(true);
		conf.setNumWorkers(2);

		if (args != null && args.length > 1) {
			conf.setNumWorkers(3);

			StormSubmitter.submitTopology(args[1], conf,
					builder.createTopology());
		}

		else {
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology(topologyName, conf, builder.createTopology());
			Utils.sleep(10000);
			// cluster.killTopology("test");
			// cluster.shutdown();
		}
	}

	private static String buildJSON(String metadataFileName) {
		String line;
		StringBuilder sb = new StringBuilder();
		BufferedReader in = null;
		String jsonFileName = metadataFileName;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(
					jsonFileName), "UTF-8"));
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String jsonTxt = sb.toString();
		return jsonTxt;
	}
}
