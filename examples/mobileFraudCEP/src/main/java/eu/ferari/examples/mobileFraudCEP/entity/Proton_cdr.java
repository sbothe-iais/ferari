package eu.ferari.examples.mobileFraudCEP.entity;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;



/**
 * @author proton
 *
 * JPA eu.ferari.mobileFraudCEP.entity class
 * 
 */

@Entity @IdClass(Proton_cdrId.class)
@Table(name="PROTON_CDR")
public class Proton_cdr {	
	
	@Id
	private Date insertion_datetime;
	@Id
	private Date call_start_date;
	@Id 
	private Integer call_cell_id;
	private String derived_event_name;
	private String other_party_tel_number;
	private String other_party_tel_number_prefix;
	private BigInteger object_id;
	private String billed_msisdn;
	private Integer call_start_time;
	private String calling_number;
	private String called_number;
	private String call_direction;
	private String tap_related;
	private Double conversation_duration;
	private Double total_call_charge_amount;
	
	
	public String getCalled_number() {
		return called_number;
	}

	public void setCalled_number(String called_number) {
		this.called_number = called_number;
	}

	public String getBilled_msisdn() {
		return billed_msisdn;
	}

	public void setBilled_msisdn(String billed_msisdn) {
		this.billed_msisdn = billed_msisdn;
	}

	public String getOther_party_tel_number() {
		return other_party_tel_number;
	}

	public void setOther_party_tel_number(String other_party_tel_number) {
		this.other_party_tel_number = other_party_tel_number;
	}

	public String getCalling_number() {
		return calling_number;
	}

	public void setCalling_number(String calling_number) {
		this.calling_number = calling_number;
	}

	public String getCall_direction() {
		return call_direction;
	}

	public void setCall_direction(String call_direction) {
		this.call_direction = call_direction;
	}

	public double getConversation_duration() {
		return conversation_duration;
	}

	public void setConversation_duration(double conversation_duration) {
		this.conversation_duration = conversation_duration;
	}

	public String getTap_related() {
		return tap_related;
	}

	public void setTap_related(String tap_related) {
		this.tap_related = tap_related;
	}

	public Date getInsertion_datetime() {
		return insertion_datetime;
	}

	public void setInsertion_datetime(Date insertion_datetime) {
		this.insertion_datetime = insertion_datetime;
	}

	public Date getCall_start_date() {
		return call_start_date;
	}

	public void setCall_start_date(Date call_start_date) {
		this.call_start_date = call_start_date;
	}

	public double getTotal_call_charge_amount() {
		return total_call_charge_amount;
	}

	public void setTotal_call_charge_amount(double total_call_charge_amount) {
		this.total_call_charge_amount = total_call_charge_amount;
	}

	public int getCall_cell_id() {
		return call_cell_id;
	}

	public void setCall_cell_id(int call_cell_id) {
		this.call_cell_id = call_cell_id;
	}

	public String getDerived_event_name() {
		return derived_event_name;
	}

	public void setDerived_event_name(String derived_event_name) {
		this.derived_event_name = derived_event_name;
	}

	public String getOther_party_tel_number_prefix() {
		return other_party_tel_number_prefix;
	}

	public void setOther_party_tel_number_prefix(
			String other_party_tel_number_prefix) {
		this.other_party_tel_number_prefix = other_party_tel_number_prefix;
	}

	public int getCall_start_time() {
		return call_start_time;
	}

	public void setCall_start_time(int call_start_time) {
		this.call_start_time = call_start_time;
	}

	public BigInteger getObject_id() {
		return object_id;
	}

	public void setObject_id(BigInteger object_id) {
		this.object_id = object_id;
	}
	
}
