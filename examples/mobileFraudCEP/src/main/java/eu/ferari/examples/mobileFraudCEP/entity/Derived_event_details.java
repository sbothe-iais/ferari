package eu.ferari.examples.mobileFraudCEP.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;



/**
 * @author Miroslav Mrak
 *
 * JPA eu.ferari.mobileFraudCEP.entity class
 * 
 */

@Entity @IdClass(Derived_event_detailsId.class)
@Table (name = "DERIVED_EVENT_DETAILS")
public class Derived_event_details {
	
	@Id
	private Date insertion_datetime;
	@Id
	private Date call_start_date;
	private String derived_event_name;
	private Date occurence_time;
	private String calling_number;
	
	public Date getCall_start_date() {
		return call_start_date;
	}
	
	public void setCall_start_date(Date call_start_date) {
		this.call_start_date = call_start_date;
	}
	
	public String getCalling_number() {
		return calling_number;
	}
	
	public void setCalling_number(String calling_number) {
		this.calling_number = calling_number;
	}

	public String getDerived_event_name() {
		return derived_event_name;
	}

	public void setDerived_event_name(String derived_event_name) {
		this.derived_event_name = derived_event_name;
	}

	public Date getInsertion_datetime() {
		return insertion_datetime;
	}

	public void setInsertion_datetime(Date insertion_datetime) {
		this.insertion_datetime = insertion_datetime;
	}

	public Date getOccurence_time() {
		return occurence_time;
	}

	public void setOccurence_time(Date occurence_time) {
		this.occurence_time = occurence_time;
	}
}
