/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.ferari.examples.mobileFraudCEP;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mortbay.log.Log;

import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.metadata.event.IEventType;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.metadata.type.TypeAttribute;
import com.ibm.hrl.proton.metadata.type.enums.AttributeTypesEnum;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.runtime.metadata.IMetadataFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;

public class TestProtonSpout extends BaseRichSpout {
	
	private boolean done;

	public static Logger LOG = Logger.getLogger(TestProtonSpout.class);
	boolean _isDistributed;
	SpoutOutputCollector _collector;
	private BufferedReader eventReader;
	private String file;
	private static final Long SENDING_DELAY = new Long(3000);
	private static final String DELIMETER = ";";
	private static final String TAG_SEPARATOR = "=";
	private static final String NULL_STRING = "null";
	private static final String dateFormat = "dd/MM/yyyy-HH:mm:ss";

	private IMetadataFacade metadataFacade;
	private EepFacade eep;
	private DateFormat dateFormatter;

	public TestProtonSpout(String fileName, String jsonFileName) {
		this(true, fileName, jsonFileName);
	}

	public TestProtonSpout(boolean isDistributed, String file,
			String jsonFileName) {
		_isDistributed = isDistributed;

		this.file = file;
		try {
			buildMetadataFacade(jsonFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("could not process input file: " + file
					+ " ,reason" + e.getMessage());
		}
	}

	public void buildMetadataFacade(String jsonfileName)
			throws ParsingException, EEPException {
		this.eep = new EepFacade();
		this.metadataFacade = new STORMMetadataFacade(buildJSON(jsonfileName),
				this.eep).getMetadataFacade();
		this.dateFormatter = new SimpleDateFormat(dateFormat);
	}

	private String buildJSON(String metadataFileName) {

		String line;
		StringBuilder sb = new StringBuilder();
		BufferedReader in = null;
		String jsonFileName = metadataFileName;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(
					jsonFileName), "UTF-8"));
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String jsonTxt = sb.toString();
		return jsonTxt;
	}

	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		_collector = collector;
		try {
			eventReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			throw new RuntimeException("could not open input file: " + file
					+ " for reading, file not found");
		}
	}

	private String getAttributeStringValue(TypeAttribute eventTypeAttribute,
			String attrStringValue) {
		AttributeTypesEnum attrType = eventTypeAttribute.getTypeEnum();
		if (attrType.equals(AttributeTypesEnum.STRING)
				|| eventTypeAttribute.getDimension() > 0) {
			return "'" + attrStringValue + "'";
		}
		return attrStringValue;
	}

	public Pair<Values, Long> fromBytes(String eventText)
			throws ParsingException {

		int nameIndex = eventText.indexOf(EventHeader.NAME_ATTRIBUTE);
		String substring = eventText.substring(nameIndex);
		int delimiterIndex = substring.indexOf(DELIMETER);
		int tagDataSeparatorIndex = substring.indexOf(TAG_SEPARATOR);
		String nameValue = substring.substring(tagDataSeparatorIndex + 1,
				delimiterIndex);
		IEventType eventType = metadataFacade.getEventMetadataFacade()
				.getEventType(nameValue);
		Map<String, Object> attrValues = new HashMap<String, Object>();
		String[] tagValuePairs = eventText.split(DELIMETER);

		for (String tagValue : tagValuePairs) {

			String[] separatedPair = tagValue.split(TAG_SEPARATOR);
			String attrName = separatedPair[0].trim(); // the tag is always the
														// first in the pair
			String attrStringValue = separatedPair[1].trim();

			if (attrName.equals(NULL_STRING) || attrName.equals("")) {
				throw new ParsingException("Could not parse the event string "
						+ eventText + ", reason: Attribute name not specified");
			}

			if (attrStringValue.equals(NULL_STRING)
					|| attrStringValue.equals("")) {
				attrValues.put(attrName, null);
				continue;
			}

			TypeAttribute eventTypeAttribute = eventType.getTypeAttributeSet()
					.getAttribute(attrName);
			attrStringValue = getAttributeStringValue(eventTypeAttribute,
					attrStringValue);
			Object attrValueObject;

			try {
				attrValueObject = TypeAttribute.parseConstantValue(
						attrStringValue, attrName, eventType, dateFormatter,
						this.eep);
			} catch (Exception e) {
				throw new ParsingException("Could not parse the event string"
						+ eventText + ", reason: " + e.getMessage());
			}
			attrValues.put(attrName, attrValueObject);
		}

		Log.info("TestProtonSpout: fromBytes: injecting event " + nameValue
				+ "with attributes" + attrValues);
		return new Pair<Values, Long>(new Values(nameValue, attrValues),
				SENDING_DELAY);
	}

	private Pair<Values, Long> nextEvent() throws Exception {
		String line = eventReader.readLine();
		if (line != null) {
			String eventLine = new String(line.getBytes(Charset
					.forName("UTF-8")), "UTF-8");
			Pair<Values, Long> pair = fromBytes(eventLine);
			return pair;
		} else {
			return null;
		}
	}

	public void nextTuple() {
		try {
			if (!done) {
				Pair<Values, Long> event = nextEvent();

				if (event == null) {
					done = true;
					eventReader.close();
					return;
				}

				Thread.sleep(event.getSecondValue());
				_collector.emit(event.getFirstValue());
			}
		} catch (Exception e) {
			System.out.println("error reading next event");
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("Name",
				STORMMetadataFacade.ATTRIBUTES_FIELD));
	}
	
	public void ack(Object msgId) {
		System.out.println("tupple acked");
	}

	public void fail(Object msgId) {
		System.out.println("tupple failed");
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		if (!_isDistributed) {
			Map<String, Object> ret = new HashMap<String, Object>();
			ret.put(Config.TOPOLOGY_MAX_TASK_PARALLELISM, 1);
			return ret;
		} else {
			return null;
		}
	}

}
