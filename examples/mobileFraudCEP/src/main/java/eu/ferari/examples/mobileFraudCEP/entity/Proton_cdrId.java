package eu.ferari.examples.mobileFraudCEP.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author proton
 * 
 * Class declares compound primary keys, related to the class Proton_cdr
 *
 */
public class Proton_cdrId implements Serializable {

	private static final long serialVersionUID = 1L;
	Integer call_cell_id;
	Date insertion_datetime;
	Date call_start_date;

	public int hashCode() {
		return getInsertion_datetime().hashCode()
				+ getCall_start_date().hashCode() + getCall_cell_id().hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Proton_cdr))
			return false;
		return getInsertion_datetime().equals(
				((Proton_cdr) obj).getInsertion_datetime());
	}

	public Date getInsertion_datetime() {
		return insertion_datetime;
	}

	public void setInsertion_datetime(Date insertion_datetime) {
		this.insertion_datetime = insertion_datetime;
	}

	public Date getCall_start_date() {
		return call_start_date;
	}

	public void setCall_start_date(Date call_start_date) {
		this.call_start_date = call_start_date;
	}

	public Integer getCall_cell_id() {
		return call_cell_id;
	}

	public void setCall_cell_id(Integer call_cell_id) {
		this.call_cell_id = call_cell_id;
	}
}
