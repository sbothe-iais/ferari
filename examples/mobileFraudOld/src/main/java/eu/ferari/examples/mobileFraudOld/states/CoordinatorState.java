package eu.ferari.examples.mobileFraudOld.states;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.mobileFraudOld.exceptions.NodeDataAlreadyExistsException;
import eu.ferari.examples.mobileFraudOld.exceptions.NodeDataNotYetCompleteException;
import eu.ferari.examples.mobileFraudOld.exceptions.NodeListCompleteException;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.examples.mobileFraudOld.misc.Violation;
import eu.ferari.examples.mobileFraudOld.misc.ViolationResolution;

public class CoordinatorState implements IState {
	//private static final Logger logger = LoggerFactory.getLogger(CoordinatorState.class);

	private final ISend sendToTopology;
	private int numNodes;
	private int globalThreshold;
	private final Map<String, ViolationResolution> violationResolutions =  new HashMap<String, ViolationResolution>();
	private final Map<String, Violation> violations = new HashMap<String, Violation>();
	private final Set<String> nodeIds = new HashSet<String>();
	private ISend violationReporter;


	private boolean dynamicThreshold;
	private Map<String,List<String>> functionNodes;
	private final GatekeeperState gatekeeperState;

	public CoordinatorState(GatekeeperState gatekeeperState, ISend sendThresholdUpdate) {
		this.sendToTopology = sendThresholdUpdate;
		functionNodes = new HashMap<String,List<String>>();
		this.gatekeeperState = gatekeeperState;
	}

	@Override
	public void update(DataTuple data) {
		Message message = new Message(data);
		if(numNodes == 0) {
			if(message.getType() == MessageType.ConfProperties) {
				List<String> properties = message.getConfProperties();
				globalThreshold = Integer.parseInt(properties.get(1));
				numNodes = Integer.parseInt(properties.get(0));
				dynamicThreshold = Boolean.parseBoolean(properties.get(4));
			}
			return;
		}
		if (message.getType().equals(MessageType.LSVToCoordinator)){

		//	logger.info("Received message {}", message);

			nodeIds.add(message.getNodeId());

			if (!violations.containsKey(message.getPhone())){
				beginViolationResolution(message);		
				violationResolutions.put(message.getPhone(), new ViolationResolution(numNodes));
			}

			Violation violation = violations.get(message.getPhone());
			ViolationResolution violationResolution = violationResolutions.get(message.getPhone());

			if (violation.getTimestamp() < message.getTimestamp()){
			//	logger.info("Violation's timestamp {} is lower than message's {}, will ignore message",
				//		violation.getTimestamp(), message.getTimestamp());
				return;
			}

			violationResolution.x(message);

			try {
				boolean isComplete = violation.add(message);
				if(violation.getNumReportedNodes() == 1){
					List<String> nodeList = functionNodes.get(message.getPhone());

					if(nodeList == null){
						nodeList = new ArrayList<String>();
						functionNodes.put(message.getPhone(), nodeList);
					}
					if(!nodeList.contains(message.getNodeId())){
						nodeList.add(message.getNodeId());                 
					}
				}
				if (isComplete){
					//logger.info("Violation resolution complete. {}", violation);
					if (violationReporter != null && violation.getGlobalCount() >= violation.getGlobalThreshold()){

						DataTuple tuple = new DataTuple(violation.getPhone(), violation.getTimestamp(), violation.getGlobalCount());
					//	logger.info("Reporting global violation. Phone, {}, Timestamp: {}, Count: {}", 
				//				violation.getPhone(), violation.getTimestamp(), violation.getGlobalCount());
						violationReporter.signal(tuple);
					}
					if(violationResolution.getNodeCounter() == numNodes){
					List<Float> estimate = violationResolution.EstimateCalculation(message, numNodes);
					
						Message GlobalV = new Message(MessageType.GlobalV, message.getPhone(), message.getTimestamp(), estimate);
						gatekeeperState.SetEstimate(message.getPhone(), estimate);
						sendToTopology.signal(GlobalV.toDataTuple());	

						//Violation violation = violations.get(message.getPhone());
						if(dynamicThreshold){
							List<String> nodeList = functionNodes.get(message.getPhone());
							int newThreshold;
							for (String nodeId : nodeIds) {
								if (nodeList.contains(nodeId)){
									newThreshold = globalThreshold / nodeList.size();
								}
								else{
									newThreshold = 0;
								}
								Message updateThresholdMessage = new Message(MessageType.UpdateThreshold, nodeId, violation.getPhone(), violation.getTimestamp(), newThreshold);
								gatekeeperState.SetThreshold(violation.getPhone(), nodeId, newThreshold);
								sendToTopology.signal(updateThresholdMessage.toDataTuple());
							}
						}
						else{
							for (String nodeId : nodeIds) {
								try {
									int newThreshold = violation.getNewThreshold(nodeId);						
									Message updateThresholdMessage = new Message(MessageType.UpdateThreshold, nodeId, violation.getPhone(), violation.getTimestamp(), newThreshold);
									sendToTopology.signal(updateThresholdMessage.toDataTuple());
								} catch (NodeDataNotYetCompleteException e) {
								//	logger.error(e.getMessage());
									e.printStackTrace();
								}
							}
						}
						violations.remove(violation.getPhone());
					}

				} else if (violation.getNumReportedNodes() == 1) { // We ask for other nodes' counters when the first node is reported				
					Message LSVRequestMessage = new Message(
							MessageType.SendLSV, 
							message.getNodeId(), 
							message.getPhone(), 
							message.getTimestamp()
							);
					sendToTopology.signal(LSVRequestMessage.toDataTuple());
				}
			} catch (NodeDataAlreadyExistsException e) {
				//logger.error("{}, {}", e.getMessage(), e);
			} catch (NodeListCompleteException e1) {
				//logger.error("{}, {}", e1.getMessage(), e1);
			}
		}

		else if(message.getType() == MessageType.CounterUpdate || 
				message.getType() == MessageType.NewThreshold || 
				message.getType() == MessageType.GlobalV)
		{

		}
		else{
			//logger.error("Received unexpected message type: {}", message);
			return;
		}
	}	

	public void setViolationReporter(ISend violationReporter){
		this.violationReporter = violationReporter;
	}

	private void beginViolationResolution(Message thresholdPassMessage){
		//logger.debug("Begin violation resolution for phone {} at timestamp {} from node {}", 
			//	thresholdPassMessage.getPhone(), thresholdPassMessage.getTimestamp(), thresholdPassMessage.getNodeId());
		violations.put(thresholdPassMessage.getPhone(), new Violation(thresholdPassMessage, numNodes, globalThreshold));
	}
}
