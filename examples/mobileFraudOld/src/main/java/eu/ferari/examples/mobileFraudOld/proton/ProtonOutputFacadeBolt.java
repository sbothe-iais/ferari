package eu.ferari.examples.mobileFraudOld.proton;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraudOld.storm.bolts.BaseDistributedCountBolt;

public class ProtonOutputFacadeBolt extends BaseRichBolt {
	private static final Pattern pattern = Pattern.compile("count=(\\w+).+?OccurrenceTime=(\\d+).+?phoneNumber=(\\w+).+?nodeID=(\\w+)");
	private OutputCollector collector;
	private String ConfPropertiesToProtonOutputFacadeBoltStream;
	private String monitoringObject;

	public ProtonOutputFacadeBolt(String ConfPeropertiesToProtonOutputFacadeBoltStream) {
		this.ConfPropertiesToProtonOutputFacadeBoltStream = ConfPeropertiesToProtonOutputFacadeBoltStream;
	}

	@Override
	public void execute(Tuple tuple) {
		if(tuple.getSourceStreamId().equals(ConfPropertiesToProtonOutputFacadeBoltStream)){
			monitoringObject = tuple.getString(0);
			return;
		}
		if(monitoringObject != null) {
			List<Object> outputTuple = parseLine(tuple.toString());
			if (outputTuple != null)
				collector.emit(outputTuple);
		}
	}

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(BaseDistributedCountBolt.DEFAULT_OUTPUT_FIELDS);
	}

	public List<Object> parseLine(String line){
		if (!line.contains(monitoringObject))
			return null;
		Matcher match = pattern.matcher(line);
		if (match.find()){
			int count = Integer.valueOf(match.group(1));
			long timestamp = Long.valueOf(match.group(2));
			String phone = match.group(3);
			String nodeId = match.group(4);

			Message message = new Message(Message.MessageType.CounterUpdate, nodeId, phone, timestamp, count);
			return message.toDataTuple().asList();
		} else {
			throw new RuntimeException("Unable to parse line from Proton");
		}
	}

	//public static void main(String[] args){
		//List<Object> list = new ProtonOutputFacadeBolt().parseLine("source: routingBolt:7, stream: consumerEvents, id: {}, [LocalCounter, {Certainty=1, count=1.0, Duration=0, ExpirationTime=null, Cost=0.0, Annotation=, Name=LocalCounter, OccurrenceTime=1431414002286, EventSource=, phoneNumber=4, EventId=440bff0a-32cf-4bde-995d-dc6455169056, DetectionTime=1431414002286, nodeID=a11, Chronon=null}]");
		//if (list != null)
		//	for (Object object : list) 
		//		System.out.println(object);
		//		String line = "source: routingBolt:7, stream: consumerEvents, id: {}, [LocalCounter, {Certainty=1, count=1.0, Duration=0, ExpirationTime=null, Cost=0.0, Annotation=, Name=LocalCounter, OccurrenceTime=1431414002286, EventSource=, phoneNumber=4, EventId=440bff0a-32cf-4bde-995d-dc6455169056, DetectionTime=1431414002286, nodeID=a11, Chronon=null}]";
		//		Pattern p = Pattern.compile(".*count=(\\d+).*");
		//		System.out.println(p.matches(".*count=(\\d+).*", line));
		//		Matcher m = p.matcher(line);
		//		System.out.println(m.find());
		//		System.out.println(m.group(1));
	//}
}
