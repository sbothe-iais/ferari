package eu.ferari.examples.mobileFraudOld.function;

import java.util.Date;

public class Tuple {
    private int source;
    private String nodeId;
    private Date date;
    private int value;
    private String functionId;
    
    public Tuple(int source, String nodeId, Date date, int value, String functionId) {
        super();
        this.source = source;
        this.nodeId = nodeId;
        this.date = date;
        this.value = value;
        this.functionId = functionId;
    }
    
    public Tuple(String nodeId, Date date, int value, String functionId) {
        this(0, nodeId, date, value, functionId);
    }
    
    public Tuple(org.apache.storm.tuple.Tuple tuple) {
        this(tuple.getSourceTask(), tuple.getString(0), (Date)tuple.getValue(1), tuple.getInteger(2), tuple.getString(3));
    }
    
    public int getSource() {
        return source;
    }
    public String getNodeId() {
        return nodeId;
    }
    public Date getDate() {
        return date;
    }
    public int getValue() {
        return value;
    }
    public String getFunctionId() {
        return functionId;
    }
     
}
