package eu.ferari.examples.mobileFraudOld.misc;

import java.util.Map;

public class TimeWindowLSV implements Comparable<TimeWindowLSV>{
	private Map<String, Float[]> LSV ;
	private long timestamp;				

	public TimeWindowLSV(Map<String, Float[]> LSV, long timestamp) {
		this.LSV = LSV;
		this.timestamp = timestamp;
	}

	public Map<String, Float[]> getLSV() {
		return LSV;
	}
	public long getTimestamp() {
		return timestamp;
	}	

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof TimeWindowLSV))
			return false;

		return getTimestamp() == ((TimeWindowLSV) obj).getTimestamp(); 			
	}

	@Override
	public int hashCode() {
		return Long.valueOf(timestamp).hashCode();
	}

	@Override
	public int compareTo(TimeWindowLSV other) {		
		if (this.getTimestamp() < other.getTimestamp()){
			return -1;
		} else if (this.getTimestamp() == other.getTimestamp()){
			return 0;
		} else {
			return 1;
		}
	}
}
