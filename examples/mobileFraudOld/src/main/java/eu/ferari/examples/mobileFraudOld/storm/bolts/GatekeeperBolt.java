package eu.ferari.examples.mobileFraudOld.storm.bolts;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import eu.ferari.core.DataTuple;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.mobileFraudOld.bare.senders.CommSender;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.examples.mobileFraudOld.states.CoordinatorState;
import eu.ferari.examples.mobileFraudOld.states.GatekeeperState;
import eu.ferari.backend.storm.sender.DirectSender;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;


public class GatekeeperBolt extends BaseDistributedCountBolt {
	private final String communicatorName;
	private final String LSVsumToCommunicatorStreamId;

	private boolean isCoordinator;

	/*private JedisPool jedisPool;*/
/*	private Jedis publisher;
*/	private CommSender coordinator_send_to_communicatorSpout;
	private CommSender violationSender;
	private CoordinatorState coordState;
	private Map _stormConf;

	public GatekeeperBolt(String communicatorName, String LSVsumToCommunicatorStreamId) {
		this.communicatorName = communicatorName;
		this.LSVsumToCommunicatorStreamId = LSVsumToCommunicatorStreamId;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		super.prepare(stormConf, context, collector);
		this._stormConf = stormConf;
		
		coordinator_send_to_communicatorSpout = new CommSender(UUID.randomUUID().toString(), false, (String) stormConf.get(CommIntegrationConstants.EVENT_CONTEXT));
		
		violationSender = new CommSender(UUID.randomUUID().toString(), false, (String) _stormConf.get("topology.name"));
		
		ISend sendLSVsumToCommunicator = new DirectSender(collector, context, communicatorName, LSVsumToCommunicatorStreamId);

/*		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();
		jedisPool = new JedisPool(poolConfig, _stormConf.get("jedisHost").toString(),
				jedisConfig.getPort(), jedisConfig.getTimeout());*/
		/*publisher = jedisPool.getResource();*/	
		this.state = new GatekeeperState(sendLSVsumToCommunicator);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(LSVsumToCommunicatorStreamId, true, new Fields("type", "phone", "timestamp", "LSVsum", "numNodes", "nodeId"));
	}

	@Override
	public void cleanup() {
		super.cleanup();
		coordinator_send_to_communicatorSpout.close();
/*		jedisPool.returnResource(publisher);
		jedisPool.close();*/
	}
	
	@Override
	public void execute(Tuple tuple) {	
		super.execute(tuple);
		Message m = new Message(new DataTuple(tuple.getValues()));
		if(m.getType() == MessageType.ConfProperties) {
			List<String> properties = m.getConfProperties();
			this.isCoordinator = Boolean.parseBoolean(properties.get(16));
			if(isCoordinator) {
				String coordinator_to_communicatorSpoutChannel = properties.get(15);
				coordState = new CoordinatorState((GatekeeperState)state, new ISend() {
					@Override
					public void signal(DataTuple data) {
						coordinator_send_to_communicatorSpout.signal(data,"cent1");
					}
					
					@Override
					public int getTaskId() {
						return 0;
					}
				});
				
				
				/*ISend violationReporter = new JedisSender(publisher, "violations");*/
				coordState.setViolationReporter(new ISend() {
					
					@Override
					public void signal(DataTuple data) {
						violationSender.signal(data, "cent1");
					}
					
					@Override
					public int getTaskId() {
						// TODO Auto-generated method stub
						return 0;
					}
				});
//				logger.info("Coordinator ready");
			}
		}
		if(isCoordinator && coordState != null)
			coordState.update(new DataTuple(tuple.getValues()));
	}
}
