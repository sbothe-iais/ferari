package eu.ferari.examples.mobileFraudOld.states;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;

import javax.persistence.EntityManager;

import eu.ferari.examples.mobileFraudOld.misc.DashBoardHelper;
import eu.ferari.examples.mobileFraudOld.function.Function;
import statistics.Statistics;

import com.ibm.ferari.client.DashboardForwarder;
import com.ibm.ferari.client.FerariClientBuilder;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.mobileFraudOld.function.Tuple;
import eu.ferari.examples.mobileFraudOld.misc.DefaultHashMap;
import eu.ferari.core.misc.Event;
import eu.ferari.core.misc.LimitedSizeOrderedSet;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.examples.mobileFraudOld.storm.ProtonMobileFraudTopology;
import eu.ferari.examples.mobileFraud.misc.PullRequest;
import eu.ferari.examples.mobileFraudOld.misc.TimeWindowCount;
import eu.ferari.examples.mobileFraudOld.misc.TimeWindowLSV;

public class TimeMachineState implements IState {
	// private static final Logger logger =
	// LoggerFactory.getLogger(TimeMachineState.class);

	private final int BUFFER_SIZE = 100;
	private final DashBoardHelper dashBoardHelper;
	private final DashboardForwarder dashboardForwarder;

	public enum PhoneState {
		Pause, Play
	}

	private ISend sendToGatekeeper;
	private ISend sendToCommunicator;
	private ISend sendPushToCommunicator;
	private Function f;
	private String nodeId;
	private Map<String, List<Event>> sortedEvents = new HashMap<String, List<Event>>();
	private Map<String, PullRequest> pullRequests = new HashMap<String, PullRequest>();
	private boolean isset = false;

	// RedisConnection<String, String> redis;

	private Map<String, LimitedSizeOrderedSet<TimeWindowCount>> data = new DefaultHashMap<String, LimitedSizeOrderedSet<TimeWindowCount>>(
			new LimitedSizeOrderedSet<TimeWindowCount>(BUFFER_SIZE));

	private Map<String, LimitedSizeOrderedSet<TimeWindowLSV>> LSVs = new DefaultHashMap<String, LimitedSizeOrderedSet<TimeWindowLSV>>(
			new LimitedSizeOrderedSet<TimeWindowLSV>(BUFFER_SIZE));
	private Map<String, PhoneState> phoneStates = new DefaultHashMap<String, PhoneState>(PhoneState.Play);

	private final boolean sendToDashboard;
	private String eventContext;

	public TimeMachineState(ISend sendLSVToCommunicator, ISend sendToGatekeeper, ISend sendToCommunicator,
			ISend sendPushToCommunicator, ISend sendLSVToGatekeeper, String nodeID, EntityManager entityManager,
			boolean sendToDashboard, String eventContext) {
		this.sendToGatekeeper = sendToGatekeeper;
		this.sendToCommunicator = sendToCommunicator;
		this.sendPushToCommunicator = sendPushToCommunicator;
		this.nodeId = nodeID;
		this.dashboardForwarder = FerariClientBuilder.getInstance().createWSForwarder();
		this.dashBoardHelper = new DashBoardHelper(dashboardForwarder, entityManager,
				eventContext);
		this.sendToDashboard = sendToDashboard;
		this.eventContext = eventContext;

	}
	
	public void close() {
		this.dashboardForwarder.close();
	}

	@Override
	public void update(DataTuple tuple) {
		Message message = new Message(tuple);
		if (isset == false) {
			if (message.getType() == MessageType.ConfProperties) {
				List<String> properties = message.getConfProperties();
				String functionName = properties.get(5);
				String functionLocation = properties.get(6);
				float defLSVvalue = Float.parseFloat(properties.get(3));
				f = SetFunction(functionName, functionLocation, defLSVvalue);
				isset = true;
			}
			return;
		}
		// logger.debug("Received message {}", message);
		String phone = message.getPhone();

		if (!phoneStates.containsKey(phone))
			initializePhone(phone);

		TimeWindowCount timeWindow = new TimeWindowCount(message.getCounter(), message.getTimestamp());
		Tuple tuplee = new Tuple(message.getNodeId(), new Date(message.getTimestamp()), message.getCounter(),
				message.getPhone());
		if (message.getType() == MessageType.CounterUpdate) {
			f.insertVal(tuplee);
		}
		f.updateLSV();
		TimeWindowLSV timeWindowLSV = new TimeWindowLSV(f.getLSVcopy(), message.getTimestamp());

		switch (message.getType()) {
		case Statistic:
			onStatistic(message);
			break;
		case Statistics:
			onStatistics(message);
			break;
		case CounterUpdate:
			onCounterUpdate(message, tuple);
			break;
		case Pause:
			onPause(message, phone, timeWindow, timeWindowLSV);
			break;
		case Play:
			onPlay(message, phone, timeWindow);
			break;
		case Event:
			onEvent(message);
			break;
		case PullRequest:
			onPullRequest(message);
			break;
		default:
			// logger.error("Don't know how to handle message of type {}",
			// message.getType());
			break;
		}
	}

	public void setIssetforTest(boolean isset, String functionName, String functionLocation, float defLSVvalue) {
		this.isset = isset;
		f = SetFunction(functionName, functionLocation, defLSVvalue);
	}

	private void onEvent(Message message) {
		Map<String, Object> attributes = message.getAttributes();
		String eventTypeName = message.getEventName();

		List<Event> pushEvents = new ArrayList<Event>();

		PullRequest request = pullRequests.get(message.getEventName());
		if (request != null) {
			if (message.getEventOccurrenceTime() >= request.getStartTime()
					&& message.getEventOccurrenceTime() <= request.getEndTime()) {
				pushEvents.add(
						new Event(message.getEventName(), message.getEventOccurrenceTime(), message.getAttributes()));
				Message pushMessage = new Message(MessageType.Push, pushEvents, 0);// TODO
																					// isws
																					// xreiaste
																					// to
																					// requestid.
																					// twra
																					// stelnw
																					// to
																					// 0

				sendPushToCommunicator.signal(pushMessage.toDataTuple());
			} else if (message.getEventOccurrenceTime() > request.getEndTime()) {
				pullRequests.remove(message.getEventName());
			}
		}

		String EventType = message.getEventName();
		List<Event> Events = sortedEvents.get(EventType);
		if (Events == null) {
			Events = new ArrayList<Event>();
			Events.add(new Event(EventType, message.getEventOccurrenceTime(), message.getAttributes()));
			sortedEvents.put(EventType, Events);
		} else {
			Events.add(new Event(EventType, message.getEventOccurrenceTime(), message.getAttributes()));
			Collections.sort(Events);
		}

		StringBuilder sb = new StringBuilder("Timemachine : " + nodeId + "\n");
		for (Entry<String, List<Event>> sortedevents : sortedEvents.entrySet()) {
			List<Event> events = sortedevents.getValue();

			for (int i = 0; i < events.size(); i++) {
				sb.append(events.get(i).getEvent() + " " + events.get(i).getOccurrenceTime() + "\n");
			}
		}

		
		if (sendToDashboard && ProtonMobileFraudTopology.isInDevMode) {
			dashBoardHelper.sendToDashboard(message, attributes, eventTypeName);
		}

		if (sendToDashboard && !ProtonMobileFraudTopology.isInDevMode) {
			dashboardForwarder.forwardSync(message.toJSON(), eventContext);
		}

		// System.out.println(sb);
	}

	private void onPullRequest(Message message) {
		List<String> events = message.getPullEvents();
		for (String event : events) {
			if (!pullRequests.containsKey(event)) {
				List<String> pullevent = new ArrayList<String>();
				pullevent.add(event);
				pullRequests.put(event,
						new PullRequest(pullevent, message.getStartingTime(), message.getEndingTime(), null));
			} else {
				PullRequest pr = pullRequests.get(event);
				if (pr.getStartTime() > message.getStartingTime()) {
					pr.setStartTime(message.getStartingTime());
				}
				if (pr.getEndTime() < message.getEndingTime()) {
					pr.setEndTime(message.getEndingTime());
				}
			}
		}
		List<Event> pushEvents2 = new ArrayList<Event>();

		for (String event : events) {
			List<Event> sorted = sortedEvents.get(event);
			if (sorted != null) {
				for (Event e : sorted) {
					if (e.getOccurrenceTime() >= message.getStartingTime()
							&& e.getOccurrenceTime() <= message.getEndingTime()) {
						pushEvents2.add(e);
						continue;
					}
					if (e.getOccurrenceTime() >= message.getEndingTime()) {
						break;
					}
				}
			}
		}
		if (!pushEvents2.isEmpty()) {
			Message pushMessage = new Message(MessageType.Push, pushEvents2, message.getPullRequestId());
			sendPushToCommunicator.signal(pushMessage.toDataTuple());
		}
	}

	private void onPlay(Message message, String phone, TimeWindowCount timeWindow) {
		// logger.info("Playing phone {}", phone);
		phoneStates.put(phone, PhoneState.Play);

		for (TimeWindowCount currentTimeWindow : data.get(phone).rewindToLarger(timeWindow)) {
			Message counterUpdateMessage = new Message(MessageType.CounterUpdate, message.getNodeId(), phone,
					currentTimeWindow.getTimestamp(), currentTimeWindow.getCount());
			// logger.debug("Playing message {}", counterUpdateMessage);
			sendToGatekeeper.signal(counterUpdateMessage.toDataTuple());
		}
	}

	private void onPause(Message message, String phone, TimeWindowCount timeWindow, TimeWindowLSV timeWindowLSV) {
		// logger.info("Pausing phone {}", phone);
		phoneStates.put(phone, PhoneState.Pause);
		SortedSet<TimeWindowCount> equalOrLesserTimeWindows = data.get(phone).rewindToEqualOrLarger(timeWindow);
		// TimeWindowCount requestedTimeWindow =
		// data.get(phone).rewindToEqualOrLarger(timeWindow).first();
		Message pauseConfirmationMessage;

		SortedSet<TimeWindowLSV> equalOrLesserTimeWindowsLSV = LSVs.get(phone).rewindToEqualOrLarger(timeWindowLSV);
		Float[] LSV = new Float[f.getEstimateLength()];
		TimeWindowLSV requestedTimeWindowLSV;
		if (equalOrLesserTimeWindowsLSV.size() == 0) {
			for (int i = 0; i < LSV.length; i++) {
				LSV[i] = f.getDefLSVValue();
			}
			requestedTimeWindowLSV = timeWindowLSV;
		} else {
			requestedTimeWindowLSV = equalOrLesserTimeWindowsLSV.first();
			LSV = requestedTimeWindowLSV.getLSV().get(nodeId);
			if (LSV == null) {
				LSV = new Float[f.getEstimateLength()];
				for (int i = 0; i < LSV.length; i++)
					LSV[i] = f.getDefLSVValue();
			}
		}

		// TODO numnodes == 1

		if (equalOrLesserTimeWindows.size() > 0) {
			TimeWindowCount requestedTimeWindow = equalOrLesserTimeWindows.first();
			pauseConfirmationMessage = new Message(MessageType.PauseConfirmation, message.getNodeId(), phone,
					message.getTimestamp(), requestedTimeWindow.getCount(), LSV, 1);
		} else {
			pauseConfirmationMessage = new Message(MessageType.PauseConfirmation, message.getNodeId(), phone,
					message.getTimestamp(), 0, LSV, 1);
			// logger.info("Couldn't find time window for phone {} at timestamp
			// {}", phone, message.getTimestamp());
		}
		// logger.info("Emitting pause confirmation: {}",
		// pauseConfirmationMessage);
		sendToCommunicator.signal(pauseConfirmationMessage.toDataTuple());
	}

	private void onCounterUpdate(Message message, DataTuple tuple) {
		String phone = message.getPhone();

		if (!phoneStates.containsKey(phone))
			initializePhone(phone);

		TimeWindowCount timeWindow = new TimeWindowCount(message.getCounter(), message.getTimestamp());
		Tuple tuplee = new Tuple(message.getNodeId(), new Date(message.getTimestamp()), message.getCounter(),
				message.getPhone());
		if (message.getType() == MessageType.CounterUpdate) {
			f.insertVal(tuplee);
		}
		f.updateLSV();
		TimeWindowLSV timeWindowLSV = new TimeWindowLSV(f.getLSVcopy(), message.getTimestamp());

		// logger.info("Counter update received: {}", message);
		data.get(phone).add(timeWindow);
		LSVs.get(phone).add(timeWindowLSV);
		if (isPlaying(phone))
			sendToGatekeeper.signal(tuple);
	}

	private void onStatistic(Message message) {
		StringBuilder sb = new StringBuilder(
				"\n" + "__________________________Statistics at TimeMachine " + nodeId + "__________________________ ");
		sb.append("\n");
		for (Entry<String, Statistics> hmp : message.getStatistic().entrySet()) {
			sb.append(hmp.getKey());
			sb.append(" --> ");
			sb.append(hmp.getValue().toString());
			sb.append("\n");
		}
		sb.append("______________________________________________________________________________ " + "\n");
		System.out.println(sb);
	}

	private void onStatistics(Message message) {
		StringBuilder sbce = new StringBuilder("TimeMachine Statistics: " + nodeId + "\n");// received
																							// from
																							// routing
																							// bolt
		for (Entry<String, List<Statistics>> hmp : message.getStatistics().entrySet()) {
			sbce.append(hmp.getKey());
			sbce.append(" --> ");
			for (Statistics s : hmp.getValue()) {
				sbce.append(s.toString());
			}
			sbce.append("\n");
		}
		System.out.println(sbce);
	}

	public PhoneState getState(String phone) {
		return phoneStates.get(phone);
	}

	private void initializePhone(String phone) {
		// logger.debug("Received new phone {}", phone);
		phoneStates.put(phone, PhoneState.Play);
		data.put(phone, new LimitedSizeOrderedSet<TimeWindowCount>(BUFFER_SIZE));
		LSVs.put(phone, new LimitedSizeOrderedSet<TimeWindowLSV>(BUFFER_SIZE));
	}

	private boolean isPlaying(String phone) {
		return getState(phone) == PhoneState.Play;
	}

	public Function SetFunction(String functionName, String functionLocation, float defLSVvalue) {
		URLClassLoader urlCl = null;
		Class<? extends Function> newFunction = null;
		Class[] argsClass = new Class[] { int.class, float.class, boolean.class, float.class, boolean.class };
		try {
			newFunction = (Class<? extends Function>) Class.forName(functionName);
		} catch (ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] { file.toURL(), new File("target\\classes").toURL() },
						getClass().getClassLoader());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}

		try {
			if (newFunction == null) {
				newFunction = (Class<? extends Function>) urlCl.loadClass(functionName);
			}
			Object[] args = new Object[] { 10, 1, true, defLSVvalue, true };
			Constructor<? extends Function> argsConstructor = null;
			argsConstructor = newFunction.getConstructor(argsClass);
			Function f = argsConstructor.newInstance(args);
			return f;
		} catch (Exception e) {
			System.out.println(functionName + ": Class not found for this function");
			return null;
		}
	}
}
