package eu.ferari.examples.mobileFraudOld.storm.bolts;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.TransformerException;

import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.core.commIntegration.TopologyCustomConfig;
import eu.ferari.examples.mobileFraudOld.storm.ProtonMobileFraudTopology;
import org.json.JSONException;

import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.coord.EventConsumer;
import com.ibm.ferari.coord.Optimizer;
import com.ibm.ferari.util.Util;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import eu.ferari.examples.mobileFraudOld.bare.senders.CommSender;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.optimizer.configuration.EPNParser;
import eu.ferari.optimizer.locationGraph.GraphPainter;
import eu.ferari.optimizer.locationGraph.LGraphNode;
import eu.ferari.optimizer.locationGraph.LocationGraph;
import eu.ferari.optimizer.locationGraph.NetworkParser;

public class OptimizerBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;

	public static Optimizer optimizer;

	private final String OptimizerToComspoutConffilesStreamId;
	private final String OptimizerToComspoutNewJsonStreamId;
	private final String OptimizerToInputspoutConffilesStreamId;
	private final String OptimizerSpoutToOptimizerJsonStreamId;
	private final String OptimizerSpoutToOptimizerParametersStreamId;
	/*
	 * private JedisPool jedisPool; private Jedis publisher;
	 */

	public static boolean shouldStartOptimizer = true;

	private CommSender sendToRedisPubSubSpout;
	private CommSender sendToInputSpout;

	private boolean receivedjson = false;
	private boolean receivedparameters = false;

	private String _parameters = null;
	private String _json = null;
	private EPNParser epn;
	private LocationGraph lg1 = null;

	private Map _stormConf;

	private Optimizer _opt;

	private String inputDataFileName;
	private String path;

	public OptimizerBolt(String optimizerToComspoutConffilesStreamId, String OptimizerToComspoutNewJsonStreamId,
			String optimizerToInputspoutConffilesStreamId, String OptimizerSpoutToOptimizerJsonStreamId,
			String OptimizerSpoutToOptimizerParametersStreamId, String inputDataFileName, String path) {
		super();
		this.OptimizerToComspoutConffilesStreamId = optimizerToComspoutConffilesStreamId;
		this.OptimizerToComspoutNewJsonStreamId = OptimizerToComspoutNewJsonStreamId;
		this.OptimizerToInputspoutConffilesStreamId = optimizerToInputspoutConffilesStreamId;
		this.OptimizerSpoutToOptimizerJsonStreamId = OptimizerSpoutToOptimizerJsonStreamId;
		this.OptimizerSpoutToOptimizerParametersStreamId = OptimizerSpoutToOptimizerParametersStreamId;
		this.inputDataFileName = inputDataFileName;
		this.path = path;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this._stormConf = stormConf;
		if (shouldStartOptimizer) {
			int port = Integer
					.parseInt(InMemoryGlobalAddressResolver.getInstance().getOptimizerHosts()[0].split(":")[1]);
			_opt = new Optimizer(CommIntegrationConstants.OPT_SITE_ID, CommIntegrationConstants.OPT_SITE_ID, port,
					new EventConsumer() {

						@Override
						public void consumeEvent(FerariEvent event) {
							System.err.println(event);
						}
					});
			_opt.start();
		} else {
			_opt = optimizer;
		}
	}

	@Override
	public void execute(Tuple input) {

		if (input.getSourceStreamId().equals(OptimizerSpoutToOptimizerJsonStreamId)) {
			System.err.println("Optimizer received Json from Optimizer Spout");
			_json = input.getString(0);
			receivedjson = true;
		}
		if (input.getSourceStreamId().equals(OptimizerSpoutToOptimizerParametersStreamId)) {
			System.err.println("Optimizer received Network Parameters from Optimizer Spout");
			_parameters = input.getString(0);
			receivedparameters = true;
		}
		if (receivedjson && receivedparameters) {
			receivedjson = false;
			receivedparameters = false;
			NetworkParser np = new NetworkParser(null, ";");

			lg1 = np.parseFile(_parameters);
			epn = new EPNParser(null, lg1);
			System.err.println("Parsing EPN...");
			epn.parseEPN(_json);
			try {
				epn.runDPandAssignPlans(path, inputDataFileName, false, 1);
			} catch (JSONException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			return;
		}
		String centralNode = lg1.centralNode().location;

		Map<String, Serializable> topologyCustomConfig = new HashMap<String, Serializable>();

		for (LGraphNode n : lg1.nodes) {
			String json;
			String routingBoltPropertiesFile;
			String geometricPropertiesFile;
			String topologyPropertiesFile;
			if (n.equals(centralNode)) {
				json = epn.jsonFiles.get(n.location);
				routingBoltPropertiesFile = "pull=true\nstatisticsClass=statistics.CountStatistics\n\nepoch=10\n\neType=events\njarLocation=null\nstartingTime=0\nendingTime=1700000000000";
				geometricPropertiesFile = "globalThreshold=2\nnumBillingNodes=5\nequalityInAboveThresholdRegion=false\ndefLSVValue=0.0f\ndynamicThreshold=false\nfunctionName=IdentityFunction\nfunctionLocation=D:/jarakia/IdentityFunction.jar\nisCoordinator=true\nmonitoringObject=LocalCounter1\nwindowType=x\nconditionCheckingClassName=x\nLSVDimension=2";
				topologyPropertiesFile = "commToCoordinatorChannel=comm_to_coordinator\ncoordinatorToCommChannel=coordinator_to_comm\ncommToRedisPubSubSpoutChannel=comm_to_redispubsubspout\ncommToPushPullSpoutChannel=comm_to_PushPullSpout";
			} else {
				json = epn.jsonFiles.get(n.location);
				routingBoltPropertiesFile = "pull=true\nstatisticsClass=statistics.CountStatistics\n\nepoch=10\n\neType=events\njarLocation=null\nstartingTime=0\nendingTime=1700000000000\npushEvent=LongCallAtNight,halfFrequentLongCalls,halfExpensiveCalls,halfFrequentEachLongCall\npushEventReciever=site1";
				geometricPropertiesFile = "globalThreshold=2\nnumBillingNodes=5\nequalityInAboveThresholdRegion=false\ndefLSVValue=0.0f\ndynamicThreshold=false\nfunctionName=IdentityFunction\nfunctionLocation=D:/jarakia/IdentityFunction.jar\nisCoordinator=false\nmonitoringObject=LocalCounter2\nwindowType=x\nconditionCheckingClassName=x\nLSVDimension=2";
				topologyPropertiesFile = "commToCoordinatorChannel=comm_to_coordinator\ncoordinatorToCommChannel=coordinator_to_comm\ncommToRedisPubSubSpoutChannel=comm_to_redispubsubspout\ncommToPushPullSpoutChannel=comm_to_PushPullSpout";
			}
			Message messagea11 = new Message(MessageType.ConfFiles, routingBoltPropertiesFile, geometricPropertiesFile,
					topologyPropertiesFile, n.location);
			Message messageJson = new Message(MessageType.NewJsonFile, json, n.location, false);

			/*
			 * sendToRedisPubSubSpout.signal(messagea11.toDataTuple(),n.location
			 * ); System.out.println(
			 * "Optimizer sent Configuration files to site: " + n.location);
			 * sendToRedisPubSubSpout.signal(messageJson.toDataTuple(),
			 * n.location); System.out.println(
			 * "Optimizer sent new jsonFile to site:" + n.location);
			 * sendToInputSpout.signal(messageJson.toDataTuple(), n.location);
			 */

			TopologyCustomConfig customConfig = new TopologyCustomConfig();
			customConfig.setConfFile(messagea11.toJSON());
			customConfig.setJsonFile(messageJson.toJSON());

			topologyCustomConfig.put(n.location, customConfig);
		}

		sendTopology(topologyCustomConfig);

		if (! ProtonMobileFraudTopology.isInDevMode) {
			return;
			
		}
		
		///////////////// GRAPH VISUALIZATION CODE////////////////
		GraphPainter gf = null;
		try {
			gf = new GraphPainter(lg1, path);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gf.paintGraph();
	}

	private void sendTopology(Map<String, Serializable> customConfigMap) {
		Util.runAsyncTask(new Runnable() {

			@Override
			public void run() {
				Util.sleep(20);
				System.err.println("******** Sending topology... ****************");
				_opt.setTopologyGraph(new HashMap<>(), customConfigMap);
			}
		});

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(OptimizerToComspoutConffilesStreamId,
				new Fields("type", "routingBoltProperties", "geometricProperties", "routingProperties", "nodeID"));
		declarer.declareStream(OptimizerToComspoutNewJsonStreamId, new Fields("type", "newJsonFile", "unused"));
		declarer.declareStream(OptimizerToInputspoutConffilesStreamId,
				new Fields("type", "routingBoltProperties", "geometricProperties", "routingProperties", "nodeID"));
	}

	@Override
	public void cleanup() {
		super.cleanup();
		/*
		 * jedisPool.returnResource(publisher); jedisPool.close();
		 */
	}
}