package eu.ferari.examples.mobileFraudOld.function;

//import org.slf4j.LoggerFactory;


public class SphereSafeZoneFunction extends SafeZoneFunction {


    public SphereSafeZoneFunction(int Y, float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, 2, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
 //       logger = LoggerFactory.getLogger(SafeZoneFunction.class);
    }


    /**
     * If F(estimate) is inside function area safe zone: x^2+y^2-threshold=0
     * If F(estimate) is outside of function area safe zone: safe zone is defined 
     * as the plane perpendicular to the estimate vector that passes through 
     * the point sqrt(threshold)*estimate/||estimate||. 
     */
    @Override
    protected void computeSafeZones() {
        if (F(estimate) < threshold) {

            safeZones = new SafeZone[1];
            float[] factors = new float[estimate.length + 1];
            float[] powers = new float[estimate.length + 1];
            for (int i = 0; i < estimate.length; i++) {
                factors[i] = 1;
                powers[i] = 2;
            }
            factors[estimate.length] = 0-threshold;
            powers[estimate.length] = 1;
            safeZones[0] = new SafeZone(factors, powers);
        }
        else {
            double a = 0;
            double b = 0;
            safeZones = new SafeZone[1];
            float[] factors = new float[estimate.length + 1];
            float[] powers = new float[estimate.length + 1];
            for (int i = 0; i < estimate.length; i++) {       
                factors[i] = estimate[i];
                powers[i] = 1;
                a = a + Math.pow(estimate[i], 2);  
            }
            b = Math.sqrt(a);
            factors[estimate.length] = (float) (0-Math.sqrt(threshold)*(a/b));
            powers[estimate.length] = 1;
            safeZones[0] = new SafeZone(factors, powers);
        }
    }


    /**
     * This method is called whenever the bolt receives a new tuple from spout
     * In this function, the LSV hash map stores the last two values that have been received for 
     * each Node ID.
     * The index (0 or 1) of LSV where the most recent value is stored is alternating 
     * between the two indices. For example, the first value received is stored in LSV[0], 
     * the second in LSV[1], the third in LSV[0], the fourth in LSV[1] and so on. 
     */
    @Override
    public void updateLSV(String NodeID) 
    {
        Tuple[] values = lastValues.get(NodeID);
        Integer counter = counts.get(NodeID);
        Float[] tmp = new Float[dimensional]; 
        if(counter == 1){
            tmp[counter % dimensional] = 0.0f;
            tmp[(counter % dimensional) - 1] = (float)values[values.length - 1].getValue();
        }
        else{
            if(counter % dimensional == 0){
                tmp[counter % dimensional] = LSV.get(NodeID)[counter % dimensional];
                tmp[(counter % dimensional) + 1] = (float)values[values.length - 1].getValue();
            }
            else{
                tmp[counter % dimensional] = LSV.get(NodeID)[counter%dimensional];
                tmp[(counter % dimensional) - 1] = (float)values[values.length - 1].getValue();
            }
        }
        LSV.put(NodeID, tmp);

//        logger.info("LSV: {}", LSV);
    }


    @Override
    public void updateLSV(){

    }


    /**
     * LSV[0]*LSV[0] + LSV[1]*lSV[1]
     */
    @Override
    public float F(Float[] vector) {
        return (vector[0] * vector[0]) + (vector[1] * vector[1]);
    }
}
