package eu.ferari.examples.mobileFraudOld.exceptions;

public class NodeDataAlreadyExistsException extends Exception {

	public NodeDataAlreadyExistsException(String nodeId) {
		super("Data for " + nodeId + " already exists");
	}	

}
