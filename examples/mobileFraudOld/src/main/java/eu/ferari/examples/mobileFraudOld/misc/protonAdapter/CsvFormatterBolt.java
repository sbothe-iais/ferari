package eu.ferari.examples.mobileFraudOld.misc.protonAdapter;

import java.nio.charset.Charset;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import json.java.JSONArray;

import org.mortbay.log.Log;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.metadata.event.IEventType;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.metadata.type.TypeAttribute;
import com.ibm.hrl.proton.metadata.type.enums.AttributeTypesEnum;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.runtime.metadata.IMetadataFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;

public class CsvFormatterBolt extends BaseRichBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private OutputCollector _collector;
	public final static String _DEFAULT_DATE_FORMAT =  "dd/MM/yyyy-HH:mm:ss";
	private static final Long SENDING_DELAY = new Long(2000);
	private static final String DELIMETER = ";";
	private static final String TAG_SEPARATOR = "=";
	private static final String NULL_STRING = "null";
	private static final String dateFormat = "dd/MM/yyyy-HH:mm:ss";
	private IMetadataFacade metadataFacade;
	private EepFacade eep;
	private DateFormat dateFormatter;
	private double prevTimestamp = 0;
	private boolean fixdelay;
	private int delay;
	//TODO use DATE string!!
	public CsvFormatterBolt (JSONArray producerProperties, JSONArray events, int delay, boolean fixdelay){
		this.delay = delay;
		this.fixdelay = fixdelay;
	}

	@Override
	public void execute(Tuple tuple) {
		if(tuple.getSourceStreamId().equals("InputSpout_to_csvformatter_json")) {
			try {
				buildMetadataFacade((String)tuple.getValue(1));
			} catch (ParsingException e) {
				e.printStackTrace();
			} catch (EEPException e) {
				e.printStackTrace();
			}
			return;
		}
		String line = tuple.getString(0);
		Pair<Values, Long> event = null;
		try {
			event = nextEvent(line);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		double timestamp = 0;
		Map<String, Object> v =	(Map<String, Object>)event.getFirstValue().get(1);
		if(v.get("OccurrenceTime") == null){
			if(v.get("call_start_date") == null){
				System.err.println("Could not get timestamp from Event");
				return;
			}
			else
			{
				long tml = (long)v.get("call_start_date");
				timestamp = tml;
				v.put("OccurrenceTime", timestamp);
			}
		}
		else{
			timestamp = (long) v.get("OccurrenceTime");
		}

		double delayMillis = prevTimestamp > 0 ? timestamp - prevTimestamp : 0;
		prevTimestamp = delayMillis >=0 ? timestamp : prevTimestamp;
		if(fixdelay){
			try {
					Thread.sleep((long) (delay));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else{
			try {
				if(delayMillis > 0)
					Thread.sleep((long) (delayMillis));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	

		String eventName = (String) v.get("Name");
		_collector.emit("primitiveEvents",new Values(eventName, event.getFirstValue().get(1)));
		_collector.ack(tuple);
	}

	private Pair<Values, Long> nextEvent(String line) throws Exception {
		if (line != null) {
			String eventLine = new String(line.getBytes(Charset
					.forName("UTF-8")), "UTF-8");
			Pair<Values, Long> pair = fromBytes(eventLine);
			return pair;
		} else {
			return null;
		}
	}

	private String getAttributeStringValue(TypeAttribute eventTypeAttribute,
			String attrStringValue) {
		AttributeTypesEnum attrType = eventTypeAttribute.getTypeEnum();
		if (attrType.equals(AttributeTypesEnum.STRING)
				|| eventTypeAttribute.getDimension() > 0) {
			return "'" + attrStringValue + "'";
		}
		return attrStringValue;
	}

	public void buildMetadataFacade(String jsonfileName)
			throws ParsingException, EEPException {
		this.eep = new EepFacade();
		this.metadataFacade = new STORMMetadataFacade(jsonfileName,
				this.eep).getMetadataFacade();
		this.dateFormatter = new SimpleDateFormat(dateFormat);
	}

	public Pair<Values, Long> fromBytes(String eventText)
			throws ParsingException {

		int nameIndex = eventText.indexOf(EventHeader.NAME_ATTRIBUTE);
		String substring = eventText.substring(nameIndex);
		int delimiterIndex = substring.indexOf(DELIMETER);
		int tagDataSeparatorIndex = substring.indexOf(TAG_SEPARATOR);
		String nameValue = substring.substring(tagDataSeparatorIndex + 1,
				delimiterIndex);
		IEventType eventType = metadataFacade.getEventMetadataFacade()
				.getEventType(nameValue);
		Map<String, Object> attrValues = new HashMap<String, Object>();
		String[] tagValuePairs = eventText.split(DELIMETER);

		for (String tagValue : tagValuePairs) {

			String[] separatedPair = tagValue.split(TAG_SEPARATOR);
			String attrName = separatedPair[0].trim(); // the tag is always the
			// first in the pair
			String attrStringValue = separatedPair[1].trim();

			if (attrName.equals(NULL_STRING) || attrName.equals("")) {
				throw new ParsingException("Could not parse the event string "
						+ eventText + ", reason: Attribute name not specified");
			}

			if (attrStringValue.equals(NULL_STRING)
					|| attrStringValue.equals("")) {
				attrValues.put(attrName, null);
				continue;
			}

			TypeAttribute eventTypeAttribute = eventType.getTypeAttributeSet()
					.getAttribute(attrName);
			attrStringValue = getAttributeStringValue(eventTypeAttribute,
					attrStringValue);
			Object attrValueObject;

			try {
				attrValueObject = TypeAttribute.parseConstantValue(
						attrStringValue, attrName, eventType, dateFormatter,
						this.eep);
			} catch (Exception e) {
				throw new ParsingException("Could not parse the event string"
						+ eventText + ", reason: " + e.getMessage());
			}
			attrValues.put(attrName, attrValueObject);
		}

		Log.info("TestProtonSpout: fromBytes: injecting event " + nameValue
				+ "with attributes" + attrValues);
		return new Pair<Values, Long>(new Values(nameValue, attrValues),
				SENDING_DELAY);
	}

	@Override
	public void prepare(Map arg0, TopologyContext arg1, OutputCollector arg2) {
		_collector = arg2;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream("primitiveEvents",new Fields("Name", STORMMetadataFacade.ATTRIBUTES_FIELD));
	}
}
