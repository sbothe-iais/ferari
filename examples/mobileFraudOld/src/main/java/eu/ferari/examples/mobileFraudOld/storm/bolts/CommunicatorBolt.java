package eu.ferari.examples.mobileFraudOld.storm.bolts;

import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.mobileFraudOld.states.CommunicatorState;
import eu.ferari.backend.storm.sender.DirectSender;


public class CommunicatorBolt extends BaseDistributedCountBolt {
	private static final Fields NEW_THRESHOLD_FIELDS = new Fields("type", "nodeId", "phone", "newThreshold");
	private static final Fields PAUSE_FIELDS = new Fields("type", "nodeId", "phone", "timestamp");
	private static final Fields PLAY_FIELDS = new Fields("type", "nodeId", "phone", "timestamp");
	private static final Fields ESTIMATE_FIELDS = new Fields("type", "phone", "timestamp", "estimate");

	private final String timeMachineName;
	private final String gatekeeperName;
	private final String routingBoltName;
	private final String contextBoltName;
	private final String epaManagerBoltName;
	private final String toTimeMachineStreamId;
	private final String toGatekeeperStreamId;
	private final String EstToGatekeeperStreamId;
	private final String toTimeMachinePlayStreamId;
	private final String toTimeMachinePullStreamId;
	private final String ToCoordStreamId;
	private final String toRoutingBoltId;
	private final String toContextBoltId;
	private final String toEpaManagerBoltId;
	private final String nodeID;

/*	private JedisPool jedisPool;
	private Jedis publisher;*/
	private Map _stormConf;

	public CommunicatorBolt(String timeMachineName, 
			String gatekeeperName, 
			String routingBoltName, 
			String contextBoltName, 
			String epaManagerBoltName,
			String toTimeMachineStreamId,
			String toTimeMachinePlayStreamId,
			String toTimeMachinePullStreamId,
			String toGatekeeperStreamId,
			String EstToGatekeeperStreamId,
			String toCoordStreamId,
			String toRoutingBoltId,
			String toContextBoltId,
			String toEpaManagerBoltId,
			String nodeID
			) {
		this.timeMachineName = timeMachineName;
		this.gatekeeperName = gatekeeperName;
		this.routingBoltName = routingBoltName;
		this.contextBoltName = contextBoltName;
		this.epaManagerBoltName = epaManagerBoltName;
		this.toTimeMachineStreamId = toTimeMachineStreamId;
		this.toGatekeeperStreamId = toGatekeeperStreamId;	
		this.toTimeMachinePlayStreamId = toTimeMachinePlayStreamId;
		this.toTimeMachinePullStreamId = toTimeMachinePullStreamId;
		this.EstToGatekeeperStreamId = EstToGatekeeperStreamId;
		this.ToCoordStreamId = toCoordStreamId;
		this.toRoutingBoltId = toRoutingBoltId;
		this.toContextBoltId = toContextBoltId;
		this.toEpaManagerBoltId = toEpaManagerBoltId;
		this.nodeID = nodeID;
	}	


	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		super.prepare(stormConf, context, collector);
		this._stormConf = stormConf;
		ISend sendToTimeMachinePlay = new DirectSender(collector, context, timeMachineName, toTimeMachinePlayStreamId);
		ISend sendToTimeMachine = new DirectSender(collector, context, timeMachineName, toTimeMachineStreamId);
		ISend sendToTimeMachinePull = new DirectSender(collector, context, timeMachineName, toTimeMachinePullStreamId);
		ISend sendToGatekeeper = new DirectSender(collector, context, gatekeeperName, toGatekeeperStreamId);
		ISend sendToCoordinatorLSVviol = new DirectSender(collector, context, gatekeeperName, ToCoordStreamId);
		ISend sendToRoutingBolt = new DirectSender(collector, context, routingBoltName, toRoutingBoltId);
		ISend sendToContextBolt = new DirectSender(collector, context, contextBoltName, toContextBoltId);
		ISend sendToEpaManagerBolt = new DirectSender(collector, context, epaManagerBoltName, toEpaManagerBoltId);
/*		JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();
		jedisPool = new JedisPool(poolConfig, _stormConf.get("jedisHost").toString(),
				jedisConfig.getPort(), jedisConfig.getTimeout());
		publisher = jedisPool.getResource();*/

		this.state = new CommunicatorState(sendToTimeMachine,
				sendToTimeMachinePlay, sendToTimeMachinePull, 
				sendToGatekeeper, sendToCoordinatorLSVviol, 
				sendToRoutingBolt, sendToContextBolt, sendToEpaManagerBolt, null, nodeID);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(toTimeMachineStreamId, true, PAUSE_FIELDS);
		declarer.declareStream(toTimeMachinePlayStreamId, true, PLAY_FIELDS);
		declarer.declareStream(toTimeMachinePullStreamId, true, new Fields("type", "pullEvents", "windowDuration", "eventOccurrenceTime", "pullRequestId"));

		declarer.declareStream(toGatekeeperStreamId, true, NEW_THRESHOLD_FIELDS);
		declarer.declare(new Fields("message"));
		declarer.declareStream(EstToGatekeeperStreamId, true, ESTIMATE_FIELDS);
		declarer.declareStream(ToCoordStreamId, true, new Fields("type", "phone", "timestamp", "LSV", "numnodes", "nodeId"));

		declarer.declareStream(toRoutingBoltId, true, new Fields("type", "json", "rand", "rand1"));
		declarer.declareStream(toContextBoltId, true, new Fields("type", "json", "rand", "rand1"));
		declarer.declareStream(toEpaManagerBoltId, true, new Fields("type", "json", "rand", "rand1"));
	}

	@Override
	public void cleanup() {
		super.cleanup();
/*		jedisPool.returnResource(publisher);
		jedisPool.close();*/
	}
}