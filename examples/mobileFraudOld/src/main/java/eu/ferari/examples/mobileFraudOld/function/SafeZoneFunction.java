package eu.ferari.examples.mobileFraudOld.function;


abstract public class SafeZoneFunction extends BallFunction {

    protected SafeZone[] safeZones;

    public SafeZoneFunction(int Y, float threshold, int estimateLength, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, estimateLength, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
    }

    abstract protected void computeSafeZones();

    @Override
    public boolean hasLocalViolation(String NodeID) {
        computeSafeZones();
        if (safeZones == null){
            return hasBallViolation(NodeID);
        }
        else {
            for (int i = 0; i < safeZones.length; i++)
                if (safeZones[i].hasSafeZoneViolation(estimate, driftVector.get(NodeID),equalityInAboveThresholdRegion ))
                    return true;
            return false;
        }
    }
}
