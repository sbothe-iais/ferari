package eu.ferari.examples.mobileFraudOld.function;

//import org.slf4j.LoggerFactory;

import java.util.Map;


public class VarianceSafeZoneFunction extends SafeZoneFunction {

    public VarianceSafeZoneFunction(int Y, float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, 2, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
  //      logger = LoggerFactory.getLogger(VarianceSafeZoneFunction.class);
    }

    /**
     * If F(estimate) is inside function area safe zone: x-y^2-threshold=0
     * If F(estimate) is outside of function area safe zone: x-threshold=0
     */
    @Override
    protected void computeSafeZones() {
        if (F(estimate) > threshold) {
            safeZones = new SafeZone[1];
            float[] factors = new float[estimate.length + 1];
            float[] powers = new float[estimate.length + 1];

            factors[0] = 1;
            factors[1] = -1;
            factors[2] = 0-threshold;
            powers[0] = 1;
            powers[1] = 2;
            powers[2] = 1;

            safeZones[0] = new SafeZone(factors, powers);
        }
        else {
            safeZones = new SafeZone[1];
            float[] factors = new float[estimate.length + 1];
            float[] powers = new float[estimate.length + 1];

            factors[0] = 1;
            factors[1] = 0;
            factors[2] = 0-threshold;
            powers[0] = 1;
            powers[1] = 1;
            powers[2] = 1;

            safeZones[0] = new SafeZone(factors, powers);
        }
    }


    /**
     * LSV[0]=LSV[1]*LSV[1]
     * LSV[1]=lastValues[lastValues.length-1]-estimate
     * This method is called from a bolt whenever this bolt receives a new tuple from Spout
     */
    @Override
    public void updateLSV(String NodeID) {
        for(Map.Entry<String, Tuple[]> lv: lastValues.entrySet() ){
            Tuple[] lvalue = lv.getValue();
            Float[] tmp = new Float[2]; 
            tmp[1] = lvalue[lvalue.length - 1].getValue() - estimate[1];
            tmp[0] = tmp[1] * tmp[1];
            LSV.put(lv.getKey(), tmp);
        }
//        logger.info("LSV: {}", LSV);
    }

    
    /**
     * LSV[0]=LSV[1]*LSV[1]
     * LSV[1]=lastValues[lastValues.length-1]-estimate
     * This method is called from all bolts, before they calculate their sums.
     *This is done to update LSV arrays with the latest estimate.
     */
    @Override
    public void updateLSV(){
        for(Map.Entry<String, Tuple[]> lv: lastValues.entrySet() ){
            Tuple[] lvalue = lv.getValue();
            Float[] tmp = new Float[2]; 
            tmp[1] = lvalue[lvalue.length - 1].getValue() - estimate[1];
            tmp[0] = tmp[1] * tmp[1];
            LSV.put(lv.getKey(), tmp);
        }
//        logger.info("LSV: {}", LSV);
    }
    
    
    /**
     * LSV[0]-LSV[1]*LSV[1]
     */
    @Override
    public float F(Float[] vector) {
        return vector[0] - (vector[1] * vector[1]);
    }

    /**
    * This method is called whenever the coordinator requests from all bolts to send their sums.
    * Whenever a bolt sends its sum , it stores in the lastSent hash map the LSV vector-estimate.
    */
    @Override
    public void updateLastSent(){
        lastSent.clear();
        for (Map.Entry<String, Float[]> lsv: LSV.entrySet()){
            Float[] lvalue = lsv.getValue();
            Float[] tmp = new Float[lvalue.length]; 
            tmp[1] = lvalue[1] - estimate[1];
            tmp[0] = tmp[1] * tmp[1];

            lastSent.put(lsv.getKey(), tmp);
        }
 //       logger.info("LastSent (after request from coordinator): {}", lastSent);
    }


    /**
     * This method is called whenever a violation is detected from a bolt.
     * It updates lastSent hash map with the NodeID which caused the violation.
     * @param NodeID The node id which caused the violation.
     */    
    @Override
    public void updateLastSent(String NodeID){
        Float[] lvalue = LSV.get(NodeID);
        Float[] tmp = new Float[lvalue.length];
        tmp[1] = lvalue[1] - estimate[1];
        tmp[0] = tmp[1] * tmp[1];       
        lastSent.put(NodeID, tmp);
 //       logger.info("Last Sent: {}", lastSent);
    }
}
