package eu.ferari.examples.mobileFraudOld.function;

public class SafeZone {
    protected float[] factors;
    protected float[] powers;

    public SafeZone(float[] factors, float[] powers) {
        this.factors = new float[factors.length];
        for (int i = 0; i < factors.length; i++)
            this.factors[i] = factors[i];

        this.powers = new float[powers.length];
        for (int i = 0; i < powers.length; i++)
            this.powers[i] = powers[i];       
    }


    /**
     * This method returns true if the estimate lies in the same side of the 
     * safe zone where driftVector lies.
     * If result1 and result2 have the same operator, that means the estimate and the driftVector
     * lie in the same side of the safe zone. In this case there is no Local Violation.
     */
    public boolean hasSafeZoneViolation(Float[] estimate, Float[] drift, boolean equalityInAboveThresholdRegion) {
        float result1 = 0;
        for (int i = 0; i < estimate.length; i++){
            result1 += factors[i] * Math.pow(drift[i], powers[i]);
        }
        result1 += factors[estimate.length];

        float result2 = 0;
        for (int i = 0; i < estimate.length; i++){
            result2 += factors[i] * Math.pow(estimate[i], powers[i]);
        }
        result2 += factors[estimate.length];

        // if there is local violation return true
        if (equalityInAboveThresholdRegion){
            return((result1 < 0 && result2 >= 0) || (result1 >= 0 && result2 < 0));          
        }
        else{
            return((result1 <= 0 && result2 > 0) || (result1 > 0 && result2 <= 0));        
        }
    }
}
