package eu.ferari.examples.mobileFraudOld.storm.spouts;

import java.io.Serializable;
import java.util.Map;

import com.ibm.ferari.util.Util;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;

public class ShutdownDetectorSpout extends BaseRichSpout {

	private static final long serialVersionUID = -5627712240307665822L;

	private static final long TIMEOUT_THRESHOLD_MILLIS = 5000;
	
	private volatile long lastInvocationTime;
	private SerializableRunnable shutdownHook;
	
	public ShutdownDetectorSpout(SerializableRunnable shutdownHook) {
		this.shutdownHook = shutdownHook;
	}
	
	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		lastInvocationTime = System.currentTimeMillis();
		Util.runAsyncTask(() -> {
			Util.sleep(10);
			while (true) {
				long now = System.currentTimeMillis();
				if (now - lastInvocationTime > TIMEOUT_THRESHOLD_MILLIS) {
					shutdownHook.run();
					return;
				}
				Util.sleep(1);
			}
		});
	}

	@Override
	public void nextTuple() {
		lastInvocationTime = System.currentTimeMillis();
		Util.sleep(1);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		
	}
	
	
	public static interface SerializableRunnable extends Runnable, Serializable {
		
	}

}
