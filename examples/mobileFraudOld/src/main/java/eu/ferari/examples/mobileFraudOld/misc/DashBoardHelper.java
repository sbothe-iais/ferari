package eu.ferari.examples.mobileFraudOld.misc;

import com.google.gson.JsonObject;
import com.ibm.ferari.client.DashboardForwarder;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import eu.ferari.optimizer.dashboard.entity.Derived_event;
import eu.ferari.optimizer.dashboard.entity.Derived_event_details;
import eu.ferari.optimizer.dashboard.entity.Proton_cdr;
import eu.ferari.examples.mobileFraud.misc.Message;
import json.java.JSONArray;
import json.java.JSONObject;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;

public class DashBoardHelper {
	private DashboardForwarder dashBoardForwarder;
	private EntityManager entityManager;
	private String eventContext;

	public DashBoardHelper(DashboardForwarder dashBoardForwarder, EntityManager entityManager, String eventContext) {
		this.dashBoardForwarder = dashBoardForwarder;
		this.entityManager = entityManager;
		this.eventContext = eventContext;
	}

	static <K, V> List<Object> collectValues(Map<K, V> map) {

		List<V> list = new ArrayList<V>();
		for (Map.Entry<K, V> e : map.entrySet())
			list.add(e.getValue());

		return (List<Object>) list;
	}

	public void sendToDashboard(Message message, Map<String, Object> attributes, String eventTypeName) {
		if (eventTypeName != null && !eventTypeName.contains("CallPOPDWH")) {
			System.out.println();
			if (attributes.containsKey("SendToDashboard")) {
				attributes.remove("PushToCoordinators");
				attributes.remove("SendToDashboard");

				List<Object> listToSql;
				listToSql = collectValues(attributes);

				if ((listToSql.get(0)).toString().charAt(0) == '[') {
					insertToMySQLMultiDates(attributes, listToSql);
				} else {
					insertToMysqlOneDate(attributes, listToSql);
				}

				listToSql = null;

				JSONObject jsonObjToRedis = new JSONObject();
				List<String> listValuesFromMap = new ArrayList<String>();

				for (Map.Entry<String, Object> entry : attributes.entrySet()) {
					if (!entry.getKey().contains("EventId")
							&& !entry.getKey().contains("Annotation")
							&& !entry.getKey().contains("Chronon")
							&& !entry.getKey().contains("EventSource")
							&& !entry.getKey().contains("ExpirationTime")) {
						//System.out.println();

						if (entry.getKey().contains("call_start_dates")) {

							JSONArray jsonArray1 = new JSONArray();
							listValuesFromMap.add((String) entry.getValue()
									.toString());

							for (String item : listValuesFromMap) {
								jsonArray1.add(item.toString());
							}
							jsonObjToRedis.put(entry.getKey(), jsonArray1);
						} else {
							listValuesFromMap.add((String) entry.getValue()
									.toString());
							jsonObjToRedis.put(entry.getKey(), entry.getValue());
						}
					}
				}
				JsonObject taggedEvent = new JsonObject();
				taggedEvent.addProperty("context", eventContext);
				taggedEvent.addProperty("event", jsonObjToRedis.toString());
				/***
				 * Marko: Change this to forwardSync(event, context) when you want.
				 * The context is 'eventContext' variable in TimeMachineState.java
				 */
				dashBoardForwarder.forwardSync(taggedEvent.toString());
			}

		} else if (message.getEventName() != null) {
			if (attributes.containsKey("other_party_tel_number_prefix")) {
				if (attributes.get("other_party_tel_number_prefix") instanceof Integer) {
					if ((int) (attributes.get("other_party_tel_number_prefix")) == 960) {
						Map<String, String> tupleMap = new HashMap<String, String>();
						tupleMap.put("Name", message.getEventName());
						for (Map.Entry<String, Object> entry : message.getAttributes().entrySet()) {
							String value = "null";
							if (entry.getValue() != null) {
								value = entry.getValue().toString();
							}
							tupleMap.put(entry.getKey(), value);
						}
						insertToSql(tupleMap);
					}
				} else if (attributes.get("other_party_tel_number_prefix") instanceof String) {
					if (attributes.get("other_party_tel_number_prefix").equals("960")) {
						Map<String, String> tupleMap = new HashMap<String, String>();
						tupleMap.put("Name", message.getEventName());
						for (Map.Entry<String, Object> entry : message.getAttributes().entrySet()) {
							String value = "null";
							if (entry.getValue() != null) {
								value = entry.getValue().toString();
							}
							tupleMap.put(entry.getKey(), value);
						}
						insertToSql(tupleMap);
					}
				}
			}
		}
	}

	void insertToMySQLMultiDates(Map<String, ?> attributes,
								 List<Object> listToSql) {

		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();

		String multipleDates = listToSql.get(0).toString();
		multipleDates = multipleDates.substring(1, multipleDates.length() - 1);
		String[] arrayDates = multipleDates.split(", ");

		int arrayDatesSize = arrayDates.length;

		Derived_event derivedEventMultiDates = new Derived_event();

		if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
				&& (!attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
				.equals("null"))) {
			derivedEventMultiDates.setDerived_event_name(attributes.get(
					EventHeader.NAME_ATTRIBUTE).toString());
		}
		if (attributes.containsKey(EventHeader.CERTAINTY_ATTRIBUTE)
				&& !attributes.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()
				.equals("null")) {
			derivedEventMultiDates.setCertainty((Double) Double
					.parseDouble(attributes
							.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()));
		}
		if (attributes.containsKey(EventHeader.COST_ATTRIBUTE)
				&& !attributes.get(EventHeader.COST_ATTRIBUTE).toString()
				.equals("null")) {
			derivedEventMultiDates.setCost((Double) Double
					.parseDouble(attributes.get(EventHeader.COST_ATTRIBUTE)
							.toString()));
		}
		if (attributes.containsKey(EventHeader.DURATION_ATTRIBUTE)
				&& !attributes.get(EventHeader.DURATION_ATTRIBUTE).toString()
				.equals("null")) {
			derivedEventMultiDates.setDuration((Double) Double
					.parseDouble(attributes.get(EventHeader.DURATION_ATTRIBUTE)
							.toString()));
		}
		if (attributes.containsKey("call_direction")
				&& !attributes.get("call_direction").toString().equals("null")) {
			derivedEventMultiDates.setCall_direction(attributes.get(
					"call_direction").toString());
		}
		if (attributes.containsKey("calling_number")
				&& !attributes.get("calling_number").toString().equals("null")) {
			derivedEventMultiDates.setCalling_number(attributes.get(
					"calling_number").toString());
		}
		if (attributes.containsKey(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				&& !attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				.toString().equals("null")) {
			derivedEventMultiDates
					.setOccurence_time(new Timestamp((Long) attributes
							.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
		}
		if (attributes.containsKey("CallsCount")
				&& !attributes.get("CallsCount").toString().equals("null")) {
			double callCount = (Double) attributes.get("CallsCount");
			derivedEventMultiDates.setCalls_count((int) callCount);
		}
		if (attributes.containsKey(EventHeader.EXPIRATION_TIME_ATTRIBUTE)
				&& attributes.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE) != null) {
			derivedEventMultiDates.setExpiration_time(new Timestamp(
					(Long) attributes
							.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE)));
		}
		derivedEventMultiDates.setInsertion_datetime(now);

		Long callStartDate = Long.parseLong(arrayDates[0].toString());
		if (callStartDate != null) {
			derivedEventMultiDates.setCall_start_date(new Timestamp(
					callStartDate));
		}

		entityManager.getTransaction().begin();
		entityManager.persist(derivedEventMultiDates);

		for (int i = 1; i < (arrayDatesSize + 1); i++) {

			Derived_event_details tableDetails = new Derived_event_details();

			if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
					&& !attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
					.equals(null)) {
				tableDetails.setDerived_event_name(attributes.get(
						EventHeader.NAME_ATTRIBUTE).toString());
			}
			if (attributes.containsKey("calling_number")
					&& !attributes.get("calling_number").toString()
					.equals(null)) {
				tableDetails.setCalling_number(attributes.get("calling_number")
						.toString());
			}
			tableDetails.setInsertion_datetime(now);
			if (!attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
					.toString().equals("null")) {
				tableDetails.setOccurence_time(new Timestamp(
						(Long) attributes
								.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
			}

			Long callStartTime = Long.parseLong(arrayDates[i - 1].toString());
			if (callStartTime != null) {
				tableDetails.setCall_start_date(new Date(callStartTime));
			}

			entityManager.persist(tableDetails);
			if (i == arrayDatesSize) {
				entityManager.getTransaction().commit();
			}
		}
	}

	void insertToMysqlOneDate(Map<String, ?> attributes,
							  List<Object> listToSql) {

		Derived_event derivedEventOneDate = new Derived_event();

		if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
				&& (!attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
				.equals("null"))) {
			derivedEventOneDate.setDerived_event_name(attributes.get(
					EventHeader.NAME_ATTRIBUTE).toString());
		}
		if (attributes.containsKey(EventHeader.CERTAINTY_ATTRIBUTE)
				&& !attributes.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()
				.equals("null")) {
			derivedEventOneDate.setCertainty((Double) Double
					.parseDouble(attributes
							.get(EventHeader.CERTAINTY_ATTRIBUTE).toString()));
		}
		derivedEventOneDate.setOther_party_tel_number(listToSql.get(6)
				.toString());
		if (attributes.containsKey(EventHeader.COST_ATTRIBUTE)
				&& !attributes.get(EventHeader.COST_ATTRIBUTE).toString()
				.equals("null")) {
			derivedEventOneDate.setCost((Double) Double.parseDouble(attributes
					.get(EventHeader.COST_ATTRIBUTE).toString()));
		}
		if (attributes.containsKey(EventHeader.DURATION_ATTRIBUTE)
				&& !attributes.get(EventHeader.DURATION_ATTRIBUTE).toString()
				.equals("null")) {
			derivedEventOneDate.setDuration((Double) Double
					.parseDouble(attributes.get(EventHeader.DURATION_ATTRIBUTE)
							.toString()));
		}
		if (attributes.containsKey("call_direction")
				&& !attributes.get("call_direction").toString().equals("null")) {
			derivedEventOneDate.setCall_direction(attributes.get(
					"call_direction").toString());
		}
		if (attributes.containsKey("calling_number")
				&& !attributes.get("calling_number").toString().equals("null")) {
			derivedEventOneDate.setCalling_number(attributes.get(
					"calling_number").toString());
		}
		if (attributes.containsKey("called_number")
				&& !attributes.get("called_number").toString().equals("null")) {
			derivedEventOneDate.setCalled_number(attributes
					.get("called_number").toString());
		}
		derivedEventOneDate.setCall_start_date(new Timestamp(
				(Long) listToSql.get(0)));
		derivedEventOneDate.setCalls_count(1);

		if (attributes.containsKey(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				&& !attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				.toString().equals("null")) {
			derivedEventOneDate
					.setOccurence_time(new Timestamp((Long) attributes
							.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
		}
		if (attributes.containsKey("conversation_duration")
				&& !attributes.get("conversation_duration").toString()
				.equals("null")) {
			derivedEventOneDate.setConversation_duration(Integer
					.parseInt(attributes.get("conversation_duration")
							.toString()));
		}
		derivedEventOneDate.setCalls_length_sum(Integer.parseInt(attributes
				.get("conversation_duration").toString()));

		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		derivedEventOneDate.setInsertion_datetime(now);

		if (attributes.containsKey(EventHeader.EXPIRATION_TIME_ATTRIBUTE)) {
			if (attributes.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE) != null) {
				derivedEventOneDate.setExpiration_time(new Date(
						(Long) attributes
								.get(EventHeader.EXPIRATION_TIME_ATTRIBUTE)));
			}
		}

		Derived_event_details tableDetails = new Derived_event_details();

		if (attributes.containsKey(EventHeader.NAME_ATTRIBUTE)
				&& (!attributes.get(EventHeader.NAME_ATTRIBUTE).toString()
				.equals("null"))) {
			tableDetails.setDerived_event_name(attributes.get(
					EventHeader.NAME_ATTRIBUTE).toString());
		}
		if (attributes.containsKey("calling_number")
				&& (!attributes.get("calling_number").toString().equals("null"))) {
			tableDetails.setCalling_number(attributes.get("calling_number")
					.toString());
		}
		tableDetails.setInsertion_datetime(now);

		if (attributes.containsKey(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				&& !attributes.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)
				.toString().equals("null")) {
			tableDetails
					.setOccurence_time(new Timestamp((Long) attributes
							.get(EventHeader.OCCURENCE_TIME_ATTRIBUTE)));
		}
		tableDetails.setCall_start_date(new Timestamp((Long) listToSql
				.get(0)));
		//generating cell_id artificially, which will connect to artificial coordinates in the base
		Random r = new Random();
		int randomInt = r.nextInt(300) + 1;
		tableDetails.setCall_cell_id(randomInt);
		entityManager.getTransaction().begin();
		entityManager.persist(tableDetails);
		entityManager.persist(derivedEventOneDate);
		entityManager.getTransaction().commit();
	}

	void insertToSql(Map<String, String> tupleMap) {

		Proton_cdr cdr = new Proton_cdr();

		if (tupleMap.containsKey("Name")
				&& (!tupleMap.get("Name").toString().equals("null"))) {
			cdr.setDerived_event_name(tupleMap.get(
					"Name").toString());
		}
		if (tupleMap.containsKey("billed_msdn")
				&& (!tupleMap.get("billed_msdn").toString().equals("null"))) {
			cdr.setBilled_msisdn(tupleMap.get(
					"billed_msdn").toString());
		}
		if (tupleMap.containsKey("other_party_tel_number_prefix")
				&& (!tupleMap.get("other_party_tel_number_prefix").toString().equals("null"))) {
			System.out.println();
			cdr.setOther_party_tel_number_prefix(tupleMap.get(
					"other_party_tel_number_prefix").toString());
		}
		if ((tupleMap.containsKey("other_party_tel_number"))
				&& (!tupleMap.get("other_party_tel_number").toString().equals("null"))) {
			cdr.setOther_party_tel_number(tupleMap.get(
					"other_party_tel_number").toString());
		}
		if (tupleMap.containsKey("call_direction")
				&& (!tupleMap.get("call_direction").toString().equals("null"))) {
			cdr.setCall_direction(tupleMap.get(
					"call_direction").toString());
		}
		if (tupleMap.containsKey("called_number")
				&& (!tupleMap.get("called_number").toString().equals("null"))) {
			cdr.setCalled_number(tupleMap.get(
					"called_number").toString());
		}
		if (tupleMap.containsKey("calling_number")
				&& (!tupleMap.get("calling_number").toString().equals("null"))) {
			cdr.setCalling_number(tupleMap.get(
					"calling_number").toString());
		}
		if (tupleMap.containsKey("conversation_duration")
				&& (!tupleMap.get("conversation_duration").toString().equals("null"))) {
			cdr.setConversation_duration(Integer
					.parseInt(tupleMap.get("conversation_duration").toString()));
		}
		if (tupleMap.containsKey("tap_related")
				&& (!tupleMap.get("tap_related").toString().equals("null"))) {
			cdr.setTap_related(tupleMap.get(
					"tap_related").toString());
		}
		if (tupleMap.containsKey("total_call_charge_amount")
				&& (!tupleMap.get("total_call_charge_amount").toString().equals("null"))) {
			cdr.setTotal_call_charge_amount((Double) Double
					.parseDouble(tupleMap.get("total_call_charge_amount").toString()));
		}
		if (tupleMap.containsKey("call_start_date")
				&& (!tupleMap.get("call_start_date").toString().equals("null"))) {
			cdr.setCall_start_date(new Timestamp((Long) Long
					.parseLong(tupleMap.get("call_start_date").toString())));
		}
		if (tupleMap.containsKey("call_start_time")
				&& (!tupleMap.get("call_start_time").toString().equals("null"))) {
			cdr.setCall_start_time(Integer.parseInt(tupleMap.get("call_start_time").toString()));
		}
		if (tupleMap.containsKey("object_id")
				&& (!tupleMap.get("object_id").toString().equals("null"))) {
			cdr.setObject_id(new BigInteger(tupleMap.get(
					"object_id").toString()));
		}

		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		cdr.setInsertion_datetime(now);

		entityManager.getTransaction().begin();
		entityManager.persist(cdr);
		entityManager.getTransaction().commit();
	}
}