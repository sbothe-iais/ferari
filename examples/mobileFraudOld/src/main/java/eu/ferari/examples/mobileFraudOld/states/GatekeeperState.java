package eu.ferari.examples.mobileFraudOld.states;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.mobileFraudOld.function.Function;
import eu.ferari.examples.mobileFraudOld.function.Tuple;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;


public class GatekeeperState implements IState {
	//private static final Logger logger = LoggerFactory.getLogger(GatekeeperState.class);

	private int defaultThreshold;

	private ISend sendLSVsumToCommunicator;

	private int Y = 10;
	private Map<String, Function> Functions;
	private boolean equalityInAboveThresholdRegion;
	private float defLSVValue;
	private boolean dynamicThreshold;
	private String functionName;
	private String functionLocation;
	private String conditionCheckingClassName;
	private int LSVDimension;
	private Function defaultf;

	
	public GatekeeperState(ISend sendLSVsumToCommunicator){
		this.sendLSVsumToCommunicator = sendLSVsumToCommunicator;
		Functions = new HashMap<String, Function>();
	}

	@Override
	public void update(DataTuple data) {
		Message message = new Message(data);
		if(functionName == null) {
			if(message.getType() == MessageType.ConfProperties) {
				List<String> properties = message.getConfProperties();
				int globalThreshold = Integer.parseInt(properties.get(1));
				int numBillingNodes = Integer.parseInt(properties.get(0));
				equalityInAboveThresholdRegion = Boolean.parseBoolean(properties.get(2));
				defLSVValue = Float.parseFloat(properties.get(3));
				dynamicThreshold = Boolean.parseBoolean(properties.get(4));
				functionName = properties.get(5);
				functionLocation = properties.get(6);
				conditionCheckingClassName = properties.get(22);
				LSVDimension =  Integer.parseInt(properties.get(23));
				defaultThreshold = globalThreshold / numBillingNodes;
				defaultf = SetFunction();
			}
			return;
		}
		//logger.debug("Received message {}", message);
		switch (message.getType()) {
		case CounterUpdate:
			OnCounterUpdate (message);
			break;
		case NewThreshold:
			OnNewThreshold(message);
			break;
		case GlobalV:
			OnGlobalV(message);
			break;
		case LSVToCoordinator:
			break;
		default:
		//	logger.error("Received message of type {} in gatekeeper", message.getType());
		}
	}
	public void setForTest(String phone, String nodeId){
		int globalThreshold = 2;
		int numBillingNodes = 5;
		equalityInAboveThresholdRegion = false;
		defLSVValue = 0.0f;
		dynamicThreshold = false;;
		functionName = "eu.ferari.examples.mobileFraudOld.function.IdentityFunction";
		functionLocation = "null";
		defaultThreshold = globalThreshold / numBillingNodes;
		defaultf = SetFunction();
		Function f;
		if(Functions.get(phone) == null)
		{
			f = SetFunction();
			f.InitializeNodeThreshold(nodeId);
			Functions.put(phone, f);
		}
		else
		{
			f = Functions.get(phone);
			f.resetFunction();
		}
	}
	
	public void OnGlobalV(Message message){
		Function Violation_function = Functions.get(message.getPhone());
		if(Violation_function  != null){
			Violation_function.setEstimate(message.getAvg());
		}
	}
	
	public void OnCounterUpdate(Message message){
		Function f;
		if(Functions.get(message.getPhone()) == null)
		{
			f = SetFunction();
			f.InitializeNodeThreshold(message.getNodeId());
			Functions.put(message.getPhone(), f);
		}
		else
		{
			f = Functions.get(message.getPhone());
			f.resetFunction();
		}
		Tuple tuple = new Tuple(message.getNodeId(), new Date (message.getTimestamp()), message.getCounter(), message.getPhone());
		f.insertVal(tuple);
		f.updateDV();
		//if (message.getCounter() > getThreshold(message.getPhone())){
		if(f.hasLocalViolation(tuple.getNodeId())) {
			f.updateLastSent(tuple.getNodeId());
			f.updateDV();
//			//logger.info("Passed threshold {} for phone {} with counter {} at timestamp {} node {}",
//					new Object[]{
//					f.getThresholds().get(message.getNodeId()),
//					message.getPhone(),
//					message.getCounter(),
//					message.getTimestamp(),
//					message.getNodeId()
//			});
			System.out.println("Passed threshold " + f.getThresholds().get(message.getNodeId())+" for phone "+ message.getPhone()+" with counter "+ message.getCounter()
					+" at timestamp " +message.getTimestamp()+" node "+message.getNodeId());
			if(f != null){
				f.updateLSV();
				Message LSVMessage = new Message(MessageType.LSV, message.getPhone(), message.getTimestamp(), f.sum(), f.numOfNodes(), message.getNodeId());
				sendLSVsumToCommunicator.signal(LSVMessage.toDataTuple());
			}
		} else {
//			logger.debug("Phone {} didn't pass threshold {} at timestamp {} with counter {}",
//					new Object[]{
//					message.getPhone(),
//					f.getThresholds().get(message.getNodeId()),
//					message.getTimestamp(),
//					message.getCounter()
//			});
		}
	}
	
	public void OnNewThreshold(Message message){
	//	logger.info("Updating threshold for phone {} to {}", message.getPhone(), message.getNewThreshold());
		Function V_f = Functions.get(message.getPhone());
		if(V_f == null) {
			V_f= SetFunction();
			V_f.InitializeNodeThreshold(message.getNodeId());
			Functions.put(message.getPhone(),V_f);
		}
		V_f.UpdateNodeThrehold(message.getNodeId(), message.getNewThreshold());
	}

	public int getThreshold(String phone, String nodeID){
		return (Functions.containsKey(phone) ? Functions.get(phone).getThresholds().get(nodeID) : defaultThreshold);
	}

	public int getDefaultThreshold(){
		return defaultThreshold;
	}

	public Function SetFunction()
	{
		URLClassLoader urlCl = null;
		Class<? extends Function> newFunction = null;
		Class[] argsClass = new Class[] {int.class, float.class, boolean.class,  float.class, boolean.class};
		try{
			newFunction = (Class<? extends Function>) Class.forName(functionName);
		} catch(ClassNotFoundException cnfe) {
			try {
				File file = new File(functionLocation);
				urlCl = new URLClassLoader(new URL[] {file.toURL(), new File("target\\classes").toURL()}, getClass().getClassLoader());   
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}

		try {
			if(newFunction == null) {
				newFunction = (Class<? extends Function>) urlCl.loadClass(functionName);
			}
			Integer YArg = new Integer (Y);
			Object[] args = new Object[] {YArg, defaultThreshold, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold};
			Constructor<? extends Function> argsConstructor = null;
			argsConstructor = newFunction.getConstructor(argsClass);
			Function f = argsConstructor.newInstance(args);
			return f;
		} catch (Exception e) {
			System.out.println(functionName+": Class not found for this function");
			return null;
		}
	}	


	public void SetEstimate(String phone, List<Float> avg)
	{
		Function Violation_function = Functions.get(phone);
		if(Violation_function  != null){
			Violation_function.setEstimate(avg);
		}
	}

	public void SetThreshold(String phone, String nodeId, int newThreshold)
	{
	//	logger.info("Updating threshold for phone {} to {}", phone, newThreshold);
		Function V_f = Functions.get(phone);
		if(V_f == null) {
			V_f= SetFunction();
			V_f.InitializeNodeThreshold(nodeId);
			Functions.put(phone,V_f);
		}
		V_f.UpdateNodeThrehold(nodeId, newThreshold);
	}
}
