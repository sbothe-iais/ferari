//package eu.ferari.examples.mobileFraud.storm.bolts;
//
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import eu.ferari.core.interfaces.ISend;
//import eu.ferari.examples.mobileFraud.states.StreamAggregatorState;
//import eu.ferari.backend.storm.sender.SimpleSender;
//import org.apache.storm.task.OutputCollector;
//import org.apache.storm.task.TopologyContext;
//import org.apache.storm.topology.OutputFieldsDeclarer;
//
//public class StreamAggregatorBolt extends BaseDistributedCountBolt {
//	private static final Logger logger = LoggerFactory.getLogger(StreamAggregatorBolt.class);
//	private static final long serialVersionUID = 4067829286249291922L;
//	private final String timeMachineBoltName;
//	private final String streamId;
//	
//	public StreamAggregatorBolt(String timeMachineBoltName, String streamId){
//		this.timeMachineBoltName = timeMachineBoltName;
//		this.streamId = streamId;
//	}
//	
//	@Override
//	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
//		super.prepare(conf, context, collector);		
//		ISend sendToTimeMachine = new SimpleSender(collector);
//		try {
//			this.state = new StreamAggregatorState(sendToTimeMachine);
//		} catch (InterruptedException e) {
//			logger.error("Error initializing stream aggregator state", e);
//			throw new RuntimeException(e);
//		}
//	}
//
//	@Override
//	public void declareOutputFields(OutputFieldsDeclarer declarer) {
//		declarer.declare(DEFAULT_OUTPUT_FIELDS);
//	}
//
//}
