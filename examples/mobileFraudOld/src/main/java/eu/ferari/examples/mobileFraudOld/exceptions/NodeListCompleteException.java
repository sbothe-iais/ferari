package eu.ferari.examples.mobileFraudOld.exceptions;

public class NodeListCompleteException extends Exception {

	public NodeListCompleteException() {
		super("Tried to add a node counter report to a full list");
	}

}
