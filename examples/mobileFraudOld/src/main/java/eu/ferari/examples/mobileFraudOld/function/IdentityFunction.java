package eu.ferari.examples.mobileFraudOld.function;

//import org.slf4j.LoggerFactory;

import java.util.Map;


public class IdentityFunction extends BallFunction {


    public IdentityFunction(int Y, float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, 1, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
 //       logger= LoggerFactory.getLogger(IdentityFunction.class);
    }
	
    /**
     * LSV[0]=LSV[1]*LSV[1]
     * LSV[1]=lastValues[lastValues.length-1]
     * This method is called whenever the bolt receives a new tuple from spout
     */
    @Override
    public void updateLSV(String NodeID) {
        for(Map.Entry<String, Tuple[]> lv: lastValues.entrySet() ){
            Tuple[] lvalue = lv.getValue();
            Float[] tmp = new Float[1]; 
            tmp[0] = (float)lvalue[lvalue.length-1].getValue();
            LSV.put(lv.getKey(), tmp);
        }
        //Util.PrintMap(LSV, "LSV");
    }

     
    /**
     * LSV[0]=LSV[1]*LSV[1]
     * LSV[1]=lastValues[lastValues.length-1]-estimate
     * This method is called from all bolts, before they calculate their sums.
     * This is done to update LSV arrays with the latest estimate.
     */
    @Override
    public void updateLSV()
    {
        for(Map.Entry<String, Tuple[]> lv: lastValues.entrySet() ){
            Tuple[] lvalue = lv.getValue();
            Float[] tmp =new Float[1]; 
            tmp[0] = (float)lvalue[lvalue.length-1].getValue();
            LSV.put(lv.getKey(), tmp);
        }
    }


	@Override
	public float F(Float[] vector) {		
		return vector[0];
	}

}
