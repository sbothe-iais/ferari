package eu.ferari.examples.mobileFraudOld.states;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;


import redis.clients.jedis.Jedis;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.mobileFraudOld.bare.senders.CommSender;
import eu.ferari.examples.mobileFraudOld.misc.DefaultHashMap;
import eu.ferari.core.misc.Event;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.examples.mobileFraud.misc.PullRequest;

public class CommunicatorState implements IState {
	//private static final Logger logger = LoggerFactory.getLogger(CommunicatorState.class);

	public enum State {
		Monitoring,
		PausingCounterRequestMessage,
		PausingLocalThresholdCrossing,
		WaitingForCoordinator
	}

	private ISend sendToTimeMachine;
	private ISend sendToGatekeeper;
	//private CommSender sendToCoordinator;
	private ISend sendToCoordinatorLSVviol;
	private ISend sendToTimeMachinePlay;
	private ISend sendToTimeMachinePull;
	private ISend sendToRoutingBolt;
	private ISend sendToContextBolt;
	/*	private CommSender sendToRedisPubSubSpout;
	private CommSender sendPushToInputSpout;*/
	private CommSender sender;
	private ISend sendToEpaManagerBolt;
	private String nodeId;
	private Map<String, State> phoneStates = new DefaultHashMap<String, State>(State.Monitoring);
	private Map<String, Long> timeRegister = new DefaultHashMap<String, Long>(-1l);
	private Map<String, Long> lastEventPerSite = new HashMap<String, Long>();
	private  boolean isCoordinator;
	private Map<Integer, PullRequest> pullRequests = new HashMap<Integer, PullRequest>();
	private static int pullRequestId = 1;

	private boolean isPropertiesSet =  false;
	private boolean isJsonSet =  false;

	public CommunicatorState(ISend sendToTimeMachine,
			ISend sendToTimeMachinePlay,
			ISend sendToTimeMachinePull,
			ISend sendToGatekeeper,
			ISend sendToCoordinatorLSVviol,
			ISend sendToRoutingBolt,
			ISend sendToContextBolt,
			ISend sendToEpaManagerBolt,
			Jedis publisher,
			String nodeID) {
		this.sender = new CommSender(nodeID, true);
		this.sendToTimeMachine = sendToTimeMachine;
		this.sendToGatekeeper = sendToGatekeeper;
		this.sendToCoordinatorLSVviol = sendToCoordinatorLSVviol;
		this.sendToRoutingBolt = sendToRoutingBolt;
		this.sendToContextBolt = sendToContextBolt;
		this.sendToEpaManagerBolt = sendToEpaManagerBolt;
		this.sendToTimeMachinePlay = sendToTimeMachinePlay;
		this.sendToTimeMachinePull = sendToTimeMachinePull;
		this.nodeId = nodeID;
	}

	@Override
	public void update(DataTuple data) {
		Message message = new Message(data);

		if(!isPropertiesSet) {

			if(message.getType() == MessageType.ConfProperties) {
				List<String> properties = message.getConfProperties();
				String commToRedisPubSubSpoutChannel = properties.get(14);
				String commToPushPullSpoutChannel = properties.get(17);
				String commToCoordinatorChannel = properties.get(13);
				isCoordinator = Boolean.parseBoolean(properties.get(16));
				/*				sendToRedisPubSubSpout = new CommSender(nodeId, false); // commToRedisPubSubSpoutChannel
				sendPushToInputSpout = new CommSender(nodeId, false); // commToPushPullSpoutChannel
				sendToCoordinator = new CommSender(nodeId, false); // commToCoordinatorChannel
				 */				isPropertiesSet = true;
			}
		}
		if(!isJsonSet) {
			if(message.getType() == MessageType.NewJsonFile) {
				onNewJsonFile(message);
				return;
			}
		}
		if(!isPropertiesSet || !isJsonSet) {
			return;
		}

		if (nodeId == null && message.getType().equals(MessageType.CounterUpdate)){
			//		logger.info("Setting nodeId to {}", message.getNodeId());
			//		logger.debug("nodeId set from message {}", message);
			nodeId = message.getNodeId();
		}

		switch (message.getType()) {
		case CounterUpdate:
			onCounterUpdate(message);
			break;
		case PauseConfirmation:
			onPauseConfirmation(message);
			break;
		case SendLSV:
			onLSVRequest(message);
			break;
		case UpdateThreshold:
			onUpdateThreshold(message);
			break;
		case LSV:
			onLSV(message);
			break;
		case Pull://Message from Routing Bolt
			onPull_fromRouting(message);
			break;
		case Pull2:
			onPull(message);
			break;
		case Push:
			onPush(message);
			break;
		case NewJsonFile:
			onNewJsonFile(message);
			break;
		case PushModeEvent:
			onpushmodeEvent_fromRouting(message);
			break;
		default:
			// TODO: do not send these stuff to me
			//throw new IllegalArgumentException("Don't know how to handle message of type " + message.getType());
		}
	}
	private void onPull_fromRouting(Message message){
		//send Pull message to CommSpout
		Message messagePull = new Message(MessageType.Pull, message.getPullEvents(), message.getStartingTime(), message.getEndingTime(), message.getNodeId());
		sender.signal(messagePull.toDataTuple(), message.getNodeId());
	}
	private void onpushmodeEvent_fromRouting(Message message){
		//send Pull message to CommSpout
		logMessageReceived(message);
		String EventType = message.getEventName();
		String nodes = message.getNodeIds();
		String[] parts = nodes.split(",");
		List<String> nodeIds = new ArrayList<String>();
		for(int i=0; i<parts.length; i++){
			nodeIds.add(parts[i]);
		}
		List<String> pushEvents = new ArrayList<String>();
		pushEvents.add(EventType);

		//		for (String siteId : new String[]{"site1", "site2", "site3", "site4", "cent1"}) {
		//			Message messagePull = new Message(MessageType.Pull, pushEvents, 0, Long.MAX_VALUE, siteId);
		//			System.err.println("Sent to " + siteId + " : "  + messagePull);
		//			sender.signal(messagePull.toDataTuple(), siteId);
		//		}
		for(int i=0; i<nodeIds.size(); i++){
			Message messagePull = new Message(MessageType.Pull, pushEvents, 0, Long.MAX_VALUE, nodeIds.get(i));
			for (String siteId : new String[]{"site1", "site2", "site3", "site4", "cent1"}) {
				//Message messagePull = new Message(MessageType.Pull, pushEvents, 0, Long.MAX_VALUE, siteId);
				System.err.println("Sent to " + siteId + " : "  + messagePull);
				sender.signal(messagePull.toDataTuple(), siteId);
			}
		}
	}

	private void onLSVRequest(Message message){
		logMessageReceived(message);

		if (message.getNodeId().equals(nodeId)){
			//	logger.debug("Ignoring counter request for node {} because this nodeId is {}", message.getNodeId(), nodeId);
			return;
		}

		String phone = message.getPhone();
		Message pausemessage;
		switch (getState(phone)) {
		case Monitoring:
			setStateAndTime(phone, State.PausingCounterRequestMessage, message.getTimestamp());
			pausemessage = new Message(MessageType.Pause, nodeId, message.getPhone(),
					message.getTimestamp());
			sendToTimeMachine.signal(pausemessage.toDataTuple());
			break;
		case PausingCounterRequestMessage:
		case PausingLocalThresholdCrossing:
			if (message.getTimestamp() < timeRegister.get(phone)){
				//		logger.debug("Received counter request for phone {} at time {} while it was at time {}. Will switch to earlier time.",
				//				phone, message.getTimestamp(), timeRegister.get(message.getTimestamp()));
				setStateAndTime(phone, State.PausingCounterRequestMessage, message.getTimestamp());
			}
			break;
		case WaitingForCoordinator:
			if (message.getTimestamp() < getRegisteredTime(phone)){
				//		logger.debug("Received counter request for phone {} at time {} while it was at time {}. Will switch to earlier time.",
				//				phone, message.getTimestamp(), getRegisteredTime(phone));
				setStateAndTime(phone, State.PausingCounterRequestMessage, message.getTimestamp());
				pausemessage = new Message(MessageType.Pause, message.getNodeId(), message.getPhone(),
						message.getTimestamp());
				sendToTimeMachine.signal(pausemessage.toDataTuple());
			}
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}

	public State getState(String phone){
		return phoneStates.get(phone);
	}

	/**
	 * For unit testing purposes only.
	 * @param nodeId
	 */
	public void setNodeId(String nodeId){
		this.nodeId = nodeId;
	}

	public long getRegisteredTime(String phone){
		return timeRegister.get(phone);
	}

	public void setStateAndTime(String phone, State state, long timestamp){
		//	logger.info("Switching phone {} at node {} to state {} at timestamp {}", phone, nodeId, state, timestamp);
		phoneStates.put(phone, state);
		timeRegister.put(phone, timestamp);
	}

	private void onCounterUpdate(Message message){
		logMessageReceived(message);

		String phone = message.getPhone();
		long timestamp = message.getTimestamp();

		switch (getState(phone)) {
		case Monitoring:
			setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
			sendToTimeMachine.signal(message.toPauseMessage(nodeId).toDataTuple());
			break;
		case PausingCounterRequestMessage:
			if (timestamp < timeRegister.get(phone))
				setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
			break;
		case PausingLocalThresholdCrossing:
			break;
		case WaitingForCoordinator:
			//	logger.error("Received counter update while waiting for coordinator");
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}

	private void onPauseConfirmation(Message message){
		logMessageReceived(message);

		String phone = message.getPhone();
		switch (getState(phone)) {
		case Monitoring:
			//	logger.error("Received pause confirmation while monitoring");
			break;
		case PausingCounterRequestMessage:
		case PausingLocalThresholdCrossing:
			Message counterReplyMessage =
			new Message(MessageType.LSVToCoordinator, phone, message.getTimestamp(), message.getLSVsum(), message.getNumNodes(), message.getNodeId());
			setStateAndTime(phone, State.WaitingForCoordinator, message.getTimestamp());
			if(isCoordinator){
				sendToCoordinatorLSVviol.signal(counterReplyMessage.toDataTuple());
			}
			else{
				sender.signal(counterReplyMessage.toDataTuple(), message.getNodeId());
			}
			break;
		case WaitingForCoordinator:
			//	logger.error("Received pause confirmation while waiting for coordinator");
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}

	private void onUpdateThreshold(Message message) {
		logMessageReceived(message);

		if (!message.getNodeId().equals(nodeId)){
			//	logger.info("Ignoring threshold update message for node {} because this node is {}", message.getNodeId(), nodeId);
			return;
		}

		switch (getState(message.getPhone())) {
		case Monitoring:
		case PausingCounterRequestMessage:
		case PausingLocalThresholdCrossing:
			//logger.error("Received threshold update while in {} state", getState(message.getPhone()));
			break;
		case WaitingForCoordinator:
			Message newThresholdMessage = new Message(MessageType.NewThreshold, message.getNodeId(), message.getPhone(), message.getNewThreshold());
			Message playMessage = new Message(MessageType.Play, message.getNodeId(), message.getPhone(), message.getTimestamp());
			unsetStateAndTime(message.getPhone());
			sendToGatekeeper.signal(newThresholdMessage.toDataTuple());
			sendToTimeMachinePlay.signal(playMessage.toDataTuple());
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(message.getPhone()));
		}
	}

	private void onLSV(Message message){
		logMessageReceived(message);

		String phone = message.getPhone();
		long timestamp = message.getTimestamp();

		switch (getState(phone)) {
		case Monitoring:
			setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
			sendToTimeMachine.signal(message.toPauseMessage(nodeId).toDataTuple());
			break;
		case PausingCounterRequestMessage:
			if (timestamp < timeRegister.get(phone))
				setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
			break;
		case PausingLocalThresholdCrossing:
			break;
		case WaitingForCoordinator:
			//	logger.error("Received counter update while waiting for coordinator");
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}

	private void onPull(Message message){
		StringBuilder sb = new StringBuilder("\n"+"_________________________Pull request at Communicator_________________________ "+ nodeId);
		sb.append("\n");
		sb.append("Pull events: --->" + message.getPullEvents());
		sb.append("\n");
		sb.append("StartingTime: ---> " + message.getStartingTime());
		sb.append("\n");
		sb.append("EndingTime: --->" + message.getEndingTime());
		sb.append("\n");
		sb.append("Node ID: --->" + message.getNodeId());
		sb.append("\n");
		sb.append("_______________________________________________________________________________ "+"\n");
		System.out.println(sb);
		for(Map.Entry<Integer, PullRequest> pr : pullRequests.entrySet()) {	
			List<String> EventType = pr.getValue().getEventTypes();
			long st =  pr.getValue().getStartTime();
			long et =  pr.getValue().getEndTime();
			String ni= pr.getValue().getNodeID();
			if(EventType.equals(message.getPullEvents()) && 
					st == message.getStartingTime() && 
					et == message.getEndingTime() && 
					ni.equals(message.getNodeId())){
				Message messagePullRequest = new Message(MessageType.PullRequest, message.getPullEvents(),
						message.getStartingTime(), message.getEndingTime(), pr.getKey());
				sendToTimeMachinePull.signal(messagePullRequest.toDataTuple());
				return;
			}
		}
		pullRequests.put(pullRequestId, new PullRequest(message.getPullEvents(), message.getStartingTime(), message.getEndingTime(), message.getNodeId()));
		Message messagePullRequest = new Message(MessageType.PullRequest, message.getPullEvents(), message.getStartingTime(), message.getEndingTime(), pullRequestId);
		pullRequestId++;
		sendToTimeMachinePull.signal(messagePullRequest.toDataTuple());
	}


	private void onPush(Message message){
		System.out.println(nodeId +": onPush(" + message + ")");
		//events from stream
		if(message.getPullRequestId() == 0){
			for(Map.Entry<Integer, PullRequest> pr : pullRequests.entrySet()) {
				PullRequest request = pr.getValue();
				List<Event> events = new ArrayList<Event>();
				events.addAll(message.getPushEvents());
				if(request.getEventTypes().contains(events.get(0).getEvent())){
					if(request.getStartTime() <= events.get(0).getOccurrenceTime() && request.getEndTime() >= events.get(events.size()-1).getOccurrenceTime()) {
						if(!lastEventPerSite.containsKey(events.get(0).getEvent() + "_" + request.getNodeID())){
							lastEventPerSite.put(events.get(0).getEvent() + "_" + request.getNodeID(), events.get(0).getOccurrenceTime());
						}else{
							if(lastEventPerSite.get(events.get(0).getEvent() + "_" + request.getNodeID()) < events.get(0).getOccurrenceTime()){
								lastEventPerSite.put(events.get(0).getEvent() + "_" + request.getNodeID(), events.get(0).getOccurrenceTime());
							}
						}
						Message m = new Message(MessageType.PushEvent, events.get(0).getEvent(), events.get(0).getAttributes(), request.getNodeID());
						if(!nodeId.equals( request.getNodeID())){
							try {
								System.err.println("onPush: " + nodeId + " sending to " + request.getNodeID());
								sender.signal(m.toDataTuple(), request.getNodeID());
							} catch (Exception e) {
								System.out.println("@ "+nodeId+ "CommState while trying to send push message!");
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		//past events
		else{
			PullRequest request = pullRequests.get(message.getPullRequestId());
			List<Event> events = new ArrayList<Event>();
			events.addAll(message.getPushEvents());

			for(Event event : events) {
				if(!lastEventPerSite.containsKey(event.getEvent() + "_" + request.getNodeID())){
					lastEventPerSite.put(event.getEvent() + "_" + request.getNodeID(), event.getOccurrenceTime());
					Message m = new Message(MessageType.PushEvent, event.getEvent(), event.getAttributes(), request.getNodeID());
					if(!nodeId.equals( request.getNodeID())){
						sender.signal(m.toDataTuple(), request.getNodeID());
					}
				}
				else{
					if(event.getOccurrenceTime() > lastEventPerSite.get(event.getEvent() + "_" + request.getNodeID())){
						lastEventPerSite.put(event.getEvent() + "_" + request.getNodeID(), event.getOccurrenceTime());
						Message m = new Message(MessageType.PushEvent, event.getEvent(), event.getAttributes(), request.getNodeID());
						if(!nodeId.equals( request.getNodeID())){
							sender.signal(m.toDataTuple(), request.getNodeID());
						}
					}
				}
			}
		}
	}

	private void onNewJsonFile(Message message) {
		String jsonString = message.getNewJsonFile();

		Message jsonMessage = new Message(MessageType.NewJsonFile, jsonString, null, false);
		isJsonSet = true;

		sendToRoutingBolt.signal(jsonMessage.toDataTuple());
		sendToContextBolt.signal(jsonMessage.toDataTuple());
		sendToEpaManagerBolt.signal(jsonMessage.toDataTuple());
	}


	private void unsetStateAndTime(String phone){
		//	logger.info("Deleting state and timestamp for phone {} at node {}", phone, nodeId);
		phoneStates.remove(phone);
		timeRegister.remove(phone);
	}

	private void logMessageReceived(Message message){
		//	logger.info("Received message. {}, when state is {}", message, getState(message.getPhone()));
		for (Map.Entry<String, State> kvp : phoneStates.entrySet()) {
			//		logger.debug("State of node {}: {} -> {} @ timestamp {}", nodeId, kvp.getKey(), kvp.getValue(), timeRegister.get(kvp.getKey()));
		}
	}
}
