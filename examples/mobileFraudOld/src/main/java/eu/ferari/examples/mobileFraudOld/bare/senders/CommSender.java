package eu.ferari.examples.mobileFraudOld.bare.senders;


import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.InterSiteClient;

import eu.ferari.core.DataTuple;
import eu.ferari.examples.mobileFraud.misc.Message;

public class CommSender {

	private final InterSiteClient commClient;
	private String context;

	public CommSender(String siteId, boolean connect2Optimizer) {
		this(siteId, connect2Optimizer, null);
	}
	
	public CommSender(String siteId, boolean connect2Optimizer, String context) {
		this.context = context;
		commClient = FerariClientBuilder.getInstance().createInterSiteClient(siteId, connect2Optimizer);
	}

	public void signal(DataTuple data, String siteId) {
		Message message = new Message(data);
		String json = message.toJSON();
		commClient.sendMsg(new eu.ferari.core.commIntegration.Event(json, context), siteId);
	}

	public int getTaskId() {
		return 0;
	}

	public void close() {
		commClient.close();
	}

}
