package eu.ferari.examples.mobileFraudOld.storm.bolts;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.examples.mobileFraudOld.states.TimeMachineState;
import eu.ferari.examples.mobileFraudOld.storm.ProtonMobileFraudTopology;
import eu.ferari.backend.storm.sender.DirectSender;

public class TimeMachineBolt extends BaseDistributedCountBolt {
	private static final long serialVersionUID = 8290306847153126869L;
	
	private static Map<String, TimeMachineState> instances = new ConcurrentHashMap<String, TimeMachineState>();
	
	private static transient AtomicInteger nextInt = new AtomicInteger();
	
	private volatile int index = 0;
	
	private String gatekeeperName;
	private String communicatorName;
	private	String toGatekeeperStreamId;
	private String toCommunicatorStreamId;
	private final String LSVToGatekeeperStreamId;
	private final String LSVToCommunicatorStreamId;
	private final String PushToCommunicatorStreamId;
	private final String nodeID;
	private final boolean sendToDashboard;
	private EntityManager entityManager;
	private EntityManagerFactory emfactory;
	
	public TimeMachineBolt(String gatekeeperName, 
			String communicatorName, 
			String LSVToCommunicatorStreamId, 
			String PushToCommunicatorStreamId, 
			String toGatekeeperStreamId, 
			String toCommunicatorStreamId,
			String LSVsumToGatekeeperStreamId,
			String nodeID,
			boolean sendToDashboard){
		this.gatekeeperName = gatekeeperName;
		this.communicatorName = communicatorName;
		this.toGatekeeperStreamId = toGatekeeperStreamId;
		this.toCommunicatorStreamId = toCommunicatorStreamId;
		this.LSVToGatekeeperStreamId = LSVsumToGatekeeperStreamId;
		this.LSVToCommunicatorStreamId = LSVToCommunicatorStreamId;
		this.PushToCommunicatorStreamId = PushToCommunicatorStreamId;
		this.sendToDashboard = sendToDashboard;
		this.nodeID = nodeID;
	}	

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		super.prepare(stormConf, context, collector);
		if (sendToDashboard && ProtonMobileFraudTopology.isInDevMode){
			if (entityManager == null) {
				emfactory = Persistence.createEntityManagerFactory("ferari");
				entityManager = emfactory.createEntityManager();
			}
		}
		
		String eventCtx = (String) stormConf.get(CommIntegrationConstants.EVENT_CONTEXT);
		
		ISend sendToGatekeeper = new DirectSender(collector, context, gatekeeperName, toGatekeeperStreamId);
		ISend sendToCommunicator = new DirectSender(collector, context, communicatorName, toCommunicatorStreamId);
		ISend sendLSVToGatekeeper = new DirectSender(collector, context, gatekeeperName, LSVToGatekeeperStreamId);
		ISend sendLSVToCommunicator = new DirectSender(collector, context, communicatorName, LSVToCommunicatorStreamId);
		ISend sendPushToCommunicator = new DirectSender(collector, context, communicatorName, PushToCommunicatorStreamId);
		this.state = new TimeMachineState(sendLSVToCommunicator, sendToGatekeeper, sendToCommunicator, 
				sendPushToCommunicator, sendLSVToGatekeeper, nodeID, entityManager,sendToDashboard, eventCtx);
		index = nextInt.getAndIncrement();
		instances.put(nodeID, (TimeMachineState) state);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(toGatekeeperStreamId, true, DEFAULT_OUTPUT_FIELDS);
		declarer.declareStream(toCommunicatorStreamId, true, new Fields("type", "nodeId", "phone", "timestamp", "counter", "LSV", "numNodes"));
		declarer.declareStream(LSVToGatekeeperStreamId, true, new Fields("type", "phone", "timestamp", "LSV", "numNodes"));
		declarer.declareStream(LSVToCommunicatorStreamId, true, new Fields("type", "phone", "timestamp", "LSV", "numNodes"));
		declarer.declareStream(PushToCommunicatorStreamId, true, new Fields("type", "pushEvents", "pullRequestId"));
	}	

	@Override
	public void cleanup() {
		TimeMachineState tmState = instances.get(nodeID);
		System.err.println("Closing tmState for " + nodeID);
		tmState.close();
		
		if (entityManager != null) {
			entityManager.close();
		}
		
		if (emfactory != null) {
			emfactory.close();
		}
	}
}
