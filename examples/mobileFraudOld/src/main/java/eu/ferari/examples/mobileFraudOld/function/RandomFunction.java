package eu.ferari.examples.mobileFraudOld.function;


//import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;


public class RandomFunction extends Function {


    private Random R=new Random();

    public RandomFunction(int Y, float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, Y, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
  //      logger = LoggerFactory.getLogger(RandomFunction.class);
    }
  
    
    @Override
    public boolean hasLocalViolation(String nodeID) {
        return R.nextDouble() <= threshold;
    }

    
    @Override
    public void updateLSV(String NodeID) {
        for (Map.Entry<String,Tuple[]> lv: lastValues.entrySet()){
            Tuple[] lvalue=lv.getValue();
            Float[] tmp = new Float[lvalue.length];
            for (int i=0;i<lvalue.length;i++){
                if(lvalue[i]!=null){
                    tmp[i] = (float)lvalue[i].getValue();
                }
                else{
                    tmp[i]=0f;
                }
            }
            LSV.put(lv.getKey(), tmp);
        }
 //       logger.info("LSV: {}", LSV);
    }
    
    @Override
    public void updateLSV()
    {
        
    }


    @Override
    public float F(Float[] what) {
        return 0;
    }

}
