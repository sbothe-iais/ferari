package eu.ferari.examples.mobileFraudOld.misc;

import java.io.Serializable;
import java.util.concurrent.LinkedBlockingQueue;

import eu.ferari.core.commIntegration.Event;

import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.MessageHandler;

import org.apache.storm.tuple.Values;



public class CommListener {
	
	private String nodeId;
	
	public CommListener(LinkedBlockingQueue<Serializable> queue, String nodeID, String context) {
		this.nodeId = nodeID;
		
		IntraSiteClient isc = FerariClientBuilder.getInstance().createIntraSiteClient(nodeId);
		isc.setbroadcastRequestHandler(new MessageHandler() {
			
			@Override
			public void onMessage(Serializable msg) {
				if (msg instanceof Values) {
					return;
				}
				if (msg instanceof String) {
					queue.add(msg);
					return;
				}
				if (! (msg instanceof FerariEvent)) {
					return;
				}
				Event event = (Event) ((FerariEvent) msg).getEvent();
				System.err.println("Got broadcast message: " + event.getContext() + "," + event.getEvent());
				if (context.equals(event.getContext()) || event.getContext() == null) {
					queue.add(msg);
				}
			}
		});
	}

/*	private Coordinator createCoordinator() {
		String selfPort = InMemoryGlobalAddressResolver.getInstance().getHostsOfSite(nodeId)[0].split(":")[1];
		Coordinator coord = new Coordinator(nodeId, nodeId, Integer.parseInt(selfPort), new EventConsumer() {
			
			@Override
			public void consumeEvent(FerariEvent event) {
				System.out.println(event.getSource() + ": " + ((String) event.getEvent()));
				queue.add((Event) event.getEvent());
			}
		});
		
		return coord;
	}*/
	
}
