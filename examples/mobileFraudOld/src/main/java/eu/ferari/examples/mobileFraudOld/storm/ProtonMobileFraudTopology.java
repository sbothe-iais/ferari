package eu.ferari.examples.mobileFraudOld.storm;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import eu.ferari.examples.mobileFraudOld.proton.ProtonOutputFacadeBolt;
import eu.ferari.examples.mobileFraudOld.storm.spouts.ShutdownDetectorSpout;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichSpout;

import com.ibm.ferari.coord.Coordinator;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;

import eu.ferari.examples.mobileFraudOld.Main;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.examples.mobileFraudOld.misc.JedisConfiguration;
import eu.ferari.examples.mobileFraud.proton.ProtonTopology;
import eu.ferari.examples.mobileFraud.proton.MissingInputConfigurationException;
import eu.ferari.examples.mobileFraudOld.misc.protonAdapter.ProtonTopologyBuilder;
import eu.ferari.examples.mobileFraudOld.storm.bolts.CommunicatorBolt;
import eu.ferari.examples.mobileFraudOld.storm.bolts.GatekeeperBolt;
import eu.ferari.examples.mobileFraudOld.storm.bolts.OptimizerBolt;
import eu.ferari.examples.mobileFraudOld.storm.bolts.TimeMachineBolt;
import eu.ferari.examples.mobileFraud.storm.spouts.OptiSpout;
import eu.ferari.examples.mobileFraudOld.storm.spouts.RedisPubSubSpout;


public class ProtonMobileFraudTopology {
//	private static final Logger logger = LoggerFactory.getLogger(DistributedCountTopology.class);

	private static final String COMMUNICATOR_SPOUT_NAME = "communicator_spout";
	private static final String OPTIMIZER_SPOUT_NAME = "optimizer_spout";

	private static final String TIME_MACHINE_BOLT_NAME = "timemachine";
	private static final String GATEKEEPER_BOLT_NAME = "gatekeeper";
	private static final String COMMUNICATOR_BOLT_NAME = "communicator";
	private static final String OPTIMIZER_BOLT_NAME = "optimizer";

	private static final String DEFAULT_STREAM_ID = "default_stream";
	private static final String TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID = "timemachine_to_communicator";
	private static final String COMMUNICATOR_TO_GATEKEEPER_STREAM_ID = "communicator_to_gatekeeper";
	private static final String COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID = "communicator_to_timemachine";
	private static final String COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID = "coordinator_to_comm_thresh_pass";
	private static final String COORDINATOR_TO_COMMUNICATOR_SENDLSV_STREAM_ID = "coordinator_to_timemachine_sendLSV_request";	
	private static final String TIMEMACHINE_TO_COORDINATOR_LSV_STREAM_ID = "timemachine_to_coordinator_lsv";
	private static final String COMMUNICATOR_TO_GATEKEEPER_ESTIMATE_STREAM_ID = "communicator_to_gatekeeper_Estimate";
	private static final String COORDINATOR_TO_COMMUNICATOR_ESTIMATE_STREAM_ID ="coordinator_to_comm_Estimate";
	private static final String COMMUNICATOR_TO_COORDINATOR_LSV_VIOL_STREAM_ID = "com_to_coord_CounterUpd";
	private static final String COMMUNICATORSPOUT_TO_COORDINATOR_COUNTER_UPDATE_STREAM_ID = "comSpout_to_coord_CounterUpd";
	private static final String COMMUNICATOR_TO_COORDINATOR_LSV_STREAM_ID ="ComSpout_to_coord_LSV";
	private static final String PUBSUBSPOUT_TO_TIMEMACHINE_PULL_STREAM_ID  ="ComSpout_to_timemachine_pull";
	private static final String INPUTSPOUT_TO_ROUTING_PUSH_STREAM_ID  ="InputSpout_to_routing_push";
	private static final String INPUTSPOUT_TO_CSVFORMATTER_JSON_STREAM_ID  ="InputSpout_to_csvformatter_json";
	private static final String TIMEMACHINE_TO_COMMUNICATOR_LSV_STREAM_ID = "timemachine_to_communicator_lsv";
	private static final String TIMEMACHINE_TO_COMMUNICATOR_PUSH_STREAM_ID = "timemachine_to_communicator_push";
	private static final String GATEKEEPER_TO_COMMUNICATOR_LSV_STREAM_ID = "gatekeeper_to_communicator_lsv";
	private static final String COMMUNICATOR_TO_TIMEMACHINE_PLAY_STREAM_ID = "communicator_to_timemachine_play";
	private static final String COMMUNICATOR_TO_TIMEMACHINE_PULL_STREAM_ID = "communicator_to_timemachine_pull";
	private static final String COMMUNICATORSPOUT_TO_PROTON_OUTPUT_FACADE_BOLT_PROPERTIES_STREAM_ID = "Comspout_to_POFB_properties";
	private static final String COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID = "Comspout_to_routing_properties";
	private static final String COMMUNICATORSPOUT_TO_TIMEMACHINE_PROPERTIES_STREAM_ID = "Comspout_to_timeMachine_properties";
	private static final String COMMUNICATORSPOUT_TO_GATEKEEPER_PROPERTIES_STREAM_ID = "Comspout_to_gatekeeper_properties";
	private static final String COMMUNICATORSPOUT_TO_COMMUNICATOR_PROPERTIES_STREAM_ID = "Comspout_to_communicator_properties";
	private static final String COMMUNICATORSPOUT_TO_COMMUNICATOR_NEW_JSON_STREAM_ID = "Comspout_to_communicator_jsonfile";
	private static final String OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON = "Optimizerspout_to_optimizer_json";
	private static final String OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS = "Optimizerspout_to_optimizer_parameters";
	private static final String OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_STREAM_ID = "optimizer_to_comspout_conffiles";
	private static final String OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_NEWJSON_STREAM_ID = "optimizer_to_comspout_newjson";
	private static final String OPTIMIZER_BOLT_TO_INPUTSPOUT_STREAM_ID = "optimizer_to_inputspout_conffiles";
	private static final String FILEREADERSPOUT_TO_ROUTING_BOLT_PRIM_EVENTS_STREAM_ID = "frs_to_routing_primitiveEvents";
	
	public static volatile boolean isInDevMode = true;


	private final String pathToProtonJSON;
	private boolean hasOptimizer;
	private final String nodeID;
	private final LocalCluster cluster = new LocalCluster();
	private boolean dashboard ;
	private	String	path;
	private boolean	fixdelay;
	private	int	delay;
	private String inputFileName;

	private final boolean local = true;

	public ProtonMobileFraudTopology(String protonFile, boolean hasOptimizer, String nodeID,
			boolean dashboard, String	path, boolean	fixdelay, int	delay, String inputFileName) throws IOException{
		this.nodeID = nodeID;
		this.pathToProtonJSON = protonFile;
		this.hasOptimizer = hasOptimizer;
		this.dashboard = dashboard;
		this.path = path;
		this.fixdelay = fixdelay;
		this.delay = delay;
		this.inputFileName = inputFileName;
		
		System.out.println(nodeID + " " + path + " " + inputFileName);
	}

	public void start(String topologyId) throws IOException {
		if (! nodeID.equals("cent1") && isInDevMode) {
			Coordinator coord = Main.createCoordinator(nodeID);
			coord.start();
		}
		
		System.err.println("Start invoked");

		System.out.println("Starting topology " + topologyId);
		createTopologyDescriptor(topologyId, new StormDescriptorConsumer() {
			
			@Override
			public void consume(StormTopology topology, Config stormConfig) {
				cluster.submitTopology("distributed-count-" + topologyId, stormConfig, topology);
			}
		}, "context");
	}

	public void createTopologyDescriptor(String topologyId, StormDescriptorConsumer consumer, String context) {
		try{
			ProtonTopology protontopology = new ProtonTopology();
			//		logger.info("Setting up topology "+ topologyId);
			Config stormConfig = new Config();
			stormConfig.setDebug(false);
			stormConfig.setMaxTaskParallelism(1);
			stormConfig.put(CommIntegrationConstants.EVENT_CONTEXT, context);

			TopologyBuilder builder = new TopologyBuilder();

			String protonRoutingBoltName= "protonRoutingBolt";
			String protonOutputBoltName = "protonOutputBolt";

			if(local){
				stormConfig.put("mode", "local");
				stormConfig.put("homedir", "./src/main/resources/config/outputFiles/");
				JedisConfiguration jedisConfig = new JedisConfiguration();
				stormConfig.put("jedisHost",jedisConfig.getHost());
			}
			else{
				stormConfig.put("jedisHost", "clu01.softnet.tuc.gr");
				Configuration conf = new Configuration();
				Path homeDir = null;

				conf.set("fs.defaultFS", "hdfs://clu01.softnet.tuc.gr:8020");
				//conf.set("fs.default.name", "hdfs://master:8020");
				conf.set("fs.hdfs.impl",org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
				conf.set("fs.file.impl",org.apache.hadoop.fs.LocalFileSystem.class.getName());
				conf.addResource(new Path("/etc/hadoop/conf.empty/core-site.xml"));
				conf.addResource(new Path("/etc/hadoop/conf.empty/hdfs-site.xml"));

				FileSystem hdfs;
				try {
					hdfs = FileSystem.get(new URI("hdfs://clu01.softnet.tuc.gr:8020"),conf);
					homeDir = hdfs.getHomeDirectory(); 
					System.out.println("!!!!home dir" + homeDir);
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
				stormConfig.put("mode", "remote");
				stormConfig.put("homedir", homeDir.toString()+"/");
			}
			
			BaseRichSpout communicatorRedisSpout = new RedisPubSubSpout(
					COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID,
					COORDINATOR_TO_COMMUNICATOR_SENDLSV_STREAM_ID,
					COORDINATOR_TO_COMMUNICATOR_ESTIMATE_STREAM_ID,
					COMMUNICATOR_BOLT_NAME,
					GATEKEEPER_BOLT_NAME,
					TIME_MACHINE_BOLT_NAME,
					COMMUNICATORSPOUT_TO_COORDINATOR_COUNTER_UPDATE_STREAM_ID,
					COMMUNICATOR_TO_COORDINATOR_LSV_STREAM_ID,
					PUBSUBSPOUT_TO_TIMEMACHINE_PULL_STREAM_ID,
					COMMUNICATORSPOUT_TO_PROTON_OUTPUT_FACADE_BOLT_PROPERTIES_STREAM_ID,
					COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID,
					COMMUNICATORSPOUT_TO_TIMEMACHINE_PROPERTIES_STREAM_ID,
					COMMUNICATORSPOUT_TO_GATEKEEPER_PROPERTIES_STREAM_ID,
					COMMUNICATORSPOUT_TO_COMMUNICATOR_PROPERTIES_STREAM_ID,
					COMMUNICATORSPOUT_TO_COMMUNICATOR_NEW_JSON_STREAM_ID,
					nodeID
					);
			
			try {
				ProtonTopologyBuilder.
				buildProtonTopology(
						builder,
						pathToProtonJSON,
						protonRoutingBoltName,
						protontopology,
						INPUTSPOUT_TO_ROUTING_PUSH_STREAM_ID,
						INPUTSPOUT_TO_CSVFORMATTER_JSON_STREAM_ID,
						COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID,
						FILEREADERSPOUT_TO_ROUTING_BOLT_PRIM_EVENTS_STREAM_ID,
						nodeID,
						inputFileName,
						delay, 
						fixdelay,
						path
						);

			} catch (MissingInputConfigurationException e){
				System.err.println("Proton configuration is missing.");
			}


			IRichBolt protonOutputBolt = new ProtonOutputFacadeBolt(COMMUNICATORSPOUT_TO_PROTON_OUTPUT_FACADE_BOLT_PROPERTIES_STREAM_ID);
			TimeMachineBolt timeMachineBolt = new TimeMachineBolt(
					GATEKEEPER_BOLT_NAME,
					COMMUNICATOR_BOLT_NAME,
					TIMEMACHINE_TO_COMMUNICATOR_LSV_STREAM_ID,
					TIMEMACHINE_TO_COMMUNICATOR_PUSH_STREAM_ID,
					DEFAULT_STREAM_ID,
					TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID,
					TIMEMACHINE_TO_COORDINATOR_LSV_STREAM_ID,
					nodeID, 
					dashboard
					);
			IRichBolt gatekeeperBolt = new GatekeeperBolt(
					COMMUNICATOR_BOLT_NAME,
					GATEKEEPER_TO_COMMUNICATOR_LSV_STREAM_ID
					);

			IRichBolt communicatorBolt = new CommunicatorBolt(
					TIME_MACHINE_BOLT_NAME,
					GATEKEEPER_BOLT_NAME,
					protonRoutingBoltName,
					"protonContextBolt",
					"protonEpaManagerBolt",
					COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID,
					COMMUNICATOR_TO_TIMEMACHINE_PLAY_STREAM_ID,
					COMMUNICATOR_TO_TIMEMACHINE_PULL_STREAM_ID,
					COMMUNICATOR_TO_GATEKEEPER_STREAM_ID,
					COMMUNICATOR_TO_GATEKEEPER_ESTIMATE_STREAM_ID,
					COMMUNICATOR_TO_COORDINATOR_LSV_VIOL_STREAM_ID,
					"metadatatoRouting",
					"metadatatoContext",
					"metadatatoEpaManager",
					nodeID
					);
			
			
			ShutdownDetectorSpout shutdownDetectorSpout = new ShutdownDetectorSpout(()-> {
				timeMachineBolt.cleanup();
			});
			
			builder.setSpout(UUID.randomUUID().toString(),shutdownDetectorSpout);

			builder.setSpout(COMMUNICATOR_SPOUT_NAME, communicatorRedisSpout, 1);
			BoltDeclarer protonOutputBoltDeclarer = builder.setBolt(protonOutputBoltName, protonOutputBolt);
			BoltDeclarer timeMachineBoltDeclarer =
					builder.setBolt(TIME_MACHINE_BOLT_NAME, timeMachineBolt, 1);
			BoltDeclarer gatekeeperBoltDeclarer =
					builder.setBolt(GATEKEEPER_BOLT_NAME, gatekeeperBolt, 1);
			BoltDeclarer communicatorBoltDeclarer =
					builder.setBolt(COMMUNICATOR_BOLT_NAME, communicatorBolt, 1);


			protonOutputBoltDeclarer.shuffleGrouping(protonRoutingBoltName, STORMMetadataFacade.CONSUMER_EVENTS_STREAM);
			protonOutputBoltDeclarer.shuffleGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_PROTON_OUTPUT_FACADE_BOLT_PROPERTIES_STREAM_ID);
			timeMachineBoltDeclarer.shuffleGrouping(protonRoutingBoltName, "Statistics");
			timeMachineBoltDeclarer.shuffleGrouping(protonRoutingBoltName, "Statistic");
			communicatorBoltDeclarer.shuffleGrouping(protonRoutingBoltName, "Pull");
			timeMachineBoltDeclarer.shuffleGrouping(protonRoutingBoltName, "Event");
			communicatorBoltDeclarer.shuffleGrouping(protonRoutingBoltName, "PushModeEvent");


			protontopology.getRoutingBoltDeclarer().allGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID);
			protontopology.getRoutingBoltDeclarer().directGrouping(COMMUNICATOR_BOLT_NAME, "metadatatoRouting");
			protontopology.getContextBoltDeclarer().directGrouping(COMMUNICATOR_BOLT_NAME, "metadatatoContext");
			protontopology.getEpaManagerBoltDeclarer().directGrouping(COMMUNICATOR_BOLT_NAME, "metadatatoEpaManager");


			timeMachineBoltDeclarer.shuffleGrouping(protonOutputBoltName);
			communicatorBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, DEFAULT_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_GATEKEEPER_STREAM_ID);
			timeMachineBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COORDINATOR_TO_COMMUNICATOR_SENDLSV_STREAM_ID);
			communicatorBoltDeclarer.allGrouping(COMMUNICATOR_SPOUT_NAME, COORDINATOR_TO_COMMUNICATOR_ESTIMATE_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, TIMEMACHINE_TO_COORDINATOR_LSV_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_GATEKEEPER_ESTIMATE_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_COORDINATOR_LSV_VIOL_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_COORDINATOR_COUNTER_UPDATE_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATOR_TO_COORDINATOR_LSV_STREAM_ID);
			gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_GATEKEEPER_PROPERTIES_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, TIMEMACHINE_TO_COMMUNICATOR_LSV_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(GATEKEEPER_BOLT_NAME, GATEKEEPER_TO_COMMUNICATOR_LSV_STREAM_ID);
			timeMachineBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_TIMEMACHINE_PLAY_STREAM_ID);
			timeMachineBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_TIMEMACHINE_PULL_STREAM_ID);
			timeMachineBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, PUBSUBSPOUT_TO_TIMEMACHINE_PULL_STREAM_ID);
			timeMachineBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_TIMEMACHINE_PROPERTIES_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, TIMEMACHINE_TO_COMMUNICATOR_PUSH_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_COMMUNICATOR_PROPERTIES_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_COMMUNICATOR_NEW_JSON_STREAM_ID);
			communicatorBoltDeclarer.directGrouping(COMMUNICATOR_SPOUT_NAME, "pullToCommunicator");

			if(hasOptimizer && isInDevMode) {
				System.out.println("************** Creating optmizer... *******************");
				BaseRichSpout optimizerSpout = new OptiSpout(OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON,
						OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS, nodeID);
				builder.setSpout(OPTIMIZER_SPOUT_NAME, optimizerSpout, 1);
				IRichBolt optimizer = new OptimizerBolt(OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_STREAM_ID,
						OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_NEWJSON_STREAM_ID, 
						OPTIMIZER_BOLT_TO_INPUTSPOUT_STREAM_ID,
						OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON, 
						OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS, 
						inputFileName, path);
				BoltDeclarer optimizerBoltDeclarer = builder.setBolt(OPTIMIZER_BOLT_NAME, optimizer, 1);
				optimizerBoltDeclarer.allGrouping(OPTIMIZER_SPOUT_NAME, OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON);
				optimizerBoltDeclarer.allGrouping(OPTIMIZER_SPOUT_NAME, OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS);
			}
			//		logger.info("Submitting topology");
			String topologyName = "distributed-count-" + topologyId;
			if(!local){	
				try {
					System.err.println("Running non locally");
					StormSubmitter.submitTopology(topologyName,stormConfig,builder.createTopology());
				} catch (AlreadyAliveException | InvalidTopologyException e) {
					e.printStackTrace();
				}
			}
			else{
				System.err.println("Running locally");
				stormConfig.put("mode", "local");
				consumer.consume(builder.createTopology(), stormConfig);
			}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void stop(){
		//		logger.info("Shutting down topology");
		cluster.shutdown();
	}
	
	public static interface StormDescriptorConsumer {
		public void consume(StormTopology topology, Config stormConfig);
	}
}
