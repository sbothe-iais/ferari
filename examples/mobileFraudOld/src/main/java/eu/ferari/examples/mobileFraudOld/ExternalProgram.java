package eu.ferari.examples.mobileFraudOld;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ExternalProgram {
	
	private static final int PORT = 4550;

	private ServerSocket socket;
	private PrintWriter sender;

	private BufferedReader readerJson;
	private BufferedReader readerParameters;
	private String pathParameters;
	private String pathJson;

	private static String INPUT_FOLDER = "./src/main/resources/config/inputFiles/";

	public ExternalProgram(int port) {
		try {
			this.socket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		pathParameters = INPUT_FOLDER + "networkParameters2.1_.csv";
		//pathParameters = INPUT_FOLDER + "networkParameters.csv";
		pathJson = INPUT_FOLDER + "Synthetic.json";
		//pathJson = INPUT_FOLDER + "MobileFraud_add3.json";
		//pathJson = INPUT_FOLDER + "MobileFraud_kr.json";

		readerParameters = setReader(pathParameters, readerParameters);
		readerJson = setReader(pathJson, readerJson);
		String json;
		String networkParameters;
		String networkParameters_main = null;
		
		//read json file
		StringBuilder sb= new StringBuilder("j");
		String line="";
		while(line != null){
			try {
				line = readerJson.readLine();
				if(line !=null){
					sb.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		json = sb.toString();

		//read network parameters file
		StringBuilder sb1 = new StringBuilder("n");
		String linepr = "";
		while(linepr != null){
			try {
				linepr = readerParameters.readLine();
				if(linepr != null){
					sb1.append(linepr);
					networkParameters_main = linepr;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		networkParameters = sb1.toString();
		try {
			Socket socket = this.socket.accept();
			sender = new PrintWriter(socket.getOutputStream(), true);
			sender.println(networkParameters_main);
			System.out.println("External program sent network parameters to Main ");
			sender.flush();
			Thread.sleep(1000);
			sender.close();
			socket = this.socket.accept();
			sender = new PrintWriter(socket.getOutputStream(), true);
			//send json
			sender.println(json);
			System.out.println("External program sent Json to Optimizer Spout");
			Thread.sleep(500);
			//send network parameters
			sender.println(networkParameters);
			System.out.println("External program sent network parameters to Optimizer Spout");
			sender.close();
			//sender.flush();
			Thread.sleep(1000);

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	private BufferedReader setReader(String path, BufferedReader reader) {
		try {
			if(reader != null) {
				reader.close();
			}
			reader= new BufferedReader(new FileReader(new File(path)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return reader;
	}
	public static void main(String[] args) {
		System.out.println("Starting External Program");
		ExternalProgram main = new ExternalProgram(PORT);
		main.run();
	}
}
