package eu.ferari.examples.mobileFraudOld.function;


abstract public class BallFunction extends Function {

    protected int dimensional;

    public BallFunction(int Y, float threshold, int estimateLength, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, estimateLength, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
        dimensional = estimateLength;
    }

    @Override
    public boolean hasLocalViolation(String NodeID) {
        return hasBallViolation(NodeID);
    }

    public boolean hasBallViolation(String NodeID) {
        int range = 21;     //grid range
        int Counter = 0;
        int X;
        boolean violation = false;
        int limit = (int) Math.pow(range, dimensional);

        double max = F(estimate);

        double radius = radius(NodeID);
        Float[] center = center(NodeID);
    	float threshold = thresholds.get(NodeID);
        while( Counter < limit )
        {
            X = Counter;

            double dist = 0.0;
            Float[] point =  new Float[dimensional];
            for(int i = 0 ; i < dimensional; i++){
                point[i] = center[i] - (float)radius + 2 * (float)radius * (X % range) / (range - 1);
                dist = dist + ((point[i] - center[i]) * (point[i] - center[i]));
                X /= range;
            }

            if(dist <= radius * radius) {
                double currValue = F(point);
                if(equalityInAboveThresholdRegion){
                    if((max < threshold && currValue >= threshold) || (max >= threshold && currValue < threshold)){
                        violation = true;
                        break;
                    } 
                }
                else{
                    if((max <= threshold && currValue > threshold) || (max > threshold && currValue <= threshold)){
                        violation = true;
                        break;
                    } 
                }                                               
            }
            Counter ++;
        }
        return violation;  
    }    


    /**
     *C = (estimate + driftVector) / 2
     */
    public Float[] center(String NodeID){   
        Float[] drftvalue = driftVector.get(NodeID);
        Float[] tmp = new Float[drftvalue.length];
        for (int i = 0; i < drftvalue.length; i++) {
            tmp[i] = drftvalue[i];
        }
        Float[] center = new Float[dimensional];
        for(int i = 0; i < dimensional; i++){   
            center[i] = (estimate[i] + tmp[i]) /2;      
        }
        return center;       
    }


    /**
     * r = |(estimate - driftVector)| / 2
     */
    public double radius(String NodeID){   
        double radius = 0;
        float sqrRadius = 0;
        Float[] drftvalue = driftVector.get(NodeID);
        Float[] tmp = new Float[drftvalue.length];
        for (int i = 0; i < drftvalue.length; i++){
            tmp[i] = drftvalue[i];
        }
        Float[] value = new Float[dimensional];
        for(int i = 0; i < dimensional; i++) {   
            value[i] = (estimate[i] - tmp[i]) /2;
            sqrRadius = sqrRadius + (value[i] * value[i]);
        }
        radius = Math.sqrt(sqrRadius); 
        return radius;
    }

}
