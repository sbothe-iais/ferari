package eu.ferari.examples.mobileFraudOld.storm.spouts;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;

import com.google.gson.Gson;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.MessageHandler;

import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.core.commIntegration.Event;
import eu.ferari.backend.storm.sender.DirectSender;

public class PushAndPullSpout extends BaseRichSpout {
	private LinkedBlockingQueue<String> queue;
	private String nodeID;
	private Gson gson;
	private DirectSender pushSender;
	private Map _stormConf;
	private final String routingName;
	private final String pushStream;



	public PushAndPullSpout(String nodeID, String routingName, String pushStream){
		this.nodeID = nodeID;
		this.routingName = routingName;
		this.pushStream = pushStream;
	}

	@Override
	public void nextTuple() {
		String json = queue.poll();	
		try {
			if (json != null) {
				Message message = gson.fromJson(json, Message.class);

				if(!nodeID.equals(message.getNodeId())) {
					return;
				}

				if(message.getType().equals(Message.MessageType.PushEvent)) {
					if(message.getAttributes().containsKey("call_start_date")){
						double call_start_date = (Double)message.getAttributes().get("call_start_date");
						message.getAttributes().put("call_start_date", (long)call_start_date );
					}
					pushSender.signal(message.toDataTuple());
					return;

				} else {
					//throw new IllegalArgumentException("Don't know how to handle message type " + message.getType());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void open(Map stormConf, TopologyContext context, SpoutOutputCollector collector) {
		_stormConf = stormConf;
		gson = new Gson();
		pushSender = new DirectSender(collector, context, routingName, pushStream);

		queue = new LinkedBlockingQueue<String>(1000);	
		

		IntraSiteClient intraSiteClient = FerariClientBuilder.getInstance().createIntraSiteClient(nodeID);
		intraSiteClient.setbroadcastRequestHandler(new MessageHandler() {
			
			@Override
			public void onMessage(Serializable msg) {
				if (! (msg instanceof FerariEvent)) {
					return;	
				}
				String m = ((Event) ((FerariEvent) msg).getEvent()).getEvent();
				System.err.println("PushAndPullSpout: " + m);
				queue.add(m);
			}
		});
		
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(pushStream, new Fields("type", "Name", "attributes", "nodeId"));
	}
}
