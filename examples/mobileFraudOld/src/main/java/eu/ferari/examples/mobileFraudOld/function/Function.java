package eu.ferari.examples.mobileFraudOld.function;

//import org.slf4j.Logger;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public abstract class Function implements  Serializable{
    
	protected Map<String,Float[]> LSV;       
    protected Map<String,Tuple[]> lastValues;  //stores the last Y times values received for each NodeID
    protected Map<String,Integer> counts;      //holds the number of tuples received for each NodeID
    protected Map<String,Float[]> lastSent;    //the last sent LSV
    protected Map<String,Float[]> dV;          //LSV - LastSend
    protected int Y;                            //Number of tuples stored in lastValues
    protected Float[] estimate;
    protected HashMap<String, Float[]> driftVector;    //Estimate + DV
    protected float threshold;
    protected boolean isEstimateNotZero = false;
    protected boolean equalityInAboveThresholdRegion ;
    protected int estimateLength;
    protected float defLSVValue;
    protected boolean dynamicThreshold;
    protected HashMap<String, Integer> thresholds;
//    protected Logger logger;


    public HashMap<String, Integer> getThresholds() {
		return thresholds;
	}


	public Map<String, Float[]> getLSV() {
		return LSV;
	}

	public Map<String, Float[]> getLSVcopy() {
		Map<String,Float[]> copyLSV = new HashMap<String, Float[]>();       
		for(Entry<String, Float[]> lsv: LSV.entrySet())	
		{
			Float[] newlsv = new Float[lsv.getValue().length]; 
			for(int i=0; i<lsv.getValue().length; i++)
			{
				newlsv[i] = lsv.getValue()[i];
			}
			copyLSV.put(lsv.getKey(), newlsv);
		}
		return copyLSV;
	}

	public Map<String, Tuple[]> getLastValues() {
		return lastValues;
	}


	public Map<String, Integer> getCounts() {
		return counts;
	}


	public Map<String, Float[]> getLastSent() {
		return lastSent;
	}


	public Map<String, Float[]> getdV() {
		return dV;
	}


	public int getY() {
		return Y;
	}


	public Float[] getEstimate() {
		return estimate;
	}


	public HashMap<String, Float[]> getDriftVector() {
		return driftVector;
	}


	public float getThreshold() {
		return threshold;
	}


	public boolean isEstimateNotZero() {
		return isEstimateNotZero;
	}


	public boolean isEqualityInAboveThresholdRegion() {
		return equalityInAboveThresholdRegion;
	}
	
	
    public int getEstimateLength() {
        return estimateLength;
    }

    public float getDefLSVValue() {
		return defLSVValue;
	}


	public boolean isDynamicThreshold() {
		return dynamicThreshold;
	}

    public Function(int Y, float threshold, int estimateLength, boolean equalityInAboveThresholdRegion, float defValue, boolean dynamicThreshold) {
        this.Y = Y;
        this.threshold = threshold;
        this.equalityInAboveThresholdRegion = equalityInAboveThresholdRegion;
        this.estimateLength = estimateLength;
        counts = new HashMap<String, Integer>();
        lastValues = new HashMap<String,Tuple[]>();
        lastSent = new HashMap<String,Float[]>();
        estimate = new Float[estimateLength];
        dV = new HashMap<String,Float[]>();
        driftVector = new HashMap<String,Float[]>();
        LSV = new HashMap<String,Float[]>();
        this.defLSVValue = defValue;
        this.dynamicThreshold = dynamicThreshold;
        thresholds = new HashMap<String, Integer>();

        

        // Initialize estimate to zero
        for (int i = 0; i < estimateLength; i++){
            estimate[i] = 0.0f;
        }
    }


	//abstract methods
    public abstract boolean hasLocalViolation(String NodeID);
    public abstract void updateLSV(String NodeID);
    public abstract void updateLSV();
    public abstract float F(Float[] vector);


    //non abstract methods


    /**
     * This method inserts the tuples sent by the spouts to lastValues, shifting its values left,
     * keeping the last Y values.
     * It also updates the counts hash map which holds the number of tuples received for each nodeID.
     * This method is called whenever new message received from a spout.
     * @param newValue The tuple sent by the spout.  
     */
    public void insertVal(Tuple newValue){

        String nodeID = newValue.getNodeId();
        Integer countValue = counts.get(nodeID);
        if(countValue == null) {
            counts.put(nodeID, 1);
        } else {
            counts.put(nodeID, countValue + 1);
        }

        Tuple[] values = lastValues.get(nodeID);
        if(values == null) {
            values = new Tuple[Y];  
        } else {
            for (int i = 1; i < Y; i++ ){
                values[i - 1] = values[i];
            }
        }
        values[Y - 1] = newValue;
        lastValues.put(nodeID, values);
        updateLSV(nodeID);
        updateDV();
    }


    /**
     * This method is called whenever the coordinator requests from all bolts to send their sums.
     * Whenever a bolt sends its sum , it stores in the lastSent hash map the LSV vector. 
     */
    public void updateLastSent(){
        lastSent.clear();
        for (Map.Entry<String, Float[]> lsv: LSV.entrySet()){
            Float[] lastValue = lsv.getValue();
            Float[] tmp = new Float[lastValue.length];
            for (int i = 0; i < lastValue.length; i++){
                tmp[i] = lastValue[i];
            }
            lastSent.put(lsv.getKey(), tmp);
        }
        updateDV();
    }


    /**
     * This method is called whenever a violation is detected from a bolt.
     * It updates lastSent hash map with the NodeID which caused the violation.
     * @param NodeID The node id which caused the violation.
     */
    public void updateLastSent(String NodeID){
        Float[] lastValue = LSV.get(NodeID);
        if(lastValue != null){   
            Float[] tmp = new Float[lastValue.length];
            for (int i = 0; i < lastValue.length; i++){
                tmp[i] = lastValue[i];
            }
            lastSent.put(NodeID, tmp);   
        }
        updateDV();
    }


    /**
     * This method updates the DV hash map each time lastValues or lastSent is updated.
     * DV = LSV - lastSent
     */
    public void updateDV(){
        for (Map.Entry<String, Float[]> lsv: LSV.entrySet()){     
            Float[] lsvValue = lsv.getValue();   
            Float[] tmp = new Float[lsvValue.length];
            for (int i = 0; i < lsvValue.length; i++){
                if (lsvValue[i] != null){
                    tmp[i] = lsvValue[i];
                }
                else{
                    tmp[i] = null;
                }
            }
            Float[] last = lastSent.get(lsv.getKey());
            if(last != null){
                for (int i = 0; i < lsvValue.length; i++){
                    if (last[i] != null){
                        tmp[i] = tmp[i] - last[i];
                    }
                } 
            }
            dV.put(lsv.getKey(), tmp);
        }
        setDriftVector();
    }

    
    /** drift vector= e + dV */
    public void setDriftVector(){
        for (Entry<String, Float[]> dv: dV.entrySet()){
            Float[] dvValue = dv.getValue();
            Float[] tmp = new Float[dvValue.length];
            for (int i = 0; i < dvValue.length; i++){
                tmp[i] = dvValue[i];
            }
            Float[] drift = new Float[estimateLength];
            for(int i = 0; i < estimateLength; i++){
                drift[i] = estimate[i] + tmp[i];
            }
            driftVector.put(dv.getKey(), drift);
        }
    }

    /**
     * This method calculate the sum that each bolt is sending to coordinator.
     * The sum is calculated like this:
     *  |Key| Values                                      |
     *  |ID1| valueID1[0]| valueID1[1]| ...| valueID1[Y-1]|
     *  |ID2| valueID2[0]| valueID2[1]| ...| valueID2[Y-1]|
     *  |ID3| valueID3[0]| valueID3[1]| ...| valueID3[Y-1]|
     *  
     *  Then the result will be:
     *  |valueID1[0]+valueID2[0]+valueID3[0] | valueID1[1]+valueID2[1]+valueID3[1] |...| valueID1[Y-1]+valueID2[Y-1]+valueID3[Y-1] |
     * @return
     */
    public Float[] sum(){
        StringBuilder sb = new StringBuilder("\n");
        sb.append("Bolt ");
        sb.append(":");
        sb.append("\n");
        Float[] sum = new Float[estimateLength];
        for (int i = 0; i < estimateLength; i++){
            sum[i]= defLSVValue;
        }
        if(LSV.size() == 0){
            sb.append("adding ");
            sb.append(Arrays.toString(sum));
            sb.append(" to sum");
            sb.append("\n");
        }
        else{
            for (Map.Entry<String, Float[]> lv: LSV.entrySet() ){    
                Float[] lvalue = lv.getValue();   
                float[] tmp = new float[lvalue.length];
                for (int i = 0; i < lvalue.length; i++){
                    if (lvalue[i] != null){
                        tmp[i] = lvalue[i];
                    }
                    else{
                        tmp[i] = 0;
                    }
                    sum[i] = sum[i] + tmp[i];
                }
                sb.append("adding ");
                sb.append(Arrays.toString(tmp));
                sb.append(" to sum");
                sb.append("\n");
            }
            sb.append("Sending sum to coordinator :");
            sb.append(Arrays.toString(sum));
            sb.append("\n");
//            logger.info(sb.toString());
        }
        return sum;
    }


    /**
     * The coordinator requires the number of Nodes that each bolt manages to compute the average
     * @return The number of Nodes managed by this bolt.
     */
    public int numOfNodes(){
        return LSV.size();
    }


    /**
     * This method stores the estimate message received from coordinator.
     * @param newEstimate The estimate vector received from coordinator. It copies its parameter to
     * the bolt's estimate vector
     */
    public void setEstimate(List<Float> newEstimate){
        Float tmp[] = new Float[newEstimate.size()];
        tmp = newEstimate.toArray(tmp);
        isEstimateNotZero = false;
        for (int i = 0; i < estimateLength; i++){
            estimate[i] = new Float(tmp[i]);
            if (estimate[i] != 0){
                isEstimateNotZero = true;
            }
        }
        setDriftVector();
    }
   

    /**
     * This method returns True if a new NodeID has appeared for the first time in the input
     * file and estimate is not all zeros.
     */
    public boolean isNumberOfNodesChanged(Tuple newvalue){
        Integer c = counts.get(newvalue.getNodeId());
        if(c != 1)
            return false;
        else 
            return isEstimateNotZero;
    }

    public boolean LSVNotEmpty()
    {
        if(LSV.isEmpty()){
            return false;
        }
        return true;
    }


    /**
     * Deletes the Node with Node ID = NodeID
     * @param NodeID The Node ID to delete
     * @return True if the deletion was successful
     */
    public boolean deleteNode(int NodeID)
    {
        if(lastValues.containsKey(NodeID)){
            lastValues.remove(NodeID);
            LSV.remove(NodeID);
            dV.remove(NodeID);
            driftVector.remove(NodeID);
            lastSent.remove(NodeID);
            counts.remove(NodeID);
            return true;
        }
        else{
            return false;
        }
    }
    
    
    public void resetFunction()
    {
    	lastValues.clear();
    	LSV.clear();
    	dV.clear();
    	driftVector.clear();
    	lastSent.clear();
    	counts.clear();
    	for (int i = 0; i< estimateLength; i++)
    	{
    		estimate[i] = 0.0f;
    	}
    	isEstimateNotZero = false;
    }
   
    
    public void InitializeNodeThreshold(String NodeID)
    {
        if(dynamicThreshold){
            if(thresholds.get(NodeID) == null)
            {
                thresholds.put(NodeID, 0);
            }
        }
        else
        {
            if(thresholds.get(NodeID) == null)
            {
                thresholds.put(NodeID, (int)threshold);
            }
        }
    }
    
    
    public void UpdateNodeThrehold(String NodeID, int threshold)
    {
        thresholds.put(NodeID, threshold);
    }
}
