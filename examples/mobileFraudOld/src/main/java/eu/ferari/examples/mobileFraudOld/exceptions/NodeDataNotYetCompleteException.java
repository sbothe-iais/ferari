package eu.ferari.examples.mobileFraudOld.exceptions;

public class NodeDataNotYetCompleteException extends Exception {

	public NodeDataNotYetCompleteException() {
		super("Node data not yet complete");
	}

}
