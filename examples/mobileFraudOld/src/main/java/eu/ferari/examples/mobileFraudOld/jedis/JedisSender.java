package eu.ferari.examples.mobileFraudOld.jedis;

import com.google.gson.Gson;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import redis.clients.jedis.Jedis;

/**
 * Created by mofu on 15/09/15.
 */ // TODO: consolidate all Jedis classes
public class JedisSender implements ISend {

	private final Jedis publisherJedis;
	private final String channel;
	private final Gson gson = new Gson();

	public JedisSender(Jedis publisherJedis, String channel) {
		this.publisherJedis = publisherJedis;
		this.channel = channel;
	}

	public int getTaskId() {
		//TODO
		return 0;
	}

	public void signal(DataTuple data) {
        String message = gson.toJson(data);
        publisherJedis.publish(channel, message);
    }

}
