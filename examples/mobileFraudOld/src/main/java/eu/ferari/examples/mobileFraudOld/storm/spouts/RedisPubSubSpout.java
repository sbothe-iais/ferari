package eu.ferari.examples.mobileFraudOld.storm.spouts;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.TopologyRequestHandler;

import eu.ferari.core.DataTuple;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.core.commIntegration.TopologyCustomConfig;
import eu.ferari.examples.mobileFraudOld.misc.CommListener;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.backend.storm.sender.DirectSender;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.commIntegration.Event;

/**
 * Stolen from
 * https://github.com/stormprocessor/storm-redis-pubsub/blob/master/src
 * /jvm/yieldbot/storm/spout/RedisPubSubSpout.java
 *
 */
public class RedisPubSubSpout extends BaseRichSpout {

	public static enum ConfigKeys {
		commToCoordinatorChannel, 
		coordinatorToCommChannel, 
		commToRedisPubSubSpoutChannel, 
		commToInputSpoutChannel,
		commToPushPullSpoutChannel
	}

	public static enum ConfigKeysGeometric {
		globalThreshold, 
		numBillingNodes, 
		equalityInAboveThresholdRegion, 
		defLSVValue, 
		dynamicThreshold, 
		functionName, 
		functionLocation,
		isCoordinator, 
		monitoringObject,
		windowType,
		conditionCheckingClassName,
		LSVDimension
	}

	public static enum ConfigRoutingBoltKeys {
		pull, 
		statisticsClass, 
		epoch, 
		eType, 
		jarLocation, 
		startingTime, 
		endingTime, 
		pushEvent, 
		pushEventReciever
	}

	static {
		FerariClientBuilder.getInstance().setAddressResolver(InMemoryGlobalAddressResolver.getInstance());
	}
	
	static final long serialVersionUID = 737015313288609460L;
//	private static final Logger logger = LoggerFactory.getLogger(RedisPubSubSpout.class);

	private SpoutOutputCollector collector;
	private Map _stormConf;
	private LinkedBlockingQueue<Serializable> queue;
/*	private JedisPool jedisPool;
*/	private  String coordinatorToCommSpoutChannel;
	private  String commToCoordinatorChannel;
	private  String commToRedisPubSubSpoutChannel;
	private final String thresholdUpdateStream;	
	private final String sendLSVStream; 
	private final String globalVStream;
	private final String CounterUpdateStream;
	private final String LSVStream;
	private final String pullStream;
	private final String ConfPropertiesToProtonOutputFacadeBoltStream;
	private final String ConfPropertiesToRoutingBoltStream;
	private final String ConfPropertiesToTimeMachineStream;
	private final String ConfPropertiesToGateKeeperStream;
	private final String ConfPropertiesToCommunicatorStream;
	private final String JsonToCommunicatorStream;
	private final String pulltocomStream = "pullToCommunicator";
	private final String communicatorName;
	private final String gateKeeperName;
	private final String timeMachineName;
	private final String nodeID;

	private Gson gson;
	private DirectSender thresholdUpdateSender;
	private DirectSender sendLSVSender;
	private DirectSender globalVSender;
	private DirectSender LSVToCoordSender;
	private DirectSender pullSender;
	private DirectSender pullTocommSender ;
	private DirectSender propToTimeMachineSender;
	private DirectSender propToGateKeeperSender;
	private DirectSender propToCommunicatorSender;
	private DirectSender jsonToCommunicatorSender;
	private  boolean isCoordinator;
	private List<String> ConfProperties;
	
	private IntraSiteClient _intraSiteClient;
	private boolean gotTopology = false;
	
	
	public RedisPubSubSpout(String thresholdUpdateStream, 
			String sendLSVStream, 
			String globalVStream, 
			String communicatorName, 
			String gateKeeperName, 
			String timeMachineName, 
			String CounterUpdateStream, 
			String LSVStream, 
			String pullStream, 
			String ConfPropertiesToProtonOutputFacadeBoltStream, 
			String ConfPropertiesToRoutingBoltStream, 
			String ConfPropertiesToTimeMachineStream,
			String ConfPropertiesToGateKeeperStream,
			String ConfPropertiesToCommunicatorStream,
			String JsonToCommunicatorStream,
			String nodeID) {
		this.thresholdUpdateStream = thresholdUpdateStream;
		this.CounterUpdateStream = CounterUpdateStream;
		this.LSVStream = LSVStream;
		this.pullStream = pullStream;
		this.ConfPropertiesToProtonOutputFacadeBoltStream = ConfPropertiesToProtonOutputFacadeBoltStream;
		this.ConfPropertiesToRoutingBoltStream = ConfPropertiesToRoutingBoltStream;
		this.ConfPropertiesToTimeMachineStream = ConfPropertiesToTimeMachineStream;
		this.ConfPropertiesToGateKeeperStream = ConfPropertiesToGateKeeperStream;
		this.ConfPropertiesToCommunicatorStream = ConfPropertiesToCommunicatorStream;
		this.JsonToCommunicatorStream = JsonToCommunicatorStream;
		this.communicatorName = communicatorName;
		this.gateKeeperName = gateKeeperName;
		this.timeMachineName = timeMachineName;
		this.sendLSVStream = sendLSVStream;
		this.globalVStream = globalVStream;
		this.nodeID = nodeID;
	}	

	public void open(Map stormConf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
		this._stormConf = stormConf;
		gson = new Gson();
		thresholdUpdateSender = new DirectSender(collector, context, communicatorName, thresholdUpdateStream);
		sendLSVSender = new DirectSender(collector, context, communicatorName, sendLSVStream);
		globalVSender = new DirectSender(collector, context, gateKeeperName, globalVStream);
		LSVToCoordSender = new DirectSender(collector, context, gateKeeperName, LSVStream);
		pullSender = new DirectSender(collector, context, timeMachineName, pullStream);
		pullTocommSender = new DirectSender(collector, context, communicatorName, pulltocomStream);
		propToTimeMachineSender = new DirectSender(collector, context, timeMachineName, ConfPropertiesToTimeMachineStream);
		propToGateKeeperSender = new DirectSender(collector, context, gateKeeperName, ConfPropertiesToGateKeeperStream);
		propToCommunicatorSender = new DirectSender(collector, context, communicatorName, ConfPropertiesToCommunicatorStream);
		jsonToCommunicatorSender = new DirectSender(collector, context, communicatorName, JsonToCommunicatorStream);
		queue = new LinkedBlockingQueue<Serializable>();
		ConfProperties = new ArrayList<String>();
		
		CommListener listener = new CommListener(queue, nodeID, (String) stormConf.get(CommIntegrationConstants.EVENT_CONTEXT));
		
		//CommSender sndr = new CommSender(nodeID, true);
		
		_intraSiteClient = FerariClientBuilder.getInstance().createIntraSiteClient(nodeID);
		_intraSiteClient.setTopologyRequestHandler(new TopologyRequestHandler() {
			
			@Override
			public void onTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
				if (gotTopology) {
					System.out.println(nodeID + " already got topology, ignoring");
					return;
				}
				
				gotTopology = true;
				
				Serializable s = customConfig.get(nodeID);
				if (s == null) {
					System.err.println("No " + nodeID + " in topoloy file");
					return;
				}
				
				if (nodeID.equals("cent1")) {
					System.err.println("+++++++++++++++++++ cent1 got topology ++++++++++++++++");
				}

				TopologyCustomConfig conf = (TopologyCustomConfig) s;
				queue.add(new Event(conf.getConfFile()));
				queue.add(new Event(conf.getJsonFile()));
			}
		});
		
		System.out.println("Redis pubsub spout started for " + nodeID);
		
		
/*		RedisListenerThread listener = new RedisListenerThread(queue, jedisPool, "CommSpoutChannel", nodeID);
		listener.start();*/
	}

	public void close() {
		//jedisPool.close();
	}

	public void nextTuple() {
		String json = takeJSONfromQueue();
		
		if (json == null) {
			return;
		}

		System.err.println(nodeID + ":Got message " + json);
		try {
			Message message = gson.fromJson(json, Message.class);

			switch (message.getType()) {
			case UpdateThreshold:
				thresholdUpdateSender.signal(message.toDataTuple());
				break;
			case SendLSV:
				sendLSVSender.signal(message.toDataTuple());
				break;
			case GlobalV:
				globalVSender.signal(message.toDataTuple());
				break;
			case LSVToCoordinator:
				LSVToCoordSender.signal(message.toDataTuple());
				break;
			case Pull:
				System.err.println("line 245");
				Message pullRequestMessage = new Message(MessageType.Pull2, message.getPullEvents(),
						message.getStartingTime(), message.getEndingTime(), message.getNodeId());
				pullTocommSender.signal(pullRequestMessage.toDataTuple());
				break;
			case PullRequest:
				pullSender.signal(message.toDataTuple());
				break;
			case ConfFiles:
				if (!nodeID.equals(message.getNodeId())) {
					return;
				}
				processConfigurationChange(message);
				break;
			case NewJsonFile:
				if (!nodeID.equals(message.getNodeId())) {
					return;
				}
				System.out.println("Site " + nodeID + " received JsonFile from Optimizer");
				jsonToCommunicatorSender.signal(message.toDataTuple());
				break;
			default:
				throw new IllegalArgumentException("Don't know how to handle message type " + message.getType());
			}
		} catch (Exception e) {
		}
	}

	private void processConfigurationChange(Message message) {
		System.err.println("Site " + nodeID + " received Configuration files from Optimizer");
		Properties routingBoltProperties = parsePropertiesString(message.getRoutingBoltPropertiesFile());
		Properties routingProperties = parsePropertiesString(message.getTopologyPropertiesFile());
		Properties geometricProperties = parsePropertiesString(message.getGeometricPropertiesFile());
		this.commToCoordinatorChannel = routingProperties
				.getProperty(ConfigKeys.commToCoordinatorChannel.toString());
		this.commToRedisPubSubSpoutChannel = routingProperties
				.getProperty(ConfigKeys.commToRedisPubSubSpoutChannel.toString());
		this.coordinatorToCommSpoutChannel = routingProperties
				.getProperty(ConfigKeys.coordinatorToCommChannel.toString());
		this.isCoordinator = Boolean
				.valueOf(geometricProperties.getProperty(ConfigKeysGeometric.isCoordinator.toString()));

/*		RedisListenerThread listener = new RedisListenerThread(queue, jedisPool, coordinatorToCommSpoutChannel,
				nodeID);
		listener.start();*/
/*		if (isCoordinator) {
			RedisListenerThread coordinatorlistener = new RedisListenerThread(queue, jedisPool,
					commToCoordinatorChannel, nodeID);
			coordinatorlistener.start();
		}
		RedisListenerThread commToRedisPubSubSpoutChannelListener = new RedisListenerThread(queue, jedisPool,
				commToRedisPubSubSpoutChannel, nodeID);
		commToRedisPubSubSpoutChannelListener.start();*/

		String commToPushPullSpoutChannel = routingProperties
				.getProperty(ConfigKeys.commToPushPullSpoutChannel.toString());

		int numBillingNodes = Integer
				.valueOf(geometricProperties.getProperty(ConfigKeysGeometric.numBillingNodes.toString()));
		int globalThreshold = Integer
				.valueOf(geometricProperties.getProperty(ConfigKeysGeometric.globalThreshold.toString()));
		boolean equalityInAboveThresholdRegion = Boolean.valueOf(
				geometricProperties.getProperty(ConfigKeysGeometric.equalityInAboveThresholdRegion.toString()));
		float defLSVValue = Float
				.valueOf(geometricProperties.getProperty(ConfigKeysGeometric.defLSVValue.toString()));
		boolean dynamicThreshold = Boolean
				.valueOf(geometricProperties.getProperty(ConfigKeysGeometric.dynamicThreshold.toString()));
		String functionName = geometricProperties.getProperty(ConfigKeysGeometric.functionName.toString());
		String functionLocation = geometricProperties
				.getProperty(ConfigKeysGeometric.functionLocation.toString());
		String windowType = geometricProperties.getProperty(ConfigKeysGeometric.windowType.toString());
		String conditionCheckingClassName = geometricProperties.getProperty(ConfigKeysGeometric.conditionCheckingClassName.toString());
		int LSVDimension = Integer
				.valueOf(geometricProperties.getProperty(ConfigKeysGeometric.LSVDimension.toString()));
		String monitoringObject = geometricProperties
				.getProperty(ConfigKeysGeometric.monitoringObject.toString());

		boolean pull = Boolean
				.valueOf(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.pull.toString()));
		String statisticsClass = routingBoltProperties
				.getProperty(ConfigRoutingBoltKeys.statisticsClass.toString());
		int epoch = Integer.valueOf(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.epoch.toString()));
		String eType = routingBoltProperties.getProperty(ConfigRoutingBoltKeys.eType.toString());
		String jarLocation = routingBoltProperties.getProperty(ConfigRoutingBoltKeys.jarLocation.toString());
		double startingTime = Double
				.valueOf(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.startingTime.toString()));
		double endingTime = Double
				.valueOf(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.endingTime.toString()));
		String pushEvents = routingBoltProperties.getProperty(ConfigRoutingBoltKeys.pushEvent.toString());
		String pushEventReciever = routingBoltProperties
				.getProperty(ConfigRoutingBoltKeys.pushEventReciever.toString());

		ConfProperties.clear();

		ConfProperties.add(Integer.toString(numBillingNodes));
		ConfProperties.add(Integer.toString(globalThreshold));
		ConfProperties.add(Boolean.toString(equalityInAboveThresholdRegion));
		ConfProperties.add(Float.toString(defLSVValue));
		ConfProperties.add(Boolean.toString(dynamicThreshold));
		ConfProperties.add(functionName);
		ConfProperties.add(functionLocation);
		ConfProperties.add(windowType);
		ConfProperties.add(Boolean.toString(pull));
		ConfProperties.add(statisticsClass);
		ConfProperties.add(Integer.toString(epoch));
		ConfProperties.add(eType);
		ConfProperties.add(jarLocation);
		ConfProperties.add(commToCoordinatorChannel);
		ConfProperties.add(commToRedisPubSubSpoutChannel);
		ConfProperties.add(coordinatorToCommSpoutChannel);
		ConfProperties.add(Boolean.toString(isCoordinator));
		ConfProperties.add(commToPushPullSpoutChannel);
		ConfProperties.add(Double.toString(startingTime));
		ConfProperties.add(Double.toString(endingTime));
		ConfProperties.add(pushEvents);
		ConfProperties.add(pushEventReciever);
		ConfProperties.add(conditionCheckingClassName);
		ConfProperties.add(Integer.toString(LSVDimension));

		Values values = new Values();
		values.add(ConfProperties);
		collector.emit(ConfPropertiesToRoutingBoltStream, values);

		values = new Values();
		values.add(monitoringObject);
		collector.emit(ConfPropertiesToProtonOutputFacadeBoltStream, values);

		Message propMessage = new Message(MessageType.ConfProperties, ConfProperties);
		DataTuple dt = propMessage.toDataTuple();
		propToTimeMachineSender.signal(dt);
		propToGateKeeperSender.signal(dt);
		propToCommunicatorSender.signal(dt);
		// path to proton file?
		// TODO send params
	}

	private String takeJSONfromQueue() {
		String json;
		while (true) {
			try {
				Object o = queue.take();
				Serializable event;
				if (o instanceof FerariEvent) {
					event = ((FerariEvent) o).getEvent();
				} else {
					event = (Serializable) o;
				}
				if (event instanceof Event) {
					json = ((Event)event).getEvent();
				} else {
					json = (String) event;
				}
				break;
			} catch (InterruptedException e) {
			}
		}
		return json;
	}

	private Properties parsePropertiesString(String s) {
		final Properties p = new Properties();
		try {
			p.load(new StringReader(s));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}

	public void ack(Object msgId) {
		// TODO Auto-generated method stub

	}

	public void fail(Object msgId) {
		// TODO Auto-generated method stub

	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(thresholdUpdateStream, new Fields("type", "nodeId", "phone", "timestamp", "counter"));
		declarer.declareStream(sendLSVStream, new Fields("type", "nodeId", "phone", "timestamp"));
		declarer.declareStream(globalVStream, new Fields("type", "phone","timestamp", "estimate"));
		declarer.declareStream(CounterUpdateStream, new Fields("type", "nodeId", "phone", "timestamp", "counter"));
		declarer.declareStream(LSVStream, new Fields("type", "phone","timestamp", "LSVsum", "numNodes", "nodeId"));
		declarer.declareStream(pullStream, new Fields("type", "pullEvents", "starting", "ending", "pullrequestID"));
		declarer.declareStream(pulltocomStream, new Fields("type", "pullEvents", "starting", "ending", "noeID"));
		declarer.declareStream(ConfPropertiesToProtonOutputFacadeBoltStream, new Fields("monitoringObject"));
		declarer.declareStream(ConfPropertiesToRoutingBoltStream, new Fields("confProperties"));
		declarer.declareStream(ConfPropertiesToTimeMachineStream, new Fields("type", "confProperties"));
		declarer.declareStream(ConfPropertiesToGateKeeperStream, new Fields("type", "confProperties"));
		declarer.declareStream(ConfPropertiesToCommunicatorStream, new Fields("type", "confProperties"));
		declarer.declareStream(JsonToCommunicatorStream, new Fields("type", "jsonFile", "nodeId", "unused"));
	}
}
