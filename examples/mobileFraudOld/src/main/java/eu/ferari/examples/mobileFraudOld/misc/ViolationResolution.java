package eu.ferari.examples.mobileFraudOld.misc;

import eu.ferari.examples.mobileFraud.misc.Message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViolationResolution {

	private int nodeCounter = 0;     //the number of bolts that have sent their data

	private List<Float> inpt;
	private List<Float> sumofLSV;       
	private int estimatelength;
	private int sumnumofNodes;
	private int numNodes;


	public ViolationResolution(int numNodes) {
		this.inpt = new ArrayList<Float>();
		this.sumofLSV = new ArrayList<Float>();
		this.numNodes = numNodes;
	}

	public void x(Message message){
		inpt.clear();
		Collections.addAll(inpt, (Float[])message.getLSVsum());
		estimatelength = inpt.size();
		nodeCounter++;
		if(nodeCounter == 1){
			for (int i = 0; i < estimatelength ; i++){ 
				sumofLSV.add(i, 0.0f);
			}
		}
		sumnumofNodes += message.getNumNodes();

		for (int i = 0; i < estimatelength ; i++){ 
			sumofLSV.set(i, sumofLSV.get(i) + inpt.get(i));
		}

	}

	public List<Float> EstimateCalculation(Message message, int numNodes){
		List<Float> avg = new ArrayList<Float>();

			if(sumnumofNodes != 0){
				for (int i = 0 ; i < estimatelength ; i++){
					avg.add(i, sumofLSV.get(i) / sumnumofNodes);
				}
			}
			else{
				for (int i = 0 ; i < estimatelength ; i++){
					avg.add(i, 0.0f);
				}
			}

			sumofLSV.clear();
			nodeCounter = 0;
			sumnumofNodes = 0;

			return avg;
	}

	public int getNodeCounter() {
		return nodeCounter;
	}
}

