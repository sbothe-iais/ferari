package eu.ferari.examples.mobileFraudOld.function;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AverageFunction extends BallFunction{

    private Logger logger = LoggerFactory.getLogger(BallFunction.class);
    public AverageFunction(int Y, float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(Y, threshold, 2, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
    }

    
    /**
     * This method is called whenever the bolt receives a new tuple from spout
     * In this function, the LSV hash map stores the last two values that have been received for 
     * each Node ID.
     * The index (0 or 1) of LSV where the most recent value is stored is alternating 
     * between the two indices. For example, the first value received is stored in LSV[0], 
     * the second in LSV[1], the third in LSV[0], the fourth in LSV[1] and so on. 
     */
    @Override
    public void updateLSV(String NodeID) {
        Tuple[] values = lastValues.get(NodeID);
        Integer counter = counts.get(NodeID);
        Float[] tmp = new Float[dimensional]; 
        if(counter == 1){
            tmp[counter % dimensional] = 0.0f;
            tmp[(counter % dimensional) - 1] = (float)values[values.length - 1].getValue();
        }
        else{
            if(counter % dimensional == 0){
                tmp[counter % dimensional] = LSV.get(NodeID)[counter % dimensional];
                tmp[(counter % dimensional) + 1] = (float)values[values.length - 1].getValue();
            }
            else{
                tmp[counter % dimensional] = LSV.get(NodeID)[counter % dimensional];
                tmp[(counter % dimensional) - 1] = (float)values[values.length - 1].getValue();
            }
        }

        LSV.put(NodeID, tmp);

        logger.info("LSV: {}", LSV);
    }
    

    @Override
    public void updateLSV(){
        
    }

    @Override
    /**
     * LSV[0]*LSV[0] + LSV[1]*LSV[1]
     * This method tries to determine if the actual average of LSV is inside the sphere with radius 
     * sqrt(Threshold)
     */
    public float F(Float[] vector) {
        return (vector[0] * vector[0]) + (vector[1] * vector[1]);
    }
}
