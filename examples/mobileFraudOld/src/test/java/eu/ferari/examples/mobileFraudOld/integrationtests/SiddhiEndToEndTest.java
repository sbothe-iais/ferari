//package eu.ferari.examples.mobileFraud.integrationtests;
//
//import static org.mockito.Mockito.*;
//
//import eu.ferari.examples.mobileFraud.misc.Coordinator;
//import eu.ferari.examples.mobileFraud.storm.SiddhiMobileFraudTopology;
//import org.junit.Test;
//
//import backtype.storm.utils.Utils;
//import eu.ferari.core.DataTuple;
//import eu.ferari.core.interfaces.ISend;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//public class SiddhiEndToEndTest {
//
//	Logger logger = LoggerFactory.getLogger(SiddhiEndToEndTest.class);
//	
//	@Test
//	public void test() throws Exception {		
//        ISend mockGlobalViolationReporter = mock(ISend.class);
//		SiddhiMobileFraudTopology topology1 = new SiddhiMobileFraudTopology("endToEnd/events-a11.txt", "endToEnd/topology-a11.properties", "endToEnd/topology-a11.geometricproperties");
//		SiddhiMobileFraudTopology topology2 = new SiddhiMobileFraudTopology("endToEnd/events-a12.txt", "endToEnd/topology-a12.properties", "endToEnd/topology-a12.geometricproperties");
//		Coordinator coordinator = new Coordinator("endToEnd/topology-a11.properties", "endToEnd/topology-a11.geometricproperties", mockGlobalViolationReporter);
//		coordinator.start();
//		topology1.start();
//		topology2.start();
//		Utils.sleep(60000);
//		coordinator.stop();
//		logger.info("Coordinator stopped");
//		topology1.stop();
//		logger.info("Topo1 stopped");
//		topology2.stop();
//		logger.info("Topo2 stopped");
//
//		DataTuple violation1Tuple = new DataTuple("1", 1435768679000l, 3);
//		DataTuple violation2Tuple = new DataTuple("1", 1435768689000l, 4);
//		verify(mockGlobalViolationReporter, times(1)).signal(violation1Tuple);
//		verify(mockGlobalViolationReporter, times(1)).signal(violation2Tuple);
//	}
//
//}
