//package eu.ferari.examples.mobileFraud.integrationtests;
//
//import static org.junit.Assert.*;
//import static org.hamcrest.Matchers.*;
//
//import java.io.IOException;
//import java.util.Iterator;
//import java.util.Properties;
//import java.util.concurrent.LinkedBlockingQueue;
//
//import org.junit.Test;
//
//import com.google.gson.Gson;
//
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//import JedisConfiguration;
//import eu.ferari.examples.mobileFraud.misc.Message;
//import RedisListenerThread;
//import eu.ferari.examples.mobileFraud.misc.Utils;
//import eu.ferari.examples.mobileFraud.storm.SiddhiMobileFraudTopology;
//
//public class SiddhiSingleNodeTest {
//	
//	@Test
//	public void testMessagesSentToRedisAsExpected() throws IOException {
//		Properties properties = Utils.loadProperties("topology.properties", this);
//		String commToCoordinatorChannel = properties.getProperty("commToCoordinatorChannel");
//		
//		final JedisPoolConfig poolConfig = new JedisPoolConfig();
//		final JedisConfiguration jedisConfig = new JedisConfiguration();
//		final JedisPool jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(),
//				jedisConfig.getPort(), jedisConfig.getTimeout());
//		
//		LinkedBlockingQueue<String> messageQueue = new LinkedBlockingQueue<String>(5);
//        RedisListenerThread redisListener = new RedisListenerThread(messageQueue, jedisPool, commToCoordinatorChannel);
//        redisListener.start();
//        
//        SiddhiMobileFraudTopology topology = new SiddhiMobileFraudTopology("singleNodeEvents.txt", "topology.properties", "topology.geometricproperties");
//		topology.start();
//        
//        backtype.storm.utils.Utils.sleep(5000);
//		redisListener.quit();
//		topology.stop();
//        jedisPool.close();
//        jedisPool.destroy();
//
//        assertThat(messageQueue.size(), is(equalTo(2)));
//        Gson gson = new Gson();
//        for (Iterator<String> iterator = messageQueue.iterator(); iterator.hasNext();){
//        	String json = iterator.next();
//        	if (json == null)
//        		continue;
//            Message message = gson.fromJson(json, Message.class);
//            switch (message.getPhone()) {
//			case "1":
//				assertThat(message.getCounter(), is(equalTo(2)));
//				break;
//			case "2":
//				assertThat(message.getCounter(), is(equalTo(2)));
//				break;
//			default:
//				fail("Unknown phone. Message: " + message);
//			}
//        }   
//	}
//}
