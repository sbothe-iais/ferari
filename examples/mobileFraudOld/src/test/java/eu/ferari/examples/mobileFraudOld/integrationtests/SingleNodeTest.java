package eu.ferari.examples.mobileFraudOld.integrationtests;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class SingleNodeTest {

	Logger logger = LoggerFactory.getLogger(SingleNodeTest.class);
	@Test
	public void testMessagesSentToRedisAsExpected() throws IOException {
		/*
		Properties properties = Utils.loadProperties("topology.properties", this);
		String commToCoordinatorChannel = properties.getProperty("commToCoordinatorChannel");
		
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		final JedisConfiguration jedisConfig = new JedisConfiguration();
		final JedisPool jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(),
				jedisConfig.getPort(), jedisConfig.getTimeout());
		
		LinkedBlockingQueue<String> messageQueue = new LinkedBlockingQueue<String>(5);
        RedisListenerThread redisListener = new RedisListenerThread(messageQueue, jedisPool, commToCoordinatorChannel);
        redisListener.start();
        String[] properties1 = new String[2];
        properties1[0] = "topology.properties";
        properties1[1] = "topology.geometricproperties";
        MobileFraudTopology topology = new ProtonMobileFraudTopology("singleNode.json",properties1);
		topology.start();
        
        backtype.storm.utils.Utils.sleep(60000);
        topology.stop();
        redisListener.quit();        
        jedisPool.close();
        jedisPool.destroy();

		//TODO for proton only one Violation?
        assertThat(messageQueue.size(), is(equalTo(1)));
        Gson gson = new Gson();
        for (String json : messageQueue){
        	if (json == null)
        		continue;
            Message message = gson.fromJson(json, Message.class);
            switch (message.getPhone()) {
			case "1":
				assertThat(message.getCounter(), is(equalTo(3)));
				break;
			case "2":
				assertThat(message.getCounter(), is(equalTo(1)));
				break;
			default:
				fail("Unknown phone. Message: " + message);
			}
        }
        */
	}
}
