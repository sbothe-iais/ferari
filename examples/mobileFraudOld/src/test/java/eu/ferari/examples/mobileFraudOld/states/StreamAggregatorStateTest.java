//package eu.ferari.examples.mobileFraud.states;
//
//import static org.mockito.Mockito.*;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Test;
//
//import eu.ferari.core.DataTuple;
//import eu.ferari.core.interfaces.ISend;
//import eu.ferari.examples.mobileFraud.misc.Message;
//import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
//
//public class StreamAggregatorStateTest {
//	
//	/**
//	 * Assumes a 5 second time window
//	 * @throws InterruptedException
//	 */
//	@Test
//	public void testCountsAsExpected() throws InterruptedException {
//		List<DataTuple> input = new ArrayList<DataTuple>(){{
//			add(new DataTuple("1", "1", 1366335804341l));
//			add(new DataTuple("1", "1", 1366335804342l));
//			add(new DataTuple("1", "2", 1366335804342l));
//			add(new DataTuple("1", "1", 1366335814341l));
//			add(new DataTuple("1", "1", 1366335814345l));
//			add(new DataTuple("1", "1" ,1366345824341l));
//		}};
//		List<Message> expectedOutput = new ArrayList<Message>(){{
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335804341l, 1);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335804342l, 2);
//			new Message(MessageType.CounterUpdate, "1", "2", 1366335804342l, 1);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335814341l, 1);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335814345l, 2);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366345824341l, 1);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335809341l, 1);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335809342l, 0);
//			new Message(MessageType.CounterUpdate, "1", "2", 1366335809342l, 0);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335819341l, 1);
//			new Message(MessageType.CounterUpdate, "1", "1", 1366335819345l, 0);
//		}};
//		ISend mockSendToTimeMachine = mock(ISend.class);
//		
//		StreamAggregatorState state = new StreamAggregatorState(mockSendToTimeMachine);
//		for (DataTuple tuple : input)
//			state.update(tuple);
//		state.shutdown();
//		
//		for (Message message : expectedOutput)
//			verify(mockSendToTimeMachine, times(1)).signal(message.toDataTuple());
//	}
//
//}
