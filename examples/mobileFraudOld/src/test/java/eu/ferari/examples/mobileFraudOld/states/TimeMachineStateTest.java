package eu.ferari.examples.mobileFraudOld.states;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.client.FerariClientBuilder;
import eu.ferari.core.DataTuple;
import eu.ferari.examples.mobileFraudOld.states.TimeMachineState;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mockito;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;
import eu.ferari.examples.mobileFraudOld.states.TimeMachineState.PhoneState;

import java.util.Arrays;
import java.util.List;

//TODO: examine all warnings
public class TimeMachineStateTest {
	private ISend mockSendToGatekeeper;
	private ISend mockSendToCommunicator;
	private ISend mocksendPushToCommunicator;
	private ISend mocksendLSVToGatekeeper;
	private ISend mocksendLSVToCommunicator;
	private TimeMachineState timeMachineState;
	private String nodeId;
	private String phone;
	private long timestamp;
	
	@Before
	public void setUp() throws Exception {
		AddressResolver addressResolver = new AddressResolver() {

			@Override
			public String[] getOptimizerHosts() {
				return new String[] { "localhost:5610" };
			}

			@Override
			public String[] getHostsOfSite(String site) {
				if (site.equals("unknown")) {
					return new String[] { "unknown:5611" };
				}
				if (site.equals("TEST")) {
					return new String[] { "localhost:5611" };
				}
				if (site.equals("TEST2")) {
					return new String[] { "localhost:5612" };
				}
				return new String[] { "localhost:5610" };
			}

			@Override
			public String getOptimizerSite() {
				return "CENTER";
			}

			@Override
			public String getDashboardWSURI() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		FerariClientBuilder.getInstance().setAddressResolver(addressResolver);
		mockSendToGatekeeper = mock(ISend.class);
		mockSendToCommunicator = mock(ISend.class);
		mocksendPushToCommunicator = mock(ISend.class);
		mocksendLSVToGatekeeper = mock(ISend.class);
		mocksendLSVToCommunicator = mock(ISend.class); 
		timeMachineState = new TimeMachineState(mocksendLSVToCommunicator, mockSendToGatekeeper, mockSendToCommunicator, mocksendPushToCommunicator, mocksendLSVToGatekeeper, nodeId, null, false,"");
		nodeId = "node1";
		phone = "123";
		timestamp = 1l;
	}
	
	@Test
	public void testReceiveFirstCounterUpdateMessage_PassesItOn() {
		int counter = 3;
		timeMachineState.setIssetforTest(true, "eu.ferari.examples.mobileFraudOld.function.IdentityFunction", "null", 0.0f);
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		
		assertThat(timeMachineState.getState(phone), is(equalTo(PhoneState.Play)));
		verify(mockSendToGatekeeper, times(1)).signal(counterUpdateMessage.toDataTuple());
	}
	
	@Test
	public void testReceiveCounterUpdateMessageWhilePlaying_PassesItOn() {
		int counter = 3;
		timeMachineState.setIssetforTest(true, "eu.ferari.examples.mobileFraudOld.function.IdentityFunction", "null", 0.0f);

		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		
		verify(mockSendToGatekeeper, times(2)).signal(counterUpdateMessage.toDataTuple());
	}
	
	@Test
	public void testReceiveCounterUpdateMessageWhilePaused_DoesNothing() {
		int counter = 3;
		timeMachineState.setIssetforTest(true, "eu.ferari.examples.mobileFraudOld.function.IdentityFunction", "null", 0.0f);

		Message pauseMessage = new Message(MessageType.Pause, nodeId, phone, timestamp);
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		timeMachineState.update(pauseMessage.toDataTuple());
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		
		verifyZeroInteractions(mockSendToGatekeeper);
	}		
	
	@Test
	public void testReceivePlayMessage_PlaysPhoneBuffer() {

		int counter = 3;
		timeMachineState.setIssetforTest(true, "eu.ferari.examples.mobileFraudOld.function.IdentityFunction", "null", 0.0f);

		Message pauseMessage = new Message(MessageType.Pause, nodeId, phone, timestamp - 1);
		Message counterUpdateMessage1 = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		Message counterUpdateMessage2 = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp + 1, counter);
		timeMachineState.update(pauseMessage.toDataTuple());
		timeMachineState.update(counterUpdateMessage1.toDataTuple());
		timeMachineState.update(counterUpdateMessage2.toDataTuple());
		
		Message playMessage = new Message(MessageType.Play, nodeId, phone, timestamp - 1);
		timeMachineState.update(playMessage.toDataTuple());
		
		InOrder inOrder = Mockito.inOrder(mockSendToGatekeeper);
		inOrder.verify(mockSendToGatekeeper, times(1)).signal(counterUpdateMessage1.toDataTuple());
		inOrder.verify(mockSendToGatekeeper, times(1)).signal(counterUpdateMessage2.toDataTuple());
	}
	
	@Test
	public void testPauseMessage_SendsPauseConfirmation(){
		int counter = 1;
		int numNodes = 1;
		timeMachineState.setIssetforTest(true, "eu.ferari.examples.mobileFraudOld.function.IdentityFunction", "null", 0.0f);

		Float[] LSVsum = {1f};
		Message counterUpdateMessage = 
				new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		Message pauseMessage = new Message(MessageType.Pause, nodeId, phone, timestamp);

		timeMachineState.update(counterUpdateMessage.toDataTuple());
		timeMachineState.update(pauseMessage.toDataTuple());
		
		assertThat(timeMachineState.getState(phone), is(equalTo(PhoneState.Pause)));
		ArgumentCaptor<DataTuple> argument = ArgumentCaptor.forClass(DataTuple.class);
		verify(mockSendToCommunicator, times(1)).signal(argument.capture());
		List<Matcher<Object>> matcherList= Arrays.asList(
				Matchers.equalTo(MessageType.PauseConfirmation.toString()),
				Matchers.equalTo(nodeId),
				Matchers.equalTo(phone),
				Matchers.equalTo(1),
				Matchers.equalTo(timestamp),
				Matchers.anything(),
				Matchers.equalTo(numNodes));
		Matcher datatupleMatcher= Matchers.contains(matcherList);
		assertThat(argument.getValue().asList(), datatupleMatcher);
		assertThat((Float[])argument.getValue().asList().get(5),Matchers.arrayContaining(0.0f));
		//it fails because LSVsum is different from the one in TimeMachine
	}	
}
//TODO: use Mockito correctly. See http://www.baeldung.com/mockito-verify