package eu.ferari.examples.mobileFraudOld.function;

import eu.ferari.examples.mobileFraudOld.function.Function;
import eu.ferari.examples.mobileFraudOld.function.IdentityFunction;
import eu.ferari.examples.mobileFraudOld.function.Tuple;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by mofu on 23/07/15.
 */
public class IdentityFunctionTest {
    private Function f;
    private boolean equalityInAboveThresholdRegion, dynamicThreshold;
    private int y,threshold;
    private float defLsvValue;
    private String nodeName, functionID;
    private Date time;

    @Before
    public void setUp(){
        equalityInAboveThresholdRegion =true;
        dynamicThreshold=false;
        y=5;
        threshold=3;
        defLsvValue=1.0f;
        nodeName="node";
        functionID="1";
        time= new Date();
        f = new IdentityFunction(y,threshold, equalityInAboveThresholdRegion, defLsvValue,dynamicThreshold);
        f.InitializeNodeThreshold(nodeName);
    }
    @Test
    public void checkForThresholds(){
        assertNotNull(f.getThresholds());
    }
    @Test
    public void newThresholdTest(){
        f.UpdateNodeThrehold(nodeName, 3);
        assertEquals(f.getThresholds().get(nodeName), new Integer(3));
    }

    @Test
    public void initFromObjectArrayTest(){

        Class<? extends Function> newFunction = null;
        Class[] argsClass = new Class[] {int.class, float.class, boolean.class,  float.class, boolean.class};
        Object[] args = new Object[] {y, threshold, equalityInAboveThresholdRegion, defLsvValue, dynamicThreshold};
        Constructor<? extends Function> argsConstructor = null;
        Function fun = null;
        try {
            argsConstructor = IdentityFunction.class.getConstructor(argsClass);
            fun = argsConstructor.newInstance(args);
        }catch(NoSuchMethodException e) {
            fail("Could not find Constructor.");
        }catch (IllegalAccessException e){
            fail("Could not access constructor.");
        }catch (InvocationTargetException e){
            fail("Could not invoke constructor.");
        }catch (InstantiationException e){
            fail("Could not instantiate function.");
        }
        assertNotNull(fun.getThresholds());
    }

    @Test
    public void violationTriggers(){
        f.InitializeNodeThreshold(functionID);
        Tuple tuple = new Tuple(nodeName, time,(int)f.getThreshold()+1, functionID);
        f.insertVal(tuple);
        assert(f.hasLocalViolation(nodeName));

    }
}
