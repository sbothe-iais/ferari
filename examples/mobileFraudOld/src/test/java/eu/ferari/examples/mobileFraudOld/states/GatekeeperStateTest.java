/**
 * 
 */
package eu.ferari.examples.mobileFraudOld.states;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.mobileFraud.misc.Message;
import eu.ferari.examples.mobileFraud.misc.Message.MessageType;

import eu.ferari.examples.mobileFraudOld.states.GatekeeperState;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.List;


/**
 * @author yoni
 *
 */
public class GatekeeperStateTest {
	private GatekeeperState gatekeeperState;
	private ISend mockSendLSVsumToCommunicator;
	
	private String nodeId = "node1";
	private String phone = "123";
	private long timestamp = 1l;
	
	@Before
	public void setUp() throws Exception {
		mockSendLSVsumToCommunicator = mock(ISend.class);

		gatekeeperState = new GatekeeperState(mockSendLSVsumToCommunicator);
	}

	@Test
	public void testUpdateThreshold_UpdatesThreshold() {
		gatekeeperState.setForTest(phone, nodeId);
		int newThreshold = gatekeeperState.getDefaultThreshold() - 1;
		Message newThresholdMessage = new Message(MessageType.NewThreshold, nodeId, phone, newThreshold);
		
		gatekeeperState.update(newThresholdMessage.toDataTuple());
		
		assertThat(gatekeeperState.getThreshold(phone, nodeId), equalTo(newThreshold));
	}		

	@Test
	public void testMessageBelowThreshold_DoesNotPass() {
		gatekeeperState.setForTest(phone, nodeId);

		int counter = gatekeeperState.getDefaultThreshold() - 1;
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		
		gatekeeperState.update(counterUpdateMessage.toDataTuple());
		
		verifyZeroInteractions(mockSendLSVsumToCommunicator);
	}
	
	@Test
	public void testMessageAboveThreshold_Passes() {
		gatekeeperState.setForTest(phone, nodeId);

		int counter = gatekeeperState.getDefaultThreshold() + 1;
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		gatekeeperState.update(counterUpdateMessage.toDataTuple());
		
		Message lsvMessage = new Message(MessageType.LSV, phone, timestamp, new Float[]{1.0f}, 1, nodeId);
		ArgumentCaptor<DataTuple> argument = ArgumentCaptor.forClass(DataTuple.class);
		verify(mockSendLSVsumToCommunicator, times(1)).signal(argument.capture());
		List<Matcher<Object>> matcherList= Arrays.asList(
		Matchers.equalTo("LSV"),
		Matchers.equalTo("123"),
		Matchers.equalTo(1l),
		Matchers.anything(),
		Matchers.equalTo(1),
		Matchers.equalTo(nodeId));

		Matcher datatupleMatcher= Matchers.contains(matcherList);
		assertThat(argument.getValue().asList(), datatupleMatcher);
		assertThat((Float[])argument.getValue().asList().get(3),Matchers.arrayContaining(1.0f));
	}
}
