package radonTuning;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import eu.ferari.core.utils.CheckFailedException;
import eu.ferari.core.utils.Helper;

/**
 * A data spout for the decision tree
 * @author Rania
 *
 */
public class RadonDataSpout implements IRichSpout{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4665757993118587803L;
	private SpoutOutputCollector collector;
	private BufferedReader br;
	private int numFeatures = 10;
	private String filename;
	private int count = 0;
	
	public RadonDataSpout(String filename) {
		this.filename = filename;
	}
	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		this.collector = collector;
		File inFile = new File(filename);
        try {
            	br = new BufferedReader(new FileReader(inFile));
        } catch (IOException e) {
            throw new CheckFailedException("Cannot open:" + inFile + Helper.stackTraceOf(e));
        }
		
	}
	
	
	/**
	 * parse a line as a data instance and emit to the worker nodes
	 */
	@Override
	public void nextTuple() {
		String row;
		try {
			if ((row = br.readLine()) != null) {
				Double[] currentSample = new Double[numFeatures + 1];
				String[] features = row.split(" ");
				int i = 0;
				for (String feature : features) {
					currentSample[i++] = Double.parseDouble(feature);
				}
				Values values = new Values();
				values.add(1);
				values.add(currentSample);
				collector.emit(values);
				count++;
				if(count > 1200) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(count > 1400) {
					count = 0;
				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		/*private void produceTuple(){
			try {
			String row;
			Values value = new Values(0);
			if ((row = br.readLine()) != null) {
				DataSample dataSample = parseLine(row);
				value.add(dataSample);
				collector.emit(value);
			}
			else if (isDoneCount < AlgorithmParameters.WORKERS_NUM) {
					isDoneCount++;
					value.add("done");
					collector.emit(value);
			}
		}
		catch (Exception e) {
			throw new CheckFailedException("Could not read from:" + br
					+ Helper.stackTraceOf(e));
		}
		}

	private DataSample parseLine(String row) {
		String[] values = row.split(",");
		int i = 0;
		DataSample dataSample = new DataSample();
		String classLabel = values[values.length - 1];
		dataSample.setClassLabel(classLabel);
		for (String curValue : values) {
			if (i == values.length - 1)
				continue;
			if (!curValue.equals("?")) {
				double value = Double.valueOf(curValue);
				String attName = attList[i];
				dataSample.addAttValue(attName, value);
			}
			++i;
		}
		return dataSample;
	}*/

	@Override
	public void close() {
		try {
            br.close();
        } catch (IOException e) {
            throw new CheckFailedException("Could not close:" + br + Helper.stackTraceOf(e));
        }
		
	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void ack(Object msgId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fail(Object msgId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("dummy", "dataInstance"));
		
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
