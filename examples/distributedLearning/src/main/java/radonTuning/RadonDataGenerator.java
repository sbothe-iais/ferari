package radonTuning;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Random;

import example.functions.RegressionFunction;

public class RadonDataGenerator {
	private static final double NOISE_STD = 0.1;
	
	public static void main(String[] args) throws IOException {
		Random random = new Random();
		int numFeatures = 10;
		int numSamples = 1000000;
		RegressionFunction hypothesis = new RegressionFunction(new double[] {7.9, 6.9, 4.3, 0.6, 7.8, 8.8, 0.9, 1.1, 1.8, 1.8});
		
		File fout = new File("RadonInput.in");
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
		for (int k = 0; k < numSamples; k++) {
			Double[] instance = new Double[numFeatures + 1];
			String sample = "";
			for (int i = 0; i < numFeatures; i++) {
				double rndNum = random.nextDouble();
				sample += rndNum + " ";
				instance[i] = rndNum;
			}
			// the last index is for the output value
			sample += hypothesis.evaluateInstance(instance)
					+ NOISE_STD * random.nextGaussian();
			bw.write(sample);
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}

}