package kernelDensityEstimation;
import java.io.IOException;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.learning.kde.KernelDensityEstimator;
import eu.ferari.learning.kde.model.BaseSampleDistribution;
import example.functions.GaussianPDF;
import example.functions.InputStream;
import example.functions.ProbDistInputStream;
import example.functions.ProbabilityDistibutionFunction;
import example.spout.SyntheticDataSpout;


/**
 * 
 * @author Rania
 *this is a kernel density topology example
 */
public class KdeTopology {

	 private static final String DataSpout = "dataSpout";
	 
	 private static final String LearnerBoltName = "kdeBolt";
	    
	 private static final String coordinatorBoltName = "coordinatorBolt";	
    
	 public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
	            InvalidTopologyException, IOException {
		        // Config settings
	        Config conf = new Config();
	        conf.setDebug(false);
	
	        // Build the topology
	        TopologyBuilder builder = new TopologyBuilder();
	        
	        int numSamples = 4000;
	        int minBatchSize = 200;
	        int numLearners = 4;
	        int bound = 100;
	        //ProbabilityDistibutionFunction<Double> pdf = new RationalDistribution(bound);
	        double mean = 3.0;
	        double stddev = 0.5;
	        ProbabilityDistibutionFunction<Double> pdf = new GaussianPDF(mean, stddev);

	        InputStream inputStream = new ProbDistInputStream<Double>(pdf, numSamples);
	        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, -1);
	
	        builder.setSpout(DataSpout, dataSpout, 1);
	        ILocalSyncOp <Double, BaseSampleDistribution> localSyncOperator = new StaticLocalSyncOp<Double, BaseSampleDistribution>(minBatchSize);
	        Averaging<Double, BaseSampleDistribution>meanSync = new Averaging<Double, BaseSampleDistribution>();
	        ICoordTaskOperator <Double, BaseSampleDistribution>coordinatorSyncOperator = new StaticCoordSyncOp<Double, BaseSampleDistribution>(meanSync);
	        
	        ISyncOperator<Double, BaseSampleDistribution> syncOper = new ISyncOperator<Double, BaseSampleDistribution>(localSyncOperator, coordinatorSyncOperator);
	        
	        IComEffState<Double, BaseSampleDistribution> densityEstimator = new KernelDensityEstimator();
	        ComEffNode <Double, BaseSampleDistribution>comEffNode = new ComEffNode <Double, BaseSampleDistribution>(densityEstimator, syncOper);
	        

	        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), numLearners);
	        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
	        ComEffCoordinator <Double, BaseSampleDistribution>comEffCoordinator = new ComEffCoordinator<Double, BaseSampleDistribution>(syncOper);
	       
	        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
	        
	        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);
	
	        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());
	
	        LocalCluster cluster = new LocalCluster();
	
	        cluster.submitTopology("test", conf, builder.createTopology());
	        Thread.sleep(10000000);
	    }
	    
 }