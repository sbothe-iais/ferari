package example.functions;


/**
 * a polynomial hypothesis, basically a multivariate polynomial where each array contains contains the indices of the 
 * variables in a single component, so for example, if components has {1,2}{1,3,4} and coeffs = {1.2, 2} then
 * the polonmial represented by this object is 1*x1*x2 + 2*x1*x2*x4
 * @author Rania
 *
 */
public class PolynomialHypothesis extends Hypothesis<Double>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2742803189533056197L;
	
	// the polynomial components, separated by a plus. so for example, if you have the lists {2,3}, {1,2} then the polynomial is x2*x3 + x1*x2. 
	private int[][] components;
	
	/**
	 * the coefficients for each component
	 */
	private double [] coeffs;
	
	public PolynomialHypothesis(double [] coeffs, int[][] components) {
		this.coeffs = new double [coeffs.length];
		System.arraycopy(coeffs, 0, this.coeffs, 0, coeffs.length);
		this.components = components;
	}
	
	@Override
	public Double evaluateInstance(Double[] values) {
		if (values.length-1 != coeffs.length) {
			throw new RuntimeException(
					"Vectors length are not equal, can't evaluate the function");
		}
		double val = 0;//coeffs[0];
		for (int i = 0; i < coeffs.length; i++) {
			int[] currentComponent = components[i];
			double currentValue = 1;
			for(int valIndex : currentComponent) {
				currentValue*= values[valIndex];
			}
			val += coeffs[i] * currentValue;
		}
		return val;
	}
	
	
	@Override
	public int getHypothesisLength() {
		return coeffs.length;
	}
	
	public double[] getCoeffs() {
		return coeffs;
	}	
}