package example.functions;

import java.util.Random;

import org.apache.commons.math3.distribution.PoissonDistribution;

/**
 * represents a Poisson distribution function
 */
public class PoissonPdf extends ProbabilityDistibutionFunction<Integer>{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1821212740429543741L;
	
	private PoissonDistribution poisson;

	/**
	 * the upper limit of the discrete distribution
	 */
	public PoissonPdf() {
		poisson = new PoissonDistribution(20.0);
	}
	public PoissonPdf(double mean) {
		poisson = new PoissonDistribution(mean);
	}
	


	@Override
	public Integer[] generateInstance() {
		Integer instance[] = new Integer[dimension];
		for(int i =0; i<dimension; i++) {
			instance[i] = poisson.sample();
		}
		return instance;
	}

}
