package example.functions;


/**
 * A class that represents a simple conjunction/disjunction hypothesis
 * @author Rania
 *
 */
public class BooleanHypothesis extends Hypothesis <Boolean>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1968263733601446340L;

	public enum HypothesisType {Conjunction, Disjunction};
    /**
     * a boolean array that represents a hypothesis, hypothesis[i] = true means that variables i appears as is in the formula and 
     * hypothesis[i] = false means the hypothesis appears negated in the formula. 
     */
	private Boolean [] hypothesis;
	private HypothesisType hypType = HypothesisType.Conjunction;



	public BooleanHypothesis(Boolean[] hypothesis) {
		this.hypothesis = hypothesis;
	}
	
	public BooleanHypothesis(int[] posIndices, int[] negIndices, int length) {
		hypothesis = new Boolean [length];
		for(int i: posIndices) {
			hypothesis[i] = true;
		}
		for(int i: negIndices) {
			hypothesis[i] = false;
		}
	}
	
	public BooleanHypothesis(Boolean [] hypothesis, HypothesisType type) {
		this(hypothesis);
		this.hypType = type;
	}


	/**
	 * evaluate the hypothesis on a given data row
	 * @param dataRow
	 * @return
	 */
	public Boolean evaluateInstance(Boolean [] dataRow) {
		if(dataRow.length-1 != hypothesis.length) { // last index is reserved for the output
			throw new RuntimeException("The data length is not equal to the hypothesis length");
		}
		switch(hypType) {
		case Conjunction:
			for(int i = 0; i < hypothesis.length; i++) {
				if(hypothesis[i]!= null && hypothesis[i].equals(dataRow[i]))
					return true;
			}
			return false;
		case Disjunction:
			for(int i = 0; i < hypothesis.length; i++) {
				if(hypothesis[i]!= null && !hypothesis[i].equals(dataRow[i]))
					return false;
			}
			return true;
			default:
				return false;
		}	
	}

	@Override
	public int getHypothesisLength() {
		return hypothesis.length;
	}	
	
	
}
