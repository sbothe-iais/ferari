package example.functions;

/**
 * assume a simple linear regression function of the form w0+w_1*x_1+...+w_n*x_n
 * @author Rania
 *
 */
public class RegressionFunction extends Hypothesis<Double>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2742803189533056197L;

	private double [] coeffs;
	
	
	public RegressionFunction(double [] coeffs) {
		this.coeffs = new double [coeffs.length];
		System.arraycopy(coeffs, 0, this.coeffs, 0, coeffs.length);
		
	}
	
	@Override
	public Double evaluateInstance(Double[] values) {
		if (values.length-1 != coeffs.length) {
			throw new RuntimeException(
					"Vectors length are not equal, can't evaluate the function");
		}
		double val = 0;//coeffs[0];
		for (int i = 0; i < coeffs.length; i++) {
			val += coeffs[i] * values[i];
		}
		return val;
	}
	
	
	@Override
	public int getHypothesisLength() {
		return coeffs.length;
	}
	
	public double[] getCoeffs() {
		return coeffs;
	}	

}
