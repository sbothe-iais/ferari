package example.spout;

import java.util.Map;

import example.functions.InputStream;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

public class SyntheticDataSpout implements IRichSpout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7306915515470277591L;
	
	private InputStream inputStream;

	private SpoutOutputCollector collector;
	
	private int pauseBetweenExamples = 0;
	
	public SyntheticDataSpout(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	public SyntheticDataSpout(InputStream inputStream, int pauseBetweenExamples) {
		this.inputStream = inputStream;
		this.pauseBetweenExamples = pauseBetweenExamples;
	}
	
	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		this.collector = collector;

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub

	}

	@Override
	public void nextTuple() {
		Values values = inputStream.nextExample();
		if (values != null)
			collector.emit(values);
		if (pauseBetweenExamples > 0) {
			try {
				Thread.sleep(this.pauseBetweenExamples);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ack(Object msgId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fail(Object msgId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("instanceId", "data"));

	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
