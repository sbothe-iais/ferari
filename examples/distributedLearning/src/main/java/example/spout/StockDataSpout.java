package example.spout;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import eu.ferari.core.utils.CheckFailedException;
import eu.ferari.core.utils.Helper;

/**
 * 
 * @author Rania
 * Here's our method for forming the features vector of the stock data:
 * Say you have a target stock s* and all other stocks s_1,...,s_n.
 *  For all of these stocks you have their prices P(s*,t), p(s_1,t),... for all timepoints t in [1,T]. 
 * The features phi(s*,t) for a target stock s* at time t, given a time window W are the following:
 *	phi(s*,t)= (p(s*,t),avg_11(s*,t), avg_50(s*,t), avg_200(s*,t), p(s_1,t), avg_11(s_1,t), avg_50(s_1,t), avg_200(s_1,t),...)
 */
public class StockDataSpout implements IRichSpout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7306915515470277591L;

	private SpoutOutputCollector collector;
	
	private BufferedReader br;
	
	private String filename;
	
	private long count;
	
	private int numStocks;
	private final static int maxInterval = 200;
	private int [] averageIntervals = new int []{5, 11, 50, maxInterval};
	// we need to keep track of the last (maxInterval) values for all features for performing the average
	private double[][] lastValues;
	// the index of the stock value that we would like to predict
	private int targetIndex = -1;
	
	private List<Double[]> featuresArray = new ArrayList<>();
	
	private Double[] prevFeatures;
	
	public StockDataSpout(String filename) {
		this.filename = filename;
	}
	
	public StockDataSpout(String filename, int targetIndex) {
		this(filename);
		this.targetIndex = targetIndex;
	}
	
	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		this.collector = collector;
		File inFile = new File(filename);
        try {
            	br = new BufferedReader(new FileReader(inFile));
            	String header = br.readLine();
            	numStocks = header.split("\t").length - 1; // the first column is for the date
            	if(targetIndex < 0) {
            		targetIndex = numStocks - 1;
            	}
            	lastValues = new double[numStocks][maxInterval]; // save last 200 entries for each stock company
        } catch (IOException e) {
            throw new CheckFailedException("Cannot open:" + inFile + Helper.stackTraceOf(e));
        }

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub

	}

	@Override
	public void nextTuple() {
		Values values = nextExample();

		if(values != null) {
			collector.emit(values);
		}
	}
	
	public Values nextExample() {
 		String row = null;
 		Double[] instance = null;
 		try {
			if ((row = br.readLine()) == null) {
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
 		instance = getInstance(row);
 		++count;
 		Values values = null;
 		if(count >= maxInterval) {
 			if(prevFeatures != null) {
 				prevFeatures[prevFeatures.length-1] = instance[targetIndex];
 				values = new Values();
 			    values.add(1);
 			    values.add(prevFeatures);
 			}
 			prevFeatures = formFeaturesVector(instance);
		    /*featuresArray.add(features);
		    if(featuresArray.size() == 200) {
		    	computeMeanDistance();
		    }*/
		    return values;
 		}
		return null;
 	}
	// compute the euclidean distance between each pair of features vectors and then divide the number of
	// distances calculated to obtain a mean value, this will be our sigma in the gaussian model
	private double computeMeanDistance() {
		double totalDistance = 0;
		for (int i = 0; i < featuresArray.size(); i++) {
			for (int j = i + 1; j < featuresArray.size(); j++) {
				double currentDistance = computeDistance(featuresArray.get(i),
						featuresArray.get(j));
				totalDistance += currentDistance;
			}
		}
		int numDistances = (featuresArray.size() * (featuresArray.size() - 1)) / 2;
		double meanDistance = totalDistance / numDistances;
		System.out.println("********mean distance: " + meanDistance);
		return meanDistance;
	}
		
		public double computeDistance(Double[] point1, Double[] point2) {
			double dist = 0;
			for (int i = 0; i < point1.length; i++) {
				dist += Math.pow(point1[i] - point2[i], 2);
			}
			return Math.sqrt(dist);
		}

	private Double[] formFeaturesVector(Double[]instance) {
		Double features[] = new Double[numStocks + numStocks * averageIntervals.length + 1];
		// this a just a way to push the target value at the end of the array.
		/*System.arraycopy(instance, 0, features, 0, targetIndex);
		if(targetIndex < numStocks-1) {
			System.arraycopy(instance, targetIndex+1, features, targetIndex, instance.length - targetIndex-1);
		}*/
		System.arraycopy(instance, 0, features, 0, numStocks);
		for(int i = 0; i < averageIntervals.length; i++) {
			for(int k =0; k < numStocks; k++) {
				Double average = getAvgLast(averageIntervals[i], k);
				features[instance.length + i * numStocks + k] = average;
		}
	}
		features[features.length-1] = instance[targetIndex];
		if(Arrays.asList(features).contains(null)) {
			Arrays.asList(features).indexOf(null);
		}
		return features;
	}

	private double getAvgLast(int lastNumDays, int stockumber) {
		double average;
		double[] subArray;
		if(lastNumDays < maxInterval) {
			 subArray = Arrays.copyOfRange(lastValues[stockumber], lastValues.length-lastNumDays, lastValues[stockumber].length);
		}
		else {
			subArray = lastValues[stockumber];
		}

		average = Arrays.stream(subArray).map(i->i).average().getAsDouble();
		return average;
	}

	private Double[] getInstance(String row) {
		String[] rawInstance = row.split("\t");
		Double[] instance = new Double[rawInstance.length - 1];
		for (int i = 1; i < rawInstance.length; i++) {
			double value = Double.parseDouble(rawInstance[i]);
			instance[i - 1] = value;
			lastValues[i-1][(int)count%maxInterval] = value;
		}
		return instance;
	}

	@Override
	public void ack(Object msgId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fail(Object msgId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("instanceId", "data"));

	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
