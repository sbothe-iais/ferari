package example.topology.kernel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.coord.DynamicCoordSyncOp;
import eu.ferari.core.sync.local.DynamicLocalSyncOp;
import eu.ferari.core.sync.resolution.ExponentialResolution;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.aggregation.KernelAggregation;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import example.functions.Hypothesis;
import example.functions.InputStream;
import example.functions.RealValuedInputStream;
import example.functions.RegressionFunction;
import example.spout.SyntheticDataSpout;


/**
 * an example of a dynamic learning topology using kernels
 * @author Rania
 *
 */
public class KernelRegressionLearningTopologyDynamic {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        Hypothesis <Double> hypothesis = new RegressionFunction(new double [] {0.88, 0.1, 0.3, 0.8, 1.6, 0.7, 1.1, 0.3, 0.2, 0.11});
        //RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(Parameters.DATA_LENGTH);
        //System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);
        LearningParamsMap paramsMap = initLearningParams();

        builder.setSpout(DataSpout, dataSpout, 1);
        double varianceThreshold = 0.1;
        ILocalSyncOp <RealValuedVector, Double> localSyncOperator = new DynamicLocalSyncOp<RealValuedVector, Double>(100, varianceThreshold);
        KernelAggregation kernelSync = new KernelAggregation();
        ICoordTaskOperator <RealValuedVector, Double> coordinatorSyncOperator = new DynamicCoordSyncOp<RealValuedVector, Double>(varianceThreshold, kernelSync,new ExponentialResolution());
        
        ISyncOperator<RealValuedVector, Double> syncOper = new ISyncOperator<RealValuedVector, Double>(localSyncOperator, coordinatorSyncOperator );
        
        ComEffLearner<RealValuedVector, Double> commEffLearner = new ComEffLearner<RealValuedVector, Double> (LossFunctionType.SquareError, ModelType.KernelRegression, paramsMap);
        ComEffNode <RealValuedVector, Double> comEffNode = new ComEffNode <RealValuedVector, Double> (commEffLearner, syncOper);
        
        comEffNode.setLogAnnotation("dynamic");
        commEffLearner.setLogAnnotation("dynamic");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <RealValuedVector, Double> comEffCoordinator = new ComEffCoordinator<RealValuedVector, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }

	private static LearningParamsMap initLearningParams() {
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(String.valueOf(LearningParameter.LinearLearningRate), "0.1");
		paramsMap.put(String.valueOf(LearningParameter.GaussianWidth), "296");
		paramsMap.put(String.valueOf(LearningParameter.KernelLearningRate), "0.32");
		paramsMap.put(String.valueOf(LearningParameter.PolynomialKernelDegree), "2");
		// the regularization constant of the kernel stochastic update rule 
		paramsMap.put(String.valueOf(LearningParameter.Regularizer), "0.00000005");
		// the polynomial function constant
		paramsMap.put(String.valueOf(LearningParameter.c), "0.0002");
		// the kernel function to be used in the kernel model, exmaple linear, gaussian, polynomial
		paramsMap.put(String.valueOf(LearningParameter.KernelFunction), "Gaussian");
		return new LearningParamsMap(paramsMap);
	}
}


