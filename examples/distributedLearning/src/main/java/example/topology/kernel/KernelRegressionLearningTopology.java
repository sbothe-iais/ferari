package example.topology.kernel;

import java.io.IOException;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.aggregation.KernelAggregation;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import example.functions.Hypothesis;
import example.functions.InputStream;
import example.functions.RealValuedInputStream;
import example.functions.RegressionFunction;
import example.spout.SyntheticDataSpout;


/**
 * an example of a static learning topology using kernels
 * @author Rania
 *
 */
public class KernelRegressionLearningTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        Hypothesis <Double> hypothesis = new RegressionFunction(new double [] {0.88, 0.1, 0.3, 0.8, 1.6, 0.7, 1.1, 0.3, 0.2, 0.11});
        //RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(Parameters.DATA_LENGTH);
        //System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);

        builder.setSpout(DataSpout, dataSpout, 1);
        
        ILocalSyncOp <RealValuedVector, Double> localSyncOperator = new StaticLocalSyncOp<RealValuedVector, Double>(100);
        KernelAggregation kernelSync = new KernelAggregation();
        ICoordTaskOperator <RealValuedVector, Double> coordinatorSyncOperator = new StaticCoordSyncOp<RealValuedVector, Double>(kernelSync);
        
        ISyncOperator<RealValuedVector, Double> syncOper = new ISyncOperator<RealValuedVector, Double>(localSyncOperator, coordinatorSyncOperator );
        
        ComEffLearner<RealValuedVector, Double> commEffLearner = new ComEffLearner<RealValuedVector, Double> (LossFunctionType.SquareError, ModelType.KernelRegression);
        ComEffNode <RealValuedVector, Double> comEffNode = new ComEffNode <RealValuedVector, Double> (commEffLearner, syncOper);
        
        comEffNode.setLogAnnotation("static");
        commEffLearner.setLogAnnotation("static");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <RealValuedVector, Double> comEffCoordinator = new ComEffCoordinator<RealValuedVector, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }
}


