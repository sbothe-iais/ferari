

package example.topology;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import example.functions.InputStream;
import example.functions.RealValuedInputStream;
import example.functions.RegressionFunction;
import example.spout.SyntheticDataSpout;


/**
 * A learning topology that can be adjusted to run any learning function and learning parameters that the user wishes 
 * @author Rania
 *
 */
public class GenericLearningTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {

    	JSONObject config = new JSONObject();
        ComEffNode<Integer, Double> comEffNode = null;
        ComEffCoordinator<Integer, Double> comEffCoordinator = null;
    	try {
    		config.put("name", "LinearRegressionLerner");
    		config.put("functionName", "eu.ferari.learning.ComEffLearner");
    		Map<String, String> eventAtt1 = new HashMap<String, String>();
        	eventAtt1.put("name", "event1");
        	eventAtt1.put("attr", "attr1");
        	config.put("monitoringObject", new JSONObject(eventAtt1));
        	Map<String, String> outEvent = new HashMap<String, String>();
        	outEvent.put("name", "outEvent");
        	outEvent.put("attr", "prediction");
        	
        	config.put("derivedEvents", new JSONObject(outEvent));
        	config.put("context", "none");
        	config.put("initialMode", "random");
        	//config.put("synchProtocol", "eu.ferari.core.sync.StaticSyncOp");
        	config.put("synchProtocol", "eu.ferari.core.sync.DynamicSyncOp");
        	Map<String, String> synchProtocolParams = new HashMap<String, String>();
        	synchProtocolParams.put("batchSize", "100");
        	synchProtocolParams.put("varianceThreshold", "0.1");// relevant for the dynamic synchronization scheme
        	config.put("synchProtocolParams", synchProtocolParams);
        	config.put("resolutionProtocol", "eu.ferari.core.sync.resolution.ExponentialResolution");
        	Map<String, String> resolutionProtocolParams = new HashMap<String, String>();
        	config.put("resolutionProtocolParams", new JSONObject(resolutionProtocolParams));
        	config.put("serviceType", "regression");
        	//config.put("updateRule", "eu.ferari.learning.updateRules.StochasticGradientDescent"); // relevant for linear models
        	config.put("updateRule", "eu.ferari.learning.updateRules.KernelStochasticGradientDescentRegression"); // relevant for kernel models
        	Map<String, String> updateRuleParams = new HashMap<String, String>();
        	updateRuleParams.put("learningRate", "0.1");
        	updateRuleParams.put("regularizer", "0.01"); // relevant for kernel stochastis gradient descent
        	config.put("updateRuleParams", new JSONObject(updateRuleParams));
        	//config.put("modelType", "eu.ferari.learning.model.LinearModelRegression");
        	config.put("modelType", "eu.ferari.learning.model.KernelModelRegression");
        	Map<String, String> modelParams = new HashMap<String, String>();
        	modelParams.put("kernelFunction", "eu.ferari.learning.model.kernelFunc.DotProductKernel"); // relevant for kernel models
        	//modelParams.put("gaussianWidth", "196");// relevant for the gaussian function
        	//modelParams.put("polynomialKernelDegree", "2"); // relevant for the polynomial kernel
        	//modelParams.put("c", "0.0002"); // relevant for the polynomial function
        	config.put("modelParams", new JSONObject(modelParams));
        	config.put("lossFunction", "eu.ferari.learning.lossFunctions.SquaredLoss");
        	Map<String, String> lossFunctionParams = new HashMap<String, String>();
        	//lossFunctionParams.put("epsilon", "0.1"); // relevant for the epsilon insensitive error.
        	config.put("lossFunctionParams", new JSONObject(lossFunctionParams));
        	config.put("aggregationMethod", "eu.ferari.core.sync.aggregation.Averaging");
        	comEffNode = new ComEffNode <Integer, Double> (config);
        	comEffCoordinator = new ComEffCoordinator<Integer, Double>(config);
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
    	 	    	
    	
        // Config settings
        Config conf = new Config();
        conf.setDebug(false);


        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        RegressionFunction hypothesis = new RegressionFunction(new double [] {7.9, 6.9, 4.3, 0.6, 7.8, 8.8, 0.9, 1.1, 1.8, 1.8});
        //RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(Parameters.DATA_LENGTH);
        System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);

        builder.setSpout(DataSpout, dataSpout, 1);
       
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        
		try {
			comEffCoordinator = new ComEffCoordinator<Integer, Double>(config);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }
}


	