package example.topology;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.learning.kde.model.BaseSampleDistribution;
import example.functions.GaussianPDF;
import example.functions.InputStream;
import example.functions.ProbDistInputStream;
import example.functions.ProbabilityDistibutionFunction;
import example.spout.SyntheticDataSpout;

public class KernelDensityEstimationTopology {
	
	private static final String DataSpout = "dataSpout";
	 
	 private static final String LearnerBoltName = "kdeBolt";
	    
	 private static final String coordinatorBoltName = "coordinatorBolt";	
   
	 public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
	            InvalidTopologyException, IOException {
		        // Config settings
	        Config conf = new Config();
	        conf.setDebug(false);
	
	        // Build the topology
	        TopologyBuilder builder = new TopologyBuilder();
	        
	        int numSamples = 8000;
	        int minBatchSize = 200;
	        int numLearners = 4;
	        int bound = 100;
	        //ProbabilityDistibutionFunction<Double> pdf = new RationalDistribution(bound);
	        double mean = 3.0;
	        double stddev = 0.5;
	        ProbabilityDistibutionFunction<Double> pdf = new GaussianPDF(mean, stddev);

	        InputStream inputStream = new ProbDistInputStream<Double>(pdf, numSamples);
	        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, -1);
	
	        builder.setSpout(DataSpout, dataSpout, 1);
	        ComEffNode<Double, BaseSampleDistribution> comEffNode = null;
	        ComEffCoordinator <Double, BaseSampleDistribution>comEffCoordinator = null;
	    	JSONObject config = new JSONObject();
	    	try {
	    		config.put("name", "LinearRegressionLerner");
	    		config.put("functionName", "eu.ferari.learning.kde.KernelDensityEstimator");
	    		Map<String, String> eventAtt1 = new HashMap<String, String>();
	        	eventAtt1.put("name", "event1");
	        	eventAtt1.put("attr", "attr1");
	        	config.put("monitoringObject", new JSONObject(eventAtt1));
	        	Map<String, String> outEvent = new HashMap<String, String>();
	        	outEvent.put("name", "outEvent");
	        	outEvent.put("attr", "prediction");
	        	
	        	config.put("derivedEvents", new JSONObject(outEvent));
	        	config.put("context", "none");
	        	config.put("initialMode", "random");
	        	config.put("functionName", "eu.ferari.learning.kde.KernelDensityEstimator");
	        	//config.put("synchProtocol", "eu.ferari.core.sync.StaticSyncOp");
	        	config.put("synchProtocol", "eu.ferari.core.sync.DynamicSyncOp");
	        	Map<String, String> synchProtocolParams = new HashMap<String, String>();
	        	synchProtocolParams.put("batchSize", "100");
	        	synchProtocolParams.put("varianceThreshold", "3");// relevant for the dynamic synchronization scheme
	        	config.put("synchProtocolParams", synchProtocolParams);
	        	config.put("resolutionProtocol", "eu.ferari.core.sync.resolution.ExponentialResolution");
	        	Map<String, String> resolutionProtocolParams = new HashMap<String, String>();
	        	config.put("resolutionProtocolParams", new JSONObject(resolutionProtocolParams));
	        	config.put("serviceType", "kde");
	
	        	config.put("modelType", "eu.ferari.learning.kde.algorithm.KernelDensityModel");
	        	Map<String, String> modelParams = new HashMap<String, String>();
	        	modelParams.put("forgettingFactor", "1");
	        	modelParams.put("compressionThreshold", "0.01");
	        	config.put("modelParams", new JSONObject(modelParams));
	        	config.put("aggregationMethod", "eu.ferari.core.sync.aggregation.Averaging");
	        	comEffNode = new ComEffNode <Double, BaseSampleDistribution> (config);
	        	comEffCoordinator = new ComEffCoordinator<Double, BaseSampleDistribution>(config);
	       
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	    	 
	        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), numLearners);
	        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
	       
	       
	        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
	        
	        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);
	
	        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());
	
	        LocalCluster cluster = new LocalCluster();
	
	        cluster.submitTopology("test", conf, builder.createTopology());
	        Thread.sleep(10000000);
	    }
}
