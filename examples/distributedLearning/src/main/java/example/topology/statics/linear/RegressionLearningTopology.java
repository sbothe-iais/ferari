

package example.topology.statics.linear;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import example.functions.InputStream;
import example.functions.RealValuedInputStream;
import example.functions.RegressionFunction;
import example.spout.SyntheticDataSpout;


/**
 * an example of a dynamic learning topology
 * @author Rania
 *
 */
public class RegressionLearningTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
    	
    	//learning parameters
    	JSONObject config = new JSONObject();
    	try {
    		config.put("name", "LinearRegressionLerner");
    		config.put("functionName", "eu.ferari.learning.ComEffLearner");
    		Map<String, String> eventAtt1 = new HashMap<String, String>();
        	eventAtt1.put("name", "event1");
        	eventAtt1.put("attr", "attr1");
        	config.put("monitoringObject", new JSONObject(eventAtt1));
        	Map<String, String> outEvent = new HashMap<String, String>();
        	outEvent.put("name", "outEvent");
        	outEvent.put("attr", "prediction");
        	
        	config.put("derivedEvents", new JSONObject(outEvent));
        	config.put("context", "none");
        	config.put("initialMode", "random");
        	config.put("syncProtocol", "eu.ferari.core.DynamicSyncOp");
        	Map<String, String> synchProtocolParams = new HashMap<String, String>();
        	config.put("synchProtocolParams", synchProtocolParams);
        	config.put("resolutionProtocol", "eu.ferari.core.sync.resolution.ExponentialResolution");
        	Map<String, String> resolutionProtocolParams = new HashMap<String, String>();
        	config.put("resolutionProtocolParams", new JSONObject(resolutionProtocolParams));
        	config.put("serviceType", "regression");
        	config.put("updateRule", "eu.ferari.learning.updateRules.StochasticGradientDescent");
        	Map<String, String> updateRuleParams = new HashMap<String, String>();
        	updateRuleParams.put("learningRate", "0.1");
        	config.put("updateRuleParams", new JSONObject(updateRuleParams));
        	config.put("modelType", "eu.ferari.learning.model.LinearModelRegression");
        	Map<String, String> modelParams = new HashMap<String, String>();
        	config.put("modelParams", new JSONObject(modelParams));
        	config.put("lossFunction", "eu.ferari.learning.lossFunctions.HingeLoss");
        	Map<String, String> lossFunctionParms = new HashMap<String, String>();
        	config.put("lossFunctionParms", new JSONObject(lossFunctionParms));
        	config.put("aggregationMethod", "eu.ferari.core.sync.aggregation.Averaging");
       
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
    	 	    	
        // Config settings
        Config conf = new Config();
        conf.setDebug(false);


        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        RegressionFunction hypothesis = new RegressionFunction(new double [] {7.9, 6.9, 4.3, 0.6, 7.8, 8.8, 0.9, 1.1, 1.8, 1.8});
        //RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(Parameters.DATA_LENGTH);
        System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);

        builder.setSpout(DataSpout, dataSpout, 1);
        
        ILocalSyncOp <Integer, Double> localSyncOperator = new StaticLocalSyncOp<Integer, Double>(100);
        Averaging <Integer, Double>meanSync = new Averaging<Integer, Double>();
        ICoordTaskOperator <Integer, Double> coordinatorSyncOperator = new StaticCoordSyncOp<Integer, Double>(meanSync);
        
        ISyncOperator<Integer, Double> syncOper = new ISyncOperator<Integer, Double>(localSyncOperator, coordinatorSyncOperator);
        
        ComEffLearner<Integer, Double> commEffLearner = new ComEffLearner<Integer, Double> (LossFunctionType.EpsilonInsensitive, ModelType.LinearRegression);
        ComEffNode <Integer, Double> comEffNode = new ComEffNode <Integer, Double> (commEffLearner, syncOper);
        comEffNode.setLogAnnotation("static");
        commEffLearner.setLogAnnotation("static");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        
        
        ComEffCoordinator <Integer, Double> comEffCoordinator = new ComEffCoordinator<Integer, Double>(syncOper);
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }
}


	