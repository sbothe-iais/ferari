package example.topology.statics.radon;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import radonTuning.RadonDataSpout;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;


/**
 * an example of a static learning topology using the iterated Radon point algorithm
 * @author Rania
 *
 */
public class RegressionLearningTopologyMeanTuning {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();
        RadonDataSpout dataSpout = new RadonDataSpout("RadonInput.in");

        builder.setSpout(DataSpout, dataSpout, 1);
        
        ILocalSyncOp <Integer, Double> localSyncOperator = new StaticLocalSyncOp<Integer, Double>(100);
        Averaging <Integer, Double> averagingFunction = new Averaging<Integer, Double>();
        ICoordTaskOperator <Integer, Double> coordinatorSyncOperator = new StaticCoordSyncOp<Integer, Double>(averagingFunction);
        
        ISyncOperator<Integer, Double> syncOper = new ISyncOperator<Integer, Double>(localSyncOperator, coordinatorSyncOperator );
        LearningParamsMap paramsMap = initLearningParams();
        ComEffLearner<Integer, Double> commEffLearner = new ComEffLearner<Integer, Double> (LossFunctionType.SquareError, ModelType.LinearRegression, paramsMap);
        ComEffNode <Integer, Double> comEffNode = new ComEffNode <Integer, Double> (commEffLearner, syncOper);
        
        comEffNode.setLogAnnotation("static");
        commEffLearner.setLogAnnotation("static");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <Integer, Double> comEffCoordinator = new ComEffCoordinator<Integer, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();   
    }
    
    private static LearningParamsMap initLearningParams() {
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(String.valueOf(LearningParameter.LinearLearningRate), "0.1");
		return new LearningParamsMap(paramsMap);
	}

}


