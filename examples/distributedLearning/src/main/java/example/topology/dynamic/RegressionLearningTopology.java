package example.topology.dynamic;

import java.io.IOException;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOperator;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.DynamicCoordSyncOp;
import eu.ferari.core.sync.local.DynamicLocalSyncOp;
import eu.ferari.core.sync.resolution.ExponentialResolution;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import example.functions.HypothesisGenerator;
import example.functions.InputStream;
import example.functions.RealValuedInputStream;
import example.functions.RegressionFunction;
import example.spout.SyntheticDataSpout;


/**
 * an example of a dynamic learning topology
 * @author Rania
 *
 */
public class RegressionLearningTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        //Hypothesis <Double> hypothesis = new RegressionFunction(new double [] {2.1, 4, 4.3, 7.8, 3.6, 5.7, 9.1, 4.3, 6.2, 2.11});
        RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(10);
        //System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);

        builder.setSpout(DataSpout, dataSpout, 1);
        double varianceThreshold = 0.1;
        ILocalSyncOp <Integer, Double> localSyncOperator = new DynamicLocalSyncOp<Integer, Double>(100, varianceThreshold);
        IAggregationMethod<Integer, Double> meanSync = new Averaging<Integer, Double>();
        ICoordTaskOperator<Integer, Double> coordinatorSyncOperator = new DynamicCoordSyncOp<Integer, Double>(varianceThreshold, meanSync, new ExponentialResolution());
        
        ISyncOperator<Integer, Double> syncOper = new ISyncOperator<Integer, Double>(localSyncOperator, coordinatorSyncOperator );
        
        ComEffLearner<Integer, Double> commEffLearner = new ComEffLearner<Integer, Double> (LossFunctionType.SquareError, ModelType.LinearRegression);
        ComEffNode <Integer, Double> comEffNode = new ComEffNode <Integer, Double> (commEffLearner, syncOper);
        
        comEffNode.setLogAnnotation("dynamic");
        commEffLearner.setLogAnnotation("dynamic");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <Integer, Double> comEffCoordinator = new ComEffCoordinator<Integer, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        //learnerBolt = learnerBolt.allGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME);
        //learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, BulkDirectGrouping.POLL_LOCAL_STREAM, new BulkDirectGrouping());
        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }
}


