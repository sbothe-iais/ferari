var socket = new WebSocket("ws://127.0.0.1:7379/.json");
channels = ["LocalInfoChannel","LocalTraceChannel","LocalDebugChannel", "CoordInfoChannel","CoordTraceChannel","CoordDebugChannel"];
socket.onopen = function() {
	for (var i = 0; i < channels.length; i++) {
		channel = channels[i];
		socket.send(JSON.stringify(["SUBSCRIBE", channel]));
	}
};
socket.onmessage = function(evt)
{
     callback(evt);
};