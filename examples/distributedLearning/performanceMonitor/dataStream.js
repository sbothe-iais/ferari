var lastTimestamp = -1;
var interval = 100; //in milliseconds
var windowed = false;
var windowSize = 200;


var random = new Rickshaw.Fixtures.RandomData(150);
var seriesData = [ [], [], [], []];
seriesError = [
	      	{
	      		color: '#FF5921',
	      		data: seriesData[0],
	      		name: 'Error (dynamic)'
	      	}, {
	      		color: '#F0DE1D',
	      		data: seriesData[2],
	      		name: 'Error (static)'
	      	}
	      ];

seriesCom = [	      	
	      	{
	      		color: '#21C7FF',
	      		data: seriesData[1],
	      		name: 'Communication (dynamic)'
	      	}, {
	      		color: '#1D2FF0',
	      		data: seriesData[3],
	      		name: 'Communication (static)'
	      	}
	      ];

initSeries(seriesData);

var palette = new Rickshaw.Color.Palette( { scheme: 'cool' } );

function initSeries(data) {
	data.forEach( function(series) {
		series.push( { x: new Date().getTime() / 1000, y: 0.0 } );
	} );
}

function removeData(data){	
	data.forEach( function(series) {
		series.shift();
	} );
};

function updateData(graph, graph1) {
	var d = new Date();
	actTimestamp = d.getTime();
	diff = actTimestamp - lastTimestamp;//diff here could be used to measure throughput at monitor site.
	if (diff > interval) {
		if (windowed && seriesData[0].length > windowSize) {
			removeData(seriesData);
		}		
		addData(seriesData);
		checkAnnotations();
		graph.update();
		graph1.update();
		lastTimestamp = actTimestamp;
	}
}

function checkAnnotations() {
	if (annotationsDynamic.length > 0) {
		addAnnotation("full synchronization", seriesData[1]); //dynamic communication
		annotationsDynamic = [];
	}
}

function getSum(array) {
	var sum = 0;	
	for (var i = 0; i < array.length; i++) {
		sum += parseFloat(array[i]);
	}
	return sum;
}

function getAvg(array) {
	var avg = getSum(array);
	if (array.length > 0) {
		avg = avg / parseFloat(array.length);
	}
	return avg;
}

function getSafeValue(array, leFunction, data, bUseLast) {
	if (array.length > 0) {
		return leFunction(array);
	}
	else {
		if (bUseLast == true) {
			return data[data.length - 1].y;
		}
		else {
			return 0.0;
		}
	}
}

function addData(seriesData) {
	time = new Date().getTime() / 1000;
	//console.log("Error-Array: "+JSON.stringify(errorDynamic));
	var val = getSafeValue(errorDynamic, getAvg, seriesData[0], true);	
	seriesData[0].push({ x: time, y: val });
	errorDynamic = [];
	val = getSafeValue(communicationDynamic, getSum, seriesData[1], false);
	seriesData[1].push({ x: time, y: val });
	communicationDynamic = [];
	val = getSafeValue(errorStatic, getAvg, seriesData[2], true);
	seriesData[2].push({ x: time, y: val });
	errorStatic = [];	
	val = getSafeValue(communicationStatic, getSum, seriesData[3], false);
	seriesData[3].push({ x: time, y: val });
	communicationStatic = [];
}

function lossFunction(predVal, trueVal){
	return Math.abs(predVal - trueVal)
}

function parsePayload(payload) {
	payload = JSON.parse(payload);
	var message = {}
	message["timestamp"] = payload["timestamp"];
	type = "unknown";
	if (payload.hasOwnProperty("message")) {
		messageStr = payload["message"];	
		if (~messageStr.indexOf("prediction")) {
			type = "prediction";
			vals = messageStr.split(",");
			message["pred"] = vals[0];
			message["true"] = vals[1];
		}
		if (~messageStr.indexOf("communication")) {			
			vals = messageStr.split(",");
			type = "communication" 
			message["subtype"] = vals[1];
			message["nodeID"] = vals[2];
		}
		if (~messageStr.indexOf("static")) {
			message["protocolType"] = "static";
		}
		if (~messageStr.indexOf("dynamic")) {
			message["protocolType"] = "dynamic";
		}
	}
	message["type"] = type;
	return message;
}
	
var errorDynamic = [];
var communicationDynamic = [];
var annotationsDynamic = []
var errorStatic = [];
var communicationStatic = [];




function receiveEvent(eventData) {
	data = JSON.parse(eventData);
	messageType = data.level;
	//channel = data[1];
	payload = data.event
	message = parsePayload(payload);
	if (message["type"] == "prediction") {
	        var pred = message["pred"].split(": ")[1];
                var truev = message["true"].split(": ")[1];
		error = lossFunction(pred,truev);
		//if (message["protocolType"] == "dynamic") {
			errorDynamic.push(error);
		//}	
		if (message["protocolType"] == "static") {
			errorStatic.push(error);
		}
	}
	else if (message["type"] == "communication") {
		if (message["protocolType"] == "dynamic") {
			communicationDynamic.push(1.0);
		}	
		if (message["protocolType"] == "static") {
			communicationStatic.push(1.0);
		}
		
		if (message["subtype"] == "UpdateRefPoint") {
			annotationsDynamic.push(["UpdateRefPoint",message["nodeID"]]);
		}
	}
}

