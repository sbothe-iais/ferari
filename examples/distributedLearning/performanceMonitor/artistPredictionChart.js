if ("WebSocket" in window) {
     // alert("WebSocket is supported by your Browser!");
 } else {
     // The browser doesn't support WebSocket
     alert("WebSocket NOT supported by your Browser!");
 }

 var connection = new WebSocket('ws://localhost:3001');


 connection.onopen = function() {
     //connection.send('Ping'); // Send the message 'Ping' to the server
     alert('connection established to dashboard server');
 };

 // Log errors
 connection.onerror = function(error) {
     alert('WebSocket Error ' + error);
 };

 // Log messages from the server
 connection.onmessage = function(e) {
     console.log('Server: ' + e.data);
     receiveArtistEvent(e.data);
     //receiveEvent(e.data);
 };

var data = [
  {
    x: ['artist1', 'artist2', 'artist3',  'artist4',  'artist5'],
    y: [0, 0, 0, 0, 0],
    type: 'bar'
  }
];

var allArtistsMap = {};
allArtistsMap[""] = 0;

var topArtistsMap = {};
topArtistsMap[""] = 0;



var chartData = getChartData(topArtistsMap);


var errorDynamic = [];
var seriesDatax = [];
var seriesDatay = [];
var aritstPlot = Plotly.newPlot('artistChart', data);
	Plotly.newPlot('artistChart', chartData);	
var plotDiv = document.getElementById('artistChart');

var lossDiv = document.getElementById('artistChart');

var topLayout = {
  xaxis: {title: ''},
  yaxis: {title: 'Slope'},
  margin: {t: 20},
  title:'Top 5 artists',
  hovermode: 'closest'
};

var allLayout = {
  xaxis: {title: ''},
  yaxis: {title: 'Slope'},
  title:'All artists',
  margin: {t: 20},
  hovermode: 'closest'
};


var lossLayout = {
 
  title:'Loss'
};


function receiveArtistEvent(eventData) {

	data = JSON.parse(eventData);
	messageType = data.level;
	//channel = data[1];
	payload = data.event
	message = parsePayload(payload);
	var artistName = message['artistName'].split(": ")[1];
        var artistId = message['artistId'].split(": ")[1];
	var pred = message['pred'].split(": ")[1];
	var intercept = message['intercept'].split(": ")[1];
	//allArtistsMap[artistName] = pred ;
	if(Object.keys(topArtistsMap).length >5) {
		var minArtist = getMinArtist(topArtistsMap);
		if(intercept > topArtistsMap[minArtist]) {
			delete topArtistsMap[minArtist];
			topArtistsMap[artistName] = intercept; // remove minArtist, replace with new artist name
		}
	}
	else {
		topArtistsMap[artistName] = intercept; 
	}
      	allArtistsMap[artistName] = intercept;
	var chartData = getChartData(topArtistsMap);
	Plotly.newPlot('artistChart', chartData, topLayout);

	var allArtistsChartData = getChartData(allArtistsMap);
	var truev = message["true"].split(": ")[1];
	var error = lossFunction(pred,truev);
	errorDynamic.push(error);


	Plotly.newPlot('allArtistChart', allArtistsChartData, allLayout);

        Plotly.newPlot('lossPlot', getLossData(), lossLayout);	

}

function getMinArtist(artistsMap) {
	var minValue = Number.MAX_VALUE;
	var minArtist = "";
	for(key in artistsMap){
	  	var value = topArtistsMap[key];
	  	if(value < minValue) {
	  		minValue = value;
	  		minArtist = key;
	  	}
  	
	}
  	return minArtist;
}



function parsePayload(payload) {
	payload = JSON.parse(payload);
	var message = {}
	message["timestamp"] = payload["timestamp"];
	type = "unknown";
	if (payload.hasOwnProperty("message")) {
		messageStr = payload["message"];	
		if (~messageStr.indexOf("prediction")) {
			type = "prediction";
			vals = messageStr.split(",");
			message["pred"] = vals[0];
			message["artistId"] =  vals[3];
			message["artistName"] =  vals[4]
			message["true"] = vals[1];
                        message["intercept"] = vals[2];
		}
		if (~messageStr.indexOf("communication")) {			
			vals = messageStr.split(",");
			type = "communication" 
			message["subtype"] = vals[1];
			message["nodeID"] = vals[2];
		}
		if (~messageStr.indexOf("static")) {
			message["protocolType"] = "static";
		}
		if (~messageStr.indexOf("dynamic")) {
			message["protocolType"] = "dynamic";
		}
	}
	message["type"] = type;
	return message;
}


function getChartData(artistsMap) {
	var keys = Object.keys(artistsMap);
var values = []


for(key in artistsMap){
  var value = artistsMap[key];
  values.push(value);
}


var chartData = [
	  {
	    x: keys,//['artist1', 'artist2', 'artist3',  'artist4',  'artist5'],
	    y: values, //[message['pred'],1,1,1,1],
	    type: 'bar',
            marker: {
        color: 'rgb(255, 0, 0)',
        line: {
          'color': "#444",
          'width': 0
        }
      },
	  }
	];

	return chartData;
}



function getLossData() {
time = new Date().getTime() / 1000;
	//console.log("Error-Array: "+JSON.stringify(errorDynamic));
	var val = getSafeValue(errorDynamic, getAvg, seriesDatax, true);	

seriesDatax.push(time);
seriesDatay.push(val);
var chartData = [
	  {
	    x: seriesDatax,
	    y: seriesDatay, 
	    type: 'scatter'
	  }
	];

	return chartData;
}



function lossFunction(predVal, trueVal){
	return Math.abs(predVal - trueVal)
}


function getSafeValue(array, leFunction, data, bUseLast) {
	if (array.length > 0) {
		return leFunction(array);
	}
	else {
		if (bUseLast == true) {
			return data[data.length - 1];
		}
		else {
			return 0.0;
		}
	}
}


function getAvg(array) {
	var avg = getSum(array);
	if (array.length > 0) {
		avg = avg / parseFloat(array.length);
	}
	return avg;
}

function getSum(array) {
	var sum = 0;	
	for (var i = 0; i < array.length; i++) {
		sum += parseFloat(array[i]);
	}
	return sum;
}
