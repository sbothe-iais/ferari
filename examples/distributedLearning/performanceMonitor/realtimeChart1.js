// set up our data series with 150 random data points

// instantiate our graph!

var graph1 = new Rickshaw.Graph( {
	element: document.getElementById("chart1"),
	width: 900,
	height: 300,
	renderer: 'area',
	stroke: true,
	preserve: true,
	series: seriesCom
} );

graph1.render();

var preview1 = new Rickshaw.Graph.RangeSlider( {
	graph: graph1,
	element: document.getElementById('preview'),
} );

var legend1 = new Rickshaw.Graph.Legend( {
	graph: graph1,
	element: document.getElementById('legend1')

} );

var shelving1 = new Rickshaw.Graph.Behavior.Series.Toggle( {
	graph: graph1,
	legend: legend1
} );

var order = new Rickshaw.Graph.Behavior.Series.Order( {
	graph: graph1,
	legend: legend1
} );

var highlighter1 = new Rickshaw.Graph.Behavior.Series.Highlight( {
	graph: graph1,
	legend: legend1
} );

var xAxis1 = new Rickshaw.Graph.Axis.Time( {
	graph: graph1,
	ticksTreatment: ticksTreatment,
	timeFixture: new Rickshaw.Fixtures.Time.Local()
} );

xAxis1.render();

var yAxis1 = new Rickshaw.Graph.Axis.Y( {
	graph: graph1,
	tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
	ticksTreatment: ticksTreatment
} );

yAxis1.render();

var smoother1 = new Rickshaw.Graph.Smoother( {
	graph: graph1,
	element: document.querySelector('#smoother1')
} );

var controls1 = new RenderControls( {
	element: document.querySelector('form'),
	graph: graph1
} );
