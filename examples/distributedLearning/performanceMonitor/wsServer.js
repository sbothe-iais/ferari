 if ("WebSocket" in window) {
     // alert("WebSocket is supported by your Browser!");
 } else {
     // The browser doesn't support WebSocket
     alert("WebSocket NOT supported by your Browser!");
 }

 var connection = new WebSocket('ws://localhost:3001');


 connection.onopen = function() {
     //connection.send('Ping'); // Send the message 'Ping' to the server
     alert('connection established to dashboard server');
 };

 // Log errors
 connection.onerror = function(error) {
     alert('WebSocket Error ' + error);
 };

 // Log messages from the server
 connection.onmessage = function(e) {
     console.log('Server: ' + e.data);
     receiveEvent(e.data);
 };
