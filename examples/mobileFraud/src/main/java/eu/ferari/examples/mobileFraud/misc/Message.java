package eu.ferari.examples.mobileFraud.misc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;
import eu.ferari.core.DataTuple;
import eu.ferari.core.misc.Event;
import statistics.Statistics;

public class Message {
	public enum MessageType { 
		CounterUpdate,
		UpdateThreshold, 
		NewThreshold, 		
		Play,
		Pause,
		PauseConfirmation,
		SendLSV,
		LSV,
		GlobalV,
		Statistics,
		Statistic,
		Pull,
		Pull2,
		PullRequest,
		PushModeEvent,
		LSVToCoordinator,
		MetadataFacade,
		Event,
		Push,
		PushEvent,
		ConfFiles,
		ConfProperties,
		NewJsonFile
	}

	private MessageType type;
	private String nodeId;
	private String phone;
	private long timestamp;
	private int counter;
	private int newThreshold;
	private int numNodes;
	private Float[] LSVsum;
	private List<Float> avg;
	private Map<String, List<Statistics>> statistics;
	private Map<String, Statistics> statistic;
	private String statisticType;
	private List<String> pullEvents;
	private List<Event> pushEvents;
	private long WindowDuration;
	private long eventOccurrenceTime;
	private transient FacadesManager facadesManager;
	private transient STORMMetadataFacade facade;
	private String eventName;
	private Map<String,Object> attributes;
	private  long startingTime;
	private long endingTime;
	private int pullRequestId;
	private List<String> ConfProperties;
	private String routingBoltPropertiesFile;
	private String geometricPropertiesFile;
	private String topologyPropertiesFile;
	private String newJsonFile;
	private boolean derivedEvent;
	private String nodeIds;
	

	/**
	 * Constructor for Counter Update, Pause Confirmation, Update Threshold message
	 * @param type
	 * @param nodeId
	 * @param phone
	 * @param timestamp
	 * @param counter
	 */

	public Message (MessageType type, 
			String routingBoltPropertiesFile, 
			String geometricPropertiesFile, 
			String topologyPropertiesFile, 
			String nodeId){
		if (!type.equals(MessageType.ConfFiles))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.routingBoltPropertiesFile = routingBoltPropertiesFile;
		this.geometricPropertiesFile = geometricPropertiesFile;
		this.topologyPropertiesFile = topologyPropertiesFile;
		this.nodeId = nodeId;
	}
	
	public Message (MessageType type, List<String> ConfProperties){
		if (!type.equals(MessageType.ConfProperties))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.ConfProperties = ConfProperties;
	}

	public Message(MessageType type, String nodeId, String phone, long timestamp, int counter){
		this(type, nodeId, phone);

		switch (type) {
		case CounterUpdate:
			this.counter = counter;
			break;
		case UpdateThreshold:
			this.newThreshold = counter;
			break;
		default:
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		}
		this.timestamp = timestamp;		
	}

	/**
	 * Constructor for Counter Request, Pause, PlayAndUpdateEstimate message
	 * @param type
	 * @param nodeId
	 * @param phone
	 * @param timestamp
	 */
	public Message(MessageType type, String nodeId, String phone, long timestamp){
		this(type, nodeId, phone);
		switch (type) {
		case Play:
		case SendLSV:
		case Pause:
			break;
		default:
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		}
		this.timestamp = timestamp;
	}

	/**
	 * Constructor for New Threshold message
	 * @param type
	 * @param nodeId
	 * @param phone
	 * @param newThreshold
	 */
	public Message(MessageType type, String nodeId, String phone, int newThreshold){
		this(type, nodeId, phone);
		if (!type.equals(MessageType.NewThreshold))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.newThreshold = newThreshold;
	}
	
	public Message(MessageType type, String newJsonFile, String nodeId, boolean unused){
		this.type = type;
		this.nodeId = nodeId;
		if (!type.equals(MessageType.NewJsonFile))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.newJsonFile = newJsonFile;
	}

	private Message(MessageType type, String nodeId, String phone){
		this.type = type;
		this.nodeId = nodeId;
		this.phone = phone;
	}

	public Message(DataTuple message){
		this.type = MessageType.valueOf(message.getString(0));
		switch (type) {
		case CounterUpdate:
			this.nodeId = message.getString(1);
			this.phone = message.getString(2);
			this.counter = message.getInteger(3);
			this.timestamp = message.getLong(4);		
			break;
		case PauseConfirmation:
			this.nodeId = message.getString(1);
			this.phone = message.getString(2);
			this.counter = message.getInteger(3);
			this.timestamp = message.getLong(4);
			this.LSVsum = (Float[])message.getValue(5);
			this.numNodes = message.getInteger(6);
			break;
		case UpdateThreshold:
			this.nodeId = message.getString(1);
			this.phone = message.getString(2);
			this.newThreshold = message.getInteger(3);
			this.timestamp = message.getLong(4);
			break;
		case Play:
		case SendLSV:
			this.nodeId = message.getString(1);
			this.phone = message.getString(2);
			this.timestamp = message.getLong(3);
			break;
		case Pause:
			this.nodeId = message.getString(1);
			this.phone = message.getString(2);
			this.timestamp = message.getLong(3);
			break;
		case NewThreshold:
			this.nodeId = message.getString(1);
			this.phone = message.getString(2);
			this.newThreshold = message.getInteger(3);
			break;
		case LSV:
		case LSVToCoordinator:
			this.phone = message.getString(1);
			this.timestamp = message.getLong(2);
			this.LSVsum = (Float[])message.getValue(3);
			this.numNodes = message.getInteger(4);
			this.nodeId = message.getString(5);
			break;
		case GlobalV:
			this.phone = message.getString(1);
			this.timestamp =  message.getLong(2);
			this.avg = (List<Float>)message.getValue(3);
			break;
		case Statistics:
			this.statistics = (Map<String, List<Statistics>>) message.getValue(1);
			break;
		case Statistic:
			this.statistic = (Map<String, Statistics>) message.getValue(1);
			this.statisticType = message.getString(2);
			break;
		case Pull:
		case Pull2:
			this.pullEvents = (List<String>) message.getValue(1);
			this.startingTime = (long) message.getValue(2);
			this.endingTime =  message.getValue(3) instanceof Double ? message.getDouble(3).longValue() : message.getLong(3);//message.getLong(3);
			this.nodeId = message.getString(4);
			break;
		case PullRequest:
			this.pullEvents = (List<String>) message.getValue(1);
			this.startingTime = (long) message.getValue(2);
			this.endingTime =  message.getValue(3) instanceof Double ? message.getDouble(3).longValue() : message.getLong(3);//message.getLong(3);
			this.pullRequestId = message.getInteger(4);
			break;
		case Push:
			this.pushEvents = (List<Event>)message.getValue(1);
			this.pullRequestId = message.getInteger(2);
			break;
		case PushEvent:
			this.eventName = message.getString(1);
			this.attributes = (Map<String,Object>)message.getValue(2);
			this.nodeId = message.getString(3);
			break;
		case MetadataFacade:
			this.facadesManager = (FacadesManager)message.getValue(1);
			this.facade = (STORMMetadataFacade)message.getValue(2);
			break;
		case Event:
			this.eventName = (String)message.getValue(1);
			this.eventOccurrenceTime = message.getLong(2);
			this.attributes = (Map<String,Object>)message.getValue(3);
			this.derivedEvent = message.getBoolean(4);
			break;
		case ConfProperties:
			this.ConfProperties = (List<String>)message.getValue(1);
			break;
		case ConfFiles:
			this.routingBoltPropertiesFile = message.getString(1);
			this.geometricPropertiesFile = message.getString(2);
			this.topologyPropertiesFile = message.getString(3);
			this.nodeId = message.getString(4);
			break;
		case NewJsonFile:
			this.newJsonFile = message.getString(1);
			this.nodeId = message.getString(2);
			break;
		case PushModeEvent:
			this.eventName = (String)message.getValue(1);
			this.nodeIds = (String)message.getValue(2);
			break;
		default:
			throw new IllegalArgumentException("Unknown message type " + type);
		}	
	}

	public Message(MessageType type, String phone, long timestamp, Float[] LSVsum, int numNodes, String nodeId){
		if (!type.equals(MessageType.LSV) && !type.equals(MessageType.LSVToCoordinator))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.phone = phone;
		this.timestamp = timestamp;
		this.LSVsum = LSVsum;
		this.numNodes = numNodes;
		this.nodeId = nodeId;
	}

	public Message(MessageType type, String nodeId, String phone, long timestamp, int counter, Float[] LSVsum, int numNodes){
		if (!type.equals(MessageType.PauseConfirmation))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.phone = phone;
		this.timestamp = timestamp;
		this.LSVsum = LSVsum;
		this.numNodes = numNodes;
		this.nodeId = nodeId;
		this.counter = counter;
	}

	public Message(MessageType type, String phone, long timestamp, List<Float> avg){
		if (!type.equals(MessageType.GlobalV))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.phone = phone;
		this.timestamp = timestamp;
		this.avg = avg;
	}

	public Message(MessageType type, Map<String, List<Statistics>> statistics){
		if (!type.equals(MessageType.Statistics))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.statistics = statistics;
	}

	public Message(MessageType type, Map<String, Statistics> statistic, String statisticType){
		if (!type.equals(MessageType.Statistic))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.statistic = statistic;
		this.statisticType = statisticType;
	}

	public Message(MessageType type, FacadesManager facadesManager, STORMMetadataFacade facade){
		if (!type.equals(MessageType.MetadataFacade))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.facadesManager = facadesManager;
		this.facade = facade;
	}

	public Message(MessageType type, List<Event> pushEvents, int pullRequestId){
		if (!type.equals(MessageType.Push))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.pushEvents = pushEvents;
		this.pullRequestId = pullRequestId;
	}

	public Message(MessageType type, String eventName, Map<String,Object> attributes, String nodeId){
		if (!type.equals(MessageType.PushEvent))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.eventName = eventName;
		this.attributes = attributes;
		this.nodeId = nodeId;
	}

	public Message(MessageType type, List<String> pullEvents, long startingTime, long endingTime, String nodeId){
		if (!type.equals(MessageType.Pull) && !type.equals(MessageType.Pull2))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.pullEvents = pullEvents;
		this.startingTime = startingTime;
		this.endingTime = endingTime;
		this.nodeId = nodeId;
	}

	public Message(MessageType type, List<String> pullEvents, long startingTime, long endingTime, int pullRequestId){
		if (!type.equals(MessageType.PullRequest))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.pullEvents = pullEvents;
		this.startingTime = startingTime;
		this.endingTime = endingTime;
		this.pullRequestId = pullRequestId;
	}

	public Message(MessageType type, String eventName, long eventOccurrenceTime, Map<String,Object> attributes, boolean derivedEvent){
		if (!type.equals(MessageType.Event))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.type = type;
		this.eventName = eventName;
		this.eventOccurrenceTime = eventOccurrenceTime;
		this.attributes = attributes;
		this.derivedEvent = derivedEvent;
	}

	public DataTuple toDataTuple(){
		DataTuple message = new DataTuple(String.valueOf(type));
		switch (type) {
		case CounterUpdate:
			message.add(nodeId, phone, counter, timestamp);
			break;
		case PauseConfirmation:	
			message.add(nodeId, phone, counter, timestamp, LSVsum, numNodes);
			break;
		case UpdateThreshold:
			message.add(nodeId, phone, newThreshold, timestamp);
			break;
		case Pause:
			message.add(nodeId, phone, timestamp);
			break;
		case Play:
		case SendLSV:
			message.add(nodeId, phone, timestamp);
			break;
		case NewThreshold:
			message.add(nodeId, phone, newThreshold);
			break;
		case LSV:
		case LSVToCoordinator:
			message.add(phone, timestamp, LSVsum, numNodes, nodeId);
			break;
		case GlobalV:
			message.add(phone, timestamp, avg);
			break;
		case Statistics:
			message.add(statistics, statisticType);
			break;
		case Statistic:
			message.add(statistic);
			break;
		case Pull:
		case Pull2:
			message.add(pullEvents, startingTime, endingTime, nodeId);
			break;
		case PullRequest:
			message.add(pullEvents, startingTime, endingTime, pullRequestId);
			break;
		case MetadataFacade:
			message.add(facadesManager, facade);
			break;
		case Event:
			Map<String,Object> attr = new HashMap<String, Object>(attributes);
			message.add(eventName, eventOccurrenceTime, attr, derivedEvent);
			break;
		case Push:
			message.add(pushEvents, pullRequestId);
			break;
		case PushEvent:
			Map<String,Object> attr1 = new HashMap<String, Object>(attributes);
			message.add(eventName, attr1, nodeId);
			break;
		case ConfProperties:
			message.add(ConfProperties);
			break;
		case ConfFiles:
			message.add(routingBoltPropertiesFile);
			message.add(geometricPropertiesFile);
			message.add(topologyPropertiesFile);
			message.add(nodeId);
			break;
		case NewJsonFile:
			message.add(newJsonFile);
			message.add(nodeId);
			message.add(false);
			break;
		case PushModeEvent:
			message.add(eventName);
			message.add(nodeIds);
			break;
		default:
			throw new IllegalArgumentException("Unknown message type " + type);
		}
		return message;
	}

	public String getNodeIds() {
		return nodeIds;
	}

	public void setNodeIds(String nodeIds) {
		this.nodeIds = nodeIds;
	}

	public Message toPauseMessage(String nodeId){
		return new Message(MessageType.Pause, nodeId, phone, timestamp);
	}

	public MessageType getType() {
		return type;
	}

	public String getNodeId() {
		return nodeId;
	}

	public FacadesManager getFacadesManager() {
		return facadesManager;
	}

	public void setFacadesManager(FacadesManager facadesManager) {
		this.facadesManager = facadesManager;
	}

	public STORMMetadataFacade getFacade() {
		return facade;
	}

	public void setFacade(STORMMetadataFacade facade) {
		this.facade = facade;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public void setNewThreshold(int newThreshold) {
		this.newThreshold = newThreshold;
	}

	public void setNumNodes(int numNodes) {
		this.numNodes = numNodes;
	}

	public void setLSVsum(Float[] lSVsum) {
		LSVsum = lSVsum;
	}

	public void setAvg(List<Float> avg) {
		this.avg = avg;
	}

	public void setStatistics(Map<String, List<Statistics>> statistics) {
		this.statistics = statistics;
	}

	public void setStatistic(Map<String, Statistics> statistic) {
		this.statistic = statistic;
	}

	public void setStatisticType(String statisticType) {
		this.statisticType = statisticType;
	}

	public void setPullEvents(List<String> pullEvents) {
		this.pullEvents = pullEvents;
	}

	public void setPushEvents(List<Event> pushEvents) {
		this.pushEvents = pushEvents;
	}

	public void setWindowDuration(long windowDuration) {
		WindowDuration = windowDuration;
	}

	public void setEventOccurrenceTime(long eventOccurrenceTime) {
		this.eventOccurrenceTime = eventOccurrenceTime;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public void setStartingTime(long startingTime) {
		this.startingTime = startingTime;
	}

	public void setEndingTime(long endingTime) {
		this.endingTime = endingTime;
	}

	public void setPullRequestId(int pullRequestId) {
		this.pullRequestId = pullRequestId;
	}

	public void setConfProperties(List<String> confProperties) {
		ConfProperties = confProperties;
	}

	public void setRoutingBoltPropertiesFile(String routingBoltPropertiesFile) {
		this.routingBoltPropertiesFile = routingBoltPropertiesFile;
	}

	public void setGeometricPropertiesFile(String geometricPropertiesFile) {
		this.geometricPropertiesFile = geometricPropertiesFile;
	}

	public void setTopologyPropertiesFile(String topologyPropertiesFile) {
		this.topologyPropertiesFile = topologyPropertiesFile;
	}

	public void setNewJsonFile(String newJsonFile) {
		this.newJsonFile = newJsonFile;
	}

	public void setDerivedEvent(boolean derivedEvent) {
		this.derivedEvent = derivedEvent;
	}

	public String getPhone() {
		return phone;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getRoutingBoltPropertiesFile() {
		return routingBoltPropertiesFile;
	}

	public String getGeometricPropertiesFile() {
		return geometricPropertiesFile;
	}

	public String getTopologyPropertiesFile() {
		return topologyPropertiesFile;
	}

	public int getCounter() {
		return counter;
	}

	public int getNewThreshold() {
		return newThreshold;
	}

	public Map<String, List<Statistics>> getStatistics() {
		return statistics;
	}

	public Map<String, Statistics> getStatistic() {
		return statistic;
	}

	public List<String> getPullEvents() {
		return pullEvents;
	}

	public List<Event> getPushEvents() {
		return pushEvents;
	}

	public long getWindowDuration() {
		return WindowDuration;
	}

	public long getEventOccurrenceTime() {
		return eventOccurrenceTime;
	}

	public String toJSON(){
		Gson gson = new Gson();

		return gson.toJson(this);
	}

	public int getNumNodes() {
		return numNodes;
	}

	public Float[] getLSVsum() {
		return LSVsum;
	}

	public List<Float> getAvg() {
		return avg;
	}

	public String getEventName() {
		return eventName;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public long getStartingTime() {
		return startingTime;
	}

	public long getEndingTime() {
		return endingTime;
	}

	public int getPullRequestId() {
		return pullRequestId;
	}

	public List<String> getConfProperties() {
		return ConfProperties;
	}

	public String getNewJsonFile() {
		return newJsonFile;
	}

	public boolean isDerivedEvent() {
		return derivedEvent;
	}
	
	public String getStatisticType() {
		return statisticType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Type: " + type);

		sb.append(", Phone: " + phone);

		switch (type) {		
		case CounterUpdate:
			sb.append(", Phone: " + phone);
			sb.append(", NodeId: " + nodeId);
			sb.append(", Timestamp: " + timestamp);
			sb.append(", Counter: " + counter);	
			break;
		case PauseConfirmation:
			sb.append(", Phone: " + phone);
			sb.append(", NodeId: " + nodeId);
			sb.append(", Timestamp: " + timestamp);
			sb.append(", Counter: " + counter);	
			sb.append(", numNodes: " + numNodes);
			sb.append(", LSV : " + Arrays.toString(LSVsum));
			break;
		case UpdateThreshold:
			sb.append(", Phone: " + phone);
			sb.append(", NodeId: " + nodeId);
			sb.append(", Timestamp: " + timestamp);
			sb.append(", New Threshold: " + newThreshold);
			break;
		case NewThreshold:
			sb.append(", Phone: " + phone);
			sb.append(", NodeId: " + nodeId);
			sb.append(", New Threshold: " + newThreshold);
			break;
		case Play:			
		case Pause:
		case SendLSV:
			sb.append(", Phone: " + phone);
			sb.append(", Timestamp: " + timestamp);
			sb.append(", nodeId : " + nodeId);
			break;		
		case LSV:
		case LSVToCoordinator:
			sb.append(", Timestamp: " + timestamp);
			sb.append(", LSV : " + LSVsum);
			sb.append(", numNodes: " + numNodes);
			sb.append(", nodeId : " + nodeId);
			break;
		case GlobalV:
			sb.append(", Phone: " + phone);
			sb.append(", Timestamp: " + timestamp);
			sb.append(", Avg: " + avg);
		case Event:
			sb.append(", EventName: " + eventName);
			sb.append(", EventOccurrenceTime: " + eventOccurrenceTime);
			sb.append(", Attributes: " + attributes);
			sb.append(", DerivedEvent: " + derivedEvent);
			break;
		case Pull:
		case Pull2:
			sb.append(", nodeId : " + nodeId);
			sb.append(", PullEvents: " + pullEvents);
			sb.append(", StartingTime: " + startingTime);
			sb.append(", EndingTime: " + endingTime);
			break;
		case Push:
			sb.append(", PushEvents : " + pushEvents);
			sb.append(", PullRequestId : " + pullRequestId);
			break;
		case PushEvent:
			sb.append(", nodeId : " + nodeId);
			sb.append(", EventName: " + eventName);
			sb.append(", Attributes: " + attributes);
			break;
		case ConfFiles:
			sb.append(", nodeId : " + nodeId);
			sb.append(", routingBoltPropertiesFile : " + routingBoltPropertiesFile);
			sb.append(", geometricPropertiesFile : " + geometricPropertiesFile);
			sb.append(", topologyPropertiesFile : " + topologyPropertiesFile);
			break;		case ConfProperties:
			break;
		case NewJsonFile:
			sb.append(", nodeId : " + nodeId);
			sb.append(", JsonFile : " + newJsonFile);
			break;
		case PushModeEvent:
			sb.append(", EventName: " + eventName);
			sb.append(", recievers: " + nodeIds);
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle message of type: " + type);
		}
		
		return sb.toString();
	}
}
