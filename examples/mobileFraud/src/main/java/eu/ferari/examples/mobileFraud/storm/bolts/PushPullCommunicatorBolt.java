package eu.ferari.examples.mobileFraud.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import eu.ferari.backend.storm.sender.DataTupleStreamSender;
import eu.ferari.backend.storm.sender.SimpleSender;
import eu.ferari.core.DataTuple;
import eu.ferari.examples.mobileFraud.state.PushPullCommunicator;

import java.util.Map;

/**
 * Created by mofu on 15/11/16.
 */
public class PushPullCommunicatorBolt extends BaseRichBolt {
	PushPullCommunicator pushPullCommunicator;
	private String pushPullComToBufferChannel;
	private String nodeId;
	private OutputCollector collector;


	public PushPullCommunicatorBolt(String pushPullComToBufferChannel, String nodeId){
		pushPullCommunicator=new PushPullCommunicator(nodeId);
		this.pushPullComToBufferChannel=pushPullComToBufferChannel;
	}
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		pushPullCommunicator.setSender(new SimpleSender(collector));
		pushPullCommunicator.setPushPullBufferSender(new DataTupleStreamSender(collector,pushPullComToBufferChannel));
		this.collector=collector;
	}

	@Override
	public void execute(Tuple input) {
		if(input.getValue(0)instanceof DataTuple)
			pushPullCommunicator.update((DataTuple)input.getValue(0));
		else pushPullCommunicator.update(new DataTuple(input.getValues()));
		collector.ack(input);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("PushPullTuple"));
		declarer.declareStream(pushPullComToBufferChannel, new Fields("PushPullTuple"));
	}
}
