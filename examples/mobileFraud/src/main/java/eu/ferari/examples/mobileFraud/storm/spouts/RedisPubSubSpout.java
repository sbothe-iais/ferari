package eu.ferari.examples.mobileFraud.storm.spouts;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.TopologyRequestHandler;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.backend.storm.sender.StreamSender;
import eu.ferari.core.Constants;
import eu.ferari.core.DataTuple;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.utils.NoopSender;
import eu.ferari.examples.mobileFraud.misc.CommListener;
import eu.ferari.examples.mobileFraud.misc.MetaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * Stolen from
 * https://github.com/stormprocessor/storm-redis-pubsub/blob/master/src
 * /jvm/yieldbot/storm/spout/RedisPubSubSpout.java
 *
 */
public class RedisPubSubSpout extends BaseRichSpout {


	public static enum ConfigRoutingBoltKeys {
		pull, 
		statisticsClass, 
		epoch, 
		eType, 
		jarLocation,
		//FIXME remaining config keys not used???
		startingTime, 
		endingTime, 
		pushEvent, 
		pushEventReciever
	}

	static {
		FerariClientBuilder.getInstance().setAddressResolver(InMemoryGlobalAddressResolver.getInstance());
	}
	
	static final long serialVersionUID = 737015313288609460L;
    private static final Logger logger = LoggerFactory.getLogger(RedisPubSubSpout.class);

	private LinkedBlockingQueue<Serializable> queue;

	private final String toRoutingBoltStream;
	private final String toContextBoltStream;
	private final String toEpaManagerBoltStream;
	private final String geomToComStream;
	private final String geomConfigStream;
	private final String geomToCoordStream;
	private final String pullBufferStream;
	private final String pullComStream;
	private final String statisticsConfToRoutingStream;
	private final String relayConfStream;
	private final String nodeID;
	private String coordinatorId;


	private StreamSender geomConfigSender;
	private StreamSender propertiesToRoutingSender;
	private SegmentedStreamSender geomComSender;
	private ISend geomCoordinatorSender;
	private StreamSender pullBufferSender;
	private StreamSender pullComSender;
	private StreamSender sendEpnToRoutingBolt;
	private StreamSender sendEpnToContextBolt;
	private StreamSender sendEpnToEpaManagerBolt;
	private StreamSender relayConfSender;
	private  boolean isCoordinator;

	private IntraSiteClient _intraSiteClient;
	private boolean gotTopology = false;
	
	
	public RedisPubSubSpout(String pullBufferStream,
							String pullComStream,
							String geomConfigStream,
							String geomToComStream,
							String geomToCoordStream,
							String statisticsConfToRoutingStream,
							String toRoutingBoltStream,
							String toEpaManagerBoltStream,
							String toContextBoltStream,
							String relayConfStream,
							String nodeID) {


		this. toContextBoltStream=toContextBoltStream;
		this.toEpaManagerBoltStream=toEpaManagerBoltStream;
		this.toRoutingBoltStream=toRoutingBoltStream;
		this.statisticsConfToRoutingStream=statisticsConfToRoutingStream;
		this.geomToComStream =geomToComStream;
		this.geomConfigStream=geomConfigStream;
		this.geomToCoordStream=geomToCoordStream;
		this.pullBufferStream=pullBufferStream;
		this.pullComStream=pullComStream;
		this.relayConfStream=relayConfStream;
		this.nodeID = nodeID;
	}

	public void open(Map stormConf, TopologyContext context, SpoutOutputCollector collector) {

		geomComSender = new SegmentedStreamSender(collector,geomToComStream);
		geomConfigSender = new StreamSender(collector,geomConfigStream);
		geomCoordinatorSender = new SegmentedStreamSender(collector,geomToCoordStream);
		pullBufferSender=new StreamSender(collector,pullBufferStream);
		pullComSender=new StreamSender(collector,pullComStream);
		sendEpnToContextBolt = new StreamSender(collector,toContextBoltStream);
		sendEpnToEpaManagerBolt = new StreamSender(collector,toEpaManagerBoltStream);
		sendEpnToRoutingBolt=new StreamSender(collector,toRoutingBoltStream);
		propertiesToRoutingSender= new StreamSender(collector, statisticsConfToRoutingStream);
		relayConfSender = new StreamSender(collector,relayConfStream);
		queue = new LinkedBlockingQueue<Serializable>();

		CommListener listener = new CommListener(queue, nodeID, (String) stormConf.get(CommIntegrationConstants.EVENT_CONTEXT));
		

		_intraSiteClient = FerariClientBuilder.getInstance().createIntraSiteClient(nodeID);
		_intraSiteClient.setTopologyRequestHandler(new TopologyRequestHandler() {
			
			@Override
			public void onTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
				if (gotTopology) {
					return;
				}
				
				gotTopology = true;
				
				Serializable s = customConfig.get(nodeID);
				if (s == null) {
					logger.warn("No " + nodeID + " in topoloy file");
					return;
				}
				
				if (nodeID.equals("cent1")) {
					logger.info("+++++++++++++++++++ cent1 got topology ++++++++++++++++");
				}
				DataTuple tuple = (DataTuple) s;
				coordinatorId=tuple.getString(0);
				if(coordinatorId.equals(nodeID))
					isCoordinator=true;
				else{
					isCoordinator=false;
					geomCoordinatorSender=new NoopSender();
				}
				queue.add(new MetaMessage(MetaMessage.PROTON_CONFIG,new DataTuple("NewJsonFile",tuple.getString(1),null,false),null));
				queue.add(new MetaMessage(MetaMessage.PUSH_PULL_CONFIG,new DataTuple(tuple.getString(2)),null));
				queue.add(new MetaMessage(MetaMessage.GM_Config,new DataTuple(tuple.getString(3)),null));
			}
		});
		
		logger.info("Redis pubsub spout started for " + nodeID);
		
		
	}

	public void close() {
		//jedisPool.close();
	}

	public void nextTuple() {
		Serializable message = takefromQueue();
		if(message instanceof MetaMessage){
			MetaMessage mMessage = (MetaMessage)message;
			logger.debug("mMessage type {}",mMessage.getFunctionality());
			switch(mMessage.getFunctionality()){
				case(MetaMessage.P_P_FUNCTIONALITY):
					handlePushPullMessage(mMessage.getData());
					break;
				case(MetaMessage.GM_FUNCTIONALITY):
					if(isCoordinator) geomCoordinatorSender.signal(mMessage.getData());
					break;
				case(MetaMessage.GM_COORDINATOR):
					geomComSender.signal(mMessage.getData());
					break;
				//TODO config needs nodeId check?? probably not atm.
				case MetaMessage.GM_Config:
					geomConfigSender.signal(mMessage.getData());
					relayConfSender.signal(new DataTuple(coordinatorId));
					break;
				case MetaMessage.PROTON_CONFIG:
					sendEpnToContextBolt.signal(mMessage.getData());
					sendEpnToRoutingBolt.signal(mMessage.getData());
					sendEpnToEpaManagerBolt.signal(mMessage.getData());
					break;
				case MetaMessage.PUSH_PULL_CONFIG:
					Properties routingBoltProperties = parsePropertiesString(mMessage.getData().getString(0));
					ArrayList<String> ret =new ArrayList<>(13);
					//stupid hack for compatibility. See RoutingBolt.
					ret.add(null);
					ret.add(null);
					ret.add(null);
					ret.add(null);

					ret.add(null);
					ret.add(null);
					ret.add(null);
					ret.add(null);
					ret.add(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.pull.toString()));
					ret.add(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.statisticsClass.toString()));
					ret.add(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.epoch.toString()));
					ret.add(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.eType.toString()));
					ret.add(routingBoltProperties.getProperty(ConfigRoutingBoltKeys.jarLocation.toString()));
					propertiesToRoutingSender.signal(new DataTuple(ret));
					break;
			}
		}

	}
	private void handlePushPullMessage(DataTuple tuple) {
		switch (tuple.getString(0)) {
			case "Pull":
				DataTuple ret = new DataTuple("Pull2", tuple.getValue(1),
						tuple.getValue(2), tuple.getValue(3), tuple.getValue(4));
				pullComSender.signal(ret);
				break;
			case "PullRequest":
				pullBufferSender.signal(tuple);
				break;
		}
	}



	private void processGeomMessage(DataTuple tuple){
		eu.ferari.core.utils.MessageType type=(eu.ferari.core.utils.MessageType)tuple.getValue(Constants.mesageTypeIndex);
		switch (type){
			case Violation:
				if(isCoordinator){
					logger.error("Received Violation at Coordinator.");
					geomCoordinatorSender.signal(tuple);
				}
				break;
			default:
				geomComSender.signal(tuple);
		}
	}

	private Serializable takefromQueue() {
		return queue.poll();
	}

	private Properties parsePropertiesString(String s) {
		final Properties p = new Properties();
		try {
			p.load(new StringReader(s));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}



	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(pullBufferStream,new Fields("type", "pullEvents", "starting", "ending", "pullrequestID"));
		declarer.declareStream(pullComStream, new Fields("type", "pullEvents", "starting", "ending", "nodeID"));
		declarer.declareStream(geomConfigStream, new Fields("configurationTuple"));
		declarer.declareStream(geomToComStream, new Fields(SegmentedStreamSender.fields));
		declarer.declareStream(geomToCoordStream,new Fields(SegmentedStreamSender.fields));
		declarer.declareStream(toRoutingBoltStream,  new Fields("type", "json", "rand", "rand1"));
		declarer.declareStream(toContextBoltStream,  new Fields("type", "json", "rand", "rand1"));
		declarer.declareStream(toEpaManagerBoltStream, new Fields("type", "json", "rand", "rand1"));
		declarer.declareStream(statisticsConfToRoutingStream, new Fields("properties"));
		declarer.declareStream(relayConfStream,new Fields("coordinatorId"));
	}
}
