package eu.ferari.examples.mobileFraud.storm;

import java.io.IOException;

/**
 * Created by mofu on 01/10/15.
 */
public interface MobileFraudTopology {
	
	public static enum ConfigKeys {
		commToCoordinatorChannel, coordinatorToCommChannel
	}
	public static enum ConfigKeysGeometric {
		nodeId, globalThreshold, numBillingNodes, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold, functionName, functionLocation
	}
	
    public void start() throws IOException;
    public void stop();
}
