package eu.ferari.examples.mobileFraud.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ILocalState;

import java.util.Map;

/**
 * Created by mofu on 26/10/16.
 */
public class LocalBolt extends BaseRichBolt {
	private String _coordinatorChannel;
	private ILocalState _state;

	public LocalBolt(ILocalState localState, String coordinatorChannel){
		_state=localState;
		_coordinatorChannel=coordinatorChannel;
	}
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {

	}

	@Override
	public void execute(Tuple input) {
		if (input.getSourceStreamId()==_coordinatorChannel)
			_state.handleFromCommunicator((DataTuple) input.getValue(0));
		else
			_state.update((DataTuple)input.getValue(0));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}
}
