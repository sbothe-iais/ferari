package eu.ferari.examples.mobileFraud;


import org.apache.storm.utils.Utils;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.coord.Coordinator;
import com.ibm.ferari.coord.EventConsumer;
import com.ibm.ferari.util.Util;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;
import eu.ferari.optimizer.locationGraph.LGraphNode;
import eu.ferari.optimizer.locationGraph.LocationGraph;
import eu.ferari.optimizer.locationGraph.NetworkParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
	//	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	private static Socket socket;
	private static LinkedBlockingQueue<String> queue;
	private static final String REMOTE_IP = "localhost";
	private static final int REMOTE_PORT = 4550;
	private static HashMap<String, String> properties ;
	private static BufferedReader in;

	private static String inputFileName = "FERARI_1M_v2.txt";
	private static boolean dashboard = false;
	private	static String	pathFiles = "/home/yacovm/FERARI_DEMO/";
	private static boolean	fixdelay = true;
	private	static int	delay = 50;

	static {
		FerariClientBuilder.getInstance().setAddressResolver(InMemoryGlobalAddressResolver.getInstance());
	}
	
	//	private static boolean dashboard;
	//	private	static String pathFiles;
	//	private static boolean fixdelay;
	//	private	static int delay;
	//	private static String inputFileName;


	public static void main(String[] args) throws IOException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				ExternalProgram_demo.main(new String[]{});
			}
		}).start();
		
		Util.sleep(2);
		
		//		properties = new HashMap<String, String>();
		//		String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		//		String paths[] = path.split("/");
		//		paths[paths.length-1] = "conf.txt";
		//		String newPath="";
		//		for(int s=0;s<paths.length;s++)
		//		{
		//			newPath+=paths[s];
		//			if(s!=paths.length-1)
		//				newPath+="/";
		//		}
		//
		//		String decodedPath = URLDecoder.decode(newPath, "UTF-8");
		//		FileInputStream fis = new FileInputStream(decodedPath);
		//		BufferedReader in = new BufferedReader(new InputStreamReader(fis));
		//		String line1;
		//		while((line1 = in.readLine())!=null)
		//		{
		//			String[] parts = line1.split("="); 
		//			properties.put(parts[0], parts[1]);  
		//		}
		//
		//		dashboard = Boolean.valueOf(properties.get("dashboard"));
		//		pathFiles = properties.get("path");
		//		fixdelay = Boolean.valueOf(properties.get("fixdelay"));
		//		delay = Integer.parseInt(properties.get("delay"));
		//		inputFileName = properties.get("InputFileName");

		queue = new LinkedBlockingQueue<String>(1000);
		String line = null;
		Map<String, ProtonMobileFraudTopology> topologies = null;
		try{
			socket = new Socket(REMOTE_IP, REMOTE_PORT);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			topologies = new HashMap<String, ProtonMobileFraudTopology>();
			line = in.readLine();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		LocationGraph lg1 = null;
		NetworkParser np = new NetworkParser(null, ";");
		lg1 = np.parseFile(line);
		lg1.allPairsShortestPath(false);
		lg1.eventLatenciesPerPlacement();
		lg1.calculateHops(2);
		lg1.computeLocalHopedFrequency();
		String centralNode = lg1.centralNode().location;

		//String DATA_FILE_NAME = InputFileName;
		//DataSplitter ds = new DataSplitter(OUTPUT_FOLDER , OUTPUT_FOLDER, DATA_FILE_NAME, lg1);
		//ds.splitFile();
		//ds.dataGenerator(864, "_" + InputFileName);

		try {
			while (line != null) {
				System.out.println("main received optimizer parameters");			
				ProtonMobileFraudTopology topology;
				for(LGraphNode n: lg1.nodes)
				{
					topology = new ProtonMobileFraudTopology(null, n.location.equals(centralNode), n.location, dashboard,
							pathFiles, fixdelay, delay, inputFileName);
					topologies.put(n.location, topology);
				}
				line = in.readLine();
			}
			in.close();
			socket.close();
		} catch (Exception ioe) {
			System.out.println("socket : " + ioe.getMessage());
			if (socket != null)
				socket.close();
			in.close();
		}
		
		
		ProtonMobileFraudTopology centTop = topologies.get(centralNode);
		topologies.remove(centralNode);
		
		for (Map.Entry<String, ProtonMobileFraudTopology> topology : topologies.entrySet()){
			Util.runAsyncTask(() -> {
				startTopology(topology);
			});
		}
		
		createCoordinator(centralNode).start();
		
		Util.sleep(10);
		
		centTop.start(centralNode);


		Utils.sleep(Long.MAX_VALUE);

/*		for (Map.Entry<String, ProtonMobileFraudTopology> topology : topologies.entrySet())
			topology.getValue().stop();*/
	}

	private static void startTopology(Map.Entry<String, ProtonMobileFraudTopology> topology) {
		try {
			topology.getValue().start(topology.getKey());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Coordinator createCoordinator(String nodeId) {
		String selfPort = InMemoryGlobalAddressResolver.getInstance().getHostsOfSite(nodeId)[0].split(":")[1];
		IntraSiteClient relayer = FerariClientBuilder.getInstance().createIntraSiteClient(nodeId);
		Coordinator coord = new Coordinator(nodeId, nodeId, Integer.parseInt(selfPort), new EventConsumer() {
			
			@Override
			public void consumeEvent(FerariEvent event) {
				relayer.broadcastInOurSite(event);
			}
		});
		return coord;
	}
}