package eu.ferari.examples.mobileFraud.storm.spouts;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.FerariRawDataItem;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.MessageHandler;
import eu.ferari.backend.storm.sender.DirectSender;
import eu.ferari.core.DataTuple;
import eu.ferari.examples.mobileFraud.misc.MetaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class PushAndPullSpout extends BaseRichSpout {
	private LinkedBlockingQueue<DataTuple> queue;
	private String nodeID;
	private DirectSender pushSender;
	private final String routingName;
	private final String pushStream;
	private final static Logger logger= LoggerFactory.getLogger(PushAndPullSpout.class);



	public PushAndPullSpout(String nodeID, String routingName, String pushStream){
		this.nodeID = nodeID;
		this.routingName = routingName;
		this.pushStream = pushStream;
	}

	@Override
	public void nextTuple() {
		DataTuple tuple= queue.poll();
		try {
			if (tuple != null) {

				if(!nodeID.equals(tuple.getString(tuple.getDataLength()-1))) {
					return;
				}

				if(tuple.getString(0).equals("PushEvent")) {
					Map<String,Object> attributes=(Map<String,Object>)tuple.getValue(2);
					if(attributes.containsKey("call_start_date")){
						Number call_start_date = (Number) attributes.get("call_start_date");
						attributes.put("call_start_date", call_start_date.longValue() );
					}
					pushSender.signal(tuple);
					return;

				} else {
					//throw new IllegalArgumentException("Don't know how to handle message type " + message.getType());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void open(Map stormConf, TopologyContext context, SpoutOutputCollector collector) {
		pushSender = new DirectSender(collector, context, routingName, pushStream);

		queue = new LinkedBlockingQueue<>(1000);
		

		IntraSiteClient intraSiteClient = FerariClientBuilder.getInstance().createIntraSiteClient(nodeID);
		intraSiteClient.setbroadcastRequestHandler(new MessageHandler() {
			
			@Override
			public void onMessage(Serializable msg) {
				if (! (msg instanceof FerariEvent)) {
					logger.trace("Received a none Ferari event of type {} at {}:\n\t{}", msg.getClass(),nodeID,((FerariRawDataItem)msg).getRawEvent());
					return;	
				}
				MetaMessage mMessage =((MetaMessage)((FerariEvent) msg).getEvent());
				if(MetaMessage.P_P_FUNCTIONALITY.equals(mMessage.getFunctionality())){
					DataTuple tuple = mMessage.getData();
					logger.debug("PushAndPullSpout: " + tuple.toString());
					queue.add(tuple);
				}
			}
		});
		
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(pushStream, new Fields("type", "Name", "attributes", "nodeId"));
	}
}
