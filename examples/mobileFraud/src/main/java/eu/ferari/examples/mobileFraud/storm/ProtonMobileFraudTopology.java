package eu.ferari.examples.mobileFraud.storm;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import com.ibm.ferari.coord.Coordinator;
import eu.ferari.backend.storm.Topology;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.examples.mobileFraud.Main;
import eu.ferari.examples.mobileFraud.proton.ProtonTopology;
import eu.ferari.examples.mobileFraud.proton.MissingInputConfigurationException;
import eu.ferari.examples.mobileFraud.storm.spouts.OptiSpout;
import eu.ferari.examples.mobileFraud.proton.ProtonTopologyBuilder;
import eu.ferari.examples.mobileFraud.storm.bolts.CoordinatorBolt;
import eu.ferari.examples.mobileFraud.storm.bolts.OptimizerBolt;
import eu.ferari.examples.mobileFraud.storm.bolts.RelayBolt;
import eu.ferari.examples.mobileFraud.storm.spouts.RedisPubSubSpout;

import java.io.IOException;

public class ProtonMobileFraudTopology {
	private static final String INPUTSPOUT_TO_ROUTING_PUSH_STREAM_ID  ="InputSpout_to_routing_push";
	private static final String INPUTSPOUT_TO_CSVFORMATTER_JSON_STREAM_ID  ="InputSpout_to_csvformatter_json";
	private static final String COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID = "Comspout_to_routing_properties";
	public static final String OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON = "Optimizerspout_to_optimizer_json";
	public static final String OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS = "Optimizerspout_to_optimizer_parameters";
	public  static final String OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_STREAM_ID = "optimizer_to_comspout_conffiles";
	public  static final String OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_NEWJSON_STREAM_ID = "optimizer_to_comspout_newjson";
	public  static final String OPTIMIZER_BOLT_TO_INPUTSPOUT_STREAM_ID = "optimizer_to_inputspout_conffiles";
	public static final String FILEREADERSPOUT_TO_ROUTING_BOLT_PRIM_EVENTS_STREAM_ID = "frs_to_routing_primitiveEvents";

	// new channels and names begin here:
	private static final String COMMUNICATOR_SPOUT_NAME = "communicator_spout";
	private static final String OPTIMIZER_SPOUT_NAME = "optimizer_spout";

	private static final String OPTIMIZER_BOLT_NAME = "optimizer";
	private static final String GM_COORDINATOR_BOLT_NAME="gmCoordinatinator";



	private static final String GM_CONFIG_STREAM = "GM_CONFIG_STREAM";
	private static final String GM_COM_STREAM = "GM_COMMUNICATOR_STREAM";
	private static final String GM_COORDINATOR_STREAM = "GM_COORDINATOR_STREAM";
	private static final String EVENT_CHANNEL_NAME="Event";
	private static final String COMMUNICATOR_SPOUT_TO_PUSH_PULL_BUFFER_STREAM="COMMUNICATOR_SPOUT_TO_PUSH_PULL_BUFFER_STREAM";
	private static final String COMMUNICATOR_SPOUT_TO_PUSH_PULL_COMMUNICATOR_STREAM ="COMMUNICATOR_SPOUT_TO_PUSH_PULL_COMMUNICATOR_STREAM";
	private static final String SPOUT_TO_RELAY_STREAM="relayConfStream";

	//TODO the following three variables are hardcoded into the Proton on Storm changes. Do not change them here.
	private static final String STATISTICS_CONF_STREAM = "Comspout_to_routing_properties";
	private static final String METADATA_TO_ROUTING_STREAM="metadatatoRouting";
	private static final String METADATA_TO_CONTEXT_STREAM="metadatatoContext";
	private static final String METADATA_TO_EPA_STREAM="metadatatoEpaManager";

	public static volatile boolean isInDevMode = true;


	private final String pathToProtonJSON;
	private boolean hasOptimizer;
	private final String nodeID;
	private final LocalCluster cluster = new LocalCluster();
	private boolean dashboard ;
	private	String	path;
	private boolean	fixdelay;
	private	int	delay;
	private String inputFileName;

	private final boolean local = true;

	public ProtonMobileFraudTopology(String protonFile, boolean hasOptimizer, String nodeID,
			boolean dashboard, String	path, boolean	fixdelay, int	delay, String inputFileName) throws IOException {
		this.nodeID = nodeID;
		this.pathToProtonJSON = protonFile;
		this.hasOptimizer = hasOptimizer;
		this.dashboard = dashboard;
		this.path = path;
		this.fixdelay = fixdelay;
		this.delay = delay;
		this.inputFileName = inputFileName;

		System.out.println(nodeID + " " + path + " " + inputFileName);
	}

	public void start(String topologyId) throws IOException {
		if (! nodeID.equals("cent1") && isInDevMode) {
			Coordinator coord = Main.createCoordinator(nodeID);
			coord.start();
		}

		System.err.println("Start invoked");

		System.out.println("Starting topology " + topologyId);
		createTopologyDescriptor(topologyId, new StormDescriptorConsumer() {

			@Override
			public void consume(StormTopology topology, Config stormConfig) {
				cluster.submitTopology("distributed-count-" + topologyId, stormConfig, topology);
			}
		}, "context");
	}

	public void createTopologyDescriptor(String topologyId, StormDescriptorConsumer consumer, String context) {
		try{
			ProtonTopology protontopology = new ProtonTopology();
			//		logger.info("Setting up topology "+ topologyId);
			Config stormConfig = new Config();
			stormConfig.setDebug(false);
			stormConfig.setMaxTaskParallelism(1);
			stormConfig.put(CommIntegrationConstants.EVENT_CONTEXT, context);

			TopologyBuilder builder = new TopologyBuilder();

			String protonRoutingBoltName= "protonRoutingBolt";

			if(local){
				stormConfig.put("mode", "local");
				stormConfig.put("homedir", "./src/main/resources/config/outputFiles/");
			}
			BaseRichSpout communicatorRedisSpout = new RedisPubSubSpout(
					COMMUNICATOR_SPOUT_TO_PUSH_PULL_BUFFER_STREAM,
					COMMUNICATOR_SPOUT_TO_PUSH_PULL_COMMUNICATOR_STREAM,
					GM_CONFIG_STREAM,
					GM_COM_STREAM,
					GM_COORDINATOR_STREAM,
					STATISTICS_CONF_STREAM,
					METADATA_TO_ROUTING_STREAM,
					METADATA_TO_EPA_STREAM,
					METADATA_TO_CONTEXT_STREAM,
					SPOUT_TO_RELAY_STREAM,
					nodeID
					);

			try {
				ProtonTopologyBuilder.
				buildProtonTopology(
						builder,
						pathToProtonJSON,
						protonRoutingBoltName,
						protontopology,
						INPUTSPOUT_TO_ROUTING_PUSH_STREAM_ID,
						COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID,
						FILEREADERSPOUT_TO_ROUTING_BOLT_PRIM_EVENTS_STREAM_ID,
						nodeID,
						inputFileName,
						delay,
						fixdelay,
						path
						);

			} catch (MissingInputConfigurationException e){
				System.err.println("Proton configuration is missing.");
			}

			builder.createTopology();



			builder.setSpout(COMMUNICATOR_SPOUT_NAME, communicatorRedisSpout, 1);
			String gMComName= Topology.extendTopology(builder,
					EVENT_CHANNEL_NAME, protonRoutingBoltName,
					GM_COM_STREAM,COMMUNICATOR_SPOUT_NAME,
					GM_CONFIG_STREAM,COMMUNICATOR_SPOUT_NAME);
			//TODO create output relay and set sender!

			//part of push/pull
			String pPCom=PushPullTopology.extendTopology(builder,dashboard,nodeID,COMMUNICATOR_SPOUT_NAME,
					COMMUNICATOR_SPOUT_TO_PUSH_PULL_BUFFER_STREAM,
					COMMUNICATOR_SPOUT_TO_PUSH_PULL_COMMUNICATOR_STREAM,protonRoutingBoltName);

			//proton config from spout.
			protontopology.getRoutingBoltDeclarer().allGrouping(COMMUNICATOR_SPOUT_NAME, COMMUNICATORSPOUT_TO_ROUTING_BOLT_PROPERTIES_STREAM_ID);
			protontopology.getRoutingBoltDeclarer().allGrouping(COMMUNICATOR_SPOUT_NAME, METADATA_TO_ROUTING_STREAM);
			protontopology.getContextBoltDeclarer().allGrouping(COMMUNICATOR_SPOUT_NAME, METADATA_TO_CONTEXT_STREAM);
			protontopology.getEpaManagerBoltDeclarer().allGrouping(COMMUNICATOR_SPOUT_NAME, METADATA_TO_EPA_STREAM);



			BoltDeclarer coordinatorDeclarer= builder.setBolt(GM_COORDINATOR_BOLT_NAME,new CoordinatorBolt(GM_CONFIG_STREAM));
			coordinatorDeclarer.fieldsGrouping(COMMUNICATOR_SPOUT_NAME,GM_COORDINATOR_STREAM, new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
			coordinatorDeclarer.allGrouping(COMMUNICATOR_SPOUT_NAME,GM_CONFIG_STREAM);

			BoltDeclarer relayDeclarer=builder.setBolt("relayBoltId",new RelayBolt(pPCom,gMComName,GM_COORDINATOR_BOLT_NAME,nodeID));
			relayDeclarer.allGrouping(pPCom);
			relayDeclarer.allGrouping(gMComName);
			relayDeclarer.allGrouping(GM_COORDINATOR_BOLT_NAME);
			relayDeclarer.allGrouping(COMMUNICATOR_SPOUT_NAME,SPOUT_TO_RELAY_STREAM);




			if(hasOptimizer && isInDevMode) {
				System.out.println("************** Creating optmizer *******************");
				BaseRichSpout optimizerSpout = new OptiSpout(OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON,
						OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS, nodeID);
				builder.setSpout(OPTIMIZER_SPOUT_NAME, optimizerSpout, 1);
				IRichBolt optimizer = new OptimizerBolt(OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_STREAM_ID,
						OPTIMIZER_BOLT_TO_COMMUNICATORSPOUT_NEWJSON_STREAM_ID,
						OPTIMIZER_BOLT_TO_INPUTSPOUT_STREAM_ID,
						OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON,
						OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS,
						inputFileName, path);
				BoltDeclarer optimizerBoltDeclarer = builder.setBolt(OPTIMIZER_BOLT_NAME, optimizer, 1);
				optimizerBoltDeclarer.allGrouping(OPTIMIZER_SPOUT_NAME, OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_JSON);
				optimizerBoltDeclarer.allGrouping(OPTIMIZER_SPOUT_NAME, OPTIMIZER_SPOUT_TO_OPTIMIZER_STREAM_ID_PARAMETERS);
			}
			//		logger.info("Submitting topology");
			String topologyName = "distributed-count-" + topologyId;
			if(!local){
				try {
					System.err.println("Running non locally");
					StormSubmitter.submitTopology(topologyName,stormConfig,builder.createTopology());
				} catch (AlreadyAliveException | InvalidTopologyException e) {
					e.printStackTrace();
				}
			}
			else{
				System.err.println("Running locally");
				stormConfig.put("mode", "local");
				consumer.consume(builder.createTopology(), stormConfig);
			}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void stop(){
		//		logger.info("Shutting down topology");
		cluster.shutdown();
	}

	public static interface StormDescriptorConsumer {
		public void consume(StormTopology topology, Config stormConfig);
	}
}
