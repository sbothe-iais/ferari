package eu.ferari.examples.mobileFraud.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ICoordinatorState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.misc.Event;
import eu.ferari.examples.mobileFraud.misc.PullRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mofu on 15/11/16.
 */
public class PushPullCommunicator implements ICoordinatorState{
	private Map<Long, PullRequest> pullRequests = new HashMap<>();
	private Map<String, Long> lastEventPerSite = new HashMap<String, Long>();
	private ISend sendToPushPullBuffer;
	private ISend sender;
	private static final Logger logger = LoggerFactory.getLogger(PushPullCommunicator.class);
	String nodeId;
	private static long pullRequestId = 1L;

	public PushPullCommunicator(String nodeId){
		this.nodeId=nodeId;
	}
	public void update(DataTuple input){
		logger.debug("Site id: {}; buffer input: {}",nodeId, input.toString());
		switch (input.getString(0)){
			case "Pull"://Message from Routing Bolt
				onPull_fromRouting(input);
				break;
			case "Pull2":
				onPull(input);
				break;
			case "Push":
				onPush(input);
				break;
			case "PushModeEvent":
				onpushmodeEvent_fromRouting(input);
				break;
		}
	}
	private void onPull_fromRouting(DataTuple input){
		//send Pull message to CommSpout
		DataTuple messagePull = new DataTuple("Pull", input.getValue(1), input.getValue(2), input.getValue(3), input.getValue(4));
		sender.signal(messagePull);
	}
	private void onpushmodeEvent_fromRouting(DataTuple input){
		//send Pull message to CommSpout
		String eventType = input.getString(1);
		String nodes = input.getString(2);
		String[] parts = nodes.split(",");
		List<String> nodeIds = new ArrayList<String>();
		for(int i=0; i<parts.length; i++){
			nodeIds.add(parts[i]);
		}
		List<String> pushEvents = new ArrayList<String>();
		pushEvents.add(eventType);

		//		for (String siteId : new String[]{"site1", "site2", "site3", "site4", "cent1"}) {
		//			Message messagePull = new Message(MessageType.Pull, pushEvents, 0, Long.MAX_VALUE, siteId);
		//			System.err.println("Sent to " + siteId + " : "  + messagePull);
		//			sender.signal(messagePull.toDataTuple(), siteId);
		//		}
		DataTuple pullMessage;
		for(int i=0; i<nodeIds.size(); i++){
			for (String siteId : new String[]{"site1", "site2", "site3", "site4", "cent1"}) {
				//Message messagePull = new Message(MessageType.Pull, pushEvents, 0, Long.MAX_VALUE, siteId);
				pullMessage = new DataTuple("Pull", pushEvents, 0L, Long.MAX_VALUE, nodeIds.get(i),siteId);
				logger.debug("Sent to {} : {}", siteId, pullMessage);
				sender.signal(pullMessage);
			}
		}
	}

	private void onPull(DataTuple input){
		List<String> pullEvents = (List<String>) input.getValue(1);
		long startingTime = input.getLong(2);
		long endingTime =  input.getValue(3) instanceof Double ? input.getDouble(3).longValue() : input.getLong(3);//message.getLong(3);
		String nodeId = input.getString(4);
		StringBuilder sb = new StringBuilder("\n"+"_________________________Pull request at Communicator_________________________ "+ this.nodeId);
		sb.append("\n");
		sb.append("Pull events: --->" + pullEvents);
		sb.append("\n");
		sb.append("StartingTime: ---> " + startingTime);
		sb.append("\n");
		sb.append("EndingTime: --->" + endingTime);
		sb.append("\n");
		sb.append("Receiver Node ID: --->" + nodeId);
		sb.append("\n");
		sb.append("_______________________________________________________________________________ "+"\n");
		System.out.println(sb);
		for(Map.Entry<Long, PullRequest> pr : pullRequests.entrySet()) {
			List<String> EventType = pr.getValue().getEventTypes();
			long st =  pr.getValue().getStartTime();
			long et =  pr.getValue().getEndTime();
			String ni= pr.getValue().getNodeID();
			if(EventType.equals(pullEvents) &&
					st == startingTime &&
					et == endingTime&&
					ni.equals(nodeId)){
				DataTuple pullRequestTuple=new DataTuple("PullRequest", pullEvents,
						startingTime, endingTime, pr.getKey());
				sendToPushPullBuffer.signal(pullRequestTuple);
				return;
			}
		}
		pullRequests.put(pullRequestId, new PullRequest(pullEvents, startingTime, endingTime, nodeId));
		DataTuple pullRequestTuple  = new DataTuple("PullRequest", pullEvents, startingTime, endingTime, pullRequestId);
		pullRequestId++;
		sendToPushPullBuffer.signal(pullRequestTuple);
	}


	private void onPush(DataTuple input){
		logger.info(nodeId +": onPush(" + input+ ")");
		long pullRequestId = input.getLong(2);
		List<Event> pushEvents= (List<Event>)input.getValue(1);
		//events from stream
		if(pullRequestId == 0L){
			for(Map.Entry<Long, PullRequest> pr : pullRequests.entrySet()) {
				PullRequest request = pr.getValue();
				List<Event> events = new ArrayList<Event>();
				events.addAll(pushEvents);
				if(request.getEventTypes().contains(events.get(0).getEvent())){
					if(request.getStartTime() <= events.get(0).getOccurrenceTime() && request.getEndTime() >= events.get(events.size()-1).getOccurrenceTime()) {
						if(!lastEventPerSite.containsKey(events.get(0).getEvent() + "_" + request.getNodeID())){
							lastEventPerSite.put(events.get(0).getEvent() + "_" + request.getNodeID(), events.get(0).getOccurrenceTime());
						}else{
							if(lastEventPerSite.get(events.get(0).getEvent() + "_" + request.getNodeID()) < events.get(0).getOccurrenceTime()){
								lastEventPerSite.put(events.get(0).getEvent() + "_" + request.getNodeID(), events.get(0).getOccurrenceTime());
							}
						}
						DataTuple m = new DataTuple("PushEvent", events.get(0).getEvent(), events.get(0).getAttributes(), request.getNodeID());
						if(!nodeId.equals( request.getNodeID())){
							try {
								logger.debug("onPush: {} sending to {}.", nodeId , request.getNodeID());
								sender.signal(m);
							} catch (Exception e) {
								logger.debug("@ {} ,CommState while trying to send push message!", nodeId);
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		//past events
		else{
			PullRequest request = pullRequests.get(pullRequestId);
			List<Event> events = new ArrayList<Event>();
			events.addAll(pushEvents);

			for(Event event : events) {
				if(!lastEventPerSite.containsKey(event.getEvent() + "_" + request.getNodeID())){
					lastEventPerSite.put(event.getEvent() + "_" + request.getNodeID(), event.getOccurrenceTime());
					DataTuple m = new DataTuple("PushEvent", event.getEvent(), event.getAttributes(), request.getNodeID());
					if(!nodeId.equals( request.getNodeID())){
						sender.signal(m);
					}
				}
				else{
					if(event.getOccurrenceTime() > lastEventPerSite.get(event.getEvent() + "_" + request.getNodeID())){
						lastEventPerSite.put(event.getEvent() + "_" + request.getNodeID(), event.getOccurrenceTime());
						DataTuple m = new DataTuple("PushEvent", event.getEvent(), event.getAttributes(), request.getNodeID());
						if(!nodeId.equals( request.getNodeID())){
							sender.signal(m);
						}
					}
				}
			}
		}
	}

	@Override
	public void setSender(ISend sender) {
		this.sender=sender;
	}
	public void setPushPullBufferSender(ISend sender){
		sendToPushPullBuffer=sender;
	}
}
