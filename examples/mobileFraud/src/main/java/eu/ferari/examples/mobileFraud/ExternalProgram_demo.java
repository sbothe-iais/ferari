package eu.ferari.examples.mobileFraud;


import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class ExternalProgram_demo {

	private static final int PORT = 4550;

	private ServerSocket socket;
	private PrintWriter sender;

	private BufferedReader readerJson;
	private BufferedReader readerParameters;
	private String parameters;
	private String json;
	private String pathParameters;
	private String pathJson;
	private String filePath;
	private static HashMap<String, String> properties ;
	private String backupPath ;

	public ExternalProgram_demo(int port){
		this(port,"/home/yacovm/FERARI_DEMO/conf.txt");
	}
	public ExternalProgram_demo(int port, String confPath) {
		properties = new HashMap<String, String>();
		String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String paths[] = path.split("/");
		paths[paths.length-1] = confPath;
		String newPath = "";
		for(int s=0;s<paths.length;s++)
		{
			newPath+=paths[s];
			if(s!=paths.length-1)
				newPath+="/";
		}


		String decodedPath = null;
		try {
			decodedPath   = confPath;
			//decodedPath = URLDecoder.decode(newPath, "UTF-8");
			FileInputStream fis = new FileInputStream(decodedPath);
			BufferedReader in = new BufferedReader(new InputStreamReader(fis));
			String line;
			while((line = in.readLine())!=null)
			{
				String[] parts = line.split("=");
				properties.put(parts[0], parts[1]);
			}
			parameters = properties.get("parameters");
			json = properties.get("json");
			filePath = properties.get("path");

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}



		this.pathParameters =  filePath + "/FERARI/Input_json_files/" + parameters;
		this.pathJson =  filePath + "/FERARI/Input_json_files/" + json;
		this.backupPath = filePath +  "/FERARI/Input_json_files/backupFiles/" + json;
		try {
			this.socket = new ServerSocket();
			this.socket.setReuseAddress(true);
			this.socket.bind(new InetSocketAddress(port));
		} catch (Exception e) {
			System.out.println("socket initialization : " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void run() {
		readerParameters = setReader(pathParameters, readerParameters);
		readerJson = setReader(pathJson, readerJson);
		String json;
		String networkParameters;
		String networkParameters_main = null;

		//read json file
		StringBuilder sb= new StringBuilder("j");
		String line="";
		while(line != null){
			try {
				line = readerJson.readLine();
				if(line !=null){
					sb.append(line);
				}
			} catch (Exception e) {
				System.out.println("read line : "+e.getMessage());
				e.printStackTrace();
			}
		}
		json = sb.toString();

		//read network parameters file
		StringBuilder sb1 = new StringBuilder("n");
		String linepr = "";
		while(linepr != null){
			try {
				linepr = readerParameters.readLine();
				if(linepr != null){
					sb1.append(linepr);
					networkParameters_main = linepr;
				}
			} catch (IOException e) {
				System.out.println("read line : "+e.getMessage());
				e.printStackTrace();
			}
		}
		networkParameters = sb1.toString();
		try {
			Socket socket = this.socket.accept();
			sender = new PrintWriter(socket.getOutputStream(), true);
			sender.println(networkParameters_main);
			System.out.println("External program sent network parameters to Main ");
			sender.flush();
			Thread.sleep(1000);
			sender.close();
			socket = this.socket.accept();
			sender = new PrintWriter(socket.getOutputStream(), true);
			//send json
			sender.println(json);
			System.out.println("External program sent Json to Optimizer Spout");
			Thread.sleep(500);
			//send network parameters
			sender.println(networkParameters);
			System.out.println("External program sent network parameters to Optimizer Spout");
			sender.close();
			//sender.flush();
			Thread.sleep(1000);
			socket.close();

		} catch (Exception  e) {
			System.out.println("socket : "+e.getMessage());
			if(sender != null)
				sender.close();
			e.printStackTrace();
		}
	}
	private BufferedReader setReader(String path, BufferedReader reader) {
		try {
			if(reader != null) {
				reader.close();
			}
			File file = new File(path);
			while(!file.exists()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){
				double bytes = file.length();
				if(bytes<10){
					file = new File (backupPath);
					if(!file.exists()){
						System.out.println("Json not found");
						System.exit(0);
					}
				}
			}
			reader = new BufferedReader(new FileReader(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return reader;
	}
	public static void main(String[] args) {
		System.out.println("Starting External Program");
		ExternalProgram_demo main;
		if((args.length>0)){
			main = new ExternalProgram_demo(PORT,args[0]);
		}
		else
			main = new ExternalProgram_demo(PORT);
		main.run();
	}
}
