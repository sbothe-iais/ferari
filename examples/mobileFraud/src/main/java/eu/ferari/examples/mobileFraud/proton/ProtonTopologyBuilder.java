package eu.ferari.examples.mobileFraud.proton;

import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import com.ibm.hrl.proton.agents.EPAManagerBolt;
import com.ibm.hrl.proton.context.ContextBolt;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.routing.RoutingBolt;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;
import eu.ferari.core.utils.csv.MissingCSVSectionException;
import eu.ferari.examples.mobileFraud.misc.Utils;
import eu.ferari.examples.mobileFraud.storm.spouts.PushAndPullSpout;
import json.java.JSON;
import json.java.JSONArray;
import json.java.JSONObject;

import java.io.IOException;

public class ProtonTopologyBuilder {

	private static final String INPUT_NAME = "protonInput";
	private static final String CONTEXT_BOLT_NAME = "protonContextBolt";
	private static final String EPA_MANAGER_BOLT_NAME = "protonEpaManagerBolt";
	private static final String PUSH_PULL_SPOUT_NAME ="PushPullSpout";

	public static TopologyBuilder buildProtonTopology(TopologyBuilder builder, 
			String pathToJson, 
			String ROUTING_BOLT_NAME, 
			ProtonTopology protontopology,
			String pushStream,
			String commToRoutingPropertiesStreamId,
			String FRSToRoutingPrimEventsSteamId,
			String nodeID,
			String inputFileName,
			int delay,
			boolean fixdelay,
			String path
			) throws MissingInputConfigurationException{

		BaseRichSpout inputSpout = null;
		BaseRichSpout pushPullSpout = null;

		EepFacade eep = null;
		STORMMetadataFacade facade = null;
		
		if(pathToJson == null) {
			inputSpout = new FileReaderSpout(null, nodeID,
					inputFileName, path, fixdelay, delay, FRSToRoutingPrimEventsSteamId);
			pushPullSpout = new PushAndPullSpout(nodeID, ROUTING_BOLT_NAME, pushStream);
		}
		else {
			String jsonString = Utils.createInputContents(pathToJson);

			JSONObject parsed = null;
			try {
				parsed = ((JSONObject) JSON.parse(jsonString));
			} catch (NullPointerException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			JSONObject root = (JSONObject) parsed.get("epn");
			JSONArray events = (JSONArray) root.get("events");
			JSONArray producers = (JSONArray) root.get("producers");

			//TODO allow multiple file inputs.
			for(Object obj : producers){
				if(!((JSONObject) obj).get("type").equals("File")) continue;
				try{
					JSONArray properties = (JSONArray) ((JSONObject)obj).get("properties");
					inputSpout = new FileReaderSpout(properties, nodeID, inputFileName,
							path, fixdelay, delay, FRSToRoutingPrimEventsSteamId);
					pushPullSpout = new PushAndPullSpout(nodeID, ROUTING_BOLT_NAME, pushStream);
				}catch(NoFilenameParameterException e){
					continue;
				}
				catch(MissingCSVSectionException e){
					inputSpout = null;
					continue;
				}
			}
			if(inputSpout == null || pushPullSpout == null) throw new MissingInputConfigurationException();
			
			try {
				eep = new EepFacade();
				facade = new STORMMetadataFacade(jsonString,eep);
			} catch (ParsingException | EEPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		FacadesManager facadesManager = new FacadesManager();
		facadesManager.setEepFacade(eep);

		TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
		facadesManager.setTimerServiceFacade(timerServiceFacade);
		WorkManagerFacade workManagerFacade = new WorkManagerFacade();
		facadesManager.setWorkManager(workManagerFacade);  
		RoutingBolt routingBolt;
		ContextBolt contextBolt;
		EPAManagerBolt epaManagerBolt;
		
		builder.setSpout(INPUT_NAME, inputSpout);
		builder.setSpout(PUSH_PULL_SPOUT_NAME, pushPullSpout);
		BoltDeclarer routingBoltDeclarer = null;
		routingBolt = new RoutingBolt(facadesManager, facade, commToRoutingPropertiesStreamId, FRSToRoutingPrimEventsSteamId, nodeID, path);
		routingBoltDeclarer = builder.setBolt(ROUTING_BOLT_NAME, routingBolt);
		routingBoltDeclarer.shuffleGrouping(EPA_MANAGER_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM).
		directGrouping(PUSH_PULL_SPOUT_NAME, pushStream).shuffleGrouping(INPUT_NAME, FRSToRoutingPrimEventsSteamId);
		contextBolt = new ContextBolt(facadesManager,facade);
		BoltDeclarer contextBoltDeclarer = builder.setBolt(CONTEXT_BOLT_NAME, contextBolt);
		contextBoltDeclarer.fieldsGrouping(ROUTING_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM,
				new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,STORMMetadataFacade.CONTEXT_NAME_FIELD));
		epaManagerBolt = new EPAManagerBolt(facadesManager,facade);
		BoltDeclarer epaManagerBoltDeclarer = builder.setBolt(EPA_MANAGER_BOLT_NAME, epaManagerBolt);
		epaManagerBoltDeclarer.fieldsGrouping(CONTEXT_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM,
				new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,STORMMetadataFacade.CONTEXT_PARTITION_FIELD));
		//TODO create some stuff for flexible output.

		protontopology.setContextBolt(contextBolt);
		protontopology.setContextBoltDeclarer(contextBoltDeclarer);
		protontopology.setEpaManagerBolt(epaManagerBolt);
		protontopology.setEpaManagerBoltDeclarer(epaManagerBoltDeclarer);
		protontopology.setRoutingBolt(routingBolt);
		protontopology.setRoutingBoltDeclarer(routingBoltDeclarer);

		return builder;
	}
}
