package eu.ferari.examples.mobileFraud.misc;

import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.InterSiteClient;
import eu.ferari.core.DataTuple;

/**
 * Created by mofu on 17/11/16.
 */
public class CommSender {	private final InterSiteClient commClient;
	private String context;

	public CommSender(String siteId, boolean connect2Optimizer) {
		this(siteId, connect2Optimizer, null);
	}

	public CommSender(String siteId, boolean connect2Optimizer, String context) {
		this.context = context;
		commClient = FerariClientBuilder.getInstance().createInterSiteClient(siteId, connect2Optimizer);
	}

	public void signal(String functionality, DataTuple data, String siteId) {
		MetaMessage metaMessage = new MetaMessage(functionality, data,context);
		commClient.sendMsg(metaMessage, siteId);
	}

	public int getTaskId() {
		return 0;
	}

	public void close() {
		commClient.close();
	}
}
