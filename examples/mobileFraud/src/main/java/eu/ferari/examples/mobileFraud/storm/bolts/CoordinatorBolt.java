package eu.ferari.examples.mobileFraud.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import com.google.gson.JsonParser;
import eu.ferari.backend.storm.sender.SimpleSender;
import eu.ferari.core.DataTuple;
import eu.ferari.core.state.CoordinatorState;

import java.util.Map;

/**
 * Created by mofu on 21/11/16.
 */
public class CoordinatorBolt extends BaseRichBolt{
	CoordinatorState state;
	String confStream;

	public CoordinatorBolt(String confStream){
		state= new CoordinatorState();
		this.confStream=confStream;
	}
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		state.setSender(new SimpleSender(collector));

	}

	@Override
	public void execute(Tuple input) {
		if(confStream.equals(input.getSourceStreamId())) {
			JsonParser parser = new JsonParser();
			state.updateConfig(parser.parse(input.getString(0)));
		}
		else
			state.update((DataTuple)input.getValue(0));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("Tuple"));
	}
}
