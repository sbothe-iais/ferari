package eu.ferari.examples.mobileFraud.misc;

import java.util.List;

/**
 * Created by vmanikaki.
 */
public class PullRequest {
	private List<String> eventTypes;
	private long startTime;
	private long endTime;
	private String nodeID;
	
	public PullRequest(List<String> eventTypes, long startTime, long endTime, String nodeID) {
		super();
		this.eventTypes = eventTypes;
		this.startTime = startTime;
		this.endTime = endTime;
		this.nodeID = nodeID;
	}
	
	public List<String> getEventTypes() {
		return eventTypes;
	}
	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public String getNodeID() {
		return nodeID;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
}
