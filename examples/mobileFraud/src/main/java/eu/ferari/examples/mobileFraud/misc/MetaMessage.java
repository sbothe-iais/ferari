package eu.ferari.examples.mobileFraud.misc;

import eu.ferari.core.DataTuple;

import java.io.Serializable;

/**
 * Created by mofu on 17/11/16.
 */
public class MetaMessage implements Serializable{
	public static final String GM_FUNCTIONALITY= "GeometricMonitoringPart";
	public static final String P_P_FUNCTIONALITY= "PushPullPart";
	public static final String GM_COORDINATOR="GeometricMonitoringCoordinator";
	public static final String GM_Config="GeometricMonitoringConfiguration";
	public static final String PROTON_CONFIG="ProtonConfiguration";
	public static final String PUSH_PULL_CONFIG="Push/PullConfiguration";
	private String functionality;
	private DataTuple data;
	private String sourceId;

	public String getSourceId() {
		return sourceId;
	}

	public MetaMessage(String functionality, DataTuple data, String sourceId) {
		this.functionality = functionality;
		this.data=data;
		this.sourceId=sourceId;

	}

	public String getFunctionality() {
		return functionality;
	}

	public DataTuple getData() {
		return data;
	}
}
