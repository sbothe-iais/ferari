package eu.ferari.examples.mobileFraud.proton;

import org.apache.storm.topology.BoltDeclarer;

import com.ibm.hrl.proton.agents.EPAManagerBolt;
import com.ibm.hrl.proton.context.ContextBolt;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.routing.RoutingBolt;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;

import eu.ferari.examples.mobileFraud.misc.Utils;

public class ProtonTopology {
	private RoutingBolt routingBolt;
	private ContextBolt contextBolt;
	private EPAManagerBolt epaManagerBolt;
	private BoltDeclarer routingBoltDeclarer;
	private BoltDeclarer contextBoltDeclarer;
	private BoltDeclarer epaManagerBoltDeclarer;

	public ProtonTopology(RoutingBolt routingBolt, ContextBolt contextBolt,
			EPAManagerBolt epaManagerBolt, BoltDeclarer routingBoltDeclarer,
			BoltDeclarer contextBoltDeclarer,
			BoltDeclarer epaManagerBoltDeclarer) {
		this.routingBolt = routingBolt;
		this.contextBolt = contextBolt;
		this.epaManagerBolt = epaManagerBolt;
		this.routingBoltDeclarer = routingBoltDeclarer;
		this.contextBoltDeclarer = contextBoltDeclarer;
		this.epaManagerBoltDeclarer = epaManagerBoltDeclarer;
	}

	public ProtonTopology() {}

	public RoutingBolt getRoutingBolt() {
		return routingBolt;
	}

	public ContextBolt getContextBolt() {
		return contextBolt;
	}

	public EPAManagerBolt getEpaManagerBolt() {
		return epaManagerBolt;
	}

	public BoltDeclarer getRoutingBoltDeclarer() {
		return routingBoltDeclarer;
	}

	public BoltDeclarer getContextBoltDeclarer() {
		return contextBoltDeclarer;
	}

	public BoltDeclarer getEpaManagerBoltDeclarer() {
		return epaManagerBoltDeclarer;
	}

	public void setRoutingBolt(RoutingBolt routingBolt) {
		this.routingBolt = routingBolt;
	}

	public void setContextBolt(ContextBolt contextBolt) {
		this.contextBolt = contextBolt;
	}

	public void setEpaManagerBolt(EPAManagerBolt epaManagerBolt) {
		this.epaManagerBolt = epaManagerBolt;
	}

	public void setRoutingBoltDeclarer(BoltDeclarer routingBoltDeclarer) {
		this.routingBoltDeclarer = routingBoltDeclarer;
	}

	public void setContextBoltDeclarer(BoltDeclarer contextBoltDeclarer) {
		this.contextBoltDeclarer = contextBoltDeclarer;
	}

	public void setEpaManagerBoltDeclarer(BoltDeclarer epaManagerBoltDeclarer) {
		this.epaManagerBoltDeclarer = epaManagerBoltDeclarer;
	}

	public void resetEPN(String pathToJson) throws MissingInputConfigurationException
	{
		String jsonString = Utils.createInputContents(pathToJson);
		EepFacade eep=null;
		STORMMetadataFacade facade=null;
		try {
			eep = new EepFacade();
			facade = new STORMMetadataFacade(jsonString,eep);
		} catch (ParsingException | EEPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacadesManager facadesManager = new FacadesManager();
		facadesManager.setEepFacade(eep);

		TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
		facadesManager.setTimerServiceFacade(timerServiceFacade);
		WorkManagerFacade workManagerFacade = new WorkManagerFacade();
		facadesManager.setWorkManager(workManagerFacade);  

		routingBolt.setFacadesManager(facadesManager);
		routingBolt.setMetadataFacade(facade);
		routingBolt.prepare();
		epaManagerBolt.setFacadesManager(facadesManager);
		epaManagerBolt.setMetadataFacade(facade);
		epaManagerBolt.prepare();
		contextBolt.setFacadesManager(facadesManager);
		contextBolt.setMetadataFacade(facade);
		contextBolt.prepare();
	}
}
