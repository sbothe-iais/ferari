package eu.ferari.examples.mobileFraud.storm;

import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import eu.ferari.examples.mobileFraud.storm.bolts.PushPullBufferBolt;
import eu.ferari.examples.mobileFraud.storm.bolts.PushPullCommunicatorBolt;

/**
 * Created by mofu on 15/11/16.
 */
public class PushPullTopology {
	private final static String PUSH_PULL_BUFFER_NAME = "PUSH_PULL_BUFFER";
	private final static String PUSH_PULL_COM_NAME = "PUSH_PULL_COMMUNICATOR";
	private final static String PUSH_PULL_COM_TO_BUFFER_STREAM = "PUSH_PULL_COM_TO_BUFFER_STREAM";
	public static String extendTopology(TopologyBuilder builder,
									  boolean sendToDashboard,
									  String nodeId,
									  String communicatorSource,
									  String communicatorToBufferStream,
									  String communicatorSourceToCommunicatorStream,
									  String protonRoutingBoltName){
		BoltDeclarer bufferDeclarer=builder.setBolt(PUSH_PULL_BUFFER_NAME, new PushPullBufferBolt(sendToDashboard,nodeId));
		BoltDeclarer communicatorDeclarer= builder.setBolt(PUSH_PULL_COM_NAME, new PushPullCommunicatorBolt(PUSH_PULL_COM_TO_BUFFER_STREAM, nodeId));
		bufferDeclarer.allGrouping(communicatorSource,communicatorToBufferStream);
		bufferDeclarer.allGrouping(PUSH_PULL_COM_NAME,PUSH_PULL_COM_TO_BUFFER_STREAM);
		bufferDeclarer.allGrouping(protonRoutingBoltName, "Statistics");
		bufferDeclarer.allGrouping(protonRoutingBoltName, "Statistic");
		communicatorDeclarer.allGrouping(protonRoutingBoltName, "Pull");
		bufferDeclarer.allGrouping(protonRoutingBoltName, "Event");
		communicatorDeclarer.allGrouping(protonRoutingBoltName, "PushModeEvent");
		communicatorDeclarer.allGrouping(communicatorSource,communicatorSourceToCommunicatorStream);
		communicatorDeclarer.allGrouping(PUSH_PULL_BUFFER_NAME);
		return PUSH_PULL_COM_NAME;
	}

	public static String getCommunicatorName(){
		return PUSH_PULL_COM_NAME;
	}
}
