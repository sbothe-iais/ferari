package eu.ferari.examples.mobileFraud.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import eu.ferari.core.DataTuple;
import eu.ferari.examples.mobileFraud.misc.CommSender;
import eu.ferari.examples.mobileFraud.misc.MetaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by mofu on 17/11/16.
 */
public class RelayBolt extends BaseRichBolt{

	private String pushPullCommunicator;
	private String geometricMonitoringCommunicator;
	private String geometricMonitoringCoordinator;
	private CommSender sender;
	private String nodeId;
	private String coordinatorId;
	private OutputCollector collector;
	private static final Logger logger= LoggerFactory.getLogger(RelayBolt.class);


	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		sender=new CommSender(nodeId, true);
		this.collector=collector;
	}

	@Override
	public void execute(Tuple input) {
		logger.debug("Received input from "+input.getSourceComponent());
		if(input.getSourceComponent().equals(pushPullCommunicator)){
			sender.signal(MetaMessage.P_P_FUNCTIONALITY,(DataTuple)input.getValue(0),
					((DataTuple)input.getValue(0)).getString(((DataTuple)input.getValue(0)).getDataLength()-1));

		}else if(input.getSourceComponent().equals(geometricMonitoringCommunicator)) {
			DataTuple tuple=(DataTuple)input.getValue(0);
			tuple.add(nodeId);
			sender.signal(MetaMessage.GM_FUNCTIONALITY,tuple ,coordinatorId );
		}else if(input.getSourceComponent().equals(geometricMonitoringCoordinator)){
			sender.signal(MetaMessage.GM_COORDINATOR,(DataTuple)input.getValue(0),
					((DataTuple)input.getValue(0)).getString(((DataTuple)input.getValue(0)).getDataLength()-1));
		}
		else{
			coordinatorId=input.getString(0);
		}
		collector.ack(input);

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}

	public RelayBolt(String pushPullCommunicator, String geometricMonitoringCommunicator, String geometricMonitoringCoordinator, String nodeId) {
		this.pushPullCommunicator = pushPullCommunicator;
		this.geometricMonitoringCommunicator = geometricMonitoringCommunicator;
		this.geometricMonitoringCoordinator= geometricMonitoringCoordinator;
		this.nodeId=nodeId;
	}
}
