package eu.ferari.examples.mobileFraud.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.coord.EventConsumer;
import com.ibm.ferari.coord.Optimizer;
import com.ibm.ferari.util.Util;
import eu.ferari.core.DataTuple;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.optimizer.configuration.EPNParser;
import eu.ferari.optimizer.locationGraph.GraphPainter;
import eu.ferari.optimizer.locationGraph.LGraphNode;
import eu.ferari.optimizer.locationGraph.LocationGraph;
import eu.ferari.optimizer.locationGraph.NetworkParser;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class OptimizerBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;

	public static Optimizer optimizer;

	private final String OptimizerToComspoutConffilesStreamId;
	private final String OptimizerToComspoutNewJsonStreamId;
	private final String OptimizerToInputspoutConffilesStreamId;
	private final String OptimizerSpoutToOptimizerJsonStreamId;
	private final String OptimizerSpoutToOptimizerParametersStreamId;

	public static boolean shouldStartOptimizer = true;


	private boolean receivedjson = false;
	private boolean receivedparameters = false;

	private String _parameters = null;
	private String _json = null;
	private EPNParser epn;
	private LocationGraph lg1 = null;

	private Map _stormConf;

	private Optimizer _opt;

	private String inputDataFileName;
	private String path;
	private static Logger logger = LoggerFactory.getLogger(OptimizerBolt.class);

	public OptimizerBolt(String optimizerToComspoutConffilesStreamId, String OptimizerToComspoutNewJsonStreamId,
						 String optimizerToInputspoutConffilesStreamId, String OptimizerSpoutToOptimizerJsonStreamId,
						 String OptimizerSpoutToOptimizerParametersStreamId, String inputDataFileName, String path) {
		super();
		this.OptimizerToComspoutConffilesStreamId = optimizerToComspoutConffilesStreamId;
		this.OptimizerToComspoutNewJsonStreamId = OptimizerToComspoutNewJsonStreamId;
		this.OptimizerToInputspoutConffilesStreamId = optimizerToInputspoutConffilesStreamId;
		this.OptimizerSpoutToOptimizerJsonStreamId = OptimizerSpoutToOptimizerJsonStreamId;
		this.OptimizerSpoutToOptimizerParametersStreamId = OptimizerSpoutToOptimizerParametersStreamId;
		this.inputDataFileName = inputDataFileName;
		this.path = path;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this._stormConf = stormConf;
		if (shouldStartOptimizer) {
			int port = Integer
					.parseInt(InMemoryGlobalAddressResolver.getInstance().getOptimizerHosts()[0].split(":")[1]);
			_opt = new Optimizer(CommIntegrationConstants.OPT_SITE_ID, CommIntegrationConstants.OPT_SITE_ID, port,
					new EventConsumer() {

						@Override
						public void consumeEvent(FerariEvent event) {
							logger.trace(event.toString());
						}
					});
			_opt.start();
		} else {
			_opt = optimizer;
		}
	}

	@Override
	public void execute(Tuple input) {

		if (input.getSourceStreamId().equals(OptimizerSpoutToOptimizerJsonStreamId)) {
			System.err.println("Optimizer received Json from Optimizer Spout");
			_json = input.getString(0);
			receivedjson = true;
		}
		if (input.getSourceStreamId().equals(OptimizerSpoutToOptimizerParametersStreamId)) {
			System.err.println("Optimizer received Network Parameters from Optimizer Spout");
			_parameters = input.getString(0);
			receivedparameters = true;
		}
		if (receivedjson && receivedparameters) {
			receivedjson = false;
			receivedparameters = false;
			NetworkParser np = new NetworkParser(null, ";");

			lg1 = np.parseFile(_parameters);
			epn = new EPNParser(null, lg1);
			System.err.println("Parsing EPN...");
			epn.parseEPN(_json);
			try {
				epn.runDPandAssignPlans(path, inputDataFileName, false, 1);
			} catch (JSONException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			return;
		}
		String centralNode = lg1.centralNode().location;

		Map<String, Serializable> topologyCustomConfig = new HashMap<String, Serializable>();

		for (LGraphNode n : lg1.nodes) {
			String json;
			String routingBoltPropertiesFile ="pull=true\nstatisticsClass=statistics.CountStatistics\n\nepoch=10\n\neType=events\njarLocation=null\nstartingTime=0\nendingTime=1700000000000";
			if (!n.equals(centralNode)) {
				routingBoltPropertiesFile = routingBoltPropertiesFile+"\npushEvent=Play,CounterEvent,SteppedCounterEvent\npushEventReciever=site1";
			}
			json = epn.jsonFiles.get(n.location);




			topologyCustomConfig.put(n.location, new DataTuple(
					centralNode,
					json,routingBoltPropertiesFile,
					epn.getMonitoringAndLearningConfig().toString()));
		}

		sendTopology(topologyCustomConfig);

		///////////////// GRAPH VISUALIZATION CODE////////////////
		GraphPainter gf = null;
		try {
			gf = new GraphPainter(lg1, path);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gf.paintGraph();
	}

	private void sendTopology(Map<String, Serializable> customConfigMap) {
		Util.runAsyncTask(new Runnable() {

			@Override
			public void run() {
				Util.sleep(20);
				System.err.println("******** Sending topology ****************");
				_opt.setTopologyGraph(new HashMap<>(), customConfigMap);
			}
		});

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(OptimizerToComspoutConffilesStreamId,
				new Fields("type", "routingBoltProperties", "geometricProperties", "routingProperties", "nodeID"));
		declarer.declareStream(OptimizerToComspoutNewJsonStreamId, new Fields("type", "newJsonFile", "unused"));
		declarer.declareStream(OptimizerToInputspoutConffilesStreamId,
				new Fields("type", "routingBoltProperties", "geometricProperties", "routingProperties", "nodeID"));
	}

	@Override
	public void cleanup() {
		super.cleanup();
	}
}