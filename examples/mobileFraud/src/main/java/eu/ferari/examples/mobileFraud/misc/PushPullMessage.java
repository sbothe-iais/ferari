package eu.ferari.examples.mobileFraud.misc;

import eu.ferari.core.misc.Event;
import statistics.Statistics;

import java.util.List;
import java.util.Map;

/**
 * Created by mofu on 15/11/16.
 */
public class PushPullMessage {
	public enum Type{
		Statistics,
		Pull,
		Pull2,
		PullRequest,
		PushModeEvent,
		Event,
		PushEvent,
		Push,
	}


	private Type type;

	private Event event;
	private List<String> pullEvents;
	private List<Event> pushEvents;
	private Map<String, List<Statistics>> statistics;
	private Map<String,Object>attributes;
	private long WindowDuration;
	private long eventOccurrenceTime;
	private  long startingTime;
	private long endingTime;
	private int pullRequestId;
}
