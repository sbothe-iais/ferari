package eu.ferari.examples.mobileFraud.proton;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import com.ibm.ferari.FerariRawDataItem;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.TopologyRequestHandler;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.event.EventHeader;
import com.ibm.hrl.proton.metadata.event.IEventType;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.metadata.type.TypeAttribute;
import com.ibm.hrl.proton.metadata.type.enums.AttributeTypesEnum;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.runtime.metadata.IMetadataFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;
import eu.ferari.core.DataTuple;
import eu.ferari.core.commIntegration.InMemoryGlobalAddressResolver;
import eu.ferari.core.utils.csv.MissingCSVSectionException;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;
import json.java.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class FileReaderSpout extends BaseRichSpout {

	static {
		FerariClientBuilder.getInstance().setAddressResolver(InMemoryGlobalAddressResolver.getInstance());
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String path = null;
	private SpoutOutputCollector _collector;
	private Map _stormConf;
	private BufferedReader reader;
	private boolean done = false;
	private String event;
	private String nodeID;

	private LinkedBlockingQueue<Serializable> queue;
	private boolean isfirst = true;
	private String inputFileName;
	private String inputFilePath;
	private double prevTimestamp = 0;

	private boolean isReader;

	public final static String _DEFAULT_DATE_FORMAT =  "dd/MM/yyyy-HH:mm:ss";
	private static final Long SENDING_DELAY = new Long(2000);
	private static final String DELIMETER = ";";
	private static final String TAG_SEPARATOR = "=";
	private static final String NULL_STRING = "null";
	private static final String dateFormat = "dd/MM/yyyy-HH:mm:ss";
	private IMetadataFacade metadataFacade;
	private EepFacade eep;
	private DateFormat dateFormatter;
	private boolean  fixdelay;
	private int delay;
	private final String primitiveEventsToRoutingBoltStreamId;
	private IntraSiteClient intraSiteClient;

	private boolean gotTopology = false;

	private static final Logger logger = LoggerFactory.getLogger(FileReaderSpout.class);

	public FileReaderSpout(JSONArray array, String nodeID,
						   String inputFileName, String inputFilePath,
						   boolean  fixdelay, int delay, String primitiveEventsToRoutingBoltStreamId){
		this(array,nodeID, inputFileName, inputFilePath, fixdelay, delay, primitiveEventsToRoutingBoltStreamId, ProtonMobileFraudTopology.isInDevMode);
	}

	/**
	 * Creates a file reader spout from the configuration stored in a JSON Array.
	 * @param array Must contain an element with "name" = "filename"  and "value" = PATH_TO_FILE
	 */
	public FileReaderSpout(JSONArray array, String nodeID,
						   String inputFileName, String inputFilePath,
						   boolean  fixdelay, int delay, String primitiveEventsToRoutingBoltStreamId, boolean isReader){
		this.nodeID = nodeID;
		this.inputFileName = inputFileName;
		this.inputFilePath = inputFilePath;
		this.fixdelay = fixdelay;
		this.delay = delay;
		this.primitiveEventsToRoutingBoltStreamId = primitiveEventsToRoutingBoltStreamId;
		this.isReader = isReader;
	}

	@Override
	public void nextTuple() {
		Serializable serializable = queue.poll();
		if (serializable != null && (serializable instanceof Values)) {
			//System.out.println("Got values:" + ((Values)serializable));
			_collector.emit(primitiveEventsToRoutingBoltStreamId, (Values) serializable);
			return;
		}
		try {
			if (serializable != null && serializable instanceof DataTuple) {
				DataTuple tuple= (DataTuple)serializable;
				if(tuple.getString(tuple.getDataLength()-1).equals("NewJsonFile")) {
				/*not needed because config is gotten per nodeId from the topology request handler.
				if(!nodeID.equals(tuple.getString(0))) {
					return;
				}
				*/
					buildMetadataFacade(tuple.getString(1));
					try{
						getPath();
						setReader();
					}catch(NoFilenameParameterException e){
						e.printStackTrace();
					}
					catch(MissingCSVSectionException e){
						e.printStackTrace();
					}
				} 
				else {
					throw new IllegalArgumentException("Don't know how to handle message "+tuple.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		if(done || path == null) return;
		String line="";
		try {
			line = reader.readLine();
			if(line!=null){
				event = line;
			}
		}catch (IOException e) {
			// TODO Another exception.
			e.printStackTrace();
		}
		Values values = new Values();
		values.add(event); 
		if(isfirst){
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			isfirst = false;
		}
		if(line!=null) {
			Pair<Values, Long> event = null;
			while (event ==null) {
				try {
					event = nextEvent(line);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			double timestamp = 0;
			Map<String, Object> v =	(Map<String, Object>)event.getFirstValue().get(1);
			if(v.get("OccurrenceTime") == null){
				if(v.get("call_start_date") == null){
					System.err.println("Could not get timestamp from Event");
					return;
				}
				else
				{
					long tml = (long)v.get("call_start_date");
					timestamp = tml;
					v.put("OccurrenceTime", timestamp);
				}
			}
			else{
				timestamp = (long) v.get("OccurrenceTime");
			}

			double delayMillis = prevTimestamp > 0 ? timestamp - prevTimestamp : 0;
			prevTimestamp = delayMillis >=0 ? timestamp : prevTimestamp;
			if(fixdelay){
				try {
					Thread.sleep((long) (delay));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else{
				try {
					if(delayMillis > 0)
						Thread.sleep((long) (delayMillis));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			String eventName = (String) v.get("Name");
			if (! ProtonMobileFraudTopology.isInDevMode) {
				FerariRawDataItem frdi = new FerariRawDataItem();
				frdi.setRawEvent(new Values(eventName, event.getFirstValue().get(1)));
				//System.out.println("Broadcasting in our site:" + frdi);
				intraSiteClient.broadcastInOurSite(frdi);
				return;
			}
			logger.debug("emitting event of type {}",eventName);
			_collector.emit(primitiveEventsToRoutingBoltStreamId, new Values(eventName, event.getFirstValue().get(1)));
		}
		else{ 
			done = true;
		}
	}

	public void buildMetadataFacade(String jsonfileName)
			throws ParsingException, EEPException {
		this.eep = new EepFacade();
		this.metadataFacade = new STORMMetadataFacade(jsonfileName,
				this.eep).getMetadataFacade();
		this.dateFormatter = new SimpleDateFormat(dateFormat);
	}

	private Pair<Values, Long> nextEvent(String line) throws Exception {
		if (line != null) {
			String eventLine = new String(line.getBytes(Charset
					.forName("UTF-8")), "UTF-8");
			Pair<Values, Long> pair = fromBytes(eventLine);
			return pair;
		} else {
			return null;
		}
	}

	public Pair<Values, Long> fromBytes(String eventText)
			throws ParsingException {

		int nameIndex = eventText.indexOf(EventHeader.NAME_ATTRIBUTE);
		String substring = eventText.substring(nameIndex);
		int delimiterIndex = substring.indexOf(DELIMETER);
		int tagDataSeparatorIndex = substring.indexOf(TAG_SEPARATOR);
		String nameValue = substring.substring(tagDataSeparatorIndex + 1,
				delimiterIndex);
		IEventType eventType = metadataFacade.getEventMetadataFacade()
				.getEventType(nameValue);
		Map<String, Object> attrValues = new HashMap<String, Object>();
		String[] tagValuePairs = eventText.split(DELIMETER);

		for (String tagValue : tagValuePairs) {
			String[] separatedPair = tagValue.split(TAG_SEPARATOR);
			if(separatedPair.length<2)
				continue;
			String attrName = separatedPair[0].trim(); // the tag is always the
			// first in the pair
			String attrStringValue = separatedPair[1].trim();

			if (attrName.equals(NULL_STRING) || attrName.equals("")) {
				throw new ParsingException("Could not parse the event string "
						+ eventText + ", reason: Attribute name not specified");
			}

			if (attrStringValue.equals(NULL_STRING)
					|| attrStringValue.equals("")) {
				attrValues.put(attrName, null);
				continue;
			}

			TypeAttribute eventTypeAttribute = eventType.getTypeAttributeSet()
					.getAttribute(attrName);
			attrStringValue = getAttributeStringValue(eventTypeAttribute,
					attrStringValue);
			Object attrValueObject;

			try {
				attrValueObject = TypeAttribute.parseConstantValue(
						attrStringValue, attrName, eventType, dateFormatter,
						this.eep);
			} catch (Exception e) {
				throw new ParsingException("Could not parse the event string"
						+ eventText + ", reason: " + e.getMessage());
			}
			attrValues.put(attrName, attrValueObject);
		}

		logger.debug("TestProtonSpout: fromBytes: injecting event " + nameValue
				+ "with attributes" + attrValues);
		return new Pair<Values, Long>(new Values(nameValue, attrValues),
				SENDING_DELAY);
	}

	private String getAttributeStringValue(TypeAttribute eventTypeAttribute,
			String attrStringValue) {
		AttributeTypesEnum attrType = eventTypeAttribute.getTypeEnum();
		if (attrType.equals(AttributeTypesEnum.STRING)
				|| eventTypeAttribute.getDimension() > 0) {
			return "'" + attrStringValue + "'";
		}
		return attrStringValue;
	}

	private boolean getPath() {
		path = inputFilePath + "FERARI/Topology_input_files/" + nodeID + "_" + inputFileName;
		return true;
	}

	private void setReader() {
		try {
			if(reader != null) {
				reader.close();
			}
			if(_stormConf.get("mode").equals("local")){
				reader = new BufferedReader(new FileReader(new File(path)));
			}
			else{
				/*
				FileSystem hdfs = null;
				try {	
					Configuration hadconf = new Configuration();
					hadconf.set("fs.defaultFS", "hdfs://clu01.softnet.tuc.gr:8020");
					hadconf.set("fs.hdfs.impl",org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
					hadconf.set("fs.file.impl",org.apache.hadoop.fs.LocalFileSystem.class.getName());
					hadconf.addResource(new Path("/etc/hadoop/conf.empty/core-site.xml"));
					hadconf.addResource(new Path("/etc/hadoop/conf.empty/hdfs-site.xml"));
					hdfs = FileSystem.get(hadconf);           
				} catch (IOException e) {
					e.printStackTrace();
				} 
				Path hdfsFilePath = new Path(path);
				reader = new BufferedReader(new InputStreamReader(hdfs.open(hdfsFilePath)));
				*/
			}
			done = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void open(Map stormConf, TopologyContext context, SpoutOutputCollector collector) {
		queue = new LinkedBlockingQueue<Serializable>();
		intraSiteClient = FerariClientBuilder.getInstance().createIntraSiteClient(nodeID);
		_collector = collector;
		_stormConf = stormConf;

		if (! isReader) {
			intraSiteClient.setbroadcastRequestHandler(m -> {
				if (! (m instanceof FerariRawDataItem)) {
					return;
				}
				FerariRawDataItem rdm = (FerariRawDataItem) m;
				queue.add(rdm.getRawEvent());
			});
			return;
		}

		if(path != null) {
			setReader();
		}

		intraSiteClient.setTopologyRequestHandler(new TopologyRequestHandler() {
			
			@Override
			public void onTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
				Serializable s = customConfig.get(nodeID);
				if (s == null) {
					return;
				}

				if (gotTopology) {
					return;
				}

				gotTopology = true;

				DataTuple tuple  = (DataTuple) s;
				tuple.add("NewJsonFile");
				queue.add(tuple);
			}
		});
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(primitiveEventsToRoutingBoltStreamId, new Fields("Name", STORMMetadataFacade.ATTRIBUTES_FIELD));
	}
}