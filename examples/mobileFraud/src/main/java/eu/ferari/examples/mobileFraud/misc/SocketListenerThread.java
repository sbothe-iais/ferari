package eu.ferari.examples.mobileFraud.misc;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by vmanikaki.
 */
public class SocketListenerThread extends Thread {

	private final LinkedBlockingQueue<String> queue;
	private final Socket socket;
	private final BufferedReader in;

	public SocketListenerThread(LinkedBlockingQueue<String> queue, Socket socket, BufferedReader in) {
		this.queue = queue;
		this.socket = socket;
		this.in = in;
	}

	@Override
	public void run() {
		try {
			String line = in.readLine();
			while (line != null) {
				System.out.println("SocketListener:" + line);
				boolean isAdded = queue.offer(line);
				if (!isAdded) {
					System.out.println("Queue is full!");
				}
				line = in.readLine();
			}
			in.close();
			socket.close();
		} catch (IOException ioe) {
		}
	}
}
