package eu.ferari.examples.mobileFraud.misc;

import org.apache.storm.tuple.Values;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.MessageHandler;
import eu.ferari.core.DataTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.LinkedBlockingQueue;


public class CommListener {
	
	private String nodeId;
	private static final Logger logger = LoggerFactory.getLogger(CommListener.class);
	
	public CommListener(LinkedBlockingQueue<Serializable> queue, String nodeID, String context) {
		this.nodeId = nodeID;
		
		IntraSiteClient isc = FerariClientBuilder.getInstance().createIntraSiteClient(nodeId);
		isc.setbroadcastRequestHandler(new MessageHandler() {
			
			@Override
			public void onMessage(Serializable msg) {
				if (msg instanceof Values) {
					return;
				}
				if (msg instanceof String) {
					queue.add(msg);
					return;
				}
				if (! (msg instanceof FerariEvent)) {
					return;
				}
				MetaMessage mMessage= (MetaMessage)((FerariEvent) msg).getEvent();
				DataTuple tuple = mMessage.getData();
				String targetNode=tuple.getString(tuple.getDataLength()-1);
				logger.info(nodeId+" Got broadcast message: " + mMessage.getData().toString()+" from node "+targetNode);
					queue.add(mMessage);
				/*if (context.equals(event.getContext()) || event.getContext() == null) {
					queue.add(msg);
				}
				*/
			}
		});
	}

/*	private Coordinator createCoordinator() {
		String selfPort = InMemoryGlobalAddressResolver.getInstance().getHostsOfSite(nodeId)[0].split(":")[1];
		Coordinator coord = new Coordinator(nodeId, nodeId, Integer.parseInt(selfPort), new EventConsumer() {
			
			@Override
			public void consumeEvent(FerariEvent event) {
				System.out.println(event.getSource() + ": " + ((String) event.getEvent()));
				queue.add((Event) event.getEvent());
			}
		});
		
		return coord;
	}*/
	
}
