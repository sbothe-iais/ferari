package eu.ferari.examples.mobileFraud.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import eu.ferari.backend.storm.sender.SimpleSender;
import eu.ferari.core.DataTuple;
import eu.ferari.core.commIntegration.CommIntegrationConstants;
import eu.ferari.examples.mobileFraud.state.PushPullBufferState;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Map;

/**
 * Created by mofu on 15/11/16.
 */
public class PushPullBufferBolt extends BaseRichBolt{
	private boolean sendToDashboard;
	private PushPullBufferState pushPullBufferState;
	private EntityManager entityManager;
	private EntityManagerFactory emFactory;
	private OutputCollector collector;

	public PushPullBufferBolt(boolean sendToDashBoard, String nodeId){
		pushPullBufferState = new PushPullBufferState(sendToDashBoard,nodeId);
		this.sendToDashboard=sendToDashBoard;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		pushPullBufferState.setSender(new SimpleSender(collector));
			String eventContext = (String) stormConf.get(CommIntegrationConstants.EVENT_CONTEXT);
		if(sendToDashboard&& ProtonMobileFraudTopology.isInDevMode) {
			if(entityManager==null){
				emFactory = Persistence.createEntityManagerFactory("ferari");
				entityManager = emFactory.createEntityManager();
				pushPullBufferState.initDashboard(entityManager, eventContext);
			}
		}
		this.collector=collector;
	}

	@Override
	public void execute(Tuple input) {
		if(input.getValue(0)instanceof DataTuple)
			pushPullBufferState.update((DataTuple)input.getValue(0));
		else pushPullBufferState.update(new DataTuple(input.getValues()));
		collector.ack(input);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("PushPullTuple"));
	}
}
