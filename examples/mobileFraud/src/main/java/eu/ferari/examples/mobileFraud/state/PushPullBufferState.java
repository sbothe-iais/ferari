package eu.ferari.examples.mobileFraud.state;

import com.ibm.ferari.client.FerariClientBuilder;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ICoordinatorState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.misc.Event;
import eu.ferari.examples.mobileFraud.misc.DashBoardHelper;
import eu.ferari.examples.mobileFraud.misc.PullRequest;
import eu.ferari.examples.mobileFraud.storm.ProtonMobileFraudTopology;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import statistics.Statistics;

import javax.persistence.EntityManager;
import java.util.*;


/**
 * Created by mofu on 15/11/16.
 */
public class PushPullBufferState implements ICoordinatorState {
	private String nodeId;
	private Map<String, List<Event>> sortedEvents = new HashMap<String, List<Event>>();
	private Map<String, PullRequest> pullRequests = new HashMap<String, PullRequest>();
	private DashBoardHelper dashBoardHelper;
	private static final Logger logger = LoggerFactory.getLogger(PushPullBufferState.class);
	ISend sendToCommunicator;
	boolean sendToDashboard;

	public PushPullBufferState(boolean sendToDashboard, String nodeId){
		this.nodeId=nodeId;
		this.sendToDashboard=sendToDashboard;
	}


	public void update(DataTuple input){
		logger.debug("nodeId: {}; input: {}",nodeId, input.toString());
		switch(input.getString(0)){
			case "Statistics":
				//onStatistics(input);
				break;
			case "Event":
				onEvent(input);
				break;
			case "PullRequest":
				onPullRequest(input);
				break;
		}
	}
	private void onEvent(DataTuple input){
		Map<String, Object> attributes = (Map<String, Object>)input.getValue(3);
		String eventTypeName = input.getString(1);
		long eventOccurrenceTime =input.getLong(2);

		List<Event> pushEvents = new ArrayList<Event>();

		PullRequest request = pullRequests.get(eventTypeName);
		if(request != null) {
			if (eventOccurrenceTime >= request.getStartTime() && eventOccurrenceTime <= request.getEndTime()) {
				pushEvents.add(new Event(eventTypeName, eventOccurrenceTime, attributes));
				DataTuple tuple = new DataTuple("Push", pushEvents, 0L);// TODO isws xreiaste to requestid. twra stelnw to 0
				sendToCommunicator.signal(tuple);
			} else if (eventOccurrenceTime > request.getEndTime()) {
				pullRequests.remove(eventTypeName);
			}
		}

		List<Event> Events = sortedEvents.get(eventTypeName);
		if(Events == null){
			Events = new ArrayList<Event>();
			Events.add(new Event(eventTypeName, eventOccurrenceTime, attributes));
			sortedEvents.put(eventTypeName, Events);
		}else{
			Events.add(new Event(eventTypeName, eventOccurrenceTime, attributes));
			Collections.sort(Events);
		}

		StringBuilder sb = new StringBuilder("Timemachine : " + nodeId + "\n" );
		for(Map.Entry<String, List<Event>> sortedevents : sortedEvents.entrySet()){
			List<Event> events = sortedevents.getValue();

			for(int i=0; i<events.size(); i++){
				sb.append(events.get(i).getEvent() + " "+ events.get(i).getOccurrenceTime()+"\n");
			}
		}

		if(sendToDashboard&& ProtonMobileFraudTopology.isInDevMode){
			dashBoardHelper.sendToDashboard(attributes, eventTypeName);
		}

	}

	private void onPullRequest(DataTuple input){
		List<String> pullEvents = (List<String>) input.getValue(1);
		long startingTime = input.getLong(2);
		long endingTime =  input.getValue(3) instanceof Double ? input.getDouble(3).longValue() : input.getLong(3);//message.getLong(3);
		long pullRequestId = input.getLong(4);
		for(String event :pullEvents){
			PullRequest pr = pullRequests.get(event);
			if(pr==null){
				List<String> pullevent = new ArrayList<String>();
				pullevent.add(event);
				pullRequests.put(event,new PullRequest(pullevent, startingTime, endingTime, null));
			}
			else{
				//update start and end time, if necessary
				if(pr.getStartTime() > startingTime) {
					pr.setStartTime(startingTime);
				}
				if(pr.getEndTime() < endingTime) {
					pr.setEndTime(endingTime);
				}
			}
		}
		List<Event> pushEvents2 = new ArrayList<Event>();

		for(String event : pullEvents) {
			List<Event> sorted = sortedEvents.get(event);
			if(sorted != null) {
				for(Event e : sorted) {
					if(e.getOccurrenceTime() >= startingTime && e.getOccurrenceTime() <= endingTime) {
						pushEvents2.add(e);
						continue;
					}
					if(e.getOccurrenceTime() >= endingTime) {
						break;
					}
				}
			}
		}
		if(!pushEvents2.isEmpty()) {
			DataTuple tuple = new DataTuple("Push", pushEvents2, pullRequestId);
			sendToCommunicator.signal(tuple);
		}
	}


	private void onStatistics(DataTuple input){
		StringBuilder sbce = new StringBuilder("TimeMachine Statistics: "+ nodeId+"\n");//received from routing bolt
		for (Map.Entry<String, List<Statistics>> hmp: ((Map<String,List<Statistics>>)input.getValue(1)).entrySet() ){
			sbce.append(hmp.getKey());
			sbce.append(" --> ");
			for(Statistics s : hmp.getValue()){
				sbce.append(s.toString());
			}
			sbce.append("\n");
		}
		System.out.println(sbce);
	}

	@Override
	public void setSender(ISend sender) {
		sendToCommunicator=sender;
	}
	public void initDashboard(EntityManager manager, String eventContext){
		this.dashBoardHelper=new DashBoardHelper(FerariClientBuilder.getInstance().createWSForwarder(),manager,eventContext);
	}
}
