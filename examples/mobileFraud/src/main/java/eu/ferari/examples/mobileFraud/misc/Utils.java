package eu.ferari.examples.mobileFraud.misc;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.storm.Config;

public class Utils {

	public static Properties loadProperties(String filename, Object klass) throws IOException{
		Properties properties = new Properties();

		InputStream inputStream = klass.getClass().getClassLoader().getResourceAsStream(filename);

		if (inputStream != null) {
			properties.load(inputStream);
			return properties;
		} else {
			throw new FileNotFoundException("Property file '" + filename + "' not found in the classpath");
		}
	}

	public static Config propertiesToStormConfiguration(Properties routingProperties, Properties geometricProperties){
		Config conf = new Config();
        conf.setDebug(false);
        for (Enumeration e = routingProperties.keys(); e.hasMoreElements();){
        	String key = (String) e.nextElement();
        	conf.put(key, routingProperties.get(key));
        }
        for (Enumeration e = geometricProperties.keys(); e.hasMoreElements();){
        	String key = (String) e.nextElement();
        	conf.put(key, geometricProperties.get(key));
        }
        
        return conf;
    }

	public static String createInputContents(String inputFileName) {
		String line;
		StringBuilder sb = new StringBuilder();
		BufferedReader in = null;

		try
		{
			in = new BufferedReader(new FileReader(inputFileName));
			while ((line = in.readLine()) != null)
			{
				sb.append(line);
			}

		}catch(Exception e)
		{
			e.printStackTrace();
		}finally
		{
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}

