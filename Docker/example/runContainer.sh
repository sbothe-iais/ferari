#!/bin/bash
CONTAINER_NAME=ferari_example
docker build -t $CONTAINER_NAME .
docker run -t $CONTAINER_NAME  &
sleep 5 # o/w we won't get the correct container id.
CONTAINER_ID=$(docker ps -l | tail -n 1 | cut -d " " -f1)
sleep 600
docker kill $CONTAINER_ID
docker cp $CONTAINER_ID:/data/containerOutput.txt ./
#$(docker inspect --format {{.State.Pid}} $CONTAINER_NAME) --mount --uts --ipc --net --pid

