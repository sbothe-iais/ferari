package eu.ferari.protonAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import com.ibm.hrl.proton.adapters.interfaces.AdapterException;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;

public class CsvSpout extends BaseRichSpout {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1459296438082248983L;
	private CSVTextFormatter formatter;
	private SpoutOutputCollector _collector;
	private String path;
	private BufferedReader reader;
	//TODO remove this for simulated time!
	private long prevTimestamp;
	
	private boolean done = false;
	
	CsvSpout(List<String> fieldNames, List<Attribute.Type> fieldTypes, String dateFormat, String path, String delimiter){
		boolean has_time = false;
		for(String fieldname: fieldNames){
			if(fieldname == "timestamp") has_time=true;
		}
		if(!has_time){
			throw new TimeStampMissingException();
		}
		try {
			formatter = new CSVTextFormatter(fieldNames, fieldTypes, dateFormat, delimiter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.path=path;
		prevTimestamp=0;
	}
	CsvSpout(List<String> fieldNames, List<Attribute.Type> fieldTypes, String path){
		try {
			formatter=new CSVTextFormatter(fieldNames, fieldTypes, null, ",");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.path=path;
		prevTimestamp=0L;
	}
	

	@Override
	public void nextTuple() {

		
		

	}

	@Override
	public void open(Map arg0, TopologyContext arg1, SpoutOutputCollector arg2) {


	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("Name",STORMMetadataFacade.ATTRIBUTES_FIELD));

	}
	
	public class TimeStampMissingException extends RuntimeException{

		/**
		 * 
		 */
		private static final long serialVersionUID = 9184282930484685837L;
		public TimeStampMissingException(){
			
		}
	}

}
