This is a Docker container for running Proton on Storm.
At the moment it pulls Proton on Storm from the maven repo hosted on github.
It then runs a EPN for the distributed count case, as specified in the json configuration.
