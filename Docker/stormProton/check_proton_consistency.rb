#!/usr/bin/ruby
current_timestamp = 0
events = []

File.open(ARGV[0]).each_with_index do |line, i|
  match = line.match(/count=(\d+).+OccurrenceTime=(\d+).+phoneNumber=(\d+)/)
  next if match.nil?
  count = Integer(match[1])
  timestamp = Integer(match[2])
  phone = Integer(match[3])
  if ((timestamp - current_timestamp) > 100)
		events.sort_by{|e| e[2]}.each{|e| puts "phone=#{e[2]}, count=#{e[0]}"}
		puts "----------------#{i}"
		events.clear
  end
  current_timestamp = timestamp
	events << [count, timestamp, phone]
end
