package eu.ferari.backend.storm;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;

import java.util.Map;

/**
 * Created by mofu on 03/11/16.
 */
public class NoOpCoordinatorSpout extends BaseRichSpout {
	private String coordinatorStream;
	private String confStream;
	public NoOpCoordinatorSpout(String coordinatorStream, String confStream){
		this.coordinatorStream=coordinatorStream;
		this. confStream=confStream;
	}
	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {

	}

	@Override
	public void nextTuple() {
		try {
			Thread.sleep(2000);
		}catch (InterruptedException e){
			//
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(coordinatorStream,new Fields(SegmentedStreamSender.fields));
		declarer.declareStream(confStream,new Fields("configuration"));
	}
}
