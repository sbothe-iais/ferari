package eu.ferari.backend.storm;

import org.apache.storm.LocalCluster;
import eu.ferari.backend.storm.bolts.FerariBaseBolt;
import eu.ferari.core.DataTuple;
import eu.ferari.core.misc.Event;
import eu.ferari.core.utils.MessageType;
import eu.ferari.core.utils.RealValuedVector;
import org.junit.Before;
import org.junit.Test;
import static  org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mofu on 03/11/16.
 */
public class TopologyViolationTest extends AbstractTopologyTest {
	@Before
	public void setUp(){
		HashMap<String,Object> fields = new HashMap<>();
		fields.put("phoneNumber",3);
		fields.put("duration",20.0);
		Event e = new Event("call",0l,fields);
		init(e);
	}

	@Test
	public void violationTriggeredTest(){
		Map<String,Object> confMap =new HashMap<>();
		confMap.put(FerariBaseBolt.FUNCTION_CONF_PARAMETER_NAME,confObject.toString());
		LocalCluster lc = new LocalCluster();
		lc.submitTopology("FERARI_Test",confMap,builder.createTopology());
		try {
			Thread.sleep(20000);
		}catch (InterruptedException e){
			//
		}
		lc.shutdown();
		RealValuedVector violationValue=new RealValuedVector(1);
		violationValue.set(0,20.0);
		DataTuple retTuple=new DataTuple(violationValue, MessageType.Violation,"myFunction;3");
		System.out.println(mockState.toString());
		verify(mockState,times(1)).update(retTuple);
	}

}
