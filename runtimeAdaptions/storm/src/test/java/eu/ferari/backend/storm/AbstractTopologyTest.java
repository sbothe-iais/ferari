package eu.ferari.backend.storm;

import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichSpout;
import com.google.gson.JsonObject;
import eu.ferari.core.interfaces.IConfigurableState;
import eu.ferari.core.misc.Event;
import eu.ferari.core.monitoring.ConfigValues;
import eu.ferari.core.monitoring.states.IdentityState;
import eu.ferari.core.monitoring.syncOps.IdentityCheck;

import static org.mockito.Mockito.*;
/**
 * Created by mofu on 03/11/16.
 */
public abstract class AbstractTopologyTest {
	TopologyBuilder builder=new TopologyBuilder();
	String inputSpoutName="eventSpout";
	String eventStreamName="eventStream";
	String coordinatorName="coordSpout";
	String coordinatorStream="coordStream";
	String confStream="confStream";
	BaseRichSpout eventSpout;
	BaseRichSpout coordinatorSpout;
	IConfigurableState mockState = mock(IConfigurableState.class);
	TestBolt testBolt=new TestBolt(mockState);
	JsonObject conf = new JsonObject();
	String TEST_BOLT_NAME="TestBoltName";
	JsonObject confObject=new JsonObject();

	public void init(Event e){
		eventSpout = new SingleEventSpout(e,eventStreamName);
		builder.setSpout(inputSpoutName, eventSpout);
		coordinatorSpout=new NoOpCoordinatorSpout(coordinatorStream,confStream);
		builder.setSpout(coordinatorName,coordinatorSpout);
		JsonObject relevantEvents= new JsonObject();
		relevantEvents.addProperty(ConfigValues.EVENT_NAME_PARAMETER_NAME,"call");
		relevantEvents.addProperty(ConfigValues.EVENT_FIELDS_PARAMETER_NAME,"duration");
		//this is just to include the dependencies.
		IdentityState idState = new IdentityState(10,1,0.0);
		IdentityCheck idCheck= new IdentityCheck(10.0,1,true,false);

		conf.addProperty(ConfigValues.FUNCTION_LOCATION_PARAMETER_NAME,"/home/mofu/git/ferari/core/target/classes");
		conf.addProperty(ConfigValues.FUNCTION_NAME_PARAMETER_NAME,"myFunction");
		conf.addProperty(ConfigValues.FUNCTION_CLASS_PARAMETER_NAME, "eu.ferari.core.monitoring.states.IdentityState");
		conf.addProperty(ConfigValues.CONDITION_CHECK_PARAMETER_NAME,"eu.ferari.core.monitoring.syncOps.DistanceCheck");
		conf.addProperty(ConfigValues.SEGMENTATION_INFO_PARAMETER_NAME, "phoneNumber");
		conf.add(ConfigValues.RELEVANT_EVENTS_PARAMETER_NAME, relevantEvents);
		conf.addProperty(ConfigValues.DYNAMIC_THRESHOLD_PARAMETER_NAME,false);
		conf.addProperty(ConfigValues.LSV_DIMENSION_PARAMETER_NAME,1);
		conf.addProperty(ConfigValues.HISTORY_SIZE_PARAMETER_NAME,1);
		conf.addProperty(ConfigValues.DEFAULT_VALUE_PARAMETER_NAME,0.0);
		conf.addProperty(ConfigValues.THRESHOLD_PARAMETER_NAME,5);
		conf.addProperty(ConfigValues.THRESHOLD_EQUALITY_PARAMETER_NAME,true);
		confObject.add("config",conf);
		confObject.addProperty("type","gm");
		String outputBoltName = Topology.extendTopology(builder,eventStreamName,inputSpoutName,
				coordinatorStream,coordinatorName,confStream,coordinatorName);
		builder.setBolt(TEST_BOLT_NAME,testBolt).allGrouping(outputBoltName);
	}
}
