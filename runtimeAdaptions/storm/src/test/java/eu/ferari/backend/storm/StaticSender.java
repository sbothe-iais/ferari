package eu.ferari.backend.storm;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

import java.io.Serializable;

/**
 * Created by mofu on 07/11/16.
 */
public class StaticSender implements Serializable, ISend{
	private static ISend sender;

	public StaticSender(ISend sender){
		StaticSender.sender=sender;
	}
	@Override
	public void signal(DataTuple data) {
		sender.signal(data);
	}

	@Override
	public int getTaskId() {
		return 0;
	}
}
