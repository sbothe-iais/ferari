package eu.ferari.backend.storm;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import eu.ferari.backend.storm.sender.StreamSender;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.misc.Event;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by mofu on 03/11/16.
 */
public class SingleEventSpout extends BaseRichSpout{
	boolean sent=false;
	List<Object> emitValues;
	String outChannel;
	SpoutOutputCollector collector;
	TopologyContext context;
	ISend sender;
	public SingleEventSpout(Event e, String channelName){

		emitValues= Arrays.asList("Event",e.getEvent(),e.getOccurrenceTime(),e.getAttributes());
		outChannel=channelName;

	}
	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector=collector;
		this.context=context;
	}

	@Override
	public void nextTuple() {
		if(!sent){
			collector.emit(outChannel,emitValues);
			sent=true;
		}
		else try{
			Thread.sleep(1000);
		}catch (InterruptedException e){
			//
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(this.outChannel, new Fields("event","name", "occurenceTime","attributes"));
	}
}
