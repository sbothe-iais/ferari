package eu.ferari.backend.storm.sender;

import org.apache.storm.spout.SpoutOutputCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.storm.task.OutputCollector;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

import java.io.Serializable;

public class StreamSender implements Serializable,ISend {
private static final Logger logger = LoggerFactory.getLogger(StreamSender.class);
	
	private final ICollector collector;
	private final String streamId;
	
	public StreamSender(OutputCollector collector, String streamId) {
		 this.collector = new OutputCollectorProxy(collector);
		 this.streamId = streamId;
	}
	public StreamSender(SpoutOutputCollector collector, String streamId){
		this.collector=new SpoutCollectorProxy(collector);
		this.streamId=streamId;
	}

	public void signal(DataTuple data) {

		collector.emit(streamId, data.asList());
	}

	public int getTaskId() {
		// TODO Auto-generated method stub
		return 0;
	}


}
