package eu.ferari.backend.storm.trident;

import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class TridentSendToGlobal implements ISend {

    private final TridentCollector collector;

    public TridentSendToGlobal(TridentCollector collector) {
        this.collector = collector;
    }

    @Override
    public void signal(DataTuple data) {
        Values values = new Values();
        values.add(data);
        collector.emit(values);

    }

	@Override
	public int getTaskId() {
		// TODO Auto-generated method stub
		return 0;
	}


}
