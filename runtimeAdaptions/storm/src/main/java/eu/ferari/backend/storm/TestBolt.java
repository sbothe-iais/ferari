package eu.ferari.backend.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ILocalState;

import java.util.Map;

/**
 * Created by mofu on 16/11/16.
 */
public class TestBolt extends BaseRichBolt{
	static private ILocalState state;

	public TestBolt (ILocalState state){
		TestBolt.state=state;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {

	}

	@Override
	public void execute(Tuple input) {
		state.update((DataTuple)input.getValue(0));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}

	public static ILocalState getState() {
		return state;
	}
}
