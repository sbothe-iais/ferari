package eu.ferari.backend.storm.sender;

import org.apache.storm.task.OutputCollector;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mofu on 11/11/16.
 */
public class OutputCollectorProxy implements Serializable, ICollector{
	OutputCollector collector;
	public OutputCollectorProxy(OutputCollector collector) {
		this.collector=collector;
	}

	public List<Integer> emit(String streamInfo, List<Object> tuple) {
		return collector.emit(streamInfo,tuple);
	}
}
