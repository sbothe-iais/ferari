package eu.ferari.backend.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IConfigurableState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by mofu on 27/10/16.
 */
public abstract class FerariBaseBolt extends BaseRichBolt implements Serializable{
	public static String FUNCTION_CONF_PARAMETER_NAME="functionConf";
	private String coordinatorChannel;
	private String configChannel;
	private IConfigurableState state;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static JsonParser parser = new JsonParser();
	OutputCollector collector;

	public FerariBaseBolt(String coordinatorChannel, String configChannel, IConfigurableState state){
		this.coordinatorChannel=coordinatorChannel;
		this.configChannel=configChannel;
		this.state=state;
	}

	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector=collector;
		JsonParser parser =new JsonParser();
		try{
			state.updateConfig(parser.parse((String)stormConf.get(FUNCTION_CONF_PARAMETER_NAME)));
		}catch (NullPointerException e){
			//noop
		}
		setOutSenders(collector);
	}

	public void execute(Tuple input) {
		logger.debug("input {} on channel {}",input.getValue(0).toString(),input.getSourceStreamId());
		if (input.getSourceStreamId().equals(coordinatorChannel))
			state.handleFromCommunicator((DataTuple) input.getValue(0));
		else if (input.getSourceStreamId().equals(configChannel)) {
			JsonElement element = parser.parse(input.getString(0));
			if(!element.isJsonNull())
				state.updateConfig(element);
		}
		else
			state.update(getTupleFromNormalStream(input));
		collector.ack(input);
	}

	 abstract void setOutSenders(OutputCollector collector);

	public IConfigurableState getState() {
		return state;
	}
	DataTuple getTupleFromNormalStream(Tuple input){
		return (DataTuple)input.getValue(0);
	}
}
