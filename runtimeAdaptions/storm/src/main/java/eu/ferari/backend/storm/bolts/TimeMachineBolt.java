package eu.ferari.backend.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import eu.ferari.backend.storm.Topology;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.core.state.TimeMachineState;

/**
 * Created by mofu on 27/10/16.
 */
public class TimeMachineBolt extends FerariBaseBolt {
	public TimeMachineBolt(String confChannel) {
		super(Topology.CHANNEL_COM_TO_TM,confChannel, new TimeMachineState("defaultNode"));
	}

	@Override
	void setOutSenders(OutputCollector collector) {
		TimeMachineState timeMachineState = (TimeMachineState) getState();
		timeMachineState.setSendToCom(new SegmentedStreamSender(collector,Topology.CHANNEL_TM_TO_COM));
		timeMachineState.setSender(new SegmentedStreamSender(collector,Topology.CHANNEL_TM_TO_GK));
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(Topology.CHANNEL_TM_TO_COM,new Fields(SegmentedStreamSender.fields));
		declarer.declareStream(Topology.CHANNEL_TM_TO_GK,new Fields(SegmentedStreamSender.fields));
	}
}
