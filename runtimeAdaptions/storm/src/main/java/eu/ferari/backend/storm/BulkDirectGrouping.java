package eu.ferari.backend.storm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.CustomStreamGrouping;
import org.apache.storm.task.WorkerTopologyContext;
import eu.ferari.core.DataTuple;
import eu.ferari.core.utils.Message;

/**
 * a direct sender that sends a tuple to multiple tasks directly
 * 
 * @author Rania
 *
 */
public class BulkDirectGrouping implements CustomStreamGrouping, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7229595089800757291L;

	public static final String POLL_LOCAL_STREAM = "pollLocalStream";

	private List<Integer> existingTasks;

	@Override
	public void prepare(WorkerTopologyContext context, GlobalStreamId stream,
			List<Integer> targetTasks) {
		existingTasks = new ArrayList<Integer>(targetTasks);
	}

	@Override
	public List<Integer> chooseTasks(int taskId, List<Object> values) {
		DataTuple data = (DataTuple)(values.get(0));
		Message msg = new Message(data);
 		if (msg.getData().getValue(0) instanceof Collection<?>) {
 			Collection<Integer> tasksToQuery = (Collection<Integer>)msg.getData().getValue(0);
			List<Integer> existingTasksToQuery = new ArrayList<Integer>();
			for (int curTaskId : tasksToQuery) {
				if (existingTasks.contains(curTaskId)) {
					existingTasksToQuery.add(curTaskId);
				}
			}
			//System.out.println("returned tasks" + Arrays.toString(existingTasksToQuery.toArray()));
			return existingTasksToQuery;
		}
		//System.out.println("returned tasks" + Arrays.toString(existingTasks.toArray()));
		return existingTasks;
	}
}
