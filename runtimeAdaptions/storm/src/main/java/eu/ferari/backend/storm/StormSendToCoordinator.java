package eu.ferari.backend.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class StormSendToCoordinator implements ISend {

    public static final String TO_COORDINATOR_STREAM_NAME = "sendToCoordinator";

    public static final String FIELDNAME = "data";

	public static final String TASK_ID = "taskId";

    private final OutputCollector collector;
    
    private int taskId = -1;

    public StormSendToCoordinator(OutputCollector collector) {
        this.collector = collector;
    }
    
    public StormSendToCoordinator(OutputCollector collector, int taskId) {
    	this(collector);
        this.taskId = taskId;
    }

    @Override
    public void signal(DataTuple data) {
        Values values = new Values();
        values.add(data);
        //values.add(taskId);
        this.collector.emit(TO_COORDINATOR_STREAM_NAME, values);
    }
    
    public int getTaskId() {
    	return taskId;
    }
	
}
