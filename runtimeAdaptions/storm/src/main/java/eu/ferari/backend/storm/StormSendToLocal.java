package eu.ferari.backend.storm;

import java.util.List;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class StormSendToLocal implements ISend {

    public static final String TO_LOCAL_STREAM_NAME = "sendToLocal";

    public static final String FIELDNAME = "data";

    private final OutputCollector collector;

    public StormSendToLocal(OutputCollector collector) {
        this.collector = collector;
    }



    public void signal(DataTuple data) {
        Values values = new Values();
        values.add(data);
        this.collector.emit(TO_LOCAL_STREAM_NAME, values);

    }



	@Override
	public int getTaskId() {
		// TODO Auto-generated method stub
		return 0;
	}

}
