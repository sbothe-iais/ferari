package eu.ferari.backend.storm.sender;

import org.apache.storm.spout.SpoutOutputCollector;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mofu on 11/11/16.
 */
public class SpoutCollectorProxy implements Serializable, ICollector {
	SpoutOutputCollector collector;

	@Override
	public List<Integer> emit(String streamInfo, List<Object> tuple) {
		return collector.emit(streamInfo,tuple);
	}

	public SpoutCollectorProxy(SpoutOutputCollector collector) {
		this.collector = collector;
	}
}
