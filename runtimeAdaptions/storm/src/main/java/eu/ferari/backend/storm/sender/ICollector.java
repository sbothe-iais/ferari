package eu.ferari.backend.storm.sender;

import java.util.List;

/**
 * Created by mofu on 11/11/16.
 */
public interface ICollector {
	public List<Integer> emit(String streamInfo, List<Object> tuple);
}
