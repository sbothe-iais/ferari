package eu.ferari.backend.storm.sender;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Values;
import eu.ferari.core.Constants;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.misc.Event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by mofu on 27/10/16.
 */
public class SegmentedStreamSender implements Serializable,ISend{
	public static final String SEGMENTATION_FIELD_NAME="SegmentationInfo";
	public static final ArrayList<String> fields =
			new ArrayList<String>(Arrays.asList(new String[]{
					"DataTuple",SEGMENTATION_FIELD_NAME})
			);
	private final ICollector collector;
	private final String streamId;
	public SegmentedStreamSender(OutputCollector collector, String streamId) {
		this.collector = new OutputCollectorProxy(collector);
		this.streamId = streamId;
	}
	public SegmentedStreamSender(SpoutOutputCollector collector, String streamId) {
		this.collector = new SpoutCollectorProxy(collector);
		this.streamId = streamId;
	}

	public void signal(DataTuple data) {
		if(data.getDataLength()>Constants.segmentationInfoIndex) {
			collector.emit(streamId,new Values(data,data.getString(Constants.segmentationInfoIndex)));
		}
		else {
			collector.emit(streamId,new Values(data, data));

		}
	}
	public int getTaskId() {
		// TODO Auto-generated method stub
		return 0;
	}
}
