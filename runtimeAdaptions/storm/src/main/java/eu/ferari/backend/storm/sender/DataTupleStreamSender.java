package eu.ferari.backend.storm.sender;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mofu on 03/12/16.
 */
public class DataTupleStreamSender implements ISend{
	private static final Logger logger = LoggerFactory.getLogger(StreamSender.class);

	private final ICollector collector;
	private final String streamId;

	public DataTupleStreamSender(OutputCollector collector, String streamId) {
		this.collector = new OutputCollectorProxy(collector);
		this.streamId = streamId;
	}
	public DataTupleStreamSender(SpoutOutputCollector collector, String streamId){
		this.collector=new SpoutCollectorProxy(collector);
		this.streamId=streamId;
	}

	public void signal(DataTuple data) {

		collector.emit(streamId,new Values(data));
	}

	public int getTaskId() {
		// TODO Auto-generated method stub
		return 0;
	}
}
