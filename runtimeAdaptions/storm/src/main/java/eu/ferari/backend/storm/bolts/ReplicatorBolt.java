package eu.ferari.backend.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import eu.ferari.backend.storm.Topology;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.core.DataTuple;
import eu.ferari.core.misc.Event;
import eu.ferari.core.state.MessageReplicatorState;

import java.util.Map;


/**
 * Created by mofu on 27/10/16.
 */
public class ReplicatorBolt extends FerariBaseBolt{
	public ReplicatorBolt(String confChannel){
		super(null,confChannel,new MessageReplicatorState());
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(Topology.CHANNEL_REPLICATOR_TO_TM,new Fields(SegmentedStreamSender.fields));
	}

	@Override
	void setOutSenders(OutputCollector collector) {
		getState().setSender(new SegmentedStreamSender(collector,Topology.CHANNEL_REPLICATOR_TO_TM));
	}

	@Override
	DataTuple getTupleFromNormalStream(Tuple input){
		if("Event".equals(input.getString(0))){
			Event event = new Event(input.getString(1),input.getLong(2),(Map<String,Object>)input.getValue(3));
			return new DataTuple(event);
		}
		throw new RuntimeException("Unsupported input to message replicator bolt.");
	}
}
