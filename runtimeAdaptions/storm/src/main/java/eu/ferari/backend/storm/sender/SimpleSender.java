package eu.ferari.backend.storm.sender;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

import java.io.Serializable;

public class SimpleSender implements Serializable,ISend {
//	private static final Logger logger = LoggerFactory.getLogger(SimpleSender.class);
	
	private OutputCollector collector;
	
	public SimpleSender(OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void signal(DataTuple data) {
//		logger.info("Simple sender emitting message");
		collector.emit(new Values(data));
	}

	@Override
	public int getTaskId() {
		// TODO Auto-generated method stub
		return 0;
	}



}
