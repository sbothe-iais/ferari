package eu.ferari.backend.storm;

import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import eu.ferari.backend.storm.bolts.CommunicatorBolt;
import eu.ferari.backend.storm.bolts.GatekeeperBolt;
import eu.ferari.backend.storm.bolts.ReplicatorBolt;
import eu.ferari.backend.storm.bolts.TimeMachineBolt;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.core.interfaces.IConfigurableState;

/**
 * Created by mofu on 26/10/16.
 */
public class Topology {
	public static String CHANNEL_COM_TO_GK="GM_CHANNEL_COM_TO_GK";
	public static String CHANNEL_COM_TO_TM="GM_CHANNEL_COM_TO_TM";
	public static String CHANNEL_TM_TO_COM="GM_CHANNEL_TM_TO_COM";
	public static String CHANNEL_GK_TO_COM="GM_CHANNEL_GK_TO_COM";
	public static String CHANNEL_TM_TO_GK="GM_CHANNEL_TM_TO_GK";
	public static String CHANNEL_REPLICATOR_TO_TM="GM_CHANNEL_REPLICATOR_TO_TM";
	private static String FERARI_TM = "GM_TM";
	private static String FERARI_GK = "GM_GK";
	private static String FERARI_COM = "GM_COM";
	private static String FERARI_REPLICATOR = "GM_REPLICATOR";


	public static String extendTopology(TopologyBuilder builder,
													String eventChannel,
													String eventSource,
													String coordinatorChannel,
													String coordinatorSource,
													String configChannel,
													String configSource){

		BoltDeclarer replicatorDeclarer = builder.setBolt(FERARI_REPLICATOR,new ReplicatorBolt(configChannel));
		BoltDeclarer timeMachineDeclarer = builder.setBolt(FERARI_TM, new TimeMachineBolt(configChannel));
		BoltDeclarer gatekeeperDeclarer = builder.setBolt(FERARI_GK, new GatekeeperBolt(configChannel));
		CommunicatorBolt communicatorBolt= new CommunicatorBolt(coordinatorChannel,configChannel);
		BoltDeclarer communicatorDeclarer = builder.setBolt(FERARI_COM, communicatorBolt );


		replicatorDeclarer.allGrouping(eventSource,eventChannel);
		replicatorDeclarer.allGrouping(configSource,configChannel);

		timeMachineDeclarer.fieldsGrouping(FERARI_REPLICATOR,CHANNEL_REPLICATOR_TO_TM, new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		timeMachineDeclarer.fieldsGrouping(FERARI_COM,CHANNEL_COM_TO_TM,new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		timeMachineDeclarer.allGrouping(configSource,configChannel);

		gatekeeperDeclarer.fieldsGrouping(FERARI_TM,CHANNEL_TM_TO_GK,new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		gatekeeperDeclarer.fieldsGrouping(FERARI_COM,CHANNEL_COM_TO_GK,new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		gatekeeperDeclarer.allGrouping(configSource,configChannel);

		communicatorDeclarer.fieldsGrouping(FERARI_TM,CHANNEL_TM_TO_COM,new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		communicatorDeclarer.fieldsGrouping(FERARI_GK,CHANNEL_GK_TO_COM,new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		communicatorDeclarer.fieldsGrouping(coordinatorSource,coordinatorChannel, new Fields(SegmentedStreamSender.SEGMENTATION_FIELD_NAME));
		communicatorDeclarer.allGrouping(configSource,configChannel);
		return FERARI_COM;
	}
}
