package eu.ferari.backend.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import eu.ferari.backend.storm.Topology;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.core.state.GatekeeperState;

/**
 * Created by mofu on 27/10/16.
 */
public class GatekeeperBolt extends FerariBaseBolt{

	@Override
	void setOutSenders(OutputCollector collector) {
		GatekeeperState gkState = (GatekeeperState) getState();
		gkState.setSender(new SegmentedStreamSender(collector,Topology.CHANNEL_GK_TO_COM));
	}

	public GatekeeperBolt(String confChannel){
		super(Topology.CHANNEL_COM_TO_GK,confChannel,new GatekeeperState());
	}
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(Topology.CHANNEL_GK_TO_COM,new Fields(SegmentedStreamSender.fields));
	}
}
