package eu.ferari.backend.storm.sender;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

import java.io.Serializable;

public class DirectSender implements Serializable,ISend {
//	private static final Logger logger = LoggerFactory.getLogger(DirectSender.class);
	
	private OutputCollector boltCollector;
	private SpoutOutputCollector spoutCollector;
	private final TopologyContext context;
	private final int targetTaskId;
	private final String streamId;	
	
	/**
	 * Constructor for sending from a bolt.
	 * @param collector
	 * @param context
	 * @param targetComponentName
	 * @param streamId
	 */
	public DirectSender(OutputCollector collector, TopologyContext context, 
			String targetComponentName, String streamId) {
		this(context, targetComponentName, streamId);
		this.boltCollector = collector;
	}
	
	/**
	 * Constructor for sending from a spout.
	 * @param collector
	 * @param context
	 * @param targetComponentName
	 * @param streamId
	 */
	public DirectSender(SpoutOutputCollector collector, TopologyContext context, 
			String targetComponentName, String streamId) {
		this(context, targetComponentName, streamId);
		this.spoutCollector = collector;
	}
	
	private DirectSender(TopologyContext context, String targetComponentName, String streamId) {
		this.context = context;		
		targetTaskId = context.getComponentTasks(targetComponentName).get(context.getThisTaskIndex());
//		logger.trace("Set target task ID: {}, this task index: {}", targetTaskId, context.getThisTaskIndex());
		this.streamId = streamId;
	}

	@Override
	public void signal(DataTuple data) {		
//		logger.info("Emitting to {} on stream {}, task index: {}",
//				new Object[]{
//					context.getComponentId(targetTaskId), streamId, context.getThisTaskIndex()
//				});
		if (boltCollector != null)
			boltCollector.emitDirect(targetTaskId, streamId, data.asList());
		else if (spoutCollector != null)
			spoutCollector.emitDirect(targetTaskId, streamId, data.asList());
	}

	public int getTaskId() {
		//TODO
		return 0;
	}
}
