package eu.ferari.backend.storm.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import eu.ferari.backend.storm.Topology;
import eu.ferari.backend.storm.sender.SegmentedStreamSender;
import eu.ferari.backend.storm.sender.SimpleSender;
import eu.ferari.backend.storm.sender.StreamSender;
import eu.ferari.core.state.CommunicatorState;

/**
 * Created by mofu on 27/10/16.
 */
public class CommunicatorBolt extends FerariBaseBolt {
	@Override
	void setOutSenders(OutputCollector collector) {
		CommunicatorState state = (CommunicatorState) getState();
		state.setGateKeeperSender(new SegmentedStreamSender(collector,Topology.CHANNEL_COM_TO_GK));
		state.setTimeMachineSender(new SegmentedStreamSender(collector,Topology.CHANNEL_COM_TO_TM));
		state.setSender(new SimpleSender(collector));
	}

	public CommunicatorBolt(String coordStream, String confStream){
		super(coordStream,confStream,new CommunicatorState("defaultNodeId"));
	}
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(Topology.CHANNEL_COM_TO_GK, new Fields(SegmentedStreamSender.fields));
		declarer.declareStream(Topology.CHANNEL_COM_TO_TM, new Fields(SegmentedStreamSender.fields));
		declarer.declare(new Fields("DataTuple"));
	}
}
