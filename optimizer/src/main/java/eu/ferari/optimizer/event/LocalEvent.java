
package eu.ferari.optimizer.event;


public class LocalEvent extends DerivedEvent
{
    private String location;
    private String name;
    private double frequency;
 //   private EventType etype;
//    private DerivedEvent dtype;
    //private boolean derived;
    
    public LocalEvent(EventType type, double frequency, String location)
    {
    	name = type.getName();
    	this.frequency = frequency;
        this.location = location;
       
    }
//    public LocalEvent(DerivedEvent type, double frequency, String location)
//    {
//    	this.dtype = type;
//    	this.frequency = frequency;
//        this.location = location;
//        derived=true;
//    }
	public String getLocation() {
		return location;
	}

	public double getFrequency() {
		return frequency;
	}

	public String getName() {
		return name;
//		if(derived)
//			return dtype.getName();
//		else
//			return etype.getName();
	}
	           
    public void print()
    {    
//    	if(derived)   
//    		System.out.println("Event: "+dtype.getName()+" F= "+dtype.getprobability()+" localF: "+ frequency+" located at: "+location);
//    	else
    		System.out.println("DerivedEvent: "+getName()+" localF: "+ frequency+" located at: "+location);
    }
}
