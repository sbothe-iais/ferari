package eu.ferari.optimizer.event;

import java.util.ArrayList;


public  class EventType {

    public ArrayList<Attribute> attributes;
    private double probability;
    private String ename;
    
    public EventType()
    {
    	this.attributes = new ArrayList<Attribute>();
        this.probability=0;
        this.ename = "";
    }
    
    public EventType(String ename, Double probability)
    {
    	this.attributes = new ArrayList<Attribute>();
        this.probability = probability;
        this.ename = ename;
    }
    
    public EventType(String ename)
    {
    	this.attributes = new ArrayList<Attribute>();
    	this.probability = 0;
        this.ename = ename;
    }
    
    public void setprobability(double probability)
    {
        this.probability = probability;
    }
    
    public double getprobability()
    {
        return this.probability;
    }
    
    public void setName(String ename)
    {
        this.ename = ename;
    }
    
    public String getName()
    {
    	return this.ename;
    }

    public boolean isequal(EventType ev)
    {
    	return this.equals(ev);
    }
    public void putAttr(Attribute attr)
    {    	
    	attributes.add(attr);
    }
    public void printAttributes()
    {
    	for(Attribute attr:attributes)
    		attr.print();
    }
    public void print(boolean attr)
    {
        System.out.println("E: "+ename+" P: "+probability);
        if(!attributes.isEmpty() && attr)
        	printAttributes();
    }
}
