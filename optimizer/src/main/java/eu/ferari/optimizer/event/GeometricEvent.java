package eu.ferari.optimizer.event;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.ArrayList;

//import org.codehaus.jettison.json.JSONArray;
//import org.codehaus.jettison.json.JSONException;
//import org.codehaus.jettison.json.JSONObject;

import eu.ferari.optimizer.locationGraph.LGraphNode;
import eu.ferari.optimizer.locationGraph.LocationGraph;

public class GeometricEvent extends DerivedEvent {
	private LocationGraph lg;
//	private String name;
	private String functionName;
	private Path functionLocation;
	private Object monitoringObject;
	private ArrayList<DerivedEvent> deList;
	private ArrayList<LGraphNode> sites;
	private float defLSVValue;
	private String thresholdType;
	private int threshold;
	private boolean equalityInAboveThresholdRegion;
	private int resolveSteps;
	private ArrayList<Double> weightVectors;
	private String context;
	private String windowType;
	private double window;
	
	public GeometricEvent(String name) {
		super(name);
	}
	
	public GeometricEvent(String name, String functionName,
			Path functionLocation, Object monitoringObject,
			ArrayList<DerivedEvent> deList, ArrayList<LGraphNode> sites,
			float defLSVValue, String thresholdType, int threshold,
			boolean equalityInAboveThresholdRegion, int resolveSteps,
			ArrayList<Double> weightVectors, String context,String windowType,
			double window) {
		super.setName(name);
		this.functionName = functionName;
		this.functionLocation = functionLocation;
		this.monitoringObject = monitoringObject;
		this.deList = deList;
		this.sites = sites;
		this.defLSVValue = defLSVValue;
		this.thresholdType = thresholdType;
		this.threshold = threshold;
		this.equalityInAboveThresholdRegion = equalityInAboveThresholdRegion;
		this.resolveSteps = resolveSteps;
		this.weightVectors = weightVectors;
		this.context = context;
		this.windowType = windowType;
		this.window = window;
	}
	public String getName() {
		return super.getName();
	}
	public void setName(String name) {
		super.setName(name);
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public Path getFunctionLocation() {
		return functionLocation;
	}
	public void setFunctionLocation(Path functionLocation) {
		this.functionLocation = functionLocation;
	}
	public Object getMonitoringObject() {
		return monitoringObject;
	}
	public void setMonitoringObject(Object monitoringObject) {
		this.monitoringObject = monitoringObject;
	}
	public ArrayList<DerivedEvent> getDeList() {
		return deList;
	}
	public void setDeList(ArrayList<DerivedEvent> deList) {
		this.deList = deList;
	}
	public ArrayList<LGraphNode> getSites() {
		return sites;
	}
	public void setSites(ArrayList<LGraphNode> sites) {
		this.sites = sites;
	}
	public float getDefLSVValue() {
		return defLSVValue;
	}
	public void setDefLSVValue(float defLSVValue) {
		this.defLSVValue = defLSVValue;
	}
	public String getThresholdType() {
		return thresholdType;
	}
	public void setThresholdType(String thresholdType) {
		this.thresholdType = thresholdType;
	}
	public int getThreshold() {
		return threshold;
	}
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}
	public boolean isEqualityInAboveThresholdRegion() {
		return equalityInAboveThresholdRegion;
	}
	public void setEqualityInAboveThresholdRegion(
			boolean equalityInAboveThresholdRegion) {
		this.equalityInAboveThresholdRegion = equalityInAboveThresholdRegion;
	}
	public int getResolveSteps() {
		return resolveSteps;
	}
	public void setResolveSteps(int resolveSteps) {
		this.resolveSteps = resolveSteps;
	}
	public ArrayList<Double> getWeightVectors() {
		return weightVectors;
	}
	public void setWeightVectors(ArrayList<Double> weightVectors) {
		this.weightVectors = weightVectors;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getWindowType(){
		return windowType;
	}
	public void setWindowType(String windowType){
		this.windowType = windowType;
	}
	public double getWindow(){
		return window;
	}
	public void setWindow(double window){
		this.window = window;
	}
	public String toString()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    PrintStream ps = new PrintStream(baos);
	    PrintStream old = System.out; // Save the old System.out!
	    System.setOut(ps);	// Tell Java to use your special stream
	    this.print(); 	// Print some output: goes to your special stream
	    System.out.flush();
	    System.setOut(old);	// Put things back
	    
	    return baos.toString();
		
	}
	public void print()
	{
		System.out.println("name="+super.getName());
		System.out.println("functionName="+this.functionName);
		System.out.println("functionLocation="+this.functionLocation);
		System.out.println("monitoringObject: "+this.monitoringObject);
		System.out.println("defLSVValue="+this.defLSVValue);
		System.out.println("thresholdType="+this.thresholdType);
		System.out.println("threshold="+this.threshold);
		System.out.println("equalityInAboveThresholdRegion="+ this.equalityInAboveThresholdRegion);
		System.out.println("resolveSteps="+this.resolveSteps);
		System.out.println("context="+this.context);
		System.out.print("DerivedEvent=");
		for(int j = 0; j < deList.size(); j++)
		{
			if(j!=deList.size()-1)
				System.out.print(" "+deList.get(j).getName()+",");
			else
				System.out.print(" "+deList.get(j).getName());
		}
		System.out.println("");
		System.out.print("SiteInvolved=");
		for(int j = 0; j < sites.size(); j++)
		{
			if(j!=sites.size()-1)
				System.out.print(" " +sites.get(j).location+",");
			else
				System.out.print(" " +sites.get(j).location);
		}
		System.out.println("");
		System.out.print("WeightVectors= ");
		for(int j = 0; j < weightVectors.size(); j++)
		{
			if(j!=weightVectors.size()-1)
				System.out.print(" "+weightVectors.get(j)+",");
			else
				System.out.print(" "+weightVectors.get(j));
		}
		System.out.println("");
	}
}
