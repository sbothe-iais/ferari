package eu.ferari.optimizer.event;


import java.util.Set;

import eu.ferari.optimizer.poplans.Plan;


public class DerivedEvent extends EventType
{
    public Operator derivationOperator;
    public boolean placeAtAllSites;
    public boolean breakOperator;
    public Plan plan;
    
    public DerivedEvent(String name)
    {
    	super(name);
    	breakOperator = false;
    	placeAtAllSites=false;
    	plan=null;
    }
    public DerivedEvent()
    {
    	this.breakOperator=false;
    	placeAtAllSites=false;
    	plan=null;
    }
    public DerivedEvent(Operator operator, String name, boolean placeAtAllSites)
    {
    	super(name, 0.0);
        this.derivationOperator = operator;
        this.setProbablity();
        this.placeAtAllSites = placeAtAllSites;
        this.breakOperator = false;
        plan=null;
    }
    public boolean isFilter()
    {
    	derivationOperator.isActuallyFilterOperator();
    	return derivationOperator.hasFilter(); 
    }
    public void setPlan(Plan p)
    {
    	this.plan=p;
    }
    public boolean hasPlan()
    {
    	if(plan!=null)
    		return true;
    	else
    		return false;
    }
    public void setUbiquitousPlacement(boolean everywhere)
    {
    	placeAtAllSites = everywhere;
    }
    
    public boolean getUbiquitousPlacement()
    {
    	return placeAtAllSites;
    }
    public void setManualProbability(double frequency)
    {
    	super.setprobability(frequency);
    }
    public void setProbablity()
    {  
    	double frequency=0.0;
    	if(derivationOperator.getsNaivePlan())
    		frequency=derivationOperator.getSumFrequency();
    	else
    		frequency=derivationOperator.getMinFrequency();
    	super.setprobability(frequency);
    }
    
    public Query returnQuery()
    {   
        Query newQuery = new Query(derivationOperator.getEventTypes(), derivationOperator.getOperator(), derivationOperator.getWindow());
         
        return newQuery;
    }
    public Set<EventType> getEvents()
    {
        return derivationOperator.getEventTypes();
    }
    public double getWindow()
    {
        return derivationOperator.getWindow();
    }
    public void setOperator(Operator op)
    {
    	this.derivationOperator = op;
    }
    @Override
    public void print(boolean attr)
    {
        System.out.print("D");
        super.print(attr);
        if(this.placeAtAllSites)
        	System.out.println("Placed Everywhere");
        if(this.derivationOperator!=null)
        {	
        	System.out.println("Derived By: ");
        	this.derivationOperator.print(attr);
        }
        
    }
}
