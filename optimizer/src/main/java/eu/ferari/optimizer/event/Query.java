package eu.ferari.optimizer.event;

import java.util.Iterator;
import java.util.Set;


public class Query {

    //public Operator basicOperator;
    public Operator operator;
    public double window;
    
    public Query(Set<EventType> es, String op, double w)
    {        
        this.window = w;    
        this.operator = new Operator(op, w, es);        
    }
    public Query(Operator oper, double w)
    {
        operator = oper;//new Operator(oper);
        window = w;
    }
    public Operator returnOperator()
    {
         return operator;    
    }
    
    public void print(boolean derived)
    {        
        System.out.print(operator.getOperator()+"(");
        Iterator<EventType> itr = operator.getEventTypes().iterator();
        
        while(itr.hasNext())
        {
            EventType e = itr.next();
            if(e instanceof DerivedEvent)
                ((DerivedEvent)e).returnQuery().print(true);
            else
            {
                if(itr.hasNext())
                    System.out.print("e:"+e.getName()+", ");
                else
                    System.out.print("e:"+e.getName());
            }
        }
        System.out.print(")");
        
        if(!derived)
        {    
            System.out.println("");
            System.out.println("WITHIN "+window+" sec");
        }
        else
            System.out.print(", ");
    }
}
