package eu.ferari.optimizer.event;


public class Attribute {
	String name;
	String description;
	String type;
	String dimension;
	String defaultvalue;
	
	public Attribute()
	{
		this.name=null;
		this.description = null;
		this.type=null;
		this.dimension=null;
		this.defaultvalue=null;
	}
	public void addAttribute(String other, String value)
	{
		if(other.equalsIgnoreCase("name"))
			this.name=value;
		else if(other.equalsIgnoreCase("description"))
			this.description=value;
		else if(other.equalsIgnoreCase("type"))
			this.type=value;
		else if(other.equalsIgnoreCase("dimension"))
			this.dimension = value;
		else if(other.equalsIgnoreCase("defaultvalue"))
			this.defaultvalue = value;			
	}
	
	public void print()
	{
		System.out.println("name "+ name);
		if(this.description!=null)
			System.out.println("description "+description);
		if(this.type!=null)
			System.out.println("type "+type);
		if(this.dimension!=null)
			System.out.println("dimension "+dimension);
		if(this.defaultvalue!=null)
			System.out.println("defaultvalue "+defaultvalue);
		System.out.println("");
	}
}
