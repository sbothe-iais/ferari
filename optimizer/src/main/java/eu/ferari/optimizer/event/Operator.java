package eu.ferari.optimizer.event;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


public class Operator 
{
    private String op;
    private double window;
    private Set<EventType> eventSet;
    private boolean filter;
    
    public Operator()
    {
    	eventSet = new LinkedHashSet<EventType>();
    	filter=false;
    }
    public Operator(String operator)
    {
    	op=identify(operator);
    	eventSet = new LinkedHashSet<EventType>();
    	filter=false;
    }
    public Operator(String op, double window, Set<EventType> events)
    {
        this.op = identify(op);
        this.window = window;
        this.eventSet =  events;//new LinkedHashSet(events);
        filter=false;
    }
    public Operator(Operator oldOperator)
    {
        this.op = oldOperator.op;
        this.window = oldOperator.window;
        this.eventSet = oldOperator.eventSet;//new LinkedHashSet(oldOperator.eventSet);
        this.filter=oldOperator.hasFilter();
    }
    public boolean hasFilter()
    {
    	return filter;
    }
    public void setFilter()
    {
    	this.filter  = true;
    }
    public void isActuallyFilterOperator()
    {
    	if(!containsDerivedEvents() && op.equalsIgnoreCase("FILTER") && this.filter)
    		this.filter = true;
    	else
    		this.filter = false;
    }
    public void removeEvents()
    {
    	Iterator<EventType> itr = eventSet.iterator();
    	while(itr.hasNext())
    	{
    		itr.next();
    		itr.remove();
    	}
    }
    public void addEvents(EventType e)
    {
    	eventSet.add(e);
    }
    public Set<EventType> getEventTypes()
    {
        return eventSet;
    }
    public void setOperator(String operator)
    {
    	this.op = identify(operator);
    }
    public void setWindow(double w)
    {
    	window = w;
    }
    public boolean containsDerivedEvents()
    {
    	for(EventType e:eventSet)
    		if(e instanceof DerivedEvent)
    			return true;
    	
    	return false;
    }
    public String getOperator()
    {
        return op;
    }
    
    public double getWindow()
    {
        return window;
    }
    public double getSumFrequency()
    {
    	double frequency = 0;
    	for(EventType e: eventSet)
        {
    		frequency += e.getprobability();
        }
    	return frequency;
    }
    public double getMinFrequency()
    {
    	double frequency = 0;
    	for(EventType e: eventSet)
        {
    		if(frequency==0)
    			frequency = e.getprobability();
    		else if(frequency>e.getprobability())
    			frequency = e.getprobability();    			
        }
    	return frequency;
    }
    public boolean isTheSame(Operator oper)
    {
        if(oper.op.equals(this.op))
            return true;
        else
            return false;
                    
    }
    
    public boolean isTheSame(String oper)
    {
        if(this.op.equals(identify(oper)))
            return true;
        else
            return false;
                    
    }
    
    public boolean getsNaivePlan()
    {
    	if(op.compareToIgnoreCase("OR")==0 ||  op.compareToIgnoreCase("AGGREGATE")==0 || op.compareToIgnoreCase("FILTER")==0)
    		return true;
    	else
    		return false;
    }
    
    public String identify(String op)
    {
        if(op.compareToIgnoreCase("and")==0 || op.compareToIgnoreCase("all")==0 || op.compareToIgnoreCase("conjunction")==0)
            return "AND";
        else if(op.compareToIgnoreCase("or")==0 || op.compareToIgnoreCase("any")==0 || op.compareToIgnoreCase("disjunction")==0)
            return "OR";
        else if(op.compareToIgnoreCase("seq")==0 || op.compareToIgnoreCase("sequence")==0 || op.compareToIgnoreCase("next")==0)
            return "SEQ";
        else if(op.compareToIgnoreCase("not")==0 || op.compareToIgnoreCase("negation")==0 || op.compareToIgnoreCase("absence")==0)
            return "NOT";
        else if(op.compareToIgnoreCase("Aggregate")==0)
            return "AGGREGATE";
        else if(op.compareToIgnoreCase("Basic")==0)
            return "FILTER";
        else if(op.compareToIgnoreCase("gm")==0)
            return "GM";
        else 
            return "UNKNOWN";
    }
     public Set<EventType> subSet(Set<EventType> S, Set<EventType> s)
    {
        Set<EventType> newSet = new LinkedHashSet<EventType>();
        boolean include = false;
        
        for(EventType allEventTypes: S)
        {
            for(EventType removingEventTypes: s)
            {
                if(allEventTypes==removingEventTypes)
                {
                    include = true; 
                    break;
                }
                else
                {
                    include = false;
                }
            }
            if(include)
                newSet.add(allEventTypes);
        }
        
        return newSet;
    }
    
    public Set<EventType> eventsContainedIn(Operator oper, boolean relativeComp)
    {
        boolean contains= false;
        
        if(oper.eventSet.equals(this.eventSet))
            return null;
        
        for(EventType allEventTypes: oper.eventSet)
        {
            for(EventType removingEventTypes: eventSet)
            {                
                if(allEventTypes==removingEventTypes)
                {
                    contains = true;
                    break;
                }
                else
                {
                    contains = false;
                }
            }
            if(!contains || !oper.op.equals(this.op))
                return null;
        }
        if(relativeComp)
            return relativeComplement(eventSet, oper.eventSet);
        else
            return subSet(eventSet, oper.eventSet);
    }
    
    private Set<EventType> relativeComplement(Set<EventType> S, Set<EventType> s)
    {
        Set<EventType> newSet = new LinkedHashSet<EventType>();
        boolean include = false;
        
        for(EventType allEventTypes: S)
        {
            for(EventType removingEventTypes: s)
            {
                if(allEventTypes==removingEventTypes)
                {
                    include = false;
                    break;
                }
                else
                    include = true;
            }
            if(include)
                newSet.add(allEventTypes);
        }
        
        return newSet;
    }
    public void print(boolean attr)
    {
        System.out.println("Operator: "+op+ " Window: "+window);
        for(EventType e: eventSet)
            e.print(attr);
    }
}
