package eu.ferari.optimizer.configuration;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

public class GMconfig {

	int globalThreshold;
	int numBillingNodes;
	public String nodeId;
	boolean equalityInAboveThresholdRegion;
	float defLSVValue;
	boolean dynamicThreshold;
	String functionName;
	Path functionLocation;

	public GMconfig(int globalThreshold, int numBillingNodes, String nodeId,
			boolean equalityInAboveThresholdRegion, float defLSVValue,
			boolean dynamicThreshold, String functionName, Path functionLocation) {
		this.globalThreshold = globalThreshold;
		this.numBillingNodes = numBillingNodes;
		this.nodeId = nodeId;
		this.equalityInAboveThresholdRegion = equalityInAboveThresholdRegion;
		this.defLSVValue = defLSVValue;
		this.dynamicThreshold = dynamicThreshold;
		this.functionName = functionName;
		this.functionLocation = functionLocation;
	}

	public void write(OutputStream out) throws IOException {
		String gt = "globalThreshold=" + globalThreshold;
		out.write(gt.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String n = "numBillingNodes=" + numBillingNodes;
		out.write(n.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String id = "nodeId=" + nodeId;
		out.write(id.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String e = "equalityInAboveThresholdRegion="
				+ equalityInAboveThresholdRegion;
		out.write(e.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String def = "defLSVValue=" + defLSVValue + "f";
		out.write(def.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String dt = "dynamicThreshold=" + dynamicThreshold;
		out.write(dt.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String fn = "functionName=" + functionName;
		out.write(fn.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String fl = "functionLocation=" + functionLocation;
		out.write(fl.getBytes());
	}
}
