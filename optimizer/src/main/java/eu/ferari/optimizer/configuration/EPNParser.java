package eu.ferari.optimizer.configuration;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;
import org.json.*;

import eu.ferari.optimizer.poplans.*;
import eu.ferari.optimizer.event.*;
import eu.ferari.optimizer.locationGraph.*;

public class EPNParser {

	String jsonfile;
	JSONObject json;
	JSONArray eventsArray;
	JSONArray gmArray;
	JSONArray gmlArray;
	ArrayList<JSONObject> halfEpasArray;
	JSONArray epasArray;
	JSONObject contexts;
	JSONArray consumers;
	JSONArray producers;
	LocationGraph lg;
	HashMap<String, EventType> events;
	HashMap<String, DerivedEvent> derivedEvents;
	public HashMap<String, DerivedEvent> brokenHalfEvents;
	public HashMap<String, String> jsonFiles;
	boolean rewrite;
	boolean reorder;
	boolean optimizeLatency;
	double latencyWeight;
	boolean setCoordinators=false;
	DerivedEvent rootEvent;
	HashMap<ArrayList<String>,ArrayList<DerivedEvent>> commonAggregationFilters;
	HashMap<DerivedEvent, ArrayList<String>> eventFilters; 
	String segExpression;
	
	public EPNParser(String jsonfile, LocationGraph lg) {
		this.jsonfile = jsonfile;
		this.lg = lg;
		this.events = new HashMap<String, EventType>();
		this.derivedEvents = new HashMap<String, DerivedEvent>();
		this.brokenHalfEvents = new HashMap<String, DerivedEvent>();
		this.commonAggregationFilters = new HashMap<ArrayList<String>, ArrayList<DerivedEvent>>();
		this.eventFilters = new HashMap<DerivedEvent, ArrayList<String>>();
		this.jsonFiles = new HashMap<String, String>();
	}

	public HashMap<String, EventType> getEvents()
	{
		return events;
	}
	public void parseEPN(String jsontxt) throws JSONException {

		if(this.jsonfile==null)
		{

			JSONTokener jtoken = new JSONTokener(jsontxt);
			json = new JSONObject(jtoken);
		}
		else
			json = new JSONObject(new JSONTokener(readJsonFile(null)));
		// get the root object

		JSONObject root = (JSONObject) json.get("epn");

		if (root == null) {
			System.err.println("ERROR");
		}

		if(root.has("optimization"))
		{
			if(!root.getJSONObject("optimization").keySet().isEmpty())
			{
//				System.out.println("Optimize For:");
//				System.out.println("-> rewrite: "+root.getJSONObject("optimization").getBoolean("rewrite"));
//				System.out.println("-> reorder: "+root.getJSONObject("optimization").getBoolean("reorder"));
//				System.out.println("-> maxLatency: "+root.getJSONObject("optimization").getDouble("latency"));
//				System.out.println("-> maxCost: "+root.getJSONObject("optimization").getDouble("cost"));
//				System.out.println();
				rewrite = root.getJSONObject("optimization").getBoolean("rewrite");
				reorder = root.getJSONObject("optimization").getBoolean("reorder");
				latencyWeight = root.getJSONObject("optimization").getDouble("latency");
				if(latencyWeight>=0.5)
				{
					optimizeLatency=true;
				}
				else
				{
					optimizeLatency=false;
				}
			}
			else
			{
				System.out.println("**********NO OPTIMIZATION PARAMETERS SELECTED**********");
			}
		}
		else
		{
			System.out.println("**********NO OPTIMIZATION PARAMETERS SELECTED**********");
		}
		if(root.has("gm"))
		{	
			gmArray = (JSONArray) root.get("gm");
			parseGM();
		}
		else
		{
			System.out.println("**********NO GM SPECIFIED**********");
		}
		if(root.has("gml"))
		{
			gmlArray = (JSONArray) root.get("gml");
		}
		else
		{
			System.out.println("**********NO GML SPECIFIED**********");
		}
		eventsArray = (JSONArray) root.get("events");
		//actionsArray = (JSONArray) root.get("actions");
		epasArray = root.getJSONArray("epas");
		contexts =  root.getJSONObject("contexts");
		consumers =  root.getJSONArray("consumers");
		producers =  root.getJSONArray("producers");

		if(root.has("name"))
		{
			rootEvent = new DerivedEvent(root.get("name").toString());
		}
		parseEvents();
		parseEPAs(epasArray);
		this.segExpression = this.getSegmantationExpression();
		rewriteAggregationOperators();
		//breakAggregationOperators();
		initializeNetworkStructures();
	}

	private void rewriteAggregationOperators() {

		Set<String> common=null;
		do{
			common = getCommonElements(eventFilters.values());
			//System.out.println("---------COMMON--------------->"+eventFilters.values().size());
			for(String s:common)
				System.out.println(s);
			Iterator<Entry<DerivedEvent, ArrayList<String>>> itr = eventFilters.entrySet().iterator();
			while(itr.hasNext())
			{
				Entry<DerivedEvent, ArrayList<String>> curr = itr.next();

				ArrayList<String> list = curr.getValue();
				for(String s:common)
				{
					list.remove(s);
				}
				if(list.isEmpty())
				{
					System.out.println("remove all from: "+curr.getKey().getName());
					itr.remove();
					if(commonAggregationFilters.containsKey(new ArrayList<String>(common)))
					{
						commonAggregationFilters.get(new ArrayList<String>(common)).add(curr.getKey());
					}
					else
					{
						ArrayList<DerivedEvent> de = new ArrayList<DerivedEvent>();
						de.add(curr.getKey());
						commonAggregationFilters.put(new ArrayList<String>(common),de);
					}
				}
			}
		}while(!common.isEmpty());

	}
	//	public void setFilterEPAOccTime(String timeAttribute)
	//	{
	//		for(DerivedEvent de : this.derivedEvents.values())
	//		{
	//			if(de.isFilter())
	//			{
	//				Iterator<Object> itr = epasArray.iterator();
	//				while(itr.hasNext())
	//				{
	//					JSONObject epa = (JSONObject)itr.next();
	//					JSONArray derev = epa.getJSONArray("derivedEvents");
	//					for (int j = 0; j < derev.length(); j++) 
	//					{
	//						JSONObject derEv = derev.getJSONObject(j);
	//						String deName = derEv.get("name").toString();
	//						if(deName==de.getName())
	//						{ 	
	//							if(epa.has("localPlacement"))		//remove annotations from final json files
	//								epa.remove("localPlacement");	
	//							if(epa.has("confidence"))
	//								epa.remove("confidence");
	//
	//							JSONObject expressions = derEv.getJSONObject("expressions");
	//							ArrayList<EventType> input = new ArrayList<EventType>(de.derivationOperator.getEventTypes());
	//							String eventname=null;
	//							if(input.size()==1)
	//								eventname=input.get(0).getName();
	//							else
	//								System.err.println("ERROR IN ASSIGNING FILTER EPA OCC-TIME");
	//							expressions.put("OccurrenceTime", eventname+"."+timeAttribute);
	//						}
	//					}
	//				}
	//			}
	//			//			else
	//			//			{
	//			//				Iterator<Object> itr = epasArray.iterator();
	//			//				while(itr.hasNext())
	//			//				{
	//			//					JSONObject epa = (JSONObject)itr.next();
	//			//					JSONArray derev = epa.getJSONArray("derivedEvents");
	//			//					for (int j = 0; j < derev.length(); j++) 
	//			//					{
	//			//						JSONObject derEv = derev.getJSONObject(j);
	//			//						String deName = derEv.get("name").toString();
	//			//						if(deName==de.getName())
	//			//						{ 	
	//			//							if(epa.has("localPlacement"))		//remove annotations from final json files
	//			//								epa.remove("localPlacement");	
	//			//							if(epa.has("confidence"))
	//			//								epa.remove("confidence");
	//			//							
	//			//							JSONObject expressions = derEv.getJSONObject("expressions");
	//			//							
	//			//							expressions.put("OccurrenceTime", "context.DetectionTime");
	//			//						}
	//			//					}
	//			//				}
	//			//			}
	//		}
	//	}
	public void breakAggregationOperators()
	{
		//this.segExpression = this.getSegmantationExpression();
		halfEpasArray = new ArrayList<JSONObject>();
		for(DerivedEvent de : this.derivedEvents.values())
		{
			if(de.breakOperator)
			{
				//System.out.println("BREAKING DERIVED EVENT: "+de.getName());
				JSONObject halfEpa = new JSONObject();
				DerivedEvent halfDe = new DerivedEvent();
				Iterator<Object> itr = epasArray.iterator();
				while(itr.hasNext())
				{
					JSONObject epa = (JSONObject)itr.next();
					JSONArray derev = epa.getJSONArray("derivedEvents");
					for (int j = 0; j < derev.length(); j++) 
					{
						JSONObject derEv = derev.getJSONObject(j);
						String deName = derEv.get("name").toString();
						if(deName==de.getName())
						{ 	
							if(epa.has("localPlacement"))		//remove annotations from final json files
								epa.remove("localPlacement");	
							if(epa.has("confidence"))
								epa.remove("confidence");

							halfDe.setName("half"+de.getName());
							halfEpa.put("name", halfDe.getName()+"EPA");
							halfEpa.put("epaType", "Basic");
							Operator op = new Operator("Basic");
							halfEpa.put("context", "Always");
							op.setWindow(Double.MAX_VALUE);
							JSONObject always = new JSONObject();
							always.put("name", "Always");
							always.put("type", "TemporalInterval");
							always.put("atStartup", true);
							always.put("neverEnding", true);
							always.put("initiators",new JSONArray());
							always.put("terminators",new JSONArray());
						//	System.out.println(always);
							JSONArray temporalContexts = contexts.getJSONArray("temporal");
							Iterator<Object> tempItr = temporalContexts.iterator();
							boolean insert=true;
							while(tempItr.hasNext())
							{
								JSONObject temp = (JSONObject)tempItr.next();
								if(temp.getString("name").equalsIgnoreCase("Always"))
									insert=false;
							}
							if(insert)
								temporalContexts.put(always);
							

							de.derivationOperator.removeEvents();
							de.derivationOperator.addEvents(halfDe);
							de.setUbiquitousPlacement(false);
							de.derivationOperator.isActuallyFilterOperator();

							halfDe.setUbiquitousPlacement(true);
							op.setFilter();
							//							halfDe.derivationOperator.setFilter();

							JSONArray inev = new JSONArray();
							inev = epa.getJSONArray("inputEvents");
							halfEpa.put("inputEvents", inev);
							JSONObject input = (JSONObject) inev.get(0);
							input.put("consumptionPolicy", "Consume");
				String inputEvent = input.getString("name");

							JSONArray attributes = null;
							String eventname=null;
							for (int l = 0; l < inev.length(); l++) {
								JSONObject inEv = inev.getJSONObject(l);
								eventname = inEv.get("name").toString();
								op.addEvents(events.get(eventname));
								if(lg.etypes.get(eventname)!=null)
									halfDe.setprobability(lg.etypes.get(eventname).getprobability());
								for(int k=0;k<eventsArray.length();k++)
								{
									JSONObject event = eventsArray.getJSONObject(k);
									if(event.get("name").toString().equalsIgnoreCase(eventname))
									{
										JSONObject newEvent = new JSONObject(event);
										newEvent.put("name", halfDe.getName());
										newEvent.put("attributes", new JSONArray(event.get("attributes").toString()));
										eventsArray.put(newEvent);
										attributes = (JSONArray)newEvent.get("attributes");

									}
								}
							}
							halfDe.setOperator(op);
							lg.addDerivedEventAsLocal(halfDe);
							JSONObject devent = new JSONObject();
							devent.put("name", halfDe.getName());
							devent.put("reportParticipants", false);
							JSONObject expressions = new JSONObject();
							Iterator<Object> iter = attributes.iterator();
							while(iter.hasNext())
							{
								JSONObject attr = (JSONObject) iter.next();
								if(!attr.get("name").toString().equals("object_id") && 
										!attr.get("name").toString().equals("Duration") && 
										!attr.get("name").toString().equals("ExpirationTime") && 
										!attr.get("name").toString().equals("Cost") && 
										!attr.get("name").toString().equals("OccurrenceTime")&&
										!attr.get("name").toString().equals("Certainty"))
									expressions.put(attr.get("name").toString(), eventname+"."+attr.get("name").toString());

								//expressions.put("OccurrenceTime", eventname+".call_start_date");
							}
							devent.put("expressions", expressions);
							JSONArray derived = new JSONArray();
							derived.put(devent);
							halfEpa.put("derivedEvents", derived);
							halfEpa.put("computedVariables", new JSONArray());
							halfEpa.put("evaluationPolicy", "Immediate");
							halfEpa.put("cardinalityPolicy", "Single");
							halfEpa.put("internalSegmentation", new JSONArray());

							JSONArray epain = new JSONArray();
							JSONObject newInEv = new JSONObject();
							newInEv.put("name", halfDe.getName());
							newInEv.put("consumptionPolicy", "Reuse");
							newInEv.put("instanceSelectionPolicy", "Override");
							epain.put(newInEv);
							epa.put("inputEvents", epain);

							JSONArray epaout = (JSONArray) epa.get("derivedEvents");
							for(int i=0;i<epaout.length();i++)
							{
								JSONObject newExpres = (JSONObject) epaout.get(i);
								JSONObject newArray = (JSONObject) newExpres.get("expressions");
								Iterator<String> itrEx = newArray.keys();
								while(itrEx.hasNext())
								{
									String nextKey = itrEx.next();
									String nextValue = newArray.getString(nextKey);
//									System.out.println(nextKey+" "+nextValue);
									if(newArray.getString(nextKey).contains(inputEvent))
									{
										newArray.put(nextKey, nextValue.replaceAll(inputEvent, halfDe.getName()));
									}
								}
//								String ex = newArray.getString("call_start_dates");
//								newArray.put("call_start_dates", ex.replaceAll("CallPOPDWH", halfDe.getName()));
							}

							JSONArray comp = (JSONArray) epa.get("computedVariables");
							for(int i=0;i<comp.length();i++)
							{
								JSONObject newExpres = (JSONObject) comp.get(i);
//								System.out.println(newExpres);
								if(newExpres.has(inputEvent))
								{
									String value = newExpres.getString(inputEvent);
									newExpres.remove(inputEvent);
//									String newValue=null;
//									if(value.contains("CallPOPDWH"))
									String	newValue = value.replace(inputEvent, halfDe.getName());
//									else

//										newValue = value;
									newExpres.put(halfDe.getName(), newValue);
								}
//								System.out.println(newExpres);
							}
							JSONObject newSegContext =new JSONObject();
							newSegContext.put("name", halfDe.getName());
							newSegContext.put("expression", halfDe.getName()+"."+this.segExpression);

							JSONArray seg= (JSONArray) this.contexts.get("segmentation");
							JSONObject partEv = (JSONObject) seg.get(0);
							JSONArray pev = (JSONArray) partEv.get("participantEvents");
							pev.put(newSegContext);

							ArrayList<String> context = new ArrayList<String>();
							HashMap<String, JSONObject> insertedContexts = new HashMap<String, JSONObject>();
							HashMap<String, String> contextType = new HashMap<String, String>();
							context.add(epa.getString("context"));

							//					        System.out.println(context.toString());
							getWindowObject(context, insertedContexts, contextType);
//							System.out.println(insertedContexts.get(epa.getString("context")));
							JSONArray temp;
							JSONObject temporal;
							//System.out.println(insertedContexts.containsKey("Always"));
							if(insertedContexts.get(epa.getString("context")).has("temporalContexts"))
							{
								temp = (JSONArray) insertedContexts.get(epa.getString("context")).get("temporalContexts");
								temporal = (JSONObject) temp.get(0);
							}
							else
							{
								temporal = insertedContexts.get(epa.getString("context"));
							}
							insertedContexts.get(temporal.get("name"));
							JSONObject act = insertedContexts.get(temporal.get("name"));
							//System.out.println(act);
							JSONArray init = (JSONArray) act.get("initiators");
							if(init.length()>0) {
								JSONObject in = (JSONObject) init.get(0);
								in.put("name", halfDe.getName());
								if (in.has("condition"))
									in.put("condition", in.getString("condition").replace(inputEvent, halfDe.getName()));
								//					        System.out.println(in.get("name"));
							}
							this.brokenHalfEvents.put(halfDe.getName(), halfDe);
							halfEpasArray.add(halfEpa);
							break;
						}
					}
				}
			}
			else if(de.isFilter())
			{
				lg.addDerivedEventAsLocal(de);
			}
		}
		for(JSONObject hepa: halfEpasArray)
			epasArray.put(hepa);
		derivedEvents.putAll(brokenHalfEvents);

	}
	public Set<String> getCommonElements(Collection<? extends Collection<String>> collections) {

		Set<String> common = new LinkedHashSet<String>();
		if (!collections.isEmpty()) {
			Iterator<? extends Collection<String>> iterator = collections.iterator();
			common.addAll(iterator.next());
			while (iterator.hasNext()) {
				common.retainAll(iterator.next());
			}
		}
		return common;
	}
	public ArrayList<Set<String>> getUniqueElements(Collection<? extends Collection<String>> collections) {

		ArrayList<Set<String>> allUniqueSets = new ArrayList<Set<String>>();
		for (Collection<String> collection : collections) {
			Set<String> unique = new LinkedHashSet<String>(collection);
			allUniqueSets.add(unique);
			for (Collection<String> otherCollection : collections) {
				if (collection != otherCollection) {
					unique.removeAll(otherCollection);
				}
			}
		}

		return allUniqueSets;
	}

	public String writeEpn(String outputFolder, String location, String inputDataFileName, ArrayList<DerivedEvent> DerivedEvents, HashMap<String, ArrayList<String>> eventsToPushToCoordinators) throws IOException, JSONException
	{
		//this.breakAggregationOperators();
		File file = new File(outputFolder+location+".json");
		file.getParentFile().mkdirs();
		FileWriter writer = new FileWriter(file);
		//Writer writer = new FileWriter(outputFolder+location+".json");
		JSONObject outjson = new JSONObject();
		JSONObject epn = new JSONObject();
		JSONObject insertContext = new JSONObject();
		JSONArray involvedEvents = new JSONArray();
		JSONArray involvedEpas = new JSONArray();
		JSONArray involvedContexts = new JSONArray();
		HashMap<String, JSONObject> inserted = new HashMap<String, JSONObject>();
		HashMap<String, JSONObject> insertedContexts = new HashMap<String, JSONObject>();
		HashMap<String, String> contextType = new HashMap<String, String>();
		ArrayList<String> contexts = new ArrayList<String>();
//System.out.println();
//System.out.println(location);

		if(lg.getGraphNode(lg.getGraphNodeIndex(location)).rootOperatorSite)
		{
			for(Plan rp:lg.getGraphNode(lg.getGraphNodeIndex(location)).assignedPlans)
			{
				if(rp.rootplan)
				{
					//System.out.println(location);
					for(Step s: rp.steps)
					{
						for(EventType e:s.events)
						{
							for(int k=0;k<eventsArray.length();k++) 
							{
								JSONObject event = eventsArray.getJSONObject(k);
								if(event.get("name").toString().equalsIgnoreCase(e.getName()))
								{
									involvedEvents.put(event);
									inserted.put(event.getString("name"), event);
								}
							}
						}
					}
				}
			}
		}
		for(DerivedEvent de : DerivedEvents)
		{
			for(int i=0;i<epasArray.length();i++)
			{
				JSONObject epa = epasArray.getJSONObject(i);
				JSONArray derev = epa.getJSONArray("derivedEvents");
				for (int j = 0; j < derev.length(); j++) {
					JSONObject derEv = derev.getJSONObject(j);
					String deName = derEv.get("name").toString();
					if(deName==de.getName())
					{ 	
						if(epa.has("localPlacement"))		//remove annotations from final json files
							epa.remove("localPlacement");	
						if(epa.has("confidence"))
							epa.remove("confidence");

						if(!de.getUbiquitousPlacement() && !de.derivationOperator.getsNaivePlan())//EPA REWRITING AND REORDING BASED ON SELECTED PLAN
						{
							if(de.plan.steps.size()>1)
							{	
								HashMap<String, Boolean> initiatorsToAdd = new HashMap<String, Boolean>();

								HashMap<Integer, ArrayList<String>> eventsPerState = new HashMap<Integer, ArrayList<String>>();
								HashMap<Integer, ArrayList<Integer>> eventsPerStateIndexes = new HashMap<Integer, ArrayList<Integer>>();
								HashMap<String, String> expressions = new HashMap<String, String>();
								String seqassertion = "";
								for(int s=0;s<de.plan.steps.size();s++)
								{

									///////////////////////////SEQUENCE REWRITING///////////////////////////////

									if(de.derivationOperator.isTheSame("seq"))
									{
										ArrayList<String> events = new ArrayList<String>();
										for(EventType pe: de.plan.steps.get(s).events)
										{
											events.add(pe.getName());
										}
										if(!events.isEmpty())
											eventsPerState.put(s, events);
										ArrayList<Integer> stateIndexes = new ArrayList<Integer>();
										for(String ev:eventsPerState.get(s))
										{	
											int seqIndex=0;
											for(EventType e:de.derivationOperator.getEventTypes())
											{
												seqIndex++;
//												System.out.println(e.getName()+" "+seqIndex);
												if(ev.equalsIgnoreCase(e.getName()))
												{
													stateIndexes.add(seqIndex);
												}
											}
										}
										if(!stateIndexes.isEmpty())
											eventsPerStateIndexes.put(s, stateIndexes);
										ArrayList<Integer> eventsInvolved = new ArrayList<Integer>();
										seqassertion="";
										ArrayList<String> sortedEvents = new ArrayList<String>();
										expressions.clear();
										for(int l=0;l<eventsPerState.size();l++)
										{
											for(int k=0;k<eventsPerState.get(l).size();k++)
											{

												if(l==s)
												{
													
													if(s<de.plan.steps.size()-1)
													{
														expressions.put("value"+eventsPerStateIndexes.get(l).get(k), eventsPerState.get(l).get(k)+".OccurrenceTime");
														if(l-1>=0)
														{
															for(int f=l-1;f>=0;f--)
															{			
																expressions.put("value"+eventsPerStateIndexes.get(f).get(k), de.getName()+"_st"+(l)+(l+1)+".value"+eventsPerStateIndexes.get(f).get(k));
															}
														}
													}
													
													Collections.sort(eventsInvolved);
													boolean insertedEvent=false;
													for(int f=0;f<eventsInvolved.size();f++)
													{
														int ev = eventsInvolved.get(f);
														if(ev<eventsPerStateIndexes.get(l).get(k))
														{
															if(sortedEvents.contains(de.getName()+"_st"+(l)+(l+1)+".value"+ev))
																sortedEvents.remove(de.getName()+"_st"+(l)+(l+1)+".value"+ev);
															sortedEvents.add(de.getName()+"_st"+(l)+(l+1)+".value"+ev);
														}
														if(ev>eventsPerStateIndexes.get(l).get(k) && !insertedEvent)
														{
															sortedEvents.add(eventsPerState.get(l).get(k)+".OccurrenceTime");
															if(sortedEvents.contains(de.getName()+"_st"+(l)+(l+1)+".value"+ev))
																sortedEvents.remove(de.getName()+"_st"+(l)+(l+1)+".value"+ev);
															sortedEvents.add(de.getName()+"_st"+(l)+(l+1)+".value"+ev);
															insertedEvent=true;
														}
														if(ev>eventsPerStateIndexes.get(l).get(k) && insertedEvent)
														{
															if(!sortedEvents.contains(de.getName()+"_st"+(l)+(l+1)+".value"+ev))
																sortedEvents.add(de.getName()+"_st"+(l)+(l+1)+".value"+ev);
														}

													}
													if(!insertedEvent)
														sortedEvents.add(eventsPerState.get(l).get(k)+".OccurrenceTime");
												}
												else
												{
													eventsInvolved.add(eventsPerStateIndexes.get(l).get(k));
												}
											}
										}
										if(s>0)
										{
											for(int f=0;f<sortedEvents.size();f++)
											{
												if(f==0)
													seqassertion+=("(( ");
												String ev=sortedEvents.get(f);
												seqassertion+=ev;
												if(f!=sortedEvents.size()-1)
													seqassertion+=" <= ";
												else
													seqassertion+=(" ) && ");
											}
											seqassertion+="( "+sortedEvents.get(sortedEvents.size()-1)+" - "+sortedEvents.get(0)+" <= "+de.getWindow()*1000+" ))";
//											System.out.println(seqassertion);
										}
									}
//									for(String ex: expressions.keySet())
//										System.out.println(ex+" "+ expressions.get(ex));
//									System.out.println(seqassertion);
									//////////////////////////////////////////////////////////
									ArrayList<EventType> elist = new ArrayList<EventType>(de.plan.steps.get(s).events);

									JSONObject splittedEpa = new JSONObject(epa.toString());
									JSONArray inev = splittedEpa.getJSONArray("inputEvents");

									for(int e=0;e<inev.length();e++) //REMOVE INPUT EVENTS FROM NEW STATE EPA
									{
										boolean keep=false;
										JSONObject inEv = inev.getJSONObject(e);
										String eventname = inEv.get("name").toString();

										for(int ie=0;ie<elist.size();ie++)
											if(eventname.equalsIgnoreCase(elist.get(ie).getName()))
												keep=true;
										if(!keep)
										{	
											inev.remove(e);
											e--;
										}
									}

									if(s!=0)	//NEW EPA'S INPUT EVENTS
									{	
										JSONObject newInEvent = new JSONObject();
										newInEvent.put("consumptionPolicy", "Consume");
										newInEvent.put("name",de.getName()+"_st"+(s)+(s+1));
										newInEvent.put("instanceSelectionPolicy", "Override");
										inev.put(newInEvent);
									}

									if(s!=0) //ASSERTION
									{
										String assertion="";
										if(de.derivationOperator.isTheSame("seq"))
										{
											assertion += seqassertion;
										}
										else
										{
											double window=this.getWindowFromTemporal(splittedEpa.getString("context"));
											ArrayList<String> inputEvents = new ArrayList<String>();
											for(int e=0;e<inev.length();e++)	
											{
												JSONObject inEv = inev.getJSONObject(e);
												String eventname = inEv.get("name").toString();
												if(!eventname.contains(de.getName()+"_st"+(s)+(s+1)))
													inputEvents.add(eventname);
											}
											for(int e=0;e<inputEvents.size();e++)
											{
												if(e!=0)
													assertion+=" && "; 
												assertion += de.getName()+"_st"+(s)+(s+1)+".maxOccTime-"+window+"<=";
												assertion+=inputEvents.get(e)+".OccurrenceTime";
												assertion+="<="+de.getName()+"_st"+(s)+(s+1)+".minOccTime+"+window;
											}
										}
										
										
										splittedEpa.put("assertion", assertion);
									}

									///////Split context per new epa/state//////////
									ArrayList<String> context = new ArrayList<String>();
									context.add(epa.getString("context"));
									getWindowObject(context, insertedContexts, contextType);
									//System.out.println(insertedContexts);		

									JSONArray temp=null;
									JSONObject temporal=null;
									if(insertedContexts.get(epa.getString("context")).has("temporalContexts"))
									{
										temp = (JSONArray) insertedContexts.get(epa.getString("context")).get("temporalContexts");
										temporal = (JSONObject) temp.get(0);
									}
									else
									{
										JSONObject temp1= insertedContexts.get(epa.getString("context"));//.get(epa.getString("context"));
										temporal = temp1;
										//System.out.println("NOT TEMPORAL CONTEXT");
									}
									//JSONObject temporal = (JSONObject) temp.get(0);
									insertedContexts.get(temporal.get("name"));
									JSONObject act = insertedContexts.get(temporal.get("name"));
									JSONArray init = (JSONArray) act.get("initiators");

									JSONObject newAct = new JSONObject(insertedContexts.get(temporal.get("name")).toString());
									newAct.put("name", temporal.get("name")+"_State_"+(s+1));
									JSONArray newInit = new JSONArray();//(JSONArray) act.get("initiators");
									newAct.remove("initiators");
									for(int e=0;e<inev.length();e++)	
									{
										JSONObject inEv = inev.getJSONObject(e);
										String eventname = inEv.get("name").toString();
										JSONObject newInitiator = new JSONObject(init.get(0).toString());
										newInitiator.put("name", eventname);
										newInit.put(newInitiator);

									}
									newAct.put("initiators", newInit);
									JSONObject newComp = new JSONObject(insertedContexts.get(epa.getString("context")).toString());
									newComp.put("name", newComp.get("name").toString()+"_State_"+(s+1));
									JSONArray newTemp=null;
									JSONObject newTemporalName=null;
									String name;
									if(newComp.has("temporalContexts"))
									{
										newTemp = (JSONArray)newComp.getJSONArray("temporalContexts");
										newTemporalName = newTemp.getJSONObject(0);
										name = newTemporalName.get("name").toString();
									}
									else
									{
										name = epa.getString("context").toString();
										newTemporalName = new JSONObject();
									}
									JSONObject newSegContext=null;
									int counter=0;
									if(newComp.has("segmentationContexts"))
									{
										
										String segName =(newComp.getJSONArray("segmentationContexts")).getJSONObject(0).get("name").toString()+"_State_"+(s+1); 
										(newComp.getJSONArray("segmentationContexts")).getJSONObject(0).put("name", segName);
										newSegContext = new JSONObject((newComp.getJSONArray("segmentationContexts")).getJSONObject(0));
										
										
										ArrayList<String> inputEvents = new ArrayList<String>();
										for(int e=0;e<inev.length();e++)	
										{
											JSONObject inEv = inev.getJSONObject(e);
											String eventname = inEv.get("name").toString();
											//if(!eventname.contains(de.getName()+"_st"+(s)+(s+1)))
												inputEvents.add(eventname);
										}
										JSONArray segeventlist=new JSONArray();
										for(int e=0;e<inputEvents.size();e++)
										{
											//System.out.println(inputEvents.get(e));
											JSONObject segevent = new JSONObject();
											segevent.put("name", inputEvents.get(e));
											segevent.put("expression", inputEvents.get(e)+"."+segExpression);
											segeventlist.put(segevent);
											counter++;
										}
										
										if(counter>100)//TODO segmentation bug
										{
											newSegContext.put("name", segName);
											newSegContext.put("participantEvents", segeventlist);
											JSONArray segContexts = this.contexts.getJSONArray("segmentation");
											segContexts.put(newSegContext);
										}
//										else
//										{
//											splittedEpa.put("context", name);
//											System.out.println(name);
//										}
										//System.out.println((newComp.getJSONArray("segmentationContexts")).getJSONObject(0).get("name"));
									}
									newTemporalName.put("name", name+"_State_"+(s+1));
									if(counter>100)//TODO segmentation bug
										splittedEpa.put("context", newComp.getString("name"));
									else
										splittedEpa.put("context", newTemporalName.getString("name"));
									JSONArray tempContexts = this.contexts.getJSONArray("temporal");
									JSONArray compContexts = this.contexts.getJSONArray("composite");
									
									tempContexts.put(newAct);
									compContexts.put(newComp);
									
									///////////////////////////////////////////////////////////////////////////

									JSONArray epaderev = splittedEpa.getJSONArray("derivedEvents");
									JSONObject epaderEv = epaderev.getJSONObject(0);///maybe for???
									//String segExpression = getSegmantationExpression();
									
									if(s!=de.plan.steps.size()-1)	//INSERT NEW EVENTS ON EVENT LIST
									{
										EventType intermediate = new EventType(de.getName()+"_st"+(s+1)+(s+2));
										epaderEv.put("name", intermediate.getName());

										splittedEpa.put("name", de.getName()+"_State"+(s+1)+"_EPA");

										for(int k=0;k<eventsArray.length();k++) 
										{
											JSONObject event = eventsArray.getJSONObject(k);
											if(event.get("name").toString().equalsIgnoreCase(de.getName()))
											{
												JSONObject newEvent = new JSONObject(event);
												newEvent.put("name", intermediate.getName());
												JSONArray attr =  new JSONArray(event.getJSONArray("attributes").toString());
												JSONObject interAttr = new JSONObject();
												interAttr.put("name", "Intermediate");
												interAttr.put("type", "boolean");
												interAttr.put("defaultValue", "true");
												interAttr.put("dimension", 0);
												interAttr.put("description", "Intermediate derived event for pulling purposes.");
												attr.put(interAttr);
												JSONObject interAttr1 = new JSONObject();
												interAttr1.put("name", "minOccTime");
												interAttr1.put("type", "Date");
												interAttr1.put("dimension", 0);
												interAttr1.put("description", "Minimum occurrence time.");
												attr.put(interAttr1);
												JSONObject interAttr2 = new JSONObject();
												interAttr2.put("name", "maxOccTime");
												interAttr2.put("type", "Date");
												interAttr2.put("dimension", 0);
												interAttr2.put("description", "Maximum occurrence time.");
												attr.put(interAttr2);
												if(de.derivationOperator.isTheSame("seq"))
												{
													for(int o=1;o<=de.derivationOperator.getEventTypes().size();o++)
													{
														JSONObject interAttr3 = new JSONObject();
														interAttr3.put("name", "value"+o);
														interAttr3.put("type", "Date");
														interAttr3.put("dimension", 0);
														interAttr3.put("description", "Occurrence time of the number "+o+" element of the sequence.");
														attr.put(interAttr3);
													}
												}
												newEvent.put("attributes", attr);
												eventsArray.put(newEvent);
												involvedEvents.put(newEvent);
												inserted.put(newEvent.getString("name"), newEvent);
												/////////////ADD INTERMEDIATE EVENTS TO SEGMENTATION CONTEXT//////////////////////
//												JSONObject newSegContext =new JSONObject();
//												newSegContext.put("name", intermediate.getName());
//											//	String segExpression = getSegmantationExpression();
//												newSegContext.put("expression", intermediate.getName()+"."+segExpression);
//												
//												if(!this.contexts.get("segmentation").toString().isEmpty() && !existsInSegmentationParticipantEvents(intermediate.getName()))
//												{
//													JSONArray seg= this.contexts.getJSONArray("segmentation");
//													JSONObject partEv = (JSONObject) seg.get(0);
//													JSONArray pev = (JSONArray) partEv.get("participantEvents");
//													pev.put(newSegContext);
//												}
											}
										}
									}
									else////////////ADD ORIGINAL DERIVED EVENT TO SEGMENTATION CONTEXT////
									{
										for(int k=0;k<eventsArray.length();k++) //ADD minOccTime and maxOccTime attributes
										{
											JSONObject event = eventsArray.getJSONObject(k);
											if(event.get("name").toString().equalsIgnoreCase(de.getName()))
											{
												JSONArray attr =  event.getJSONArray("attributes");
												JSONObject interAttr1 = new JSONObject();
												interAttr1.put("name", "minOccTime");
												interAttr1.put("type", "Date");
												interAttr1.put("dimension", 0);
												interAttr1.put("description", "Minimum occurrence time.");
												attr.put(interAttr1);
												JSONObject interAttr2 = new JSONObject();
												interAttr2.put("name", "maxOccTime");
												interAttr2.put("type", "Date");
												interAttr2.put("dimension", 0);
												interAttr2.put("description", "Maximum occurrence time.");
												attr.put(interAttr2);
											}
										}
//										JSONObject newSegContext =new JSONObject();
//										String eventname = epaderEv.getString("name");
//										newSegContext.put("name", eventname);
//									//	String segExpression = getSegmantationExpression();
//										newSegContext.put("expression", eventname+"."+segExpression);
//										
//										if(!this.contexts.get("segmentation").toString().isEmpty() && !existsInSegmentationParticipantEvents(eventname))
//										{		
//											JSONArray seg= (JSONArray) this.contexts.get("segmentation");
//											JSONObject partEv = (JSONObject) seg.get(0);
//											JSONArray pev = (JSONArray) partEv.get("participantEvents");
//
//											pev.put(newSegContext);
//										}
									}
									/////////////ADD INPUT EVENTS TO SEGMENTATION CONTEXT//////////////////////
//									for(int e=0;e<inev.length();e++)	
//									{
//										JSONObject newSegContext =new JSONObject();
//
//										JSONObject inEv = inev.getJSONObject(e);
//										String eventname = inEv.get("name").toString();
//										if(eventname.contains("_st"))
//											continue;
//										newSegContext.put("name", eventname);
////										String segExpression = getSegmantationExpression();
//										newSegContext.put("expression", eventname+"."+segExpression);
//										
//										if(!this.contexts.get("segmentation").toString().isEmpty() && !existsInSegmentationParticipantEvents(eventname))
//										{
//											JSONArray seg= (JSONArray) this.contexts.get("segmentation");
//											JSONObject partEv = (JSONObject) seg.get(0);
//											JSONArray pev = (JSONArray) partEv.get("participantEvents");
//
//											pev.put(newSegContext);
//										}
//									}

									JSONObject expr = epaderEv.getJSONObject("expressions"); //EXPRESSIONS FOR NEW EPAS
									String expression1 = "min(";
									for(int e=0;e<inev.length();e++)
									{
										JSONObject inEv = inev.getJSONObject(e);
										String eventname = inEv.get("name").toString();
										if(eventname.equals(de.getName()+"_st"+s+(s+1)))
											expression1+=eventname+".minOccTime";
										else
											expression1+=eventname+".OccurrenceTime";
										if(e!=inev.length()-1)
											expression1+=",";
									}
									expression1+=")";
									String expression2 = "max(";
									for(int e=0;e<inev.length();e++)
									{
										JSONObject inEv = inev.getJSONObject(e);
										String eventname = inEv.get("name").toString();
										if(eventname.equals(de.getName()+"_st"+s+(s+1)))
											expression2+=eventname+".maxOccTime";
										else
											expression2+=eventname+".OccurrenceTime";
										if(e!=inev.length()-1)
											expression2+=",";
									}
									expression2+=")";
									if(hasSegmentationContext(epa.getString("context")))
									{
										String expression3 = "context."+segExpression;
										expr.put(segExpression, expression3);
									}
									expr.put("maxOccTime", expression2);
									expr.put("minOccTime", expression1);
									
									if(!expressions.isEmpty())
									{
										for(String ex:expressions.keySet())
											expr.put(ex, expressions.get(ex));
									}
									if(de.derivationOperator.isTheSame("seq"))
									{
										splittedEpa.put("epaType", "All");
									}
									involvedEpas.put(splittedEpa);
									String newcontext = splittedEpa.getString("context");
									contexts.add(newcontext);
									
								}
								
							}
							else // 1-step Plan case for Operators that get plans
							{	
								if(de.derivationOperator.isTheSame("seq") || de.derivationOperator.isTheSame("and"))
								{
									for(int k=0;k<eventsArray.length();k++) 
									{
										JSONObject event = eventsArray.getJSONObject(k);
										if(event.get("name").toString().equalsIgnoreCase(de.getName()))
										{
											JSONArray attr =  new JSONArray(event.getJSONArray("attributes").toString());
											JSONObject interAttr1 = new JSONObject();
											interAttr1.put("name", "minOccTime");
											interAttr1.put("type", "Date");
											interAttr1.put("dimension", 0);
											interAttr1.put("description", "Minimum occurrence time.");
											attr.put(interAttr1);
											JSONObject interAttr2 = new JSONObject();
											interAttr2.put("name", "maxOccTime");
											interAttr2.put("type", "Date");
											interAttr2.put("dimension", 0);
											interAttr2.put("description", "Maximum occurrence time.");
											attr.put(interAttr2);
											event.put("attributes", attr);
										}
									}
									JSONObject expres = ((JSONObject)epa.getJSONArray("derivedEvents").get(0)).getJSONObject("expressions");
									JSONArray inev = (JSONArray)epa.getJSONArray("inputEvents");
									//System.out.println(inev);
									//expres.put(key, value);
									String expression1 = "min(";
									String expression2 = "max(";
									for(int e=0;e<inev.length();e++)
									{
										JSONObject inEv = inev.getJSONObject(e);
										String eventname = inEv.get("name").toString();
										expression1+=eventname+".OccurrenceTime";
										expression2+=eventname+".OccurrenceTime";
										if(e!=inev.length()-1)
										{
											expression1+=",";
											expression2+=",";
										}
									}
									expression1+=")";
									expression2+=")";
									//System.out.println(expression1);
									expres.put("minOccTime", expression1);
									expres.put("maxOccTime", expression2);
									
									epa.put("epaType", "All");
									if(de.derivationOperator.isTheSame("seq"))
									{
										String assertion="";
										String assertion2="";
										if(!epa.has("assertion"))
										{
											String first="";
											for(int e=0;e<inev.length();e++)
											{
												
												JSONObject inEv = inev.getJSONObject(e);
												String eventname = inEv.get("name").toString();
												if(e==0)
													first+=eventname+".OccurrenceTime";
												assertion+=eventname+".OccurrenceTime";
												if(e!=inev.length()-1)
												{
													assertion+=" <= ";
												}
												else
												{
													assertion2+=eventname+".OccurrenceTime"+" - "+ first;
												}
											}
											double window=this.getWindowFromTemporal(epa.getString("context"));
											assertion+=" && "+ assertion2+" <= "+window;
											epa.put("assertion", assertion);
										}
										else
										{
											System.err.println("ALREADY HAS ASSERTION");
										}
									}
									else//AND operator
									{
										double window=this.getWindowFromTemporal(epa.getString("context"));
										String assertion="";
										assertion+=expression2+" - "+expression1;
										assertion+=" <= "+window;
										epa.put("assertion", assertion);
									}
								}
								involvedEpas.put(epa);
								String context = epa.getString("context");
								contexts.add(context);
							}
						}
						else // NAIVE PLAN or UBIQUITOUS PLACEMENT
						{	
							involvedEpas.put(epa);
							String context = epa.getString("context");
							contexts.add(context);
						}
						insertedContexts.clear();
						JSONArray inev = epa.getJSONArray("inputEvents");
						for (int l = 0; l < inev.length(); l++) {
							JSONObject inEv = inev.getJSONObject(l);
							String eventname = inEv.get("name").toString();
							for(int k=0;k<eventsArray.length();k++)
							{
								JSONObject event = eventsArray.getJSONObject(k);
								if(event.get("name").toString().equalsIgnoreCase(eventname)  && !inserted.containsKey(eventname))//|| !(events.get(event.get("name").toString())instanceof DerivedEvent))
								{
									//System.out.println("Already in? : "+inserted.containsKey(eventname));
									involvedEvents.put(event);
									inserted.put(event.getString("name"), event);
									//									System.out.println("Inserting event: "+eventname);
								}
							}
						}
						for (int l = 0; l < inev.length(); l++) {
							for(int k=0;k<eventsArray.length();k++)
							{
								JSONObject event = eventsArray.getJSONObject(k);
								if(event.get("name").toString().equalsIgnoreCase(deName) && !inserted.containsKey(deName))
								{
									//System.out.println("Already in? : "+inserted.containsKey(deName));
									involvedEvents.put(event);
									inserted.put(event.getString("name"), event);
									//									System.out.println("Inserting event: "+deName);
								}
							}
						}


						break;
					}
				}
			}
		}
		for(int k=0;k<eventsArray.length();k++) 
		{
			JSONObject event = eventsArray.getJSONObject(k);
			String eventName = event.get("name").toString();
			boolean insertevent=true;
			for(String eventsToAdd:lg.getGraphNode(lg.getGraphNodeIndex(location)).incomingEvents.keySet())
			{
				if(eventName.equalsIgnoreCase(eventsToAdd))
				{
					for(int m=0;m<involvedEvents.length();m++)
					{
						JSONObject invevent = involvedEvents.getJSONObject(m);
						String invEvent = invevent.get("name").toString();
						if(invEvent.equalsIgnoreCase(eventsToAdd))
							insertevent=false;
					}
					if(insertevent)
					{
						involvedEvents.put(event);
						inserted.put(event.getString("name"), event);
					}
				}
			}
		}
		boolean root = lg.getGraphNode(lg.getGraphNodeIndex(location)).rootOperatorSite;
		for(int k=0;k<eventsArray.length();k++) 	////Push To Dashboard Attribute
		{
			JSONObject event = eventsArray.getJSONObject(k);
			String eventName = event.get("name").toString();
			//	System.out.println();
			boolean half=false;
			boolean intermediate=false;
			JSONArray attr =  event.getJSONArray("attributes");
			for(int at=0;at<attr.length();at++)
			{
				JSONObject name = attr.getJSONObject(at);
				if(name.getString("name").equalsIgnoreCase("Intermediate"))
				{
					intermediate=true;
				}
			}
			for(DerivedEvent he:brokenHalfEvents.values())
			{
				if(eventName.equalsIgnoreCase(he.getName()))
				{
					half=true;
					break;
				}
			}
			if(intermediate || half || !root)
			{
				for(int at=0;at<attr.length();at++)
				{
					JSONObject name = attr.getJSONObject(at);
					if(name.getString("name").equalsIgnoreCase("SendToDashboard"))
					{
						attr.remove(at);
						at--;
						break;
					}
				}
				continue;
			}
			for(DerivedEvent de: derivedEvents.values())
			{
				if(eventName.equalsIgnoreCase(de.getName()) && root)
				{
					boolean exists=false;
					for(int at=0;at<attr.length();at++)
					{
						JSONObject name = attr.getJSONObject(at);
						if(name.getString("name").equalsIgnoreCase("SendToDashboard"))
						{
							exists=true;
							break;
						}
					}
					if(!exists )
					{
						JSONObject interAttr = new JSONObject();
						interAttr.put("name", "SendToDashboard");
						interAttr.put("type", "boolean");
						interAttr.put("defaultValue", "true");
						interAttr.put("dimension", 0);
						interAttr.put("description", "Event should be pushed to dashboard");
						attr.put(interAttr);
					}
					break;
				}
			}
		}
		for(int k=0;k<eventsArray.length();k++) 	////Push To Coordinators Attribute
		{
			JSONObject event = eventsArray.getJSONObject(k);
			String eventName = event.get("name").toString();
			ArrayList<String> coordinators = eventsToPushToCoordinators.get(eventName);
			if(eventsToPushToCoordinators.containsKey(eventName))
			{
				JSONArray attr =  event.getJSONArray("attributes");
				for(int at=0;at<attr.length();at++)
				{
					JSONObject name = attr.getJSONObject(at);
					if(name.getString("name").equalsIgnoreCase("PushToCoordinators"))
					{
						attr.remove(at);
						at--;
					}
				}
				String coordinator="";
				for(int c=0;c<coordinators.size();c++)
				{
					if(coordinators.get(c).equalsIgnoreCase(location))
					{
						if(!coordinator.equalsIgnoreCase(""))
							coordinator+="'";
					}
					else
					{
						if(coordinator.equalsIgnoreCase(""))
							coordinator+="'";

						coordinator+=coordinators.get(c);

						if(c!=coordinators.size()-1 && !coordinators.get(c+1).equalsIgnoreCase(location))
							coordinator+=",";

						if(c==(coordinators.size()-1))
							coordinator+="'";
					}
				}
				if(!coordinator.equalsIgnoreCase(""))
				{
					JSONObject interAttr = new JSONObject();
					interAttr.put("name", "PushToCoordinators");
					interAttr.put("type", "String");
					interAttr.put("defaultValue", coordinator);
					interAttr.put("dimension", 0);
					interAttr.put("description", "List of coordinators that this event should be pushed");
					attr.put(interAttr);
				}
			}
			else
			{
				JSONArray attr =  event.getJSONArray("attributes");
				for(int at=0;at<attr.length();at++)
				{
					JSONObject name = attr.getJSONObject(at);
					if(name.getString("name").equalsIgnoreCase("PushToCoordinators"))
					{
						attr.remove(at);
						at--;
					}
				}
			}
		}
		involvedContexts.put(getWindowObject(contexts, insertedContexts, contextType));
		JSONArray temporalList = new JSONArray();
		JSONArray compositeList = new JSONArray();
		JSONArray segmentationList = new JSONArray();
		for (HashMap.Entry<String, JSONObject> entry : insertedContexts.entrySet())
		{
			//			System.out.println("Name: "+entry.getKey()+ " Type: "+ contextType.get(entry.getKey()));
			if(contextType.get(entry.getKey()).equalsIgnoreCase("temporal"))
				temporalList.put(entry.getValue());
			else if(contextType.get(entry.getKey()).equalsIgnoreCase("segmentation"))
				segmentationList.put(entry.getValue());
			else if(contextType.get(entry.getKey()).equalsIgnoreCase("composite"))
				compositeList.put(entry.getValue());
		}
		fixSegmentationParticipantEvents(inserted);
		insertContext.put("temporal", temporalList);
		insertContext.put("composite", compositeList);
		insertContext.put("segmentation", segmentationList);
		for(int k=0;k<involvedEpas.length();k++) 	//INSERT SEGMENTATION CONTEXTS(WHICH ARE HIDDEN IN EXPRESSIONS) HERE IF THEY ARE NOT ALREADY IN
		{
			JSONObject epa = involvedEpas.getJSONObject(k);
			JSONArray derEv = epa.getJSONArray("derivedEvents");
			for(int l=0;l<derEv.length();l++)
			{
				JSONObject derev = derEv.getJSONObject(l);
				JSONObject expr = derev.getJSONObject("expressions");
				Iterator itr = expr.keys();
				while(itr.hasNext())
				{
					String key = itr.next().toString();
					String value = expr.get(key).toString();
					String context=null;
					if(value.contains("context"))
					{
						String[] seg = value.split("\\.");
						context = seg[1];
					}
					if(context!=null)
					{
						JSONArray segCon = insertContext.getJSONArray("segmentation");
						Iterator segItr = segCon.iterator();
						boolean contains=false;
						while(segItr.hasNext())
						{
							JSONObject curSeg = (JSONObject) segItr.next();
							if(curSeg.getString("name").equalsIgnoreCase(context))
								contains=true;
						}
						if(!contains)
						{
							JSONArray segContexts = this.contexts.getJSONArray("segmentation");
							Iterator<Object> tempItr = segContexts.iterator();
							while(tempItr.hasNext())
							{
								JSONObject temp = (JSONObject)tempItr.next();
								segmentationList.put(temp);
							}
						}
					}
				}
			}
		}
		

		epn.put("events", involvedEvents);
		epn.put("epas", involvedEpas);
		epn.put("contexts", insertContext);
		//System.out.println("--------------------->"+this.producers.get(0));
		//getProducers();
		epn.put("consumers", getConsumers(involvedEvents));
		epn.put("producers", getProducers(location, inputDataFileName));
		outjson.put("epn", epn);
		outjson.write(writer);
		writer.close();

		return outjson.toString();
	}

	private void fixSegmentationParticipantEvents(HashMap<String, JSONObject> inserted) {
		if(this.contexts.has("segmentation"))
		{
			JSONArray seg = this.contexts.getJSONArray("segmentation");
			JSONObject partEv = seg.getJSONObject(0);
			JSONArray pev = partEv.getJSONArray("participantEvents");
			//System.out.println(seg.toString());
			Iterator<Object> itr =  pev.iterator();
			while(itr.hasNext())
			{
				JSONObject ev = (JSONObject)itr.next();
				String event =ev.getString("name");
				if(!inserted.containsKey(event))
				{
//					System.out.println("***** Removing "+event);
					itr.remove();
				}
			}
			
			
			Iterator<String> itr2 = inserted.keySet().iterator();
			String eventname;
			while(itr2.hasNext())
			{
				JSONObject newSegContext =new JSONObject();
				eventname = (String) itr2.next();
				if(!existsInSegmentationParticipantEvents(eventname))
				{
//					System.out.println("***** Adding "+eventname);
					newSegContext.put("name", eventname);
					newSegContext.put("expression", eventname+"."+segExpression);
					pev.put(newSegContext);
				}
			}
		}
	}

	private boolean existsInSegmentationParticipantEvents(String eventName) {
		if(this.contexts.has("segmentation"))
		{
			JSONArray seg = this.contexts.getJSONArray("segmentation");
			JSONObject partEv = seg.getJSONObject(0);
			JSONArray pev = partEv.getJSONArray("participantEvents");
			
			for(int i=0;i<pev.length();i++)
			{
				JSONObject ev = pev.getJSONObject(i);
				String event =ev.getString("name");
				if(eventName.equalsIgnoreCase(event))
				{
					return true;
				}
			}
		}
		return false;
	}

	private String getSegmantationExpression() {
	
		if(this.contexts.has("segmentation"))
		{
			JSONArray seg = this.contexts.getJSONArray("segmentation");
			JSONObject partEv = seg.getJSONObject(0);
			JSONArray pev = partEv.getJSONArray("participantEvents");
			JSONObject ev = pev.getJSONObject(0);
			String expres =ev.getString("expression");
			String[] ex = expres.split("\\.");
			
			return ex[1];
		}
		else
			return null;
	}

	public void writeNetworkJsonFiles(String outputFolder, String inputDataFileName, HashMap<String, ArrayList<String>> eventsToPushToCoordinators) throws JSONException, IOException
	{
		this.segExpression = getSegmantationExpression();
		ArrayList<DerivedEvent> localList = new ArrayList<DerivedEvent>();
		for(LGraphNode n: lg.nodes)
		{
			for(EventType e:n.incomingEvents.values())
			{	
				for(DerivedEvent de: getAllDerivedEvents())
				{
					if(de.getName().equalsIgnoreCase(e.getName()))
					{
						Set<EventType> s = new HashSet<EventType>();
						s.addAll(de.getEvents());
						Plan p = new Plan(lg.nodes.size(), de.getName(), lg);
						p.addStep(s);
						localList.add(de);
					}
				}
			}
			for(Plan p:n.assignedPlans)
			{
				for(DerivedEvent de: getAllDerivedEvents())
				{
					if(de.getName().equalsIgnoreCase(p.name))
					{
						de.plan=p;
						localList.add(de);
					}
				}
			}
			jsonFiles.put(n.location, writeEpn(outputFolder,n.location,inputDataFileName, localList, eventsToPushToCoordinators));
			//System.out.println("Wrote: "+n.location);
			localList.clear();
		}
	}
	public void writeSimpleJsonFiles()
	{
		for(LGraphNode n: lg.nodes)
		{
			jsonFiles.put(n.location, json.toString());
		}
	}
	public void parseEvents() throws JSONException {
		for (int i = 0; i < epasArray.length(); i++) {
			JSONObject epa = epasArray.getJSONObject(i);
			JSONArray derev = epa.getJSONArray("derivedEvents");
			for (int j = 0; j < derev.length(); j++) {
				JSONObject derEv = derev.getJSONObject(j);
				String eventName = derEv.get("name").toString();
				if(!events.containsKey(eventName))
				{
					DerivedEvent de = new DerivedEvent(eventName);
					events.put(eventName, de);
//					System.out.println(eventName);
				}
			}
//System.out.println();
		}
		for (int i = 0; i < epasArray.length(); i++) {
			JSONObject epa = epasArray.getJSONObject(i);

			JSONArray inev = epa.getJSONArray("inputEvents");
			for (int j = 0; j < inev.length(); j++) {
				JSONObject inEv = inev.getJSONObject(j);
				String eventName = inEv.get("name").toString();
				if(!events.containsKey(eventName))
				{
					EventType e = new EventType(eventName);
					events.put(eventName, e);
//					System.out.println(eventName);
				}
			}
		}
	}
	public void parseGM() throws JSONException 
	{
		for (int i = 0; i < gmArray.length(); i++) 
		{
			JSONObject gm = gmArray.getJSONObject(i);
			GeometricEvent gmi = new GeometricEvent(gm.getString("name"));

			gmi.setFunctionName(gm.getString("functionName"));
			gmi.setFunctionLocation(Paths.get(gm.getString("functionLocation")));
			gmi.setMonitoringObject(gm.get("monitoringObject"));
			gmi.setDefLSVValue((float)gm.getDouble("defLSVValue"));
			gmi.setThresholdType(gm.getString("thresholdType"));
			gmi.setThreshold(gm.getInt("threshold"));
			gmi.setEqualityInAboveThresholdRegion(gm.getBoolean("equalityInAboveThresholdRegion"));
			gmi.setResolveSteps(gm.getInt("resolveSteps"));
			gmi.setContext(gm.getString("context"));
			System.out.println(this.getWindowFromTemporal(gmi.getContext()));
			ArrayList<DerivedEvent> deList = new ArrayList<DerivedEvent>();
			JSONArray derev = gm.getJSONArray("derivedEvents");
			for (int j = 0; j < derev.length(); j++) {
				JSONObject dev = derev.getJSONObject(j);
				DerivedEvent de = new DerivedEvent(dev.getString("name"));
				deList.add(de);
			}
			gmi.setDeList(deList);
			ArrayList<LGraphNode>sites = new ArrayList<LGraphNode>();
			JSONArray s = gm.getJSONArray("sites");
			for (int j = 0; j < s.length(); j++) {
				JSONObject si = s.getJSONObject(j);
				sites.add(lg.getGraphNode(lg.getGraphNodeIndex(si.getString("name"))));
			}
			gmi.setSites(sites);
			ArrayList<Double> weightVectors = new ArrayList<Double>();
			JSONArray w = gm.getJSONArray("weightVectors");
			for (int j = 0; j < w.length(); j++) {
				JSONObject wv = w.getJSONObject(j);
				weightVectors.add(wv.getDouble(sites.get(j).location));
			}
			gmi.setWeightVectors(weightVectors);

			System.out.println(gmi.toString());
		}

	}
	public void parseEPAs(JSONArray epasArray) throws JSONException {


		for (int i = 0; i < epasArray.length(); i++) {

			JSONObject epa = epasArray.getJSONObject(i);
			Operator op = new Operator();//epa.get("name").toString());	
			DerivedEvent de=null;// = new DerivedEvent();
			if(epa.has("epaType"))
			{
				op.setOperator(epa.get("epaType").toString());
			}
			JSONArray inev = epa.getJSONArray("inputEvents");
			for (int j = 0; j < inev.length(); j++) {
				JSONObject inEv = inev.getJSONObject(j);
				String eventName = inEv.get("name").toString();
				EventType ev = events.get(eventName);
//System.out.println(ev.getName()+ " "+ (ev instanceof DerivedEvent));
				if(inEv.has("filterExpression"))
					op.setFilter();
				if(lg.etypes.get(eventName)!=null)
				{
					ev.setprobability(lg.etypes.get(eventName).getprobability());
				}
				op.addEvents(ev);
			}

			JSONArray derev = epa.getJSONArray("derivedEvents");
			for (int j = 0; j < derev.length(); j++) {
				JSONObject derEv = derev.getJSONObject(j);
				String eventName = derEv.get("name").toString();
				if(events.containsKey(eventName))
				{
					de =(DerivedEvent) events.get(eventName);
				}

				de.setOperator(op);







				de.setProbablity();


			}

			de.derivationOperator.setWindow(getWindowFromTemporal(epa.getString("context"))/1000);

			de.derivationOperator.isActuallyFilterOperator();


			if(de.derivationOperator.getsNaivePlan() && !de.derivationOperator.containsDerivedEvents())//.getOperator().equalsIgnoreCase("aggregate"))&& epa.getBoolean("localPlacement"))//||de.derivationOperator.getOperator().equalsIgnoreCase("or") 
			{
				if(!de.derivationOperator.hasFilter())
					de.breakOperator=true;
				de.setUbiquitousPlacement(true);
			}
			else
			{
				de.setUbiquitousPlacement(false);
			}

			//System.out.println(de.getName()+" localPlacement: "+de.placeAtAllSites+ " filter: "+de.derivationOperator.hasFilter());

			if(epa.has("confidence"))
			{
				System.out.println(de.getName()+" confidence: "+epa.getDouble("confidence"));
			}
			derivedEvents.put(de.getName(), de);
			events.put(de.getName(), de);
//			de.print(false);
//			System.out.println();

		}
//		derivedEvents.get("CEandall").print(false);
//		System.out.println();
//		events.get("CEand4").print(false);
	}
	public JSONArray getProducers(String fileName, String inputDataFileName) throws JSONException
	{
		JSONArray allProducers = new JSONArray();
		allProducers = producers;
		for(int i=0; i<producers.length();i++)
		{
			//JSONObject involvedProducer = new JSONObject();
			//JSONArray addEvents = new JSONArray();
			JSONObject pro = allProducers.getJSONObject(i);
			JSONArray prop = pro.getJSONArray("properties");
			JSONObject propf = prop.getJSONObject(0);
			//			System.out.println();
			//			System.out.println(propf.getString("value"));
			//			System.out.println( fileName);
			propf.put("value", fileName+"_"+inputDataFileName);
			//			System.out.println(propf);
			//			involvedConsumer.put("name", cons.get("name"));
			//			if(cons.has("createdDate"))
			//				involvedConsumer.put("createdDate",cons.get("createdDate"));
			//			involvedConsumer.put("type",cons.get("type"));
			//			involvedConsumer.put("properties",cons.getJSONArray("properties"));
			//			JSONArray events = cons.getJSONArray("events");
			//			for(int j=0;j<events.length();j++)
			//			{
			//				JSONObject ev = events.getJSONObject(j);
			//				
			//				for(int k=0;k<involvedEvents.length();k++)
			//				{
			//					if(involvedEvents.getJSONObject(k).get("name").equals(ev.get("name")))
			//					{
			//						JSONObject event = new JSONObject();
			//						event.put("name", ev.get("name"));
			//						addEvents.put(event);
			////						System.out.println(involvedEvents.getJSONObject(k).get("name")+" EQUALS--> "+ev.get("name"));
			//					}
			//				}
			//			}
			//			involvedConsumer.put("events", addEvents);
			//			allConsumers.put(involvedConsumer);
		}

		//System.out.println(allConsumers);
		// returnObject = new JSONObject();
		//returnObject.put(key, value)
		return allProducers;
	}
	public JSONArray getConsumers(JSONArray involvedEvents) throws JSONException
	{
		JSONArray allConsumers = new JSONArray();


		for(int i=0; i<consumers.length();i++)
		{
			JSONObject involvedConsumer = new JSONObject();
			JSONArray addEvents = new JSONArray();
			JSONObject cons = consumers.getJSONObject(i);
			involvedConsumer.put("name", cons.get("name"));
			if(cons.has("createdDate"))
				involvedConsumer.put("createdDate",cons.get("createdDate"));
			involvedConsumer.put("type",cons.get("type"));
			//System.out.println(cons.get("properties"));
			involvedConsumer.put("properties",cons.getJSONArray("properties"));

			JSONArray events = cons.getJSONArray("events");
			for(int k=0;k<involvedEvents.length();k++)
			{
				JSONObject event = new JSONObject();
				event.put("name", involvedEvents.getJSONObject(k).get("name"));
				addEvents.put(event);
				//				System.out.println(k+" of "+involvedEvents.length()+" "+event);
			}
			//			for(int j=0;j<events.length();j++)
			//			{
			//				JSONObject ev = events.getJSONObject(j);
			//				
			//				for(int k=0;k<involvedEvents.length();k++)
			//				{
			//					if(involvedEvents.getJSONObject(k).get("name").equals(ev.get("name")))
			//					{
			//						JSONObject event = new JSONObject();
			//						event.put("name", ev.get("name"));
			//						addEvents.put(event);
			////						System.out.println(involvedEvents.getJSONObject(k).get("name")+" EQUALS--> "+ev.get("name"));
			//					}
			//				}
			//			}

			involvedConsumer.put("events", addEvents);
			allConsumers.put(involvedConsumer);
		}

		//System.out.println(allConsumers);
		// returnObject = new JSONObject();
		//returnObject.put(key, value)
		return allConsumers;
	}
	public boolean hasSegmentationContext(String context)
	{
		boolean hasSegContext=false;

		JSONArray temporal = contexts.getJSONArray("temporal");
		for (int j = 0; j < temporal.length(); j++) {
			JSONObject particular = temporal.getJSONObject(j);

			if(particular.get("name").toString().equalsIgnoreCase(context))
			{
				//insertedContexts.put(context, particular);
				//contextType.put(context, "temporal");
				hasSegContext=false;
				return false;
			}
		}
		JSONArray segmentation = contexts.getJSONArray("segmentation");
		for (int j = 0; j < segmentation.length(); j++) {
			JSONObject particular = segmentation.getJSONObject(j);

			if(particular.get("name").toString().equalsIgnoreCase(context))
			{
				hasSegContext=true;
				return true;
			}
		}
		JSONArray composite = contexts.getJSONArray("composite");
		for (int j = 0; j < composite.length(); j++) {
			JSONObject particular = composite.getJSONObject(j);

			if(particular.get("name").toString().equalsIgnoreCase(context))
			{
				JSONObject temp1 = ((JSONArray)particular.get("temporalContexts")).getJSONObject(0);
				if(particular.getJSONArray("segmentationContexts")!=null);
				{
					return hasSegmentationContext(particular.getJSONArray("segmentationContexts").getJSONObject(0).get("name").toString());
				}
			}
		}
		return hasSegContext;
	}
	public JSONObject getWindowObject(ArrayList<String> contextList, HashMap<String, JSONObject> insertedContexts, HashMap<String, String> contextType) throws JSONException
	{
		boolean found=false;
		JSONObject tempContext = new JSONObject();
		JSONArray temporal = contexts.getJSONArray("temporal");
		for(String context:contextList)
		{
			found=false;
			for (int j = 0; j < temporal.length(); j++) {
				JSONObject particular = temporal.getJSONObject(j);

				if(particular.get("name").toString().equalsIgnoreCase(context) && !insertedContexts.containsKey(context))
				{
					insertedContexts.put(context, particular);
					contextType.put(context, "temporal");
					found=true;
				}
			}
			if(!found)
			{
				JSONArray segmentation = contexts.getJSONArray("segmentation");
				for (int j = 0; j < segmentation.length(); j++) {
					JSONObject particular = segmentation.getJSONObject(j);

					if(particular.get("name").toString().equalsIgnoreCase(context)&& !insertedContexts.containsKey(context))
					{
						insertedContexts.put(context, particular);
						contextType.put(context, "segmentation");
					}
				}
				JSONArray composite = contexts.getJSONArray("composite");
				for (int j = 0; j < composite.length(); j++) {
					JSONObject particular = composite.getJSONObject(j);

					if(particular.get("name").toString().equalsIgnoreCase(context)&& !insertedContexts.containsKey(context))
					{
						JSONObject temp1 = ((JSONArray)particular.get("temporalContexts")).getJSONObject(0);
						if(particular.getJSONArray("segmentationContexts")!=null);
						{
							ArrayList<String> slist = new ArrayList<String>();
							slist.add(particular.getJSONArray("segmentationContexts").getJSONObject(0).get("name").toString());
							insertedContexts.put(context, getWindowObject(slist, insertedContexts, contextType));
						}

						insertedContexts.put(context, particular);
						contextType.put(context, "composite");
						ArrayList<String> tlist = new ArrayList<String>();
						tlist.add(temp1.getString("name"));
						tempContext.put(context,getWindowObject(tlist, insertedContexts, contextType));
					}
				}
			}
		}

		return tempContext;
	}
	public double getWindowFromTemporal(String context) throws JSONException
	{
		boolean found=false;
		double window=0.0;
		JSONArray temporal = contexts.getJSONArray("temporal");
		for (int j = 0; j < temporal.length(); j++) {
			JSONObject particular = temporal.getJSONObject(j);

			if(particular.get("name").toString().equalsIgnoreCase(context))
			{
				boolean always = particular.getBoolean("neverEnding");
				JSONArray terminators = particular.getJSONArray("terminators");

				if(always)
				{
					window = Double.MAX_VALUE;
					found = true;
				}
				else if(terminators.length()!=0)
				{
					JSONObject times = terminators.getJSONObject(0);
					window = Double.parseDouble((String) times.get("relativeTime"));
					found=true;
				}
			}
		}
		if(!found)
		{
			JSONArray composite = contexts.getJSONArray("composite");
			for (int j = 0; j < composite.length(); j++) {
				JSONObject particular = composite.getJSONObject(j);

				if(particular.get("name").toString().equalsIgnoreCase(context))
				{
					JSONObject temp1 = ((JSONArray)particular.get("temporalContexts")).getJSONObject(0);
					window = getWindowFromTemporal(temp1.getString("name"));
				}
			}
		}
		return window;
	}

	public String readJsonFile(String fileName) {
		String line;
		StringBuilder sb = new StringBuilder();
		BufferedReader in = null;
		String jsonFileName;
		if(this.jsonfile==null)
			jsonFileName = fileName;
		else
			jsonFileName = jsonfile;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(jsonFileName), "UTF-8"));
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String jsonTxt = sb.toString();

		return jsonTxt;
	}
	public ArrayList<DerivedEvent> getDerivedEvents()//return list of derived events that are not included in another derived events
	{
		ArrayList<DerivedEvent> dlist = new ArrayList<DerivedEvent>();
		HashMap<String, DerivedEvent> exclude = new HashMap<String, DerivedEvent>();
		HashMap<String, DerivedEvent> include = new HashMap<String, DerivedEvent>();

		for (HashMap.Entry<String, DerivedEvent> entry : derivedEvents.entrySet()) 
		{
			Set<EventType> includedEvents = entry.getValue().getEvents();

			for(EventType e: includedEvents)
			{
				if(e instanceof DerivedEvent)
				{
					if(derivedEvents.containsKey(((DerivedEvent)e).getName()))
					{
						if(include.containsKey(((DerivedEvent)e).getName()))
						{
							include.remove(((DerivedEvent)e).getName());
						}
						exclude.put(((DerivedEvent)e).getName(), (DerivedEvent)e);
					}
				}
			}	
			if(exclude.containsKey(((DerivedEvent)entry.getValue()).getName()))
				continue;
			else
			{
				include.put(entry.getKey(),entry.getValue());
			}
		}

		for (HashMap.Entry<String, DerivedEvent> entry : include.entrySet()) 
		{
			dlist.add(entry.getValue());
		}
		return dlist;
	}
	public ArrayList<DerivedEvent> getAllDerivedEvents()
	{
		return new ArrayList<DerivedEvent>(this.derivedEvents.values());
	}
	public void printEvents()
	{
		for(EventType ev: events.values())
			ev.print(false);

	}

	public void initializeNetworkStructures() 
	{
		lg.allPairsShortestPath(!optimizeLatency);
		lg.printCommLatencies();
		lg.eventLatenciesPerPlacement();
		lg.printEventLatencies();
		lg.calculateHops(2);
		lg.printHops();
		lg.computeLocalHopedFrequency();
		lg.printEventFrequencies();
		lg.printHopedFrequency();

	}

	public void runDPandAssignPlans(String path, String INPUT_DATA_FILE, boolean akdere, int centralAkdereNode) throws JSONException, IOException 
	{
		if(rewrite)
		{
			ArrayList<DerivedEvent> fullList = new ArrayList<DerivedEvent>(getAllDerivedEvents());
			ArrayList<DerivedEvent> localList = new ArrayList<DerivedEvent>(getAllDerivedEvents());
			Iterator<DerivedEvent> itr = localList.iterator();
			Plan plan = new Plan(lg.nodes.size(), "MobileFraud", lg);
			HashMap<String, ArrayList<String>> eventsToPushToCoordinators = new HashMap<String, ArrayList<String>>();
			while(itr.hasNext())
			{
				DerivedEvent de = itr.next();
				if(de.getUbiquitousPlacement())
				{
					String coordinator=lg.centralNode().location;
					ArrayList<String> coordinators = new ArrayList<String>();
					coordinators.add(coordinator);     
					eventsToPushToCoordinators.put(de.getName(), coordinators);

					for(int i=0;i<coordinators.size();i++)
						System.out.println(de.getName()+" @ "+coordinators.get(i));
				}
				if(!de.getUbiquitousPlacement() && !de.derivationOperator.getsNaivePlan())
				{

					ArrayList<EventType> elist = new ArrayList<EventType>(de.getEvents());
					for(int i =0;i<elist.size();i++)
					{
						HashSet<EventType> s = new HashSet<EventType>();
						s.add(elist.get(i));
						if(i==10)
						{
							s.add(elist.get(i+1));
							i++;
						}
						plan.addStep(s);
						System.out.println("Step "+i+"  adding event: "+elist.get(i).getName());
					}
					plan.currentPlacement = lg.nodes.indexOf(lg.centralNode());
					de.setPlan(plan);
				}
				if(!de.getUbiquitousPlacement())
					itr.remove();
			}

			LGraphNode centralCoordinator = lg.centralNode();

			for(LGraphNode n:lg.nodes)
			{
				if(n.equals(centralCoordinator))
					jsonFiles.put(n.location, writeEpn(path + "FERARI/Output_json_files/",n.location,INPUT_DATA_FILE, fullList, eventsToPushToCoordinators));
				else
					jsonFiles.put(n.location, writeEpn(path + "FERARI/Output_json_files/",n.location,INPUT_DATA_FILE, localList, eventsToPushToCoordinators));
			}
		}
		else
		{
			writeSimpleJsonFiles();
		}
	}
	public JSONArray getMonitoringAndLearningConfig(){
		JSONArray retArray = new JSONArray();
		JSONObject tmp;
		if(gmArray!=null) {
			for (Object gmObject : gmArray) {
				tmp = new JSONObject();
				tmp.put("type", "gm");
				tmp.put("config", (JSONObject) gmObject);
				retArray.put(tmp);
			}
		}
		if(gmlArray!=null) {
			for (Object gmlObject : gmlArray) {
				tmp = new JSONObject();
				tmp.put("type", "gml");
				tmp.put("config", (JSONObject) gmlObject);
				retArray.put(tmp);
			}
		}
		return retArray;
	}
}
