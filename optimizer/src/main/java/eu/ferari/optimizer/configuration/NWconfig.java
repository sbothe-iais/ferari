package eu.ferari.optimizer.configuration;
import java.io.IOException;
import java.io.OutputStream;

public class NWconfig {
	String commToCoordinatorChannel;
	String coordinatorToCommChannel;

	public NWconfig(String commToCoordinatorChannel,
			String coordinatorToCommChannel) {
		this.commToCoordinatorChannel = commToCoordinatorChannel;
		this.coordinatorToCommChannel = coordinatorToCommChannel;
	}

	public void write(OutputStream out) throws IOException {
		String gt = "commToCoordinatorChannel=" + commToCoordinatorChannel;
		out.write(gt.getBytes());
		out.write(System.getProperty("line.separator").getBytes());
		String n = "coordinatorToCommChannel=" + coordinatorToCommChannel;
		out.write(n.getBytes());
	}
}
