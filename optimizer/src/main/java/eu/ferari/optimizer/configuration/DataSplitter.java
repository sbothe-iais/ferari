package eu.ferari.optimizer.configuration;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import eu.ferari.optimizer.locationGraph.*;
import eu.ferari.optimizer.event.*;

public class DataSplitter {
	private String filePath;
	private String inputFolder;
	private String outputFolder;
	private BufferedReader reader;
	private boolean EOF=false;
	private LocationGraph lg;

	public DataSplitter( String inputFolder, String outputFolder, String inputFile, LocationGraph lg1)
	{
		filePath = inputFile;
		this.inputFolder = inputFolder;
		this.outputFolder = outputFolder;
		lg=lg1;
	}
	public void dataGenerator(long seconds, String dataFileName) throws IOException
	{
		Writer[] writer = new Writer[lg.nodes.size()];
		for (int i = 0; i < lg.nodes.size(); i++) {
			writer[i] = new FileWriter(outputFolder+lg.nodes.get(i).location+dataFileName);	
		}

		Date current = new Date();
		Long now = current.getTime();
		int[] totalEventsAddedPerSite= new int[lg.nodes.size()];
		for(LGraphNode n:lg.nodes)
		{
			for(int sec=0;sec<seconds;sec++)
			{
				long timestamp=0;
				ArrayList<Long> timestamps = new ArrayList<Long>(); 
				HashMap<Long, String> events= new HashMap<Long, String>();

				for(EventType e:n.incomingEvents.values())
				{
					if(sec==0)
					{
						timestamps.add(now);
						events.put(now, e.getName());
						break;
					}
					
					if(((LocalEvent)e).getFrequency()<=1)
					{
						double rand = Math.random();
						if(rand<=((LocalEvent)e).getFrequency())
						{
							timestamp=now+((sec*1000)+((long)(rand*1000)));
							timestamps.add(timestamp);
							events.put(timestamp, e.getName());
						}
					}
					else
					{
						double currentFrequency=((LocalEvent)e).getFrequency();
						double rand=Math.random();
						while(rand<=currentFrequency || currentFrequency>1)
						{
							timestamp=now+((sec*1000)+((long)(rand*1000)));
							timestamps.add(timestamp);
							events.put(timestamp, e.getName());
							currentFrequency-=1;
							rand=Math.random();
						}
					}
				}
				Collections.sort(timestamps);
				for(int i=0;i<timestamps.size();i++)
				{

					String tuple = "Name="+events.get(timestamps.get(i))+";"+"OccurrenceTime="+timestamps.get(i)+";";
					if(totalEventsAddedPerSite[lg.nodes.indexOf(n)]!=0)
						writer[lg.nodes.indexOf(n)].write(System.getProperty( "line.separator" ));
					writer[lg.nodes.indexOf(n)].write(tuple);
					totalEventsAddedPerSite[lg.nodes.indexOf(n)]++;

				}
			}
		}
		long totalEvents=0;
		for (int i = 0; i < lg.nodes.size(); i++) {
			totalEvents+=totalEventsAddedPerSite[i];
			System.out.println("SITE: "+lg.nodes.get(i).location+" WROTE : "+ totalEventsAddedPerSite[i]);
			writer[i].close();
		}
		System.out.println("Total Events: "+totalEvents);
	}

	public void splitFile() throws IOException
	{
		Writer[] writer = new Writer[lg.nodes.size()];

		for (int i = 0; i < lg.nodes.size(); i++) {
			writer[i] = new FileWriter(outputFolder+lg.nodes.get(i).location+"_"+filePath);	
		}
		openFile();
		while(!EOF)
		{
			String line=null;
			if((line = readLine())!=null)
			{
				writer[Math.abs(line.split(";")[5].split("=")[1].hashCode())%lg.nodes.size()].write(line);
				writer[Math.abs(line.split(";")[5].split("=")[1].hashCode())%lg.nodes.size()].write(System.getProperty( "line.separator" ));
			}
		}
		for (int i = 0; i < lg.nodes.size(); i++) {
			writer[i].close();
		}
	}
	public int hash(String field, int numberOfFiles)
	{
		int hash = 7;
		for (int i = 0; i < field.length(); i++) {
			hash = Math.abs(hash*17 + field.charAt(i));
		}
		return hash%numberOfFiles;
	}
	private void openFile() 
	{

		try {
			reader= new BufferedReader(new FileReader(new File(inputFolder+filePath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private String readLine()
	{
		String line="";
		try {
			line = reader.readLine();
		}catch (IOException e) {
			e.printStackTrace();
		}
		if(line==null)
			EOF =true;

		return line;
	}
}
