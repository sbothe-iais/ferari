package eu.ferari.optimizer.poplans;


import eu.ferari.optimizer.event.EventType;
import java.util.HashSet;
import java.util.Set;

public class Step {
   
    public Set<EventType> events;

    Step()
    {
        this.events = new HashSet();      
    }
    
    public void Step(Set<EventType> s)
    {
        events.addAll(s);
    }
}
