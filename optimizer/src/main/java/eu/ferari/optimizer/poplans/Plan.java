package eu.ferari.optimizer.poplans;

import eu.ferari.optimizer.event.*;
import eu.ferari.optimizer.locationGraph.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Plan {

	public String name;
	public ArrayList<Step> steps;
	public double cost;
	public double latency;
	public double window;
	public int currentPlacement;
	public ArrayList<DerivedEvent> derivedEvents;
	public ArrayList<DerivedEvent> allDerivedEvents;
	public double[] derivedLatency;
	public double[] derivedCost;
	public int[] derivedPlacement;
	public int[] derivedMap;
	public LocationGraph lg;
	public boolean rootplan;

	public Plan(int sites, String pname, LocationGraph lg)
	{
		this.name=pname;
		steps = new ArrayList();
		cost = -1;
		latency = -1; 
		window = -1; 
		currentPlacement = -1;              
		derivedEvents=null;
		allDerivedEvents = null;
		derivedCost=null;
		derivedLatency=null;
		derivedPlacement = new int[sites];
		this.lg = lg;
		this.rootplan=false;
	}
	public Plan(Plan p)
	{
		name=p.name;
		steps = new ArrayList(p.steps);
		//cost = p.cost;
		window = p.window;
		currentPlacement = p.currentPlacement;
		derivedEvents = new ArrayList(p.derivedEvents);
		allDerivedEvents = null;
		derivedMap = new int[p.derivedMap.length];
		System.arraycopy( derivedMap, 0, p.derivedMap, 0, p.derivedMap.length );
		derivedPlacement = new int[p.derivedPlacement.length];
		derivedCost=null;
		derivedLatency=null;
		this.lg = p.lg;
		this.rootplan=p.rootplan;
	}
	public void setDerivedPlacement(int[] derplacement)
	{
		System.arraycopy( derplacement, 0, derivedPlacement, 0, derplacement.length );
	}

	public boolean containsAllDerivedEvents()
	{
		if(allDerivedEvents==null)
		{
			getAllDerivedEvents();
		}

		if(allDerivedEvents.isEmpty())
			return false;
		else
			return true;
	}
	public boolean containsDerivedEvents()
	{
		if(derivedEvents==null)
		{
			getDerivedEvents();
		}

		if(derivedEvents.isEmpty())
			return false;
		else
			return true;
	}

	public void getAllDerivedEvents()
	{
		allDerivedEvents = new ArrayList<DerivedEvent>();

		for(int i=0;i<steps.size();i++)
			for(EventType e: steps.get(i).events)
				if(e instanceof DerivedEvent)
					allDerivedEvents.add((DerivedEvent)e);

	}
	public void getDerivedEvents()
	{        
		derivedEvents = new ArrayList<DerivedEvent>();

		for(int i=0;i<steps.size();i++)
			for(EventType e: steps.get(i).events)
				if(e instanceof DerivedEvent && !((DerivedEvent)e).placeAtAllSites)
					derivedEvents.add((DerivedEvent)e);
//				else
//					System.out.println(e.getName()+" NOT ACCEPTED");


		derivedPlacement = new int[derivedEvents.size()];
		derivedMap = new int[derivedEvents.size()];
	}

	public void setDerivedMap(ArrayList<Set<EventType>> allSetOfEvents)
	{       
		int index=0;
		for(EventType de: derivedEvents)
		{
			int setIndex = 0;
			while(!allSetOfEvents.get(setIndex).containsAll(((DerivedEvent)de).getEvents()))            
				setIndex++;                

			derivedMap[index] = setIndex;
			index++;
		}       
	}

	public void computeCostAndLatency(double window, double[][] distance, double[][] eventLatency, int site)
	{
		this.window = window;
		this.currentPlacement = site;
		latency = this.computeLatency(distance, eventLatency, site);
		cost = cost(window, site);
	}

	public void computeCostAndLatency(double window)
	{//////////akdere........
		// cost = cost(window);
		latency = latency();
	}

	public void copySteps(ArrayList<Step> stepsj)
	{
		steps.addAll(stepsj);
	}

	public void addStep(Set<EventType> s)
	{
		Step st = new Step();
		st.Step(s);
		steps.add(st);        
	}

	public void clear()
	{
		steps.clear();
	}

	public double stateReachabilityProbability(int step, int site, double window, boolean print)
	{
		double SRI = 0;
		double SRIj = 0;
		double SRIt = 1;
		boolean hasAnother = false;
		Set<EventType> EventTypes = new HashSet<EventType>();

		if(step==0)
		{
			SRI=1;
			if(print)
				System.out.print("1");
		}
		else if(steps.get(0).events.size()==1 && step==1)
		{
			for(EventType e: steps.get(0).events)
			{   
				if(e instanceof DerivedEvent)
				{
					if(!((DerivedEvent)e).placeAtAllSites)
						SRI=1-(Math.pow(Math.E, - ((DerivedEvent)e).getprobability()));//*lg.hops[derivedPlacement[derivedEvents.indexOf(e)]][site]));
					else
						SRI=1-(Math.pow(Math.E, - ((DerivedEvent)e).getprobability()));
				}
				else 
					SRI=1-(Math.pow(Math.E, - e.getprobability()));//lg.hopFrequency[lg.eventIndex(e)][site]));//(e.getprobability()-lg.getGraphNode(site).localFrequency(e))));    
				//System.out.println("F: "+ e.getprobability() + " f: "+lg.getGraphNode(site).localFrequency(e));
				if(print)
					System.out.println("(1-e^(-e"+e.getName()+") = "+SRI);
			}
		}
		else
		{
			for(int j=0;j<step;j++)
			{
				for(EventType ej: steps.get(j).events)
					EventTypes.add(ej);
			}

			for(EventType ej: EventTypes)
			{       
				if(ej instanceof DerivedEvent)
				{
					if(!((DerivedEvent)ej).placeAtAllSites)
						SRIj = 1-(Math.pow(Math.E, -((DerivedEvent)ej).getprobability()));//*lg.hops[derivedPlacement[derivedEvents.indexOf(ej)]][site]));
					else
						SRIj = 1-(Math.pow(Math.E, -((DerivedEvent)ej).getprobability()));
				}
				else
					SRIj = 1-(Math.pow(Math.E, -ej.getprobability()));//lg.hopFrequency[lg.eventIndex(ej)][site]));//(ej.getprobability()-lg.getGraphNode(site).localFrequency(ej))));
				//   System.out.println("F: "+ ej.getprobability() + " f: "+lg.getGraphNode(site).localFrequency(ej));
				if(print)
					System.out.print("+ SRIj"+ej.getName()+": "+SRIj);

				SRIt=1;
				for(EventType et: EventTypes)
				{
					if(ej!=et)
					{       
						if(et instanceof DerivedEvent)
						{
							if(!((DerivedEvent)et).placeAtAllSites)
								SRIt *= 1-(Math.pow(Math.E, -((DerivedEvent)et).getprobability()));//*lg.hops[derivedPlacement[derivedEvents.indexOf(et)]][site]))*window;
							else
								SRIt*=1-(Math.pow(Math.E, -((DerivedEvent)et).getprobability()));
						}
						else
							SRIt*=1-(Math.pow(Math.E, -et.getprobability()));//lg.hopFrequency[lg.eventIndex(et)][site]*window));//(et.getprobability()-lg.getGraphNode(site).localFrequency(et))*window));
						//    System.out.println("F: "+ et.getprobability() + " f: "+lg.getGraphNode(site).localFrequency(et));
						if(print)
							System.out.print(" * SRIt"+et.getName()+": "+( 1-(Math.pow(Math.E, -(et.getprobability()-lg.getGraphNode(site).localFrequency(et))*window)))+" ");
						hasAnother = true;                               
					}                           
				}
				if(hasAnother)
				{   
					if(print)
						System.out.print("------> SRIj("+SRIj + ")* SRIt("+SRIt+") = "+SRIj*SRIt);
					if(SRI+SRIj*SRIt<1)
						SRI+=SRIj*SRIt;
					else 
						SRI=1;

				}
				if(print)
					System.out.println("Final SRI+= "+SRI);
			}                                                
		}
		return SRI;
	}
	public double stateCost(int step, int site, double window, boolean print)
	{
		double SC = 0;

		if(step==0)
		{
			for(EventType e: steps.get(step).events)
			{
				//System.out.println(lg.eventIndex(e)+" Event: "+ e.getName()+ " DE: "+(e instanceof DerivedEvent));
				if(e instanceof DerivedEvent)
				{
					if(!((DerivedEvent)e).placeAtAllSites)
						SC+=((DerivedEvent)e).getprobability()*lg.hops[derivedPlacement[derivedEvents.indexOf(e)]][site];
					else
						SC+= lg.hopFrequency[lg.eventIndex(e)][site];
				//	System.out.println(" Hops: "+lg.hops[derivedPlacement[derivedEvents.indexOf(e)]][site]);
				}
				else
					SC+= lg.hopFrequency[lg.eventIndex(e)][site];//(e.getprobability()- lg.getGraphNode(site).localFrequency(e));
				//    System.out.println(" E: "+e.getName()+" F: "+ e.getprobability() + " f: "+lg.getGraphNode(site).localFrequency(e));
				if(print)
				{
					System.out.print("*e"+e.getName());
					System.out.print("+");
				}
			}
		}
		else
		{
			for(EventType e: steps.get(step).events)
			{
				//	System.out.println("F: "+ e.getprobability() + " f: "+lg.getGraphNode(site).localFrequency(e));
				if(e instanceof DerivedEvent)
				{
					if(!((DerivedEvent)e).placeAtAllSites)
						SC+=((DerivedEvent)e).getprobability()*lg.hops[derivedPlacement[derivedEvents.indexOf(e)]][site]*2*window;
					else
					{
						SC+= lg.hopFrequency[lg.eventIndex(e)][site]*2*window;
					}
				}
				else
					SC+= lg.hopFrequency[lg.eventIndex(e)][site]*2*window;//(e.getprobability()-lg.getGraphNode(site).localFrequency(e))*2*window;
				if(print)
					System.out.print("*2w*e"+e.getName());
			}
		}
		return SC;
	}
	public double cost(double window, int site)
	{
		double Cost=0;
		double SRP = 0;
		double SC = 0;

		boolean print=false;
//		if(site==0 && steps.size()==2)
//		{
//			//System.out.println();
//			print=true;
//			//print();
//		}
		for(int i=0; i<steps.size();i++)
		{
			SRP = stateReachabilityProbability(i , site, window, print);
			SC = stateCost(i, site, window, print);
			Cost+= SRP * SC; 
			if(derivedCost!=null)
			{
				for(int j=0;j<this.derivedCost.length;j++)
				{
					Cost+=this.derivedCost[j];
					//System.out.println("--------->"+this.derivedCost[j]);
				}
			}
			if(print)
			{
				System.out.println("------------------------------------------------------");
				System.out.println("SRP: "+SRP +"  SC: "+SC);
			}
		}

		if(print)
		{
			System.out.println("===================");
			System.out.println("COST: "+ Cost);
			System.out.println("===================");
		}
		return Cost;
	}
	public int derivedIndex(DerivedEvent e)
	{      
		int index = 0;
		for(EventType de: derivedEvents)
		{        	
			if(e.isequal(de))            
				break;      

			index++;
		}
		if(index==derivedEvents.size())
			index = -1;

		return index;

	}
	public double computeLatency(double[][] distance, double[][] eventLatency, int miniCoord)
	{
		double lat=0;
		double stepLat = 0;
		double tempLat = 0;
		int stepNo=0;
		
		for(Step step: steps)
		{
			for(EventType e: step.events)
			{                
				if(e instanceof DerivedEvent)
				{
					int index = derivedIndex((DerivedEvent)e);
					if(((DerivedEvent) e).placeAtAllSites)
					{
						////////////////////////////COMPUTE IT DIFFERENTLY//////////////////////////////////////////////////
						tempLat=lg.maxEventLatencyPerSite[miniCoord];
					}
					else if(index!=-1)
					{
						tempLat = distance[miniCoord][derivedPlacement[index]] + derivedLatency[index];//derivedLatency[derivedMap[index]][derivedPlacement[index]];//is derived latency correct?
					}
					else
						System.err.println("ERROR IN INDEX OF DERIVED INSIDE PLAN computeLatency");
					//                	if(index!=-1)
					//                		System.out.println("@ Site: "+miniCoord+" DERIVED "+index+" placed@ "+derivedPlacement[index]+" lat: "+distance[miniCoord][derivedPlacement[index]]+" dlat :: "+derivedLatency[derivedMap[index]][derivedPlacement[index]]);
				}
				else
				{
					//System.out.println(" miniCoord: "+miniCoord+" lg.eventIndex: "+lg.eventIndex(e));
					tempLat = eventLatency[miniCoord][lg.eventIndex(e)];
					//System.out.println("Lat: "+ eventLatency[miniCoord][lg.eventIndex(e)]+ " miniCoord: "+miniCoord+" lg.eventIndex: "+lg.eventIndex(e));
				}

				if(tempLat>stepLat)
				{
					stepLat = tempLat;
					tempLat=0;
				}
			}
			if(stepNo==0)
				lat+=stepLat; 	//FIRST STATE ALWAYS ON PUSH MODE
			else
				lat+=stepLat*2; //TWO WAY COMMUNICATION LATENCY OVERHEAD
			
			stepLat=0;
			stepNo++;
		}
		return lat;
	}
	public double latency()
	{
		double lat=0;
		double stepLat = 0;

		for(Step step: steps)
		{
			for(EventType e: step.events)
			{
				//                if(e.latency>stepLat)
				//                    stepLat = e.latency;
			}
			lat+=stepLat;
		}
		return lat;
	}
	public void printPlacement(int[] placement)
	{
		int i=0;
		for(EventType e:this.derivedEvents)
		{
			System.out.print(e.getName()+" @site_"+this.lg.nodes.get(derivedPlacement[i]).location);
			System.out.println("");
			i++;
		}
	}
	public String print()
	{
		StringBuilder sb = new StringBuilder("");
		sb.append(this.name+"\n");
		int stepno=0;
		for(Step step: steps)
		{
			stepno++;
			sb.append("State "+ stepno+" :"+"");
			for(EventType e: step.events)
			{            
				if(e instanceof DerivedEvent)
				{
					sb.append("  "+e.getName());
					if(!((DerivedEvent)e).placeAtAllSites)
						sb.append(" @site_"+this.lg.nodes.get(derivedPlacement[derivedIndex((DerivedEvent)e)]).location);
				}
				else
					sb.append("  "+e.getName());
				sb.append(" ");
			}
			sb.append("\n");            
		}
		sb.append("Cost: "+new DecimalFormat("#0.00").format(cost)+" Latency: "+latency+"\n");//+ " Placed: "+this.lg.nodes.get(currentPlacement).location);
		//System.out.println("");
		
		return sb.toString();
	}
	public void setDerivedLatency(double[] poplanDerivedLatency) {
		this.derivedLatency = poplanDerivedLatency;		
	}
	public void setDerivedCost(double[] poplanDerivedCost) {
		this.derivedCost = poplanDerivedCost;	
	}
}