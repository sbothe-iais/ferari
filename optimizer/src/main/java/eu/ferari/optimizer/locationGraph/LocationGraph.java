package eu.ferari.optimizer.locationGraph;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import eu.ferari.optimizer.poplans.*;
import eu.ferari.optimizer.event.*;

public class LocationGraph 
{
	public ArrayList<LGraphNode> nodes;
	public ArrayList<String> events;
	public Map<String, EventType> etypes;
	public double commLatencies[][];
	public double eventLatencies[][];
	public double hopFrequency[][];
	public int hops[][];
	public double maxEventLatencyPerSite[];
	public int next[][];
	public int removedPlans=0;

	public LocationGraph()
	{
		nodes = new ArrayList<LGraphNode>();   
	}

	public int addNode(String location)
	{
		if(getGraphNodeIndex(location)==-1)
		{
			LGraphNode newNode = new LGraphNode(location);
			nodes.add(newNode);      
			return nodes.size()-1;
		}

		return -1;
	}
	public void registerNode(String node, HashMap<EventType, Double> events, HashMap<String, Double> links)
	{
		addNode(node.toLowerCase());
		if(events!=null && !events.isEmpty())
		{
			for(EventType ev:events.keySet())
			{
				EventType et;
				if(etypes.containsKey(ev.getName()))
				{
					et = etypes.get(ev.getName());
					et.setprobability(et.getprobability()+events.get(ev));				
				}
				else
				{
					et = new EventType(ev.getName(), events.get(ev));
					this.events.add(ev.getName());
				}
					
				etypes.put(ev.getName(), et);
				LocalEvent in = new LocalEvent(et, events.get(ev), node);
				getGraphNode(getGraphNodeIndex(node)).addEvent(in);
			}
		}
		if(links!=null && !links.isEmpty())
		{
			for(String lnk:links.keySet())
			{
				addNode(lnk.toLowerCase());
				addEdge(getGraphNodeIndex(node.toLowerCase()), getGraphNodeIndex(lnk.toLowerCase()), links.get(lnk));
			}
		}
	}
	public void addDerivedEventAsLocal(DerivedEvent de)
	{
		events.add(de.getName());
		etypes.put(de.getName(), de);
		for(LGraphNode n: nodes)
		{
			Double localFrequency=0.0;
			for(EventType e: de.derivationOperator.getEventTypes())
			{
				for(EventType le:n.incomingEvents.values())
				{
					if(e.getName().equalsIgnoreCase(le.getName()))
						localFrequency+=((LocalEvent)le).getFrequency();
				}
			}
			LocalEvent in = new LocalEvent(de, localFrequency, n.location);
			n.addEvent(in);
		}
	}
	public void computeLocalHopedFrequency()
	{    	
		hopFrequency = new double[events.size()][nodes.size()];

		//frequency = e.getprobability()-this.getGraphNode(site).localFrequency(e);
		for(int i=0;i<events.size();i++)
		{
			for(int j=0;j<nodes.size();j++)
			{
				double placedFrequency=0.0;
				for(int k=0; k<nodes.size();k++)
				{
					if(nodes.get(k).incomingEvents.containsKey(events.get(i)))
					{
						if(j!=k)
							placedFrequency+=(nodes.get(k).localFrequency(etypes.get(events.get(i)))*hops[j][k]);
					}
				}
				hopFrequency[i][j] = placedFrequency;
			}
		}
	}
	public void addEdge(int site1, int site2, double latency)
	{
		LGraphNode temp1, temp2;

		temp1 = nodes.get(site1);
		temp2 = nodes.get(site2);

		temp1.addEdge(temp2, latency);
		temp2.addEdge(temp1, latency);

		nodes.set(site1, temp1);
		nodes.set(site2, temp2);
	}

	public int eventIndex(String event)
	{
		return events.indexOf(event);
	}
	public int eventIndex(EventType ev)
	{
		//    	System.out.println(ev.getName()+" vs "+ events.get(0)+ " or "+events.get(1));
		return events.indexOf(ev.getName());
	}
	public LGraphNode centralNode()
	{
		int center = -1;
		double[] maxDistance = new double[commLatencies.length];
		double[] avgDistance = new double[commLatencies.length];
		Arrays.fill(maxDistance, 0);

		for(int i=0;i<commLatencies.length;i++)
		{
			for(int j=i+1;j<commLatencies.length;j++)
			{
				avgDistance[i]+=commLatencies[i][j];
				avgDistance[j]+=commLatencies[j][i];
				if(maxDistance[i]<commLatencies[i][j])
					maxDistance[i] = commLatencies[i][j];
				if(maxDistance[j]<commLatencies[j][i])
					maxDistance[j] = commLatencies[j][i];

			}
			avgDistance[i] = avgDistance[i]/commLatencies.length;
			// System.out.println("Node: "+i+" MaxValue: "+maxDistance[i]+" AvgValue: "+avgDistance[i]);
		}
		double minMaxDistance = maxDistance[0];
		for (int i = 1; i < maxDistance.length; i++) 
			if(minMaxDistance>maxDistance[i])
				minMaxDistance = maxDistance[i];

		for (int i = 0; i < maxDistance.length; i++) 
			if(minMaxDistance==maxDistance[i])
				if(center==-1)
					center=i;
				else if(avgDistance[center]>avgDistance[i])
					center = i;

		return nodes.get(center);
	}

	public void calculateHops(int policy)
	{
		hops = new int[nodes.size()][nodes.size()];
		for(int i=0;i<nodes.size();i++)
		{
			for(int j=0;j<nodes.size();j++)
			{
				if(i==j || policy==0)
					hops[i][j]=0;
				else if(policy ==1)
					hops[i][j]=1;
				else
					hops[i][j]=Path(i,j).size()-1;
			}
		}
	}

	public void eventLatenciesPerPlacement()
	{
		double latencies[][] = new double[nodes.size()][etypes.size()];

		for(int i=0;i<nodes.size();i++)
		{
			for(int j=0;j<etypes.size();j++)
			{
				latencies[i][j]=-1;                
			}
		}        
		maxEventLatencyPerSite = new double[nodes.size()];
		for(int i=0;i<nodes.size();i++)
		{
			for(int j=0;j<nodes.size();j++)
			{
				if(i==j)
					continue;


				double tempLat=commLatencies[i][j];

				if(maxEventLatencyPerSite[i]<tempLat)
					maxEventLatencyPerSite[i]=tempLat;
			}
		}

		for (int e=0;e<events.size();e++) 
		{
			String curev = events.get(e);

			for(int i=0;i<nodes.size();i++)
			{				
				double tempLat = 0;
				for(int j=0;j<nodes.size();j++)
				{
					if(i==j)
						continue;

					LGraphNode temp = nodes.get(j);
					if(temp.incomingEvents.containsKey(curev))
					{	
						if(commLatencies[i][j]>tempLat || tempLat==0)
							tempLat=commLatencies[i][j];
					}
				}
				latencies[i][e]=tempLat;
			}
		}                   
		eventLatencies=latencies;        
	}

	public void allPairsPopPaths()
	{
		int directLinks=0;
		double[][] directLink =new double[nodes.size()][nodes.size()];
		for(int i=0;i<nodes.size();i++)
			Arrays.fill(directLink[i], -1);
		ArrayList<PopPath>[][] popPaths = new ArrayList[nodes.size()][nodes.size()]; 
		for(int i=0;i<nodes.size();i++)
			for(int j=0;j<nodes.size();j++)
				popPaths[i][j]=new ArrayList<PopPath>();

		for(int i=0;i<nodes.size();i++)
		{
			LGraphNode current = nodes.get(i);
			for (Map.Entry<LGraphNode, Double> pair : current.edges.entrySet()) 
			{
				int j = nodes.indexOf(pair.getKey());
				PopPath newPath = new PopPath(i,j);
				newPath.setSingleStep((double)pair.getValue());
				popPaths[i][j].add(newPath);
				directLink[i][j]=(double)pair.getValue();
				directLinks++;
			}
		} 
		//	for(int steps=2;steps<nodes.size();steps++)
		//	{
		int no=0;
		int b=0;
		//	System.out.println("-----------------HOPS: "+steps+" -------------------");
		for(int k=0;k<nodes.size();k++)
		{
			for(int i=0;i<nodes.size();i++)
			{
				for(int j=0;j<nodes.size();j++)
				{
					if(i==j || i==k || j==k)
						continue;
					else if(!popPaths[k][j].isEmpty() &&  !popPaths[i][k].isEmpty())
					{
						for(PopPath p : popPaths[k][j])
						{
							if(p.includesNode(i))
								continue;
							else
							{
								Iterator<PopPath> it = popPaths[i][k].iterator();
								do{
									PopPath newPath = new PopPath(i,j);
									double latency=0;
									ArrayList<Integer> path = new ArrayList<Integer>();
									PopPath pop = it.next();
									if(pop.includesNode(j))
										continue;
									if(pop.path!=null)
									{
										for(int node:pop.path)
											path.add(node);
										path.add(pop.endNode);
									}
									else
										path.add(k);
									latency+=pop.latency;
									if(p.path!=null)
										for(int node:p.path)
											path.add(node);

									latency+= p.latency;
									newPath.setMultiStep(path, latency);
									if(isPathParetoOptimal(newPath, popPaths[i][j]))
									{
										popPaths[i][j].add(newPath);
										if(popPaths[i][j].size()>b)
											b=popPaths[i][j].size();
//										System.out.println("POPPATH ADDED w HOPS: "+newPath.hops);
										no++;
									}
								}while(it.hasNext());
							}
						}
					}
					else
						continue;
				}
			}
		}
	}
	public boolean isPathParetoOptimal(PopPath newPath, ArrayList<PopPath> popPath)
	{
		boolean isParetoOptimal=false;
		if(popPath.isEmpty())
			return true;
		Iterator<PopPath> it = popPath.iterator();
		while(it.hasNext())
		{
			PopPath oldPath = it.next();

			if(oldPath.hops>=newPath.hops && oldPath.latency>=newPath.latency)
			{                     
				it.remove();
				removedPlans++;
				isParetoOptimal=true;                     
			}  
			else if(oldPath.hops<newPath.hops && oldPath.latency>newPath.latency)
				isParetoOptimal=true;
			else if(oldPath.hops>newPath.hops && oldPath.latency<newPath.latency)
				isParetoOptimal=true;
			else
				return false;
		}
		return isParetoOptimal;
	}
	public boolean hasPath(ArrayList<PopPath> popPaths,int steps)
	{
		boolean ithas=false;
		for(PopPath p : popPaths)
		{
			if(p.hops==steps)
			{
				ithas=true;
				break;
			}
		}

		return ithas;
	}
	public void allPairsShortestPath(boolean minimumCost)
	{
		double distance[][] = new double[nodes.size()][nodes.size()];
		double hops[][] = new double[nodes.size()][nodes.size()];
		next = new int[nodes.size()][nodes.size()];

		for(int i=0;i<nodes.size();i++)
		{
			for(int j=0;j<nodes.size();j++)
			{
				next[i][j]=-1;

				if(i==j)
				{
					distance[i][j]=0;
					hops[i][j]=0;
				}
				else
				{
					distance[i][j]=Double.MAX_VALUE;
					hops[i][j]=Double.MAX_VALUE;
				}
			}
		}        

		for(int i=0;i<nodes.size();i++)
		{
			LGraphNode current = nodes.get(i);
			for (Map.Entry<LGraphNode, Double> pair : current.edges.entrySet()) 
			{
				int j = nodes.indexOf(pair.getKey());
				distance[i][j] = (double)pair.getValue();
				hops[i][j]=1;
				next[i][j]=j;
			}
		}    

		for(int k=0;k<nodes.size();k++)
		{
			for(int i=0;i<nodes.size();i++)
			{
				for(int j=0;j<nodes.size();j++)
				{
					if(i==j || i==k || j==k)
						continue;
					if(hops[i][j] >= hops[i][k] + hops[k][j] || minimumCost)
					{   
						if(distance[i][j] > distance[i][k] + distance[k][j])
						{
							hops[i][j] = hops[i][k]+hops[k][j];
							distance[i][j] = distance[i][k] + distance[k][j];
							next[i][j] = next[i][k];
						}
					}
				}
			}
		}
		commLatencies=distance;        
	}

	public LinkedList<Integer> Path(int u , int v)
	{
		LinkedList<Integer> path = new LinkedList<Integer>();
		if(next[u][v] == -1)
			return null;

		path.add(u);

		while(u!=v)
		{
			u = next[u][v];
			path.add(u);
		}
		return path;
	}

	public LGraphNode getGraphNode(int index)
	{
		return nodes.get(index);
	}
	public int getGraphNodeIndex(String location)
	{
		int index=-1;
		for(int i=0;i<nodes.size();i++)
		{
			if(nodes.get(i).location.equals(location))
			{
				index=i;
				break;
			}
		}

		return index;
	}

	public void printCommLatencies()
	{
		System.out.println("Inter-Communication Latencies");
		System.out.print("      ");
		for (int j = 0; j < commLatencies.length; j++) 
			System.out.print(this.nodes.get(j).location+" ");
		System.out.println();
		for(int k=0;k<commLatencies.length;k++)
		{	
			System.out.print(this.nodes.get(k).location);


			for (int j = 0; j < commLatencies.length; j++) 
				System.out.print("   "+new DecimalFormat("#00.##").format(commLatencies[k][j])+" ");

			System.out.println();
		}
		System.out.println();
	}
	public void printEventLatencies()
	{
		System.out.println("Max Latencies per Placement");
		for(int k=0;k<maxEventLatencyPerSite.length;k++)
			System.out.println(this.nodes.get(k).location+" "+maxEventLatencyPerSite[k]);
		System.out.println();
		System.out.println("Max Event Latencies per Placement");
		System.out.print("     ");
		for (int j = 0; j < eventLatencies[0].length; j++) {
			System.out.print("  "+this.events.get(j));
		}
		System.out.println();
		for(int k=0;k<eventLatencies.length;k++)
		{	System.out.print(this.nodes.get(k).location+" ");
		for (int j = 0; j < eventLatencies[0].length; j++) {
			System.out.print(" " +new DecimalFormat("#0.##").format(eventLatencies[k][j])+" ");
		}
		System.out.println("");
		}	
		System.out.println();
	}
	public void printHops()
	{
		System.out.println("Site InterCommunication Hops");
		System.out.print("      ");
		for(int k=0;k<maxEventLatencyPerSite.length;k++)
			System.out.print(this.nodes.get(k).location+" ");
		System.out.println();
		for(int i=0;i<nodes.size();i++)
		{
			System.out.print(this.nodes.get(i).location+" ");
			for(int j=0;j<nodes.size();j++)
				System.out.print("  " +hops[i][j]+"   ");
			System.out.println("");
		}
		System.out.println();
	}
	public void printHopedFrequency()
	{
		System.out.println("Event Frequency per Site Placement with Hops");
		System.out.print("    ");
		for(int k=0;k<maxEventLatencyPerSite.length;k++)
			System.out.print(" "+this.nodes.get(k).location+"   ");
		System.out.println();
		for(int i=0;i<events.size();i++)
		{
			System.out.print(this.events.get(i)+" ");
			for(int j=0;j<nodes.size();j++)
				System.out.print("  " +new DecimalFormat("#0.00").format(this.hopFrequency[i][j])+"   ");
			System.out.println("");
		}
		System.out.println();
	}
	public void printEventFrequencies()
	{
		System.out.println("Event Frequency per Site");
		System.out.print("    ");
		for(int k=0;k<maxEventLatencyPerSite.length;k++)
			System.out.print(" "+this.nodes.get(k).location+"   ");
		System.out.println();
		for(int i=0;i<events.size();i++)
		{
			System.out.print(this.events.get(i)+" ");
			for(int j=0;j<nodes.size();j++)
			{
				if(nodes.get(j).incomingEvents.containsKey(events.get(i)))
					System.out.print("  " +new DecimalFormat("#0.00").format(nodes.get(j).localFrequency(etypes.get(events.get(i))))+"   ");
				else
					System.out.print("  " +new DecimalFormat("#0.00").format(0)+"   ");
			}
			System.out.println("");
		}
		System.out.println();
	}
	public void assignPlans(Plan[] optimalPlans)
	{
		for(Plan p:optimalPlans)
			if(p != null)
				nodes.get(p.currentPlacement).assignPlan(p);
	}
	public void print()
	{

		for(int index=0;index<nodes.size();index++)
		{
			LGraphNode temp = nodes.get(index);
			System.out.println(temp.location);
			temp.printLinks();
		}
	}
}
