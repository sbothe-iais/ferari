package eu.ferari.optimizer.locationGraph;

import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.util.force.ForceSimulator;
import prefuse.visual.EdgeItem;

public class ForceDirectedVarSpringLayout extends ForceDirectedLayout {

	public int areaSize = 50;
    
	public ForceDirectedVarSpringLayout(String group)
	{
		super(group);
	}
    public ForceDirectedVarSpringLayout(String group,
            boolean enforceBounds)
    {
        super(group, enforceBounds);
    }
    
    public ForceDirectedVarSpringLayout(String group, ForceSimulator fsim,
            boolean enforceBounds, boolean runonce) {
        super(group, fsim, enforceBounds, runonce);
        
    }
    public ForceDirectedVarSpringLayout(String group, ForceSimulator fsim,
            boolean enforceBounds) {
        super(group, fsim, enforceBounds);
        
    }
    public ForceDirectedVarSpringLayout(String group, ForceSimulator fsim,
            boolean enforceBounds, int max) {
        super(group, fsim, enforceBounds);
        areaSize = max;
    }
    
    @Override
    public float getSpringLength(EdgeItem e){
        String weight = e.get("weight").toString();
        if(weight != null){
            return (float) Float.valueOf(weight)* areaSize*1.5f/10; // TODO: your function here
        } else {
            System.err.println("edge without weight.");
            return -1.f;
        }
    }
}

