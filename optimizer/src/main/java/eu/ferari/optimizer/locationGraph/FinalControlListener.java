package eu.ferari.optimizer.locationGraph;


import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.JPopupMenu;

import prefuse.controls.Control;
import prefuse.visual.DecoratorItem;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;

public class FinalControlListener implements Control {

	public void itemClicked(VisualItem item, MouseEvent e) 
	{
		if(item instanceof NodeItem || item instanceof DecoratorItem)
		{
			String events="";
			String name="";
			String plans="";
			String source="";
			String target="";
			double weight=0;
			if(!item.isInGroup("edgeDeco"))
			{
				events = ((String) item.get("events"));
				name = ((String) item.get("name"));
				if(item.get("plans")!=null)
					plans=((String) item.get("plans"));
			}
			else
			{
				source = ((String) item.get("source").toString());
				target = ((String) item.get("target").toString());
				weight = item.getDouble("weight");
			}
			JPopupMenu jpub = new JPopupMenu();
			if(!item.isInGroup("edgeDeco"))
			{
				jpub.add("Node: "+name);
				String[] event = events.split(" - ");
				for(int i=0;i<event.length;i++)
					jpub.add("Event: " + event[i]);
				if(!plans.equals(""))
					jpub.add("Plans: "+plans);
			}
			else
			{
				jpub.add("Link: "+source+" - "+target);
				jpub.add("Latency: "+ weight+" ms");
			}
			//jpub.show(e.getComponent(),0,0);
			jpub.show(e.getComponent(),e.getX(),e.getY());
		}
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public void itemPressed(VisualItem arg0, MouseEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemEntered(VisualItem arg0, MouseEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemExited(VisualItem arg0, MouseEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemKeyPressed(VisualItem arg0, KeyEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemKeyReleased(VisualItem arg0, KeyEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemKeyTyped(VisualItem arg0, KeyEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemMoved(VisualItem arg0, MouseEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemDragged(VisualItem arg0, MouseEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemReleased(VisualItem arg0, MouseEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void itemWheelMoved(VisualItem arg0, MouseWheelEvent arg1) {
		// TODO Auto-generated method stub

	}

	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseWheelMoved(MouseWheelEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void setEnabled(boolean arg0) {
		// TODO Auto-generated method stub

	}

}
