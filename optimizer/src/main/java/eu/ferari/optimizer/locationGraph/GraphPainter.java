package eu.ferari.optimizer.locationGraph;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JFrame;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import eu.ferari.optimizer.event.LocalEvent;
import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.assignment.DataShapeAction;
import prefuse.activity.Activity;
import prefuse.controls.DragControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.data.Graph;
import prefuse.data.Schema;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.PrefuseLib;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;

public class GraphPainter {
	private LocationGraph lg;
	private Document doc;
	private Element rootElement;
	public static final String EDGE_DECORATORS = "edgeDeco";
	public static final String NODE_DECORATORS = "nodeDeco";
	private static final Schema DECORATOR_SCHEMA = PrefuseLib.getVisualItemSchema(); 
	public static final String EDGES = "graph.edges";
	public static final String NODES = "graph.nodes";

	private static final String fileName = "FERARI/Debugging/networkGraph_1.xml";
	private String path;
	private String fullpath;
	
	public GraphPainter(LocationGraph lg, String path) throws TransformerException
	{
		this.lg = lg;
		this.path =  path;
		fullpath = this.path.concat(fileName);

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doc = docBuilder.newDocument();
		Element fatherElement = doc.createElement("graphml");
		fatherElement.setAttribute("xmlns", "http://graphml.graphdrawing.org/xmlns");
		fatherElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		fatherElement.setAttribute("xsi:schemaLocation", "http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");
		Element key4 = doc.createElement("key");
		key4.setAttribute("attr.type", "string");
		key4.setAttribute("attr.name", "coordinator");
		key4.setAttribute("for", "node");
		key4.setAttribute("id", "coordinator");
		fatherElement.appendChild(key4);
		Element key3 = doc.createElement("key");
		key3.setAttribute("attr.type", "string");
		key3.setAttribute("attr.name", "plans");
		key3.setAttribute("for", "node");
		key3.setAttribute("id", "plans");
		fatherElement.appendChild(key3);
		Element key2 = doc.createElement("key");
		key2.setAttribute("attr.type", "string");
		key2.setAttribute("attr.name", "events");
		key2.setAttribute("for", "node");
		key2.setAttribute("id", "events");
		fatherElement.appendChild(key2);
		Element key1 = doc.createElement("key");
		key1.setAttribute("attr.type", "string");
		key1.setAttribute("attr.name", "name");
		key1.setAttribute("for", "node");
		key1.setAttribute("id", "name");
		fatherElement.appendChild(key1);
		Element key = doc.createElement("key");
		key.setAttribute("attr.type", "double");
		key.setAttribute("attr.name", "weight");
		key.setAttribute("for", "edge");
		key.setAttribute("id", "weight");
		fatherElement.appendChild(key);
		rootElement = doc.createElement("graph");
		rootElement.setAttribute("edgedefault", "undirected");
		rootElement.setAttribute("id", "G");
		fatherElement.appendChild(rootElement);
		doc.appendChild(fatherElement);

		for(LGraphNode n: lg.nodes)
		{
			Boolean coordinator = false;
			Element node = doc.createElement("node");
			node.setAttribute("id", n.location);
			Element name = doc.createElement("data");
			name.setAttribute("key", "name");
			name.appendChild(doc.createTextNode(n.location));
			node.appendChild(name);
			Element plans = doc.createElement("data");
			plans.setAttribute("key", "plans");
			String allPlans="";
			for(int i=0;i<n.assignedPlans.size();i++)
			{
				allPlans+=n.assignedPlans.get(i).name+" - ";
			}
			if(!allPlans.equals(""))
			{
				plans.appendChild(doc.createTextNode(allPlans.substring(0, allPlans.length()-2)));
				node.appendChild(plans);
				coordinator=true;
			}
			Element events = doc.createElement("data");
			events.setAttribute("key", "events");
			Iterator it = n.incomingEvents.entrySet().iterator();
			String allEvents="";
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry)it.next();
				//System.out.println(pair.getKey() + " = " + pair.getValue());
				allEvents+=pair.getKey().toString()+"("+((LocalEvent)pair.getValue()).getFrequency()+") - ";
			}
			//System.out.println(allEvents);
			if(!allEvents.isEmpty())
				events.appendChild(doc.createTextNode(allEvents.substring(0, allEvents.length()-2)));
			node.appendChild(events);
			Element coord = doc.createElement("data");
			coord.setAttribute("key", "coordinator");
			coord.appendChild(doc.createTextNode(coordinator.toString()));
			node.appendChild(coord);
			rootElement.appendChild(node);
		}

		for(LGraphNode n:lg.nodes)
		{
			Iterator it = n.edges.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry)it.next();
				//System.out.println(pair.getKey() + " = " + pair.getValue());

				if(lg.nodes.indexOf((LGraphNode)pair.getKey())>lg.nodes.indexOf(n))
				{	
					//lg.addEdge(, pair.getKey(), pair.getValue());
					Element edge = doc.createElement("edge");
					edge.setAttribute("target", ((LGraphNode)pair.getKey()).location);
					edge.setAttribute("source", n.location);
					//edge.setAttribute("id", pair.getValue().toString());
					Element weight = doc.createElement("data");
					weight.setAttribute("key", "weight");
					weight.appendChild(doc.createTextNode(pair.getValue().toString()));
					edge.appendChild(weight);
					rootElement.appendChild(edge);
				}
			}
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(fullpath));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);

		System.out.println("File saved!");
	}
	public void paintGraph()
	{
		Graph graph = null;
		try {
			graph = new GraphMLReader().readGraph(fullpath);
		} catch (DataIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Visualization vis = new Visualization();
		vis.add("graph", graph);

		//draw the "name" label for NodeItems
		FinalRenderer r = new FinalRenderer("name");
		
		DefaultRendererFactory drf = new DefaultRendererFactory(r); 
		drf.add(new InGroupPredicate(EDGE_DECORATORS), new LabelRenderer("weight"));
		drf.add(new InGroupPredicate(NODE_DECORATORS), new LabelRenderer("name"));
		
		DECORATOR_SCHEMA.setDefault(VisualItem.TEXTCOLOR, ColorLib.gray(0));
		vis.addDecorators(EDGE_DECORATORS, EDGES, DECORATOR_SCHEMA); 
		vis.addDecorators(NODE_DECORATORS, NODES, DECORATOR_SCHEMA);
		vis.setRendererFactory(drf);
		int[] palette = new int[] { ColorLib.rgb(190,190,255), ColorLib.rgb(255,180,180)};
		DataColorAction fill = new DataColorAction("graph.nodes", "coordinator", Constants.NOMINAL, VisualItem.FILLCOLOR, palette);
		ColorAction text = new ColorAction(NODES, VisualItem.TEXTCOLOR, ColorLib.gray(0));
		ColorAction edges = new ColorAction(EDGES, VisualItem.STROKECOLOR, ColorLib.gray(200));
		ActionList color = new ActionList();
		color.add(fill);
		color.add(text);
		color.add(edges);

		// create an action list with an animated layout
		// the INFINITY parameter tells the action list to run indefinitely
		ActionList layout = new ActionList(Activity.INFINITY);
		layout.add(new ForceDirectedVarSpringLayout("graph"));
		layout.add(new RepaintAction());
		layout.add(new LabelLayout2(EDGE_DECORATORS));
		layout.add(new LabelLayout2(NODE_DECORATORS));

		// add the actions to the visualization
		vis.putAction("color", color);
		vis.putAction("layout", layout);

		// create a new Display that pull from our Visualization
		Display display = new Display(vis);
		display.addControlListener(new FinalControlListener());
		display.setSize(800, 600); // set display size
		display.addControlListener(new DragControl()); // drag items around
		display.addControlListener(new PanControl());  // pan with background left-drag
		display.addControlListener(new ZoomControl()); // zoom with vertical right-drag
		display.pan(400, 300);
		// create a new window to hold the visualization
		JFrame frame = new JFrame("Network Graph");
		// ensure application exits when window is closed
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(display);
		frame.pack();           // layout components in window
		frame.setVisible(true); // show the window

		vis.run("color");  // assign the colors
		vis.run("layout"); // start up the animated layout
	}
}
