package eu.ferari.optimizer.locationGraph;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import prefuse.render.AbstractShapeRenderer;
import prefuse.render.ImageFactory;
import prefuse.render.LabelRenderer;
import prefuse.visual.VisualItem;

public class FinalRenderer extends LabelRenderer
{
	protected Ellipse2D m_box = new Ellipse2D.Double();
	
	FinalRenderer(String string)
	{
		super(string);
	}
	public FinalRenderer() {
		super();
	}
	@Override
	protected Shape getRawShape(VisualItem item) 
	{	
		m_box.setFrame(item.getX(), item.getY(),30,30);
		return m_box;
	}
}
