package eu.ferari.optimizer.locationGraph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import eu.ferari.optimizer.event.EventType;
import eu.ferari.optimizer.event.LocalEvent;
import eu.ferari.optimizer.poplans.Plan;

public class LGraphNode 
{
    public String location;
    public Map<LGraphNode, Double> edges;
    public Map<String, EventType> incomingEvents;
    public ArrayList<Plan> assignedPlans;
    public boolean rootOperatorSite;
    
    public LGraphNode()
    {
        edges = new HashMap<LGraphNode, Double>();
        incomingEvents = new HashMap<String, EventType>();
        assignedPlans = new ArrayList<Plan>();
        rootOperatorSite=false;
    }
    
    public LGraphNode(String location)
    {
        this.location = location;
        edges = new HashMap<LGraphNode, Double>();
        incomingEvents = new HashMap<String, EventType>();
        assignedPlans = new ArrayList<Plan>();
        rootOperatorSite=false;
    }
        
    public void addEdge(LGraphNode newLinkedNode, double latency)
    {
        edges.put(newLinkedNode, latency);
    }
    public double localFrequency(EventType e)
    {
    	if(incomingEvents.get(e.getName())==null)
    		return 0;
    	else
    	{
    		if(incomingEvents.containsKey(e.getName()) && incomingEvents.get(e.getName()) instanceof LocalEvent)//if(e instanceof LocalEvent)
    				return ((LocalEvent)incomingEvents.get(e.getName())).getFrequency();
    		else
    			return 0.0;
    	}
    }
    public void addEvent(EventType le)
    {
//        LocalEvent le = new LocalEvent(ename, localFrequency, location);
        incomingEvents.put(le.getName(), le);
    }
    
    public double getEdgeWeight(LGraphNode linkedNode)
    {
        return edges.get(linkedNode);
    }
    
    public void printLinks()
    {        
        for (Map.Entry<LGraphNode, Double> pair : edges.entrySet()) 
        {
            System.out.print("From: "+location+" to: "+((LGraphNode)pair.getKey()).location);
            //LGraphNode temp = (LGraphNode)pair.getKey();
            System.out.print(" --> Latency: "+pair.getValue());
            System.out.println("");
        }
    }
    public void assignPlan(Plan plan)
    {
    	if(plan.rootplan)
    		this.rootOperatorSite=true;
    	assignedPlans.add(plan);
    }
//    public void printEvents()
//    {
//        Iterator<Entry<String, LocalEvent>> it = incomingEvents.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry<String, LocalEvent> pair = (Entry<String, LocalEvent>)it.next();
//           // System.out.println(pair.getKey());
//            pair.getValue().print();
//        }
//    }
}
