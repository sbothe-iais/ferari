package eu.ferari.optimizer.locationGraph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import eu.ferari.optimizer.event.EventType;
import eu.ferari.optimizer.event.LocalEvent;

import javax.xml.transform.TransformerException;


public class NetworkParser {	
	private String path=null;
	private BufferedReader reader;
	private boolean EOF=false;
	private LocationGraph lg;
	private String delimiter;

	
	public NetworkParser(String siteInfo, String delimiter)
	{
		path = siteInfo;
		this.delimiter = delimiter;		
	}
	public LocationGraph parseFile(String singleString)
	{
		lg = new LocationGraph();
		lg.etypes = new HashMap<String, EventType>();
		lg.events = new ArrayList<String>();
		parseString(singleString);
		
		return lg;
	}
	private void parseString(String singleString) {
		String[] line = singleString.split("siteName", -1);
		int lineNo=1;
		while(lineNo<line.length)
		{
			parseLine("siteName"+line[lineNo]);
			lineNo++;
		}
	}
	public LocationGraph parseFile() throws TransformerException
	{
		lg = new LocationGraph();
		lg.etypes = new HashMap<String, EventType>();
		lg.events = new ArrayList<String>();
		openFile();
		readFile();
		
		return lg;
	}
	
	public void readFile() throws TransformerException
	{
		while(!EOF)
		{
			String line=null;
			if((line = readLine())!=null)
			{
				//System.out.println(line);
				parseLine(line);
			}
		}
	}

	private void parseLine(String line) 
	{
		String[] values = line.split(delimiter, -1);
		for(int i=0;i<values.length-1;i++)
			values[i]=values[i].trim();
		int index=0;
		if(values[index].equalsIgnoreCase("siteName"))
			index++;

		lg.addNode(values[index].toLowerCase());
		index++;
		if(values[index].equalsIgnoreCase("links"))
		{
			index++;
		}
		while(!values[index].equalsIgnoreCase("events"))
		{
			lg.addNode(values[index].toLowerCase());
			lg.addEdge(lg.getGraphNodeIndex(values[1].toLowerCase()), lg.getGraphNodeIndex(values[index].toLowerCase()), Double.parseDouble(values[index+1]));
			index+=2;
		}
		index++;
		while(index<values.length-1)
		{	
			EventType et;
			if(lg.etypes.containsKey(values[index]))
			{
				et = lg.etypes.get(values[index]);
				et.setprobability(et.getprobability()+Double.parseDouble(values[index+1]));				
			}
			else
			{
				et = new EventType(values[index], Double.parseDouble(values[index+1]));
				lg.events.add(values[index]);
			}
				
			lg.etypes.put(values[index], et);
			LocalEvent in = new LocalEvent(et, Double.parseDouble(values[index+1]), values[0]);
			lg.getGraphNode(lg.getGraphNodeIndex(values[1])).addEvent(in);
			index+=2;
		}
	}

	private void openFile() 
	{

		try {
			reader= new BufferedReader(new FileReader(new File(path)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private String readLine()
	{
		String line="";
		try {
			line = reader.readLine();
		}catch (IOException e) {
			e.printStackTrace();
		}
		if(line==null)
			EOF =true;
		
		return line;
	}
}