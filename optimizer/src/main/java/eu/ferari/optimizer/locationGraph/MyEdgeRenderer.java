package eu.ferari.optimizer.locationGraph;

import java.awt.Shape;
import java.awt.geom.Point2D;

import prefuse.render.AbstractShapeRenderer;
import prefuse.visual.EdgeItem;
import prefuse.visual.VisualItem;

public class MyEdgeRenderer extends AbstractShapeRenderer {

	@Override
	protected Shape getRawShape(VisualItem arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected String getText(VisualItem item) {
	    EdgeItem edge = (EdgeItem)item;
	    VisualItem item1 = edge.getSourceItem();
	    VisualItem item2 = edge.getTargetItem();    

	    String t1 = null, t2 = null;
	    if ( item1.canGetString("w") ) {
	        t1 = item1.getString("w").substring(0,1);            
	    };
	    if ( item2.canGetString("w") ) {
	        t2 = item2.getString("w").substring(0,1);            
	    };
	    if (t1 != null && t2 != null)
	        return t1 + "-" + t2;
	    else
	        return null;
	}
	
	protected void getAlignedPoint(Point2D p, VisualItem item, 
	        double w, double h, int xAlign, int yAlign)
	{
	    double x=0, y=0;                

	    EdgeItem edge = (EdgeItem)item;
	    VisualItem item1 = edge.getSourceItem();
	    VisualItem item2 = edge.getTargetItem();

	    // label is positioned to the center of the edge
	    x = (item1.getX()+item2.getX())/2;
	    y = (item1.getY()+item2.getY())/2; 
	}
}