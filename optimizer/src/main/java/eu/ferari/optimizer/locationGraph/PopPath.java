package eu.ferari.optimizer.locationGraph;

import java.util.ArrayList;

public class PopPath {

	public int startNode;
	public int endNode;
	public int hops;
	public double latency;
	public ArrayList<Integer> path;
	
	public PopPath(int startNode, int endNode)
	{
		this.startNode = startNode;
		this.endNode = endNode;
		hops=-1;
		latency=-1;
	}
	
	public void setSingleStep(double latency)
	{
		hops=1;
		this.latency=latency;
		path=null;
	}
	
	public void setMultiStep(ArrayList<Integer> path, double latency)
	{
		hops=1+path.size();
		this.path = new ArrayList<Integer>(path);
		this.latency = latency;
	}

	public boolean includesNode(int nodeCycle) 
	{
		if(path==null)
			return false;
		else if(path.contains(nodeCycle))
			return true;
		else
			return false;
//		for(int node:path)
//			if(node==nodeCycle)
//				return true;
						
//		return false;
	}
}
