package eu.ferari.optimizer.dashboard.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


/**
 * @author Miroslav Mrak
 * 
 * JPA eu.ferari.mobileFraudCEP.entity class
 *
 */

@Entity @IdClass(Derived_eventId.class)
@Table (name = "DERIVED_EVENT")
public class Derived_event {

	@Id
	private Date call_start_date;
	@Id
	private Integer conversation_duration;
	@Id
	private Date insertion_datetime;
	private String derived_event_name;
	private Double certainty;
	private Date occurence_time;
	private Date expiration_time;
	private Double cost;
	private Double duration;
	private String other_party_tel_number;
	private String calling_number;
	private String called_number;
	private String call_direction;
	private String other_party_tel_number_prefix;
	private Integer calls_count;
	private Integer calls_length_sum;
	private String object_id;
	private String billed_msisdn;
	private String tap_related;
	private Date call_start_time;

	public double getCertainty() {
		return certainty;
	}
	public void setCertainty(double certainty) {
		this.certainty = certainty;
	}

	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public String getOther_party_tel_number() {
		return other_party_tel_number;
	}
	public void setOther_party_tel_number(String other_party_tel_number) {
		this.other_party_tel_number = other_party_tel_number;
	}
	public Date getCall_start_date() {
		return call_start_date;
	}
	public void setCall_start_date(Date call_start_date) {
		this.call_start_date = call_start_date;
	}
	public String getCalling_number() {
		return calling_number;
	}
	public void setCalling_number(String calling_number) {
		this.calling_number = calling_number;
	}
	public String getCalled_number() {
		return called_number;
	}
	public void setCalled_number(String called_number) {
		this.called_number = called_number;
	}
	public String getCall_direction() {
		return call_direction;
	}
	public void setCall_direction(String call_direction) {
		this.call_direction = call_direction;
	}
	public int getConversation_duration() {
		return conversation_duration;
	}
	public void setConversation_duration(int conversation_duration) {
		this.conversation_duration = conversation_duration;
	}
	public String getOther_party_tel_number_prefix() {
		return other_party_tel_number_prefix;
	}
	public void setOther_party_tel_number_prefix(
			String other_party_tel_number_prefix) {
		this.other_party_tel_number_prefix = other_party_tel_number_prefix;
	}
	public String getObject_id() {
		return object_id;
	}
	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}
	public String getTap_related() {
		return tap_related;
	}
	public void setTap_related(String tap_related) {
		this.tap_related = tap_related;
	}
	public Date getCall_start_time() {
		return call_start_time;
	}
	public void setCall_start_time(Date call_start_time) {
		this.call_start_time = call_start_time;
	}
	public String getDerived_event_name() {
		return derived_event_name;
	}
	public void setDerived_event_name(String derived_event_name) {
		this.derived_event_name = derived_event_name;
	}
	public Date getInsertion_datetime() {
		return insertion_datetime;
	}
	public void setInsertion_datetime(Date insertion_datetime) {
		this.insertion_datetime = insertion_datetime;
	}
	public Date getOccurence_time() {
		return occurence_time;
	}
	public void setOccurence_time(Date occurence_time) {
		this.occurence_time = occurence_time;
	}
	public Date getExpiration_time() {
		return expiration_time;
	}
	public void setExpiration_time(Date expiration_time) {
		this.expiration_time = expiration_time;
	}
	public int getCalls_count() {
		return calls_count;
	}
	public void setCalls_count(int calls_count) {
		this.calls_count = calls_count;
	}
	public int getCalls_length_sum() {
		return calls_length_sum;
	}
	public void setCalls_length_sum(int calls_length_sum) {
		this.calls_length_sum = calls_length_sum;
	}
	public String getBilled_msisdn() {
		return billed_msisdn;
	}
	public void setBilled_msisdn(String billed_msisdn) {
		this.billed_msisdn = billed_msisdn;
	}
	

}
