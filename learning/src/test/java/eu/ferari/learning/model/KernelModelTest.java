package eu.ferari.learning.model;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.tmp.KernelFunctionFactory.KernelFunctionType;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;


/**
 * Kernel model tests for a real valued vector 
 * @author Rania
 *
 */
public class KernelModelTest  {
	
	DataPair<RealValuedVector, Double> weight1;
	DataPair<RealValuedVector, Double> weight2;
		
	LearningInstance instance;
	
	RealValuedVector rv2;
	
	LearningParamsMap paramsMap = new LearningParamsMap();
	
	@Before public void init(){
		RealValuedVector rv1 = new RealValuedVector(new Double[]{1.0,1.2, 5.34, 0.0});	
		Double [] featuresAndTarget  = new Double[]{0.1,3.4, 1.23, 2.0, 1.2};
		weight1 = new DataPair<RealValuedVector, Double>(rv1, 3.5);
		instance = new LearningInstance(featuresAndTarget);
		rv2 = new RealValuedVector(new Double[]{2.1,0.2, 2.34, 0.01});
		weight2 = new DataPair<RealValuedVector, Double>(rv2, 3.3);
    }
	
	
	
	@Test public void testDotProdModel(){
		//KernelModel(UpdateRuleBase<RealValuedVector, Double> updater, TargetType targetType, LearningParamsMap params) {
		paramsMap.setStringParam(LearningParameter.KernelFunction, String.valueOf(KernelFunctionType.Linear));
		KernelModelRegression model = new KernelModelRegression(null, paramsMap);
		model.predictInstance(instance);
		
		model.updateWeight(weight1.getData1(), weight1.getData2());
		
		model.updateWeight(weight2.getData1(), weight2.getData2());
		
		double result = model.computePredictionScore(instance);
		
		double expected = 50.11976;
		
		assertTrue(Math.abs(result - expected) < 0.0001);
	}
	
	@Test public void testPolynomialModel(){
		paramsMap.setStringParam(LearningParameter.KernelFunction, String.valueOf(KernelFunctionType.Polynomial));
		KernelModelRegression model = new KernelModelRegression(null, paramsMap);
		model.predictInstance(instance);
		
		model.updateWeight(weight1.getData1(), weight1.getData2());
		
		model.updateWeight(weight2.getData1(), weight2.getData2());
		
		double result = model.computePredictionScore(instance);
		
		double expected = 451.709875;
		
		assertTrue(Math.abs(result - expected) < 0.0001);
	}
	
	@Test public void testGaussianModel(){
		
		paramsMap.setStringParam(LearningParameter.KernelFunction, String.valueOf(KernelFunctionType.Gaussian));
		KernelModelRegression model = new KernelModelRegression(null, paramsMap);
		
		model.predictInstance(instance);
		
		model.updateWeight(weight1.getData1(), weight1.getData2());

		model.updateWeight(weight2.getData1(), weight2.getData2());
		
		double result = model.computePredictionScore(instance);
		
		double expected = 6.799;
		
		assertTrue(Math.abs(result - expected) < 0.001);
	}

}
