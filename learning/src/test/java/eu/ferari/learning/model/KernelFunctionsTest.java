package eu.ferari.learning.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.DataInstance;
import eu.ferari.learning.model.base.BaseKernelFunction;
import eu.ferari.learning.model.kernelFunc.DotProductKernel;
import eu.ferari.learning.model.kernelFunc.GaussianKernel;
import eu.ferari.learning.model.kernelFunc.PolynomialKernel;



/**
 * Kernel functions test for real valued vectors
 * @author Rania
 *
 */
public class KernelFunctionsTest  {
	
	private DataPair<RealValuedVector, Double> supportVectorVal;
	
	private double[] features;
	
	DataInstance instance;
	
	RealValuedVector rv2;
	
	@Before public void init(){
		RealValuedVector rv1 = new RealValuedVector(new Double[]{1.0,1.2, 5.34, 0.0});	
		features = new double[]{0.1,3.4, 1.23, 2.0};
		supportVectorVal = new DataPair<RealValuedVector, Double>(rv1, 3.5);
    }
	
	
	@Test public void testDotProductFunction() {
		BaseKernelFunction kernelFunc = new DotProductKernel();
		//public double computeValue(Entry<RealValuedVector, Double> supportVectorVal, double[] instance);
		
		double result = kernelFunc.computeValue(supportVectorVal, features);
		double expected = 10.7482;
		assertThat(result, is(equalTo(expected)));
	}
	

	@Test public void testPolynomialFunction() {
		BaseKernelFunction kernelFunc = new PolynomialKernel(2, 0.0002);
		//public double computeValue(Entry<RealValuedVector, Double> supportVectorVal, double[] instance);
		
		double result = kernelFunc.computeValue(supportVectorVal, features);
		double expected = 115.5281026;
		assertTrue(Math.abs(result - expected) < 0.0001);
	}
	
	

	@Test public void testGaussianFunction() {
		BaseKernelFunction kernelFunc = new GaussianKernel(100);
		//public double computeValue(Entry<RealValuedVector, Double> supportVectorVal, double[] instance);
		
		double result = kernelFunc.computeValue(supportVectorVal, features);
		double expected = 0.76688;
		assertTrue(Math.abs(result - expected) < 0.0001);
	}
	
}
