package eu.ferari.learning.model;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.LinearModel;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.updateRules.StochasticGradientDescent;

public class LinearModelTest {

	LearningInstance instance1;
	LearningInstance instance2;
	RealValuedVector weights;
	double learningRate = 0.1;
	
	@Before public void init(){
		weights = new RealValuedVector(new Double[]{2.0, 1.0, 1.2, 5.34, -0.5});	
		
		Double [] featuresAndTarget  = new Double[]{0.1, 3.4, 1.23, 2.0, 1.2};
		instance1 = new LearningInstance(featuresAndTarget);
		instance2 = new LearningInstance(new Double[]{1.0, 3.0, 5.0, 2.0, 0.5});
    }

	

	@Test public void testLinearModelSquareError(){
		StochasticGradientDescent updater = new StochasticGradientDescent( LossFunctionType.SquareError, learningRate);
		LinearModel model = new LinearModelRegression(updater);
		
		model.predictInstance(instance1);
		
		setWeights(weights, model);
		double result = model.computePredictionScore(instance1);
		double expected = 11.7482;
		assertTrue(Math.abs(result - expected) < 0.0001);
		instance1.setPredictionScore(result);
		model.updateModel(instance1);
		
		RealValuedVector expectedWeights = new RealValuedVector(new Double[]{0.94518, 0.894518, -2.386388, 4.0425714, -2.60964});
		model.getWeights();
		
		for(int i = 0 ; i < expectedWeights.getDimension(); i++) {
    		assertTrue(Math.abs(expectedWeights.get(i) - model.getWeights().get(i)) < 0.01);
    	}
		
	   
	    double expectedLoss = 55.6322;
	    
	    assertTrue(Math.abs(instance1.getLossValue() - expectedLoss) < 0.0001);
	    
	    
	}
	
	
	@Test public void testLinearModelEpsilonInsensitive(){
		StochasticGradientDescent updater = new StochasticGradientDescent( LossFunctionType.EpsilonInsensitive, learningRate);
		LinearModel model = new LinearModelRegression(updater);
		model.predictInstance(instance1);
		
		setWeights(weights, model);
		double result = model.computePredictionScore(instance1);
		double expected = 11.7482;
		assertTrue(Math.abs(result - expected) < 0.0001);
		instance1.setPredictionScore(result);
		model.updateModel(instance1);
		
		RealValuedVector expectedWeights = new RealValuedVector(new Double[]{1.9, 0.99, 0.86, 5.217, -0.7});
		model.getWeights();
		for(int i = 0 ; i < expectedWeights.getDimension(); i++) {
    		assertTrue(Math.abs(expectedWeights.get(i) - model.getWeights().get(i)) < 0.01);
    	}
		   
	    double expectedLoss = 10.1482;
	    
	    assertTrue(Math.abs(instance1.getLossValue() - expectedLoss) < 0.0001);
	}

	
	
	private void setWeights(RealValuedVector weights, LinearModel model) {
		for(int i = 0 ; i < weights.getDimension(); i++) {
			model.updateWeight(i,  weights.get(i));
		}
		
	}
	
}
