package eu.ferari.learning.model;


import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.tmp.KernelVector;

/**
 * Kernel vector unit tests
 * @author Rania
 *
 */
public class KernelVectorTest {

	 @Test public void testAdd() {
 		   RealValuedVector rv1 = new RealValuedVector(new Double[]{1.0,1.2, 5.34, 0.0});
 		   RealValuedVector rv2 = new RealValuedVector(new Double[]{0.1,3.4, 1.23, 2.0});
 		   RealValuedVector rv3 = new RealValuedVector(new Double[]{1.0, 2.0, 3.0, 4.0});
 		   
 		   KernelVector kv1 = new KernelVector();
 		   KernelVector kv2 = new KernelVector();
 		   
 		   kv1.insertSV(rv1, 17);
 		   kv1.insertSV(rv2, 0.5);
 		   
 		   kv2.insertSV(rv1, 2.1);
 		   kv2.insertSV(rv3, 0.44);
 		   
 		   
 		   kv1.add(kv2);
 		   
 		   KernelVector expected = new KernelVector();
 		   expected.insertSV(rv1, 19.1);
 		   expected.insertSV(rv2, 0.5);
 		   expected.insertSV(rv3, 0.44);
 	 	  
 		 
 	       assertTrue(kv1.equals(expected));
 	     }
	   
	   
	 @Test public void testAverage() {
		   RealValuedVector rv1 = new RealValuedVector(new Double[]{1.0,1.2, 5.34, 0.0});
		   RealValuedVector rv2 = new RealValuedVector(new Double[]{0.1,3.4, 1.23, 2.0});
		   RealValuedVector rv3 = new RealValuedVector(new Double[]{1.0, 2.0, 3.0, 4.0});
		   
		   KernelVector kv1 = new KernelVector();
		   KernelVector kv2 = new KernelVector();
		   
		   kv1.insertSV(rv1, 17);
		   kv1.insertSV(rv2, 0.5);
		   
		   kv2.insertSV(rv1, 2.1);
		   kv2.insertSV(rv3, 0.44);
		   
		   Collection<IVector<RealValuedVector, Double>> kvList = new ArrayList<IVector<RealValuedVector, Double>>();
		   
		   kvList.add(kv1);
		   kvList.add(kv2);
		  
		   IVector<RealValuedVector, Double> result = kv1.average(kvList);
		   
		   KernelVector expected = new KernelVector();
		   expected.insertSV(rv1, 9.55);
		   expected.insertSV(rv2, 0.25);
		   expected.insertSV(rv3, 0.22);
		 
	       assertTrue(result.equals(expected));
	     }
   
}
