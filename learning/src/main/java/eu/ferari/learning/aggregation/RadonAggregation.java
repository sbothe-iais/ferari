package eu.ferari.learning.aggregation;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * using the iterated Radon point algorithm for averaging the models across all the learners. 
 * When using this method for synchronization/
 * @author Rania
 *
 * @param <Index>
 * @param <Value>
 */
public class RadonAggregation<Index, Value> implements
		IAggregationMethod<Index, Value>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1436768541164539157L;
	private static final double epsilon = 0.0001;
	private static final double max_rel_epsilon = 0.00001;
	private int treeHeight = 5;
	private Random random = new SecureRandom();

	@Override
	public IVector<Index, Value> syncWeights(
			Collection<IVector<Index, Value>> balancingSetWeights) {
		int dataDim = balancingSetWeights.iterator().next().getDimension();
		if (balancingSetWeights.size() < dataDim + 2) {//nothing to do, Radon point requires d+2 nodes in order to have a solution
			// actually in this case it's better that every learner remains with his own model rather than getting a random model of some other learner
			// to be done
			int rndindex = random.nextInt(balancingSetWeights.size());
			return ((List<IVector<Index, Value>>) balancingSetWeights).get(rndindex);
		} else if (balancingSetWeights.size() == dataDim+2) {
			Double[] radonVector = getRadonPoint(balancingSetWeights, dataDim);
			if(radonVector ==null) {
				return (IVector<Index, Value>) ((List)balancingSetWeights).get(balancingSetWeights.size()-1);
			}
			IVector<Integer, Double> averagedModel = new RealValuedVector(radonVector);	
			return (IVector<Index, Value>) averagedModel;
		}
		else {
			Collections.shuffle((List<?>) balancingSetWeights);
			int intermediateSize = dataDim+2;
			int i = 0;
			treeHeight = (int) (Math.log(balancingSetWeights.size()) / Math.log(intermediateSize));
			ArrayList<IVector<Index, Value>> prevIntermediateRadons = new ArrayList<>(balancingSetWeights);
			ArrayList<IVector<Index, Value>> intermediateRadons = null;
			while (i < treeHeight) {
				int numIters = prevIntermediateRadons.size() / intermediateSize;
				int k = 0;
				intermediateRadons = new ArrayList<>();
				while (k < numIters) {
					int endIndex = intermediateSize * k + intermediateSize;
					if (endIndex > prevIntermediateRadons.size())
						endIndex = prevIntermediateRadons.size();
					List<IVector<Index, Value>> subList = ((List<IVector<Index, Value>>) prevIntermediateRadons)
							.subList(intermediateSize * k, endIndex);
					Double[] radonVector = getRadonPoint(subList, dataDim);
					if(radonVector ==null) {
						return (IVector<Index, Value>) ((List)balancingSetWeights).get(balancingSetWeights.size()-1);
					}
					IVector<Integer, Double> averagedModel = new RealValuedVector(radonVector);
					intermediateRadons.add((IVector<Index, Value>) averagedModel);
					++k;
				}
				prevIntermediateRadons = new ArrayList<>(intermediateRadons);
				Collections.shuffle((List<?>) prevIntermediateRadons);
				++i;
			}
			int rndindex = random.nextInt(intermediateRadons.size());
			//System.out.println("Radon Mean Sync time: " + diff);
			return intermediateRadons.get(rndindex);
		}

	}

	private Double[] getRadonPoint(
			Collection<IVector<Index, Value>> balancingSetWeights, int dataDim) {
		Iterator<IVector<Index, Value>> it = balancingSetWeights.iterator();
		//RealValuedVector vec = (RealValuedVector) (balancingSetWeights.iterator().next());
		int size = dataDim+2;//vec.getValue().length + 2;
		double[][] solutions = new double[size][size];
		int i = 0;
		while (it.hasNext()) {
			double[] solAsCol = ((RealValuedVector) it.next()).getValue();
			System.arraycopy(solAsCol, 0, solutions[i], 0, solAsCol.length);
			solutions[i][size - 2] = 1;
			solutions[i][size - 1] = 0;
			i++;
		}
		solutions[0][size - 1] = 1;

		double constantsCols[] = new double[size];
		constantsCols[size - 1] = 1;


		RealMatrix S = new Array2DRowRealMatrix(solutions, false);
		S = S.transpose();
		DecompositionSolver solver = new LUDecomposition(S).getSolver();

		RealVector constants = new ArrayRealVector(constantsCols, false);
		RealVector solution = null;
		try  {
		solution = solver.solve(constants);
		}
		catch (Exception e){
			System.err.println("singular matrix, returning arbitrary");
			return null;
		}
		System.out.println("not singular matrix, returning radon");
		Vector<Double> positiveRadonSolution = new Vector<Double>();
		Vector<Double> negativeRadonSolution = new Vector<Double>();
		double positiveSum = 0;
		double negativeSum = 0;
		Vector<Integer> positiveIndices = new Vector<Integer>();
		for (i = 0; i < solution.getDimension(); i++) {
			double currentCoeff = solution.getEntry(i);
			if (currentCoeff > 0) {
				positiveRadonSolution.add(currentCoeff);
				positiveSum += currentCoeff;
				positiveIndices.add(i);
			} else if (currentCoeff < 0) {
				negativeRadonSolution.add(currentCoeff);
				negativeSum += currentCoeff;
			}
		}
		if (!areDoubleEqual(positiveSum, -negativeSum)) {
			System.err.println("Something unexpected in calculating Radon point happened: " + positiveSum + ", " + negativeSum);
		}
		
		Double[] radonVector = new Double[size - 2];
		for (i = 0; i < size - 2; i++) {
			radonVector[i] = 0.0;
			for (int k = 0; k < positiveIndices.size(); k++) {
				radonVector[i] += positiveRadonSolution.get(k) * solutions[positiveIndices.get(k)][i];
			}
			radonVector[i] /= positiveSum;
		}
		//System.out.println("****************** radon vector" + Arrays.toString(radonVector));
		return radonVector;
	}

	private boolean areDoubleEqual(double x, double y) {
		if (x == y)
			return true;
		if (Math.abs(x - y) < epsilon)
			return true;
		double relErorr;
		if (Math.abs(y) < Math.abs(x)) {
			relErorr = Math.abs((x - y) / y);
		} else {
			relErorr = Math.abs((x - y) / x);
		}
		if (relErorr < max_rel_epsilon)
			return true;
		return false;
	}
}