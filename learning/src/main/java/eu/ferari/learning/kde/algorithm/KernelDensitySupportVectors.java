package eu.ferari.learning.kde.algorithm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javassist.runtime.DotClass;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.ejml.simple.SimpleMatrix;
import org.math.plot.Plot2DPanel;

import eu.ferari.DataPair;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.kde.model.BaseSampleDistribution;
import eu.ferari.learning.kde.model.SampleModel;
import eu.ferari.learning.kde.utility.Compression.Compressor;
import eu.ferari.learning.kde.utility.Compression.Hellinger;
import eu.ferari.learning.kde.utility.Matrices.MatrixOps;
import eu.ferari.learning.model.base.BaseKernelFunction;
import eu.ferari.learning.model.kernelFunc.DotProductKernel;

/**
 * 
 *
 * @param <Index>: the support vector (data instance generated from the given
 *        distribution) type. we assume 1 dimensional data . Index can be a real
 *        or discrete value (Double or integer)
 */
public class KernelDensitySupportVectors implements IVector<Double, BaseSampleDistribution> {

	// sample model object used for sample distribution estimation
	private SampleModel sampleDistribution;

	long occurrenceTime=Long.MAX_VALUE;
	// the actual distribution, it is used only for plotting purposes
	private Map<Double, Integer> realDistMap = new HashMap<Double, Integer>();

	// The first 3 samples that are used for initialization
	private ArrayList<SimpleMatrix> initSamples = new ArrayList<SimpleMatrix>();

	private double[][] c = new double[1][1];

	private static final int BOUND = 1; // a scalar to be used when displaying a distribution whose values have been scaled

	private int numSamples = 0;

	
	private String distanceType = "hellinger";
	
	private static final long serialVersionUID = 3651118517068803218L;



	private double forgettingFactor;

	private double compressionThreshold;

	// the kernels , this value is shared for all kernels according to our
	// online algorithm


	public double getForgettingFactor() {
		return forgettingFactor;
	}
	public double getCompressionThreshold() {
		return compressionThreshold;
	}
	public KernelDensitySupportVectors(double forgettingFactor,
			double compressionThreshold) {
		sampleDistribution = new SampleModel(forgettingFactor, compressionThreshold);
		this.forgettingFactor = forgettingFactor;
		this.compressionThreshold = compressionThreshold;
	}
	// not needed
	@Override
	public Iterator<Entry<Double, BaseSampleDistribution>> iterator() {
		return null;
	}

	@Override
	public void initialise(IVector<Double, BaseSampleDistribution> v) {
		KernelDensitySupportVectors kdsv = (KernelDensitySupportVectors) v;
		try {
			sampleDistribution = new SampleModel(kdsv.getSampleDistribution()); //kdsv.getSampleDistribution();
			this.numSamples = kdsv.getNumSamples();
			this.forgettingFactor = kdsv.getForgettingFactor();
			this.compressionThreshold = kdsv.getCompressionThreshold();
		} catch (Exception e) {
			e.printStackTrace();
		}
		kdsv.setNumSamples(kdsv.getNumSamples());
	}

	private void setNumSamples(int numSamples) {
		this.numSamples = numSamples;

	}
	@Override
	public IVector<Double, BaseSampleDistribution> copy() {
		KernelDensitySupportVectors clone = new KernelDensitySupportVectors(forgettingFactor, compressionThreshold);
		clone.initialise(this);
		return clone;
	}
	//unneeded
	@Override
	public BaseSampleDistribution get(Double index) {
		return null;
	}

	@Override
	public void set(Double idx, BaseSampleDistribution val) {
	
	}

	@Override
	public void initializeToZeroVector() {
		sampleDistribution = new SampleModel(forgettingFactor, compressionThreshold);

	}

	@Override
	public int getDimension() {
		return sampleDistribution.getSubDistributions().size();
	}

	public int getNumSamples() {
		return numSamples;
	}
	
	@Override
	public void add(IVector<Double, BaseSampleDistribution> other) {

		KernelDensitySupportVectors currentKernel = (KernelDensitySupportVectors) other;
		numSamples +=currentKernel.getNumSamples();
		if(sampleDistribution.getSubDistributions().size() <= 1) {
			try {
				sampleDistribution = new SampleModel(currentKernel.getSampleDistribution());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
		try {
		sampleDistribution.mergeSampleModels(currentKernel.getSampleDistribution());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Iterable<Double> getKeys() {
		return null;
	}

	@Override
	public boolean containsIndex(Double key) {
		return false;
	}

	


	/**
	 * Average/Merge the kernel density vectors of multiple local learners
	 */
	@Override
	public IVector<Double, BaseSampleDistribution> average(Collection<IVector<Double, BaseSampleDistribution>> balancingSetWeights) {
		//System.out.println("average begin");
		IVector<Double, BaseSampleDistribution> sample = balancingSetWeights.iterator().next();
		//return sample;

		Plotter plotter = new Plotter();
		plotter.plotDistribution(((KernelDensitySupportVectors)sample).getNumSamples(), "before averaging");


		IVector<Double, BaseSampleDistribution> averagedModel = new KernelDensitySupportVectors(((KernelDensitySupportVectors) sample).getForgettingFactor(), ((KernelDensitySupportVectors) sample).getCompressionThreshold());
		for (IVector<Double, BaseSampleDistribution> currentKernel : balancingSetWeights) {
			averagedModel.add(currentKernel);
		}
		Plotter plotter1 = new Plotter();
		plotter1.plotDistribution(((KernelDensitySupportVectors)averagedModel).getNumSamples(), "after averaging");
		//System.out.println("num samples: " + ((KernelDensitySupportVectors)averagedModel).getNumSamples());
		//System.out.println("average end");
		return averagedModel;
	}

	private SampleModel getSampleDistribution() {
		return sampleDistribution;

	}

	//
	@Override
	// undefined operation
	public void scalarMultiply(double arg0) {

	}

	@Override
	// undefined operation
	public void subtract(IVector<Double, BaseSampleDistribution> other) {

	}

	@Override
	public double distance(IVector<Double, BaseSampleDistribution> other) {
		double distance = -1;
		switch (distanceType) {
			case "hellinger": 		
				distance = hellingerDistance(other);
			break;
			default:
				distance = kernelDistance(other);
		}
		System.out.println("probability distribution distance: " + distance);
		return distance;
    }
	


    private double hellingerDistance(IVector<Double, BaseSampleDistribution> other) {
    	double distance = -1;
    	KernelDensitySupportVectors otherKDSV = (KernelDensitySupportVectors) other;
		SampleModel otherSampleModel = otherKDSV.getSampleDistribution();
		try {
			distance = Hellinger.calculateUnscentedHellingerDistance(this.sampleDistribution, otherSampleModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return distance;
	}
    
    private double kernelDistance(IVector<Double, BaseSampleDistribution> other) {
    	double distance = 0;
    	KernelDensitySupportVectors otherKDSV = (KernelDensitySupportVectors) other;
		SampleModel otherSampleModel = otherKDSV.getSampleDistribution();
		
		ArrayList<SimpleMatrix> means = sampleDistribution.getSubMeans();
		ArrayList<Double> weights = sampleDistribution.getSubWeights();
		
		ArrayList<SimpleMatrix> otherMeans = otherSampleModel.getSubMeans();
		ArrayList<Double> otherWeights = otherSampleModel.getSubWeights();
		
    	
    	boolean firstIter = true;
    	for(int i =0; i < means.size(); i++) {
    		SimpleMatrix currentmean = means.get(i);
    		distance += Math.pow(weights.get(i), 2)*matrixNormSquare(currentmean);
    		for(int j =0; j < otherMeans.size(); j++) {
        		SimpleMatrix otherCurrentMean = otherMeans.get(j);
    			if(firstIter) {
    				distance += Math.pow(otherWeights.get(i), 2)*matrixNormSquare(otherCurrentMean);
    			}
				distance -= 2*weights.get(i)*otherWeights.get(j)*Compressor.euclidianDistance(currentmean, otherCurrentMean);
		
			}
    		firstIter = false;
    	}    	
		return distance;
	}
    
	private double matrixNormSquare(SimpleMatrix A) {
		double norm = MatrixOps.elemPow(A, 2).elementSum();
		return norm;
    }

	public void updateDistribution(Double sample) {
		realDistMap.put(sample, realDistMap.getOrDefault(sample, 0)+1);
		double[][] sampleArray = { { sample } };
		try {
 			SimpleMatrix sampleMatrix = new SimpleMatrix(sampleArray);
			if (numSamples < 3) {
				initSamples.add(sampleMatrix);
				if (initSamples.size() == 3) {
					double[] w = { 1, 1, 1 };
					SimpleMatrix[] cov = { new SimpleMatrix(c), new SimpleMatrix(c), new SimpleMatrix(c) };
					sampleDistribution.updateDistribution(initSamples.toArray(new SimpleMatrix[3]), cov, w);
				}
			}
			else /*if (initSamples.size()==3)*/{
				sampleDistribution.updateDistribution(sampleMatrix, new SimpleMatrix(c), 1d);
			}
			++numSamples;

		} catch (Exception e) {
			System.out.println("num samples: " + numSamples);
			e.printStackTrace();
		}
		if(numSamples  == 100) {
			//Plotter plotter = new Plotter();
			//plotter.plotDistribution(numSamples, "distribution from single");
		}
	}



	private class Plotter extends JFrame {
		private static final long serialVersionUID = -5506458801389821678L;

		public void plotDistribution(int numSamples, String title) {
			try {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						draw(numSamples, title);
						setVisible(true);
					}

				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void draw(int numSamples, String title) {
			setTitle("okde");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(900, 500);
			setLocationRelativeTo(null);
			// first create a 100x100 grid
			double[] xArray = new double[100];
			for (int i = 0; i < 100; i++) {
				xArray[i] = i *0.1;
			}
			// then evaluate the sample model at each point of the grid
			double[] zArray = evaluateSampleDistribution(xArray, sampleDistribution);

			Plot2DPanel plot = new Plot2DPanel("SOUTH");
			// add grid plot to the PlotPanel
			xArray = Arrays.stream(xArray).map(i -> i * BOUND).toArray();
			plot.addBarPlot("estimated sample distribution " + title, xArray, zArray);
			Iterator<Entry<Double, Integer>> it = realDistMap.entrySet()
					.iterator();
			double values[] = new double[realDistMap.keySet().size()];
			double counts[] = new double[realDistMap.keySet().size()];
			int i = 0;
			while (it.hasNext()) {
				Entry<Double, Integer> entry = it.next();
				values[i] = entry.getKey() * BOUND;
				counts[i] = (double) (entry.getValue()) / numSamples;
				++i;
			}
			//plot.addBarPlot("actual distribution " + title, values, counts);
			setContentPane(plot);
		}

		private double[] evaluateSampleDistribution(double[] x,
				BaseSampleDistribution dist) {
			double[] z = new double[x.length];
			for (int i = 0; i < x.length; i++) {
				double[][] point = { { x[i] } };
				SimpleMatrix pointVector = new SimpleMatrix(point);
				z[i] = dist.evaluate(pointVector);
			}
			return z;
		}
	}
	@Override
	public long getOccurrenceTime() {
		return occurrenceTime;
	}

	@Override
	public void setOccurrenceTime(long time) {
		this.occurrenceTime=time;
	}
	
	
}
