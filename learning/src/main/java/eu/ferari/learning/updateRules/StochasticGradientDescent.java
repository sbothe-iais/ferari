package eu.ferari.learning.updateRules;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.ILearningModel;
import eu.ferari.learning.model.base.LinearModel;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;

/**
 * this update rule is applicable to the linear model
 * @author Rania
 *
 */
public class StochasticGradientDescent extends UpdateRuleBase<Integer, Double>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5778859274461372046L;
	private double learningRate = 0.3;
	private static double minRandomWeight = 0.001;
	private static double maxRandomWeight = 0.01;
	private static double epsilon = 0.00001;

	public StochasticGradientDescent(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		super(config);
		JSONObject updateRuleParams = config.getJSONObject("updateRuleParams");
		this.learningRate = updateRuleParams.getDouble("learningRate");
	}
	
	public StochasticGradientDescent(LossFunctionType lossType) {
		super(lossType);
	}
	
	public StochasticGradientDescent(LossFunctionType lossType, double learningRate) {
		this(lossType);
		this.learningRate = learningRate;
	}
	
	@Override
	public void updateModel(ILearningModel<Integer, Double> linearModel,
			LearningInstance dataInst) {
		double delta = lossFunction.evaluateLossDerivative(dataInst, 1);
		double deltaScaled = learningRate * delta;
		if (Math.abs(delta) > epsilon) {
			for (int i = 0; i < linearModel.getWeights().getDimension(); i++) {
				double xValue;
				if (i == 0) {
					xValue = 1;
				} else {
					xValue = ((LinearModel)linearModel).getDoubleValueOfInstance(
							dataInst.getInstance(), i - 1);
				}
				if (xValue != 0) {
					double currentWeight = linearModel.getWeights().get(i);
					double updatedWeight = currentWeight - deltaScaled * xValue;
					linearModel.updateWeight(i, updatedWeight);
				}
			}
		}
	}
	
	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}
	
	/**
	 * initialise the weights with some small random value
	 */
	@Override
	public void initWeights(ILearningModel<Integer, Double> linearModel) {
		Random rnd = new Random();
		int dim = linearModel.getWeights().getDimension();
		for (int i = 0; i < dim; i++) {
			double initWeight = minRandomWeight + (maxRandomWeight - minRandomWeight) * rnd.nextDouble();
			linearModel.updateWeight(i, initWeight);
		}
	}

	@Override
	public void setParameters(Map<String, Double> parametersMap) {
		learningRate = parametersMap.get("learningRate");
	}
	
}
