package eu.ferari.learning.updateRules;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.interfaces.ILossFunction;
import eu.ferari.learning.model.base.ILearningModel;
import eu.ferari.learning.model.tmp.LossFunctionFactory;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;

/**
 * An update class that represents the update ruke of the learner
 * @author Rania
 *
 */
public abstract class UpdateRuleBase <Index, Value> implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -6881205551622329910L;
	
	protected ILossFunction lossFunction;

	
	public UpdateRuleBase(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		this.lossFunction = (ILossFunction)(this.getClass().getClassLoader().loadClass(config.getString("lossFunction")).getConstructor(JSONObject.class).newInstance(config));
	}
	
	public UpdateRuleBase(LossFunctionType lossType) {
		this.lossFunction = LossFunctionFactory.getLossFunction(lossType);
	}
	
	
	public ILossFunction getLossFunction() {
		return lossFunction;
	}


	public void setLossFunction(ILossFunction lossFunction) {
		this.lossFunction = lossFunction;
	}


	public abstract void updateModel(ILearningModel <Index, Value> model, LearningInstance dataInst);

	public abstract void initWeights(ILearningModel <Index, Value> model); 
	
	public double getLossValue(LearningInstance dataInst) {
		return lossFunction.evaluateLossValue(dataInst);
	}
	
	public void setParameters(Map<String, Double> parametersMap) {

	}

}
