package eu.ferari.learning.updateRules;

import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;

public class KernelStochasticGradientDescentClassification extends KernelStochasticGradientDescent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5140824407187604830L;

	public KernelStochasticGradientDescentClassification(JSONObject config) throws InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, ClassNotFoundException, JSONException {
		super(config);
	}
	
	public KernelStochasticGradientDescentClassification(LossFunctionType lossType, LearningParamsMap params) {
		super(lossType, params);
	}
	
	@Override
	protected RealValuedVector getNewSV(LearningInstance dataInst) {
		RealValuedVector newSV = new RealValuedVector((Boolean[]) dataInst.getInstance());
		return newSV;
		
	}
}
