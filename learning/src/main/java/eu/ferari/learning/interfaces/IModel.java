package eu.ferari.learning.interfaces;

import java.io.Serializable;
import java.util.Map;

import org.apache.log4j.Logger;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.learning.DataInstance;
import eu.ferari.learning.LearningInstance;

/**
 * a model interface that represents the model in a learning process
 * T represents the type of the instance attributes
 * @author
 *
 */
public interface IModel <Index, Value> extends Serializable{
	
	public static final Logger logger = Logger.getLogger("Local");
	/**
	 * update the model upon receiving the true output value of an instance
	 * based on the prediction it has made before knowing the true output
	 */
	


	public IVector<Index, Value> getWeights();
	

	public void setSyncModel(IVector <Index, Value> modelValues);
	
	

	void updateModel(DataInstance instance);




	public default void setLogAnnotation(String stAnnotation) {
		
	}


	public default void setParameters(Map<String, Double> parametersMap) {
		
	}



}
