package eu.ferari.learning.model.kernelFunc;

import org.json.JSONObject;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.base.BaseKernelFunction;

/**
 * A linear (dot product) kernel model
 * @author Rania
 *
 * @param <T>
 */
public class DotProductKernel extends BaseKernelFunction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2416124694461465248L;

	public DotProductKernel() {
		
	}

	
	public DotProductKernel(JSONObject config) {
		super(config);
	}

	@Override
	public double computeValue(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
		
		return computeDotProduct(supportVectorVal, instance);
	}

	public double computeDotProduct(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
			RealValuedVector supportVector = supportVectorVal.getData1();
			double dotProd = supportVector.innerProduct(instance);
			return dotProd;
	}
	
		
}