package eu.ferari.learning.model.tmp;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Learning parameters class which also provides default parameters in case the user doesn't want to set them himself. 
 * However no guarantees on the performance of the default parameters is given yet. The user can manually do parameter 
 * tuning offline
 * @author Rania
 *
 */
public class LearningParamsMap implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -311850480454273714L;
	private Map <String, String> paramsMap = new HashMap<String, String>();
	private static Map <String, String> defaultParamsMap;
	
	public enum LearningParameter {
		LinearLearningRate, KernelLearningRate, GaussianWidth, PolynomialKernelDegree, Regularizer, c, KernelFunction 
		};
	
	static {
		defaultParamsMap = new HashMap<String, String>();
		defaultParamsMap.put(String.valueOf(LearningParameter.LinearLearningRate), "0.1");
		defaultParamsMap.put(String.valueOf(LearningParameter.GaussianWidth), "296");
		defaultParamsMap.put(String.valueOf(LearningParameter.KernelLearningRate), "0.1");
		defaultParamsMap.put(String.valueOf(LearningParameter.PolynomialKernelDegree), "2");
		// the regularization constant of the kernel stochastic update rule 
		defaultParamsMap.put(String.valueOf(LearningParameter.Regularizer), "0.01");
		// the polynomial function constant
		defaultParamsMap.put(String.valueOf(LearningParameter.c), "0.0002");
		// the kernel function to be used in the kernel model, exmaple linear, gaussian, polynomial
		defaultParamsMap.put(String.valueOf(LearningParameter.KernelFunction), "Linear");
	}
	public LearningParamsMap() {
	}
	
	public LearningParamsMap(Map<String, String> paramsMap) {
		this.paramsMap = paramsMap;
	}
	
	
	public String getStringParam(LearningParameter key) {
		String keyStr = String.valueOf(key);
		String value = paramsMap.containsKey(keyStr) ? paramsMap.get(keyStr) : defaultParamsMap.get(keyStr);
		return value;
	}
	
	/**
	 * @param key
	 * @return
	 */
	public double getParamDouble(LearningParameter key) {
		String keyStr = String.valueOf(key);
		String value = paramsMap.containsKey(keyStr) ? paramsMap.get(keyStr) : defaultParamsMap.get(keyStr);
		return Double.parseDouble(value);
		
	}

	public int getParamInt(LearningParameter key) {
		String keyStr = String.valueOf(key);
		String value = paramsMap.containsKey(keyStr) ? paramsMap.get(keyStr) : defaultParamsMap.get(keyStr);
		return Integer.parseInt(value);
	}

	public void setStringParam(LearningParameter key, String value) {
		paramsMap.put(String.valueOf(key), value);
	}
	
	
	public void setIntParam(LearningParameter key, int value) {
		paramsMap.put(String.valueOf(key), String.valueOf(value));
	}
	
	public void setDoubleParam(LearningParameter key, double value) {
		paramsMap.put(String.valueOf(key), String.valueOf(value));
	}
}
 