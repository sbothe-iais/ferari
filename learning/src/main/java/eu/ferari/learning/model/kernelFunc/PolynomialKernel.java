package eu.ferari.learning.model.kernelFunc;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.base.BaseKernelFunction;

/**
 * Polynomial kernel, the default degree is 2.
 * @author Rania
 *
 * @param <T>
 */
public class PolynomialKernel extends BaseKernelFunction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8569186618426098156L;
	private int degree = 2;
	private double c = 0.0002;
	
	public PolynomialKernel(JSONObject config) throws JSONException {
		JSONObject modelParams = config.getJSONObject("modelParams");
		this.degree = modelParams.getInt("polynomialKernelDegree");
		this.c = modelParams.getDouble("c");
	}
	
	public PolynomialKernel() {
		
	}
	
	public PolynomialKernel(int degree) {
		this.degree = degree;
	}
	
	public PolynomialKernel(int degree, double c) {
		this(degree);
		this.c = c;
	}

	@Override
	public double computeValue(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
		return computePolynomial(supportVectorVal, instance, degree);
	}
	
	public double computePolynomial(DataPair<RealValuedVector, Double> supportVectorVal,	double[] instance, int degree) {
		RealValuedVector supportVector = supportVectorVal.getData1();
		double value = supportVector.innerProduct(instance);
		value += c;
		value = Math.pow(value, degree);
		return value;
	}
}