package eu.ferari.learning.model.base;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.interfaces.IModel;

public interface ILearningModel <Index, Value> extends IModel<Index, Value> {

	public void predictInstance(LearningInstance dataObject);
	public void updateWeight(Index i, Value updatedWeight);
	public default void setArtistIdName(String artistId, String artistName) {
		
	}
	
}
