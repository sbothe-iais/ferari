package eu.ferari.learning.model.base;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.WebSocketSender;
import eu.ferari.learning.DataInstance;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.updateRules.KernelStochasticGradientDescentRegression;
import eu.ferari.learning.updateRules.UpdateRuleBase;

/**
 * A generic model class that includes functionality relevant to all model types
 * (linear, kernel.)
 * 
 * @author Rania
 *
 * @param <Index>
 * @param <Value>
 * @param <Target>
 */
abstract class GenericModel<Index, Value> implements
		ILearningModel<Index, Value>, Serializable {

	private static final long serialVersionUID = -6443082500068598173L;

	protected UpdateRuleBase<Index, Value> updater;

	protected IVector<Index, Value> weights;

	// current data in the linear model;

	protected double epsilon = 0.04;

	protected String annotation = "";

	/*protected static final Logger logger = Logger.getLogger("model");

	static {
		try {

			// PatternLayout patternLayout = new
			// PatternLayout("%d{HH:mm:ss,SSS} %n%n");
			PatternLayout patternLayout = new PatternLayout("%r [%t] - %m%n");
			//logger.addAppender(new FileAppender(patternLayout, "output/prediction.out"));
			logger.addAppender(new WebSocketLogAppender());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	protected static WebSocketSender logger = new WebSocketSender();

	/*public GenericModel(UpdateRuleBase<Index, Value> updater) {
		this.updater = updater;
	}*/
	
	public GenericModel(JSONObject config)throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		this.updater = (UpdateRuleBase<Index, Value>) (this.getClass().getClassLoader().loadClass(config.getString("updateRule")).getConstructor(JSONObject.class).newInstance(config));
		//this.updater = (UpdateRuleBase<Index, Value>) new KernelStochasticGradientDescentRegression(config);
	}
	
	public GenericModel(UpdateRuleBase<Index, Value> updater) {
		this.updater = updater;
	}


	@Override
	public void setSyncModel(IVector<Index, Value> modelValues) {
		if (modelValues != null) {
			weights = modelValues.copy();
		} else
			weights = null;

	}

	@Override
	public IVector<Index, Value> getWeights() {
		return weights;
	}

	@Override
	public void updateModel(DataInstance instance) {
		LearningInstance learningInstance = (LearningInstance) instance;
		updater.updateModel(this, learningInstance);
		double lossValue = updater.getLossValue(learningInstance);
		learningInstance.setLossValue(lossValue);
	}

	@Override
	public void setLogAnnotation(String stAnnotation) {
		annotation = stAnnotation;
	}

	protected abstract double computePredictionScore(LearningInstance instance);
	protected abstract void evaluatePrediction(LearningInstance instance);

}

	
