package eu.ferari.learning.model.base;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.DataInstance;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.tmp.EpsilonKernelTruncator;
import eu.ferari.learning.model.tmp.KernelFunctionFactory;
import eu.ferari.learning.model.tmp.KernelFunctionFactory.KernelFunctionType;
import eu.ferari.learning.model.tmp.KernelTruncator;
import eu.ferari.learning.model.tmp.KernelVector;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.updateRules.UpdateRuleBase;

public abstract class KernelModel extends GenericModel <RealValuedVector, Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8623178981104835875L;
	private BaseKernelFunction kernelFunc;
	private KernelTruncator kernelTruncator = new EpsilonKernelTruncator();
	
	@SuppressWarnings("unchecked")
	public KernelModel(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		//super(((UpdateRuleBase<Integer, Double>)LinearModel.class.getClassLoader().loadClass(config.getString("updateRule")).getConstructor(JSONObject.class).newInstance(config)));
		super(config);
		JSONObject modelParams = config.getJSONObject("modelParams");
		this.kernelFunc = ((BaseKernelFunction)KernelModel.class.getClassLoader().loadClass(modelParams.getString("kernelFunction")).getConstructor(JSONObject.class).newInstance(config));
	}
	
	public KernelModel(UpdateRuleBase<RealValuedVector, Double> updater, LearningParamsMap params) {
		super(updater);
		KernelFunctionType kernelFuncType = KernelFunctionType.valueOf(params.getStringParam(LearningParameter.KernelFunction));
		this.kernelFunc = KernelFunctionFactory.getKernelFunction(kernelFuncType, params);
	}

	@Override
	public void predictInstance(LearningInstance instance) {
		double prediction;
		if(weights == null) {
			weights = new KernelVector(kernelFunc);
			prediction = 0;
		} else {
			prediction = computePredictionScore(instance);
		}
		instance.setPredictionScore(prediction);
		evaluatePrediction(instance);	
	}
	
	@Override
	public void updateModel(DataInstance instance) {
		 super.updateModel((LearningInstance)instance);
		 kernelTruncator.truncateModel(weights);
	}
	 
	@Override
	public void updateWeight(RealValuedVector supportVector, Double svCoeff) {
		weights.set(supportVector, svCoeff);
	}


	
	@Override
	/**
	 * get the prediction score of the model
	 * @param instance 
	 * @return
	 */
	public double computePredictionScore(LearningInstance instance) {
		double predictionValue = 0;
		Iterator<Entry<RealValuedVector, Double>> it = weights.iterator();
		while (it.hasNext()) {
			Entry<RealValuedVector, Double> entry = it.next();
			DataPair<RealValuedVector, Double> supportVectorVal = new DataPair<RealValuedVector, Double>(entry);
			double currentVal = kernelFunc.computeValue(supportVectorVal, getDoubleVector(instance.getInstance()));
			predictionValue += entry.getValue() * currentVal;
		}
		return predictionValue;
	}

	abstract protected double[] getDoubleVector(Object[] instance);
	
	
	@Override 
	public void setParameters(Map<String, Double> parametersMap) {
		updater.setParameters(parametersMap);
	}
}