package eu.ferari.learning.model.tmp;

import java.io.Serializable;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * A truncation operator used to eliminate support vectors based on criterias defined for each truncation operator 
 * @author Rania
 *
 */
public abstract class KernelTruncator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1178180935229790264L;

	public abstract void truncateModel(IVector<RealValuedVector, Double> supportVectors);
}