package eu.ferari.learning.model.tmp;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import eu.ferari.DataPair;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.base.BaseKernelFunction;

/**
 * Created by mofu on 09/11/15.
 */
public class KernelVector implements IVector<RealValuedVector, Double> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4916924009400373916L;
	private Map<RealValuedVector, Double> supportVectors = new ConcurrentHashMap<RealValuedVector, Double>();
	private long occurrenceTime;
	// needed for the distance computation when we perform dynamic sync
	private BaseKernelFunction kernelFunc;

    public KernelVector(){
        
    }
  

    public KernelVector(BaseKernelFunction kernelFunc) {
		this.kernelFunc = kernelFunc;
	}


	@Override
    public void initialise(IVector<RealValuedVector, Double> v) {
    	/* v.getKeys().forEach((key) -> {
             supportVectors.put(key, v.get(key));
         });*/
    	Iterator<Entry<RealValuedVector, Double>> it = v.iterator();
		while(it.hasNext()) {
			Entry<RealValuedVector, Double> entry = it.next();
			//System.out.println(entry.getKey() + " : " + entry.getValue() );
			supportVectors.put(entry.getKey(), entry.getValue());
    	}
    }
    
    @Override
    public void add(IVector<RealValuedVector, Double> other) {
    	add(other, 1.0);
    } 
    
    @Override
    public void add(IVector<RealValuedVector, Double> other, double averagingFactor) {
    	Iterator<Entry<RealValuedVector, Double>> it = other.iterator();
		while(it.hasNext()) {
			Entry<RealValuedVector, Double> supportVectorVal = it.next();
				RealValuedVector sv = supportVectorVal.getKey();
				Double value = supportVectorVal.getValue(); //other.get(sv);
				/*if(value == null) {
					System.out.println();
				}*/
				value = averagingFactor * value + supportVectors.getOrDefault(sv, 0.0);
				supportVectors.put(sv, value);
			}
    }
    
    public void insertSV(RealValuedVector sv, double svKernelValue) {
    	supportVectors.put(sv, svKernelValue);
    }

    @Override
    public void subtract(IVector<RealValuedVector, Double> other) {
        other.getKeys().forEach((key) -> {
            supportVectors.put(key, supportVectors.getOrDefault(key, 0.0) - other.get(key));
        });

    }

    @Override
    public void scalarMultiply(double d) {
    	Iterator<Entry<RealValuedVector, Double>> it = supportVectors.entrySet().iterator();
		while(it.hasNext()) {
			Entry<RealValuedVector, Double> supportVectorVal = it.next();
				double value = supportVectorVal.getValue() * d;
				supportVectorVal.setValue(value);
			}
	}
       

    @Override // This method needs the kernel function in order to evaluate the distance
    public double distance(IVector<RealValuedVector, Double> other) {
    	Iterator<Entry<RealValuedVector, Double>> it = supportVectors.entrySet().iterator();
    	Iterator<Entry<RealValuedVector, Double>> otherIt = other.iterator();
    	double value = 0;
    	boolean firstIter = true;
    	while(it.hasNext()) {
    		Entry<RealValuedVector, Double> supportVectorVal = it.next();
    		value += Math.pow(supportVectorVal.getValue(), 2)*kernelFunc.computeValue(new DataPair<RealValuedVector, Double>(supportVectorVal), supportVectorVal.getKey().getValue());
    		while(otherIt.hasNext()) {
    			Entry<RealValuedVector, Double> otherSupportVectorVal = otherIt.next();
    			if(firstIter) {
    				value += Math.pow(otherSupportVectorVal.getValue(), 2)*kernelFunc.computeValue(new DataPair<RealValuedVector, Double>(otherSupportVectorVal), otherSupportVectorVal.getKey().getValue());
    			}
				value -= 2*supportVectorVal.getValue()*otherSupportVectorVal.getValue()*kernelFunc.computeValue(new DataPair<RealValuedVector, Double>(supportVectorVal), otherSupportVectorVal.getKey().getValue());
		
			}
    		firstIter = false;
    	}
    	return value;
    }



	@Override
    public Set<RealValuedVector> getKeys() {
        return supportVectors.keySet();
    }

    @Override
    public IVector<RealValuedVector, Double> copy() {
        KernelVector clone = new KernelVector(kernelFunc);
        clone.initialise(this);
        return clone;
    }

    @Override
    public boolean equals(Object other) {
    	if(!(other instanceof KernelVector)) {
    		return false;
    	}
    	Iterator<Entry<RealValuedVector, Double>> it = ((KernelVector) other).iterator();
		while(it.hasNext()) {
			Entry<RealValuedVector, Double> entry = it.next();
			if(!(supportVectors.containsKey(entry.getKey())) || !(supportVectors.get(entry.getKey()).equals(entry.getValue()))){
					return false;
				}
			}
		return ((KernelVector)other).getDimension() == getDimension();
    }
 
    @Override
    public Double get(RealValuedVector idx) {
        return supportVectors.get(idx);
    }

    @Override
    public void set(RealValuedVector idx, Double val) {
        supportVectors.put(idx,val);
    }

    @Override
    public void initializeToZeroVector() {
		supportVectors=new ConcurrentHashMap<>();
    }

    @Override
    public int getDimension() {
        return supportVectors.size();
    }

	@Override
	public boolean containsIndex(RealValuedVector index) {
		return false;
		//return supportVectors.containsKey(index);
	}

	@Override
	public Iterator<Entry<RealValuedVector, Double>> iterator() {
		return supportVectors.entrySet().iterator();
	}

	@Override
	public IVector<RealValuedVector, Double> average(
			Collection<IVector<RealValuedVector, Double>> balancingSetWeights) {
		double averagingFactor = (double)1/balancingSetWeights.size();
		IVector<RealValuedVector, Double> averagedModel = new KernelVector(kernelFunc);
		for(IVector<RealValuedVector, Double> sv : balancingSetWeights) {
			averagedModel.add(sv, averagingFactor);
		}
		
		return averagedModel;
	}



	public long getOccurrenceTime() {
		return occurrenceTime;
	}

	public void setOccurrenceTime(long time) {
		occurrenceTime=time;
	}

}