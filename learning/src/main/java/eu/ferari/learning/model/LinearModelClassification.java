package eu.ferari.learning.model;

import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.LinearModel;
import eu.ferari.learning.updateRules.UpdateRuleBase;

/**
 * (Binary) classification linear model
 * @author Rania
 *
 */
public class LinearModelClassification extends LinearModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2939965826419620070L;

	@SuppressWarnings("unchecked")
	public LinearModelClassification(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		super(config);
	}
	
	public LinearModelClassification(UpdateRuleBase<Integer, Double> updater) {
		super(updater);
	}

	@Override
	public double getDoubleValueOfInstance(Object[] instance, int index) {
		return ((Boolean) instance[index] ? 1: 0);
	}
	@Override
	protected void evaluatePrediction(LearningInstance instance) {
		Boolean yHat = instance.getPredictionScore() > 0;
		boolean isCorrect = yHat.equals ((Boolean)(instance.getTarget()));
		instance.setPredCorrect(isCorrect);
		System.out.println("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		//logger.log(Level.INFO, "prediction: " + yHat + ", trueValue: " + instance.getTarget());
		//logger.log(Level.INFO , "prediction," + yHat + ",trueValue," + instance.getTarget()+","+annotation);
		}
	
	
	
	
}


