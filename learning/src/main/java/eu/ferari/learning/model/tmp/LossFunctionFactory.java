package eu.ferari.learning.model.tmp;

import org.json.JSONObject;

import eu.ferari.learning.interfaces.ILossFunction;
import eu.ferari.learning.lossFunctions.EpsilonInsensitiveLoss;
import eu.ferari.learning.lossFunctions.HingeLoss;
import eu.ferari.learning.lossFunctions.SquaredLoss;

/**
 * a factory class that creates an updater type based on the type that the user gives
 * @author Rania
 *
 */
public class LossFunctionFactory {
	
	public enum LossFunctionType {SquareError, HingeLoss, EpsilonInsensitive};
	
	public static ILossFunction getLossFunction(LossFunctionType lossFunctionType){

		switch (lossFunctionType) {
		case SquareError : 
			return new SquaredLoss();
		case HingeLoss:
			return new HingeLoss();
		case EpsilonInsensitive:
			return new EpsilonInsensitiveLoss();
		default: 
			return null;
		}
	}
	
	
	

}
