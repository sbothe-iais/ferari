package eu.ferari.learning.model;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Level;
import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.LinearModel;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.updateRules.StochasticGradientDescent;
import eu.ferari.learning.updateRules.UpdateRuleBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LinearModelRegression extends LinearModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1220674657319353578L;
	private double epsilon = 0.01;
	private String artistId;
	private String artistName;
	private Logger localLogger = LoggerFactory.getLogger(this.getClass());

	public LinearModelRegression(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		//this is just some code to make it run. The updater should be created using the config object.
		//furthermore, LossFunctionType should not be used anymore. The config references the correct loss function class directly.
		super(config);		
	}
	
	public LinearModelRegression(UpdateRuleBase<Integer, Double> updater) {
		super(updater);		
	}
	
	@Override
	public double getDoubleValueOfInstance(Object[] instance, int index) {
		return (Double)instance[index];	
	}

	protected void evaluatePrediction(LearningInstance instance) {
		double yHat = instance.getPredictionScore();
		localLogger.info("yHat: "  + yHat + "target: " + instance.getTarget());
		double diff = Math.abs((Double) (instance.getTarget()) - (double) yHat);
		boolean isCorrect = diff < epsilon ;
		instance.setPredCorrect(isCorrect);
		localLogger.info("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		//logger.log(Level.INFO, "prediction: " + yHat + ", trueValue: " + instance.getTarget());
		logger.signal("prediction: " + yHat + ", trueValue: " + instance.getTarget()+ ", intercept: " + weights.get(1) + ", artistId: " + artistId + ", artistName: " + artistName);
		}

	public void setArtistIdName(String artistId, String artistName) {
		this.artistId = artistId;
		this.artistName = artistName;
	}

}
