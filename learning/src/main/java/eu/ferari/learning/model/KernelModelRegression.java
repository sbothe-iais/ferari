package eu.ferari.learning.model;

import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.KernelModel;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.updateRules.UpdateRuleBase;

public class KernelModelRegression extends KernelModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7342291920988990569L;
	private double epsilon = 0.001;
	public KernelModelRegression(JSONObject config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, JSONException {
		super(config);
	}
	
	public KernelModelRegression(UpdateRuleBase<RealValuedVector, Double> updater, LearningParamsMap params) {
		super(updater, params);
	}
	
	
	@Override
	protected void evaluatePrediction(LearningInstance instance) {
		double yHat = instance.getPredictionScore();
		double diff = Math.abs((Double) (instance.getTarget()) - (double) yHat);
		boolean isCorrect = diff < epsilon  ;
		instance.setPredCorrect(isCorrect);
		System.out.println("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		
		}
	
	
	protected double[] getDoubleVector(Object[] instance) {
		double[] vector = new double[instance.length];
		
		for (int i = 0; i < instance.length; i++) {
			vector[i] = ((Double) instance[i]);
		}
	return vector;
}

	

}
