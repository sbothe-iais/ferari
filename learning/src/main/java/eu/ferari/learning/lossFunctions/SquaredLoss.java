package eu.ferari.learning.lossFunctions;

import org.json.JSONObject;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.interfaces.ILossFunction;


public class SquaredLoss implements ILossFunction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8099982788969012013L;

	public SquaredLoss () {
		
	}
	public SquaredLoss(JSONObject config) {
		
	}
	
	@Override
	public double evaluateLossDerivative(LearningInstance dataInst, double xValue) {
		double prediction = dataInst.getPredictionScore();
		//(t-o)*(-x) (for kernels it's simply (t-o), in that case just pass 1 for xValue and invoke this method once only 
		return (prediction - getTargetDouble(dataInst));
	}


	@Override
	public double evaluateLossValue(LearningInstance dataInst) {
		double realValue = getTargetDouble(dataInst);
		double prediction = dataInst.getPredictionScore();
		double loss = (0.5) * Math.pow(realValue - prediction, 2);
		return loss;
	}
	
	private double getTargetDouble(LearningInstance instance) {
		if(instance.getTarget() instanceof Boolean) {
			if((Boolean)instance.getTarget()) 
				return 1;
			return 0;
		}
			
		return (Double) instance.getTarget();
	}
	
}
