package eu.ferari.learning.lossFunctions;

import org.json.JSONObject;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.interfaces.ILossFunction;

/**
 * 
 * @author Rania
 * Hinge loss should only be used for binary classification
 */
public class HingeLoss implements ILossFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1025759519602021406L;
	

	public HingeLoss() {
	
	}
	
	public HingeLoss(JSONObject config) {
		
	}
	
	
	@Override
	public double evaluateLossDerivative(LearningInstance dataInst, double xValue)  {
		double trueValue = getHingeInt((Boolean)dataInst.getTarget());
		double prediction = dataInst.getPredictionScore();
		double threshold = trueValue * prediction;
		if(threshold < 1)
			return -trueValue;
		return 0;
		
	}


	@Override
	public double evaluateLossValue(LearningInstance dataInst) {
		return Math.max(0, 1- getHingeInt((Boolean)dataInst.getTarget()) * dataInst.getPredictionScore());
		
	}
	
	private int getHingeInt(boolean trueY) {
		if(trueY)
			return 1;
		return -1;
	}
}
