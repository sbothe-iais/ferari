package eu.ferari;

import java.io.Serializable;
import java.util.Map.Entry;

import eu.ferari.core.utils.RealValuedVector;

/**
 * a class that can represents any pair of data/values
 * @author Rania
 *
 * @param <E>
 * @param <T>
 */

public class DataPair<E, T>  implements Serializable {
		
		/**
	 * 
	 */
	private static final long serialVersionUID = 5172971085341898620L;
		private E data1;
		private T data2;
		
		
		public DataPair() {

		}

		public DataPair(E data1, T data2) {
			this.data1 = data1;
			this.data2 = data2;
		}

		public DataPair(Entry<E, T> entry) {
			this.data1 = entry.getKey();
			this.data2 = entry.getValue();
		}

		public E getData1() {
			return data1;
		}

		public void setData1(E data1) {
			this.data1 = data1;
		}

		public T getData2() {
			return data2;
		}

		public void setData2(T data2) {
			this.data2 = data2;
		}
}
